// Filename: gani_fps.js
// Timestamp: 2017.07.10-03:51:45 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

const gnfps_defaultmaxfps = false

// return fps 30 (default),
// { fps : 30,
//   fpms : 0.03,
//   mspf : 33.333333336 }
//
// return fps 60
// { fps : 60,
//   fpms : 0.06
//   mspf : 16.666666666 }
const gnfps_getfpsobj = (fps = 30, fpms = fps / 1000) => ({
  fps,
  fpms: fps && fpms,
  mspf: fps && 1 / fpms
})

const gnfps_assignfpsobj = (cfg, fps = gnfps_defaultmaxfps) => (
  Object.assign(cfg, gnfps_getfpsobj(fps)))

const gnfps_domRequestAnimationFrame = dom => (
  dom && dom.window && dom.window.requestAnimationFrame)

// https://developer.mozilla.org/en-US/docs/Web/API \
//   /WindowOrWorkerGlobalScope \
//   /setTimeout#Reasons_for_delays_longer_than_specified
//
// note: timer is browser-throttled to timeout >= 4ms
//
const gnfps_raf = (cfg, mspf, rafts, reqFrame, fn) => (
  (cfg.rafts === rafts) && reqFrame && reqFrame(() => (
    fn() !== false && typeof mspf === 'number'
      ? setTimeout(() => gnfps_raf(cfg, mspf, rafts, reqFrame, fn), mspf)
      : gnfps_raf(cfg, mspf, rafts, reqFrame, fn))))

// cfg must be mutable value
// updating rafts stops loop
const gnfps_rafstart = (cfg, mspf, dom, fn) => (
  cfg.rafts = Date.now(),
  gnfps_raf(cfg, mspf, cfg.rafts, gnfps_domRequestAnimationFrame(dom), fn),
  cfg)

const gnfps_rafstop = cfg => (
  cfg.rafts = Date.now(),
  cfg)

export {
  gnfps_raf as default,
  gnfps_defaultmaxfps,
  gnfps_getfpsobj,
  gnfps_assignfpsobj,
  gnfps_raf,
  gnfps_rafstart,
  gnfps_rafstop
}
