const gnrun_getfn = (cfg, fnname) => (
  cfg.spec.getfn(fnname))

const gnrun_getpass = (ss, cfg, gr, nd, props, spec, fn) => (
  cfg.spec.getpass(ss, cfg, gr, nd, props, spec.when, fn))

const gnrun_getfiltered = (ss, cfg, gr, nd, props, val, filters, fn) => (
  cfg.spec.getfiltered(ss, cfg, gr, nd, props, val, filters, fn))

const gnrun_getopts = (ss, cfg, gr, nd, props, specaccum, fn) => (
  cfg.spec.getopts(ss, cfg, gr, nd, props, specaccum, fn))

const gnrun_getargs = (ss, cfg, gr, nd, opts, props) => (
  cfg.spec.getargs(ss, gr, nd, opts, props))

const gnrun_retopt = (ss, cfg, gr, nd, props, spec, fn) => (
  cfg.spec.retopt(ss, cfg, gr, nd, props, spec, fn))

const gnrun_retobj = (ss, cfg, gr, nd, props, spec, fn) => (
  cfg.spec.retobj(ss, cfg, gr, nd, props, spec, fn))

const gnrun_valfinish = (cfg, cumval, spec, val) => (
  cfg.spec.valfinish(cumval, spec, val))

const gnrun_callfn = (ss, cfg, gr, nd, args, spec, fn) => (
  cfg.spec.callfn(ss, cfg, gr, nd, args, spec, fn))

const gnrun_retDataWHERE = (ss, cfg, gr, nd, basearr, ns, query, fn) => (
  cfg.spec.retDataWHERE(ss, cfg, gr, nd, basearr, ns, query, fn))

export {
  gnrun_getfn,
  gnrun_getpass,
  gnrun_getfiltered,
  gnrun_getopts,
  gnrun_getargs,
  gnrun_retopt,
  gnrun_retobj,
  gnrun_callfn,
  gnrun_valfinish,
  gnrun_retDataWHERE
}
