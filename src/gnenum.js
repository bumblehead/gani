// Filename: gani_token.js
// Timestamp: 2017.12.23-21:24:09 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

const gnenumKEY = 'key'
const gnenumNODE = 'node'

const gnenumACTIVEKEY = 'activekey'
const gnenumDATAKEYPROP = 'datakeyprop'
const gnenumMAPSRCKEY = 'mapsrckey'

// dynamically-generated keys constructed with
// index position are prefixed "unsafe-"
const gnenumUNSAFE = 'unsafe'

const gnenumISPATHNODE = 'ispathnode'

const gnenumDOMCHILDKEY = 'domchildkey'
const gnenumDOMCHILDKEYARR = 'domchildkeyarr'

const gnenumCHILDARR = 'childarr'

const gnenumSPECTYPES = 'spectypes'

const gnenumISMODIFIEDTS = 'ismodifiedts'
const gnenumISPERSISTEDTS = 'ispersistedts'
const gnenumISPATCHREHYDRATE = 'ispatchrehydrate'
const gnenumISDATABLEND = 'isdatablend'
const gnenumISDOM = 'isdom'
const gnenumISDATA = 'isdata'

// the value on the 'NAMEKEY' property, by a parent pattern,
// becomes the 'name' on which the child node pattern is defined,
// regardless of the name defined on the child. this is used for
// ui nodes render arrange child nodes in specific order or location
// using the name value
const gnenumNAMEKEY = 'namekey'
const gnenumNSPROP = 'nsprop'
const gnenumNAME = 'name'
const gnenumSPEC = 'spec'

const gnenumDEF = 'def' // default
const gnenumFKEY = 'fkey'
const gnenumBASE = 'base'
const gnenumBASELIST = 'baselist'
const gnenumSUBJ = 'subj'
const gnenumCHILDSUBJ = 'childsubj'
const gnenumINIT = 'init'
const gnenumFULL = 'full'
const gnenumCHILD = 'child'
const gnenumPART = 'part'
const gnenumCACHE = 'cache'
const gnenumNSISSUPPORTEDGE = [
  gnenumFULL,
  gnenumSUBJ,
  gnenumCHILDSUBJ,
  gnenumFKEY
]
const gnenumNSISSUPPORTEDGERe = new RegExp(
  `^(${gnenumNSISSUPPORTEDGE.join('|')})$`)

const gnenumSPECPROPNAMETYPE = 'gn:type'

const gnenumSORTBY = 'sortBy'
const gnenumSORTDIRECTION = 'sortDirection'
const gnenumSORTORDERDESC = 'desc'
const gnenumSORTORDERASC = 'asc'

// edgetokens
const gnenumREFNAME = 'refname'
const gnenumTYPE = 'gn:type'
const gnenumMETA = 'meta'
const gnenumNS = 'ns'

const gnenumISOLANG = 'Lang'
const gnenumISOREGION = 'Region'
const gnenumISOLOCALE = 'LangRegion'
const gnenumISOLOCALERe = /base(LangRegion|Lang|Region)/
const gnenumISOLOCALEPATHRe = /^(?!\/)(\w\w\w?-\w\w)(?!\/)/
const gnenumISOLOCALEIDREDIRECT = ':REDIRECT'

// `classNameNODE`, the node name rendered on the node's className. usually the
// node's given name is sufficient and this property is not used. In some cases,
// nodes are renamed to be unique among sibling nodes, and the original name
// can be defined classNameNODE so that it will render on the node's className
//
// `classNameROOT`, when defiined, is added to the className of the root vnode
//
const gnenumCLASSNAMENODE = 'classNameNODE'
const gnenumCLASSNAMEROOT = 'classNameROOT'

const gnenumCASTTYPESTRING = 'str'
const gnenumCASTTYPEBOOLEAN = 'bool'
const gnenumCASTTYPENUMBER = 'num'
const gnenumCASTTYPEARRAY = 'arr'

const gnenumSUBJMETATITLE = 'gnmetatitle'
const gnenumSUBJMETADESCRIPTION = 'gnmetadescription'

const gnenumPATHROOTDEFAULT = `$ROOTSPECS`
const gnenumPATHROOTSPECS = `${gnenumPATHROOTDEFAULT}/spec/`
const gnenumPATHROOTSPECSMANIFEST = `${gnenumPATHROOTSPECS}manifest.json`

const gnenumSELECTTYPESINGLE = 'single'
const gnenumSELECTTYPEMULTIPLE = 'multiple'
const gnenumSELECTTYPEISSUPPORTED = selecttype => (
  selecttype === gnenumSELECTTYPESINGLE
    || selecttype === gnenumSELECTTYPEMULTIPLE)

// 'name' is required and must be unique among sibling nodes
// else unexepected behaviour may occur
const gnenum_isvalidnameval = name => Boolean(
  typeof name === 'string' && name.length)

const gnenumEVTYPEONSUBMIT = 'onsubmit'
const gnenumEVTYPEONCHANGE = 'onchange'
const gnenumEVTYPEONCLICK = 'onclick'
const gnenumEVTYPEONKEYUP = 'onkeyup'
const gnenumEVTYPEONSUBJ = 'onsubj'
const gnenumEVTYPEONCHILDADDED = 'onchildadded'
const gnenumEVTYPEONCHILDREMOVED = 'onchildremoved'

// About fkey "[./].baselem" hydration,
// ```
// fkey:
//   { datatodo: '[./].baseelem', '[./].baseelem': ['datatodo'] }
// fkeyhydrated:
//   { datatodo: '/data-users/2', '[./].baseelem': null }
// ```
//
// "[./].baselem" is a special-named-property at fkey,
//  * it is "pre-hydrated" at spec creation, outside common node hydration,
//    * an important restriction --prevent nodes from using this directly
//  * it is pre-hydrated around "baseelem" data, not given to node hydration,
//  * it is pre-hydrated with baseelem-only properties,
//  * assigning pre-hydrated fkeys writes baselem properties only,
//  * `fkeyhydrated['[./].baselem'] === null` indicates hydrated baselem
//  * pre-hydrated fkey values are lost by regular fkey-hydration below,
//  * regular fkey-hydration can re-assign pre-hydrated fkeys to keep them
//
// fkey spec pre-hydrated
//  -> regular fkey hydration erases spec pre-hydrated
//     -> spec pre-hydrated is assigned back into fkey hydrated
//
// sequence seems clumsy but is not terrible,
//  * this flow avoids separate branches of hydration logic for baselem,
//  * restated above; continue using one prehydration sequence for all nodes,
//  * avoids using and managing new fields to handle baseelem
//  * addl. operations apply practically only to baselem-referencing nodes,
//  * baseelem-referencing nodes occur less frequently,
//  * hydrated fkey always a plain object, natural to use with 'assign',
//
// to remove special-case baseelem behaviour here, support for baselem could
// be restricted to spec-wrapping childs, auto-created at spec-generation
// ```diff
// - child[{ fkey: { ...fkeybaselem }, node, name }]
// + child[{ fkey: { ...fkeybaselem }, spec: { node, name }]
// ```
const gnenumFKEYBASEELEM = '[./].baseelem'

// value with this 'type' indicates a full value defined elsewhere that is
// local to page-deploy specs
//
// ```
// {
//   "gn:type": "local-ref",
//   "path": "../root-content-foot/eng.json"
// }
// ```
const gnenumSPECPROPTYPELOCALREF = 'local-ref'

// 'prefixdir' distinguishes two different web-app scenarious
//  1. served from origin root, ex "myapp.com/"
//  2. served from prefix root, ex "myapp.com/prefix-dir/"
//
// for example, gitlab and github pages are served from a prefix,
//  * github.com/user54/touchboom uses prefix "touchboom"
//    * https://user54.github.io/touchboom/
//  * gitlab.com/user54/todo.gani uses prefix "todo.gani"
//    * https://user54.gitlab.io/todo.gani/
//
// gani requests and reads files relative to the location of $ROOTSPECS
// when "prefixdir" is defined, it becomes expected rootpath for $ROOTSPECS
const gnenumCFGPROPTYPEPREFIXDIR = 'prefixdir'

const gnenumSESSLOCALE = 'locale'
const gnenumSESSREQURL = 'requrl'

export {
  gnenumKEY,
  gnenumNODE,

  gnenumACTIVEKEY,
  gnenumDATAKEYPROP,
  gnenumMAPSRCKEY,

  gnenumUNSAFE,

  gnenumISDOM,
  gnenumISDATA,
  gnenumISPATHNODE,

  gnenumDOMCHILDKEY,
  gnenumDOMCHILDKEYARR,

  gnenumCACHE,
  gnenumCHILDARR,

  gnenumSPECTYPES,

  gnenumISMODIFIEDTS,
  gnenumISPERSISTEDTS,
  gnenumISPATCHREHYDRATE,
  gnenumISDATABLEND,

  gnenumNAMEKEY,
  gnenumNSPROP,
  gnenumNAME,
  gnenumSPEC,

  gnenumDEF,

  gnenumFKEY,
  gnenumBASE,
  gnenumBASELIST,
  gnenumSUBJ,
  gnenumCHILDSUBJ,
  gnenumINIT,
  gnenumFULL,
  gnenumCHILD,
  gnenumPART,

  gnenumREFNAME,
  gnenumTYPE,
  gnenumMETA,
  gnenumNS,
  gnenumNSISSUPPORTEDGE,
  gnenumNSISSUPPORTEDGERe,
  gnenumSPECPROPNAMETYPE,

  gnenumSORTBY,
  gnenumSORTDIRECTION,
  gnenumSORTORDERDESC,
  gnenumSORTORDERASC,

  gnenumISOLANG,
  gnenumISOREGION,
  gnenumISOLOCALE,
  gnenumISOLOCALERe,
  gnenumISOLOCALEPATHRe,
  gnenumISOLOCALEIDREDIRECT,

  gnenumCLASSNAMENODE,
  gnenumCLASSNAMEROOT,

  gnenumCASTTYPESTRING,
  gnenumCASTTYPEBOOLEAN,
  gnenumCASTTYPENUMBER,
  gnenumCASTTYPEARRAY,

  gnenumSUBJMETATITLE,
  gnenumSUBJMETADESCRIPTION,

  gnenumPATHROOTDEFAULT,
  gnenumPATHROOTSPECS,
  gnenumPATHROOTSPECSMANIFEST,

  gnenumEVTYPEONSUBMIT,
  gnenumEVTYPEONCHANGE,
  gnenumEVTYPEONCLICK,
  gnenumEVTYPEONKEYUP,
  gnenumEVTYPEONSUBJ,
  gnenumEVTYPEONCHILDADDED,
  gnenumEVTYPEONCHILDREMOVED,

  gnenumSELECTTYPESINGLE,
  gnenumSELECTTYPEMULTIPLE,
  gnenumSELECTTYPEISSUPPORTED,

  gnenumFKEYBASEELEM,

  gnenum_isvalidnameval,
  gnenumSPECPROPTYPELOCALREF,

  gnenumCFGPROPTYPEPREFIXDIR,

  gnenumSESSLOCALE,
  gnenumSESSREQURL
}
