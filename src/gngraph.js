// Filename: gani_graph.js
// Timestamp: 2018.01.15-23:42:21 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import {
  sphgraph_get,
  sphgraph_setnode,
  sphgraph_getfromjs,
  sphgraph_followedge,
  sphgraph_setnodeedge,
  sphgraph_setnodeoutedge
} from 'spectraph/src/sphgraph.js'

import {
  gnnode_childspecresolve,
  gnnode_childsetfromspec,
  gnnode_dfsattach,
  gnnode_pgonframe,
  gnnode_pgonframehas,
  gnnode_pgvnodeget,
  gnnode_pgget,
  gnnode_fromspecpginit,
  gnnode_graphset,
  gnnode_childrm,
  gnnode_dfshydrateall,
  gnnode_childkeymv
} from './gnnode.js'

import {
  gnreport_get,
  gnreport_markvnode
} from './gnreport.js'

import {
  gnerr_rootnodenotfound
} from './gnerr.js'

import {
  gnspec_pathseedanycreate
} from './gnspec.js'

import {
  gnenumKEY as KEY
} from './gnenum.js'

import {
  gnpathdef_pathpgarrget,
  gnpathdef_arrpermittedget,
  gnpathdef_prefixstrip
} from './gnpathdef.js'

const gngraph_setnode = sphgraph_setnode
const gngraph_setnodeedge = sphgraph_setnodeedge
const gngraph_setnodeoutedge = sphgraph_setnodeoutedge
const gngraph_followedge = sphgraph_followedge

const gngraph_create = sphgraph_getfromjs

const gngraph_getnoderoot = graph => (
  graph.get('/') ||
    gnerr_rootnodenotfound(graph))

const gngraph_attacheventsalld = (sess, cfg, graph, fn) => (
  gnnode_dfsattach(sess, cfg, graph, gngraph_getnoderoot(graph), fn))

const gngraph_onframekeyarr = (ss, cfg, gr, keyarr) => (
  keyarr.map(key => gnnode_pgonframe(ss, cfg, gr, gr.get(key))))

const gngraph_getrenderkeyarr = (ss, cfg, gr) => {
  let renderkeyarr = []

  gr.map((node, key) => {
    if (gnnode_pgonframehas(ss, cfg, gr, gr.get(key))) {
      renderkeyarr.push(key)
    }
  })

  return renderkeyarr
}

const gngraph_createrootnode = (cfg, gr, data) => {
  let nd = gnnode_fromspecpginit(cfg, gr, data),
      ndkey = nd.get(KEY)

  if (!/^\//.test(ndkey)) {
    ndkey = `/${ndkey}`
  }

  return nd.set(KEY, ndkey)
}

const gngraph_getpopulated = (ss, cfg, gr, isfresh, fn) => {
  const ndroot = gngraph_getnoderoot(gr)
  const report = gnreport_get('build')

  if (!isfresh)
    return fn(null, gr, ndroot, report)

  gnnode_dfshydrateall(ss, cfg, gr, ndroot, report, fn)
}

// isfresh builds data AND render vnode
// !isfresh use existing data ONLY render vnode
const gngraph_getvnode = (ss, cfg, gr, isfresh, fn) => {
  gngraph_getpopulated(ss, cfg, gr, isfresh, (e, gr, nd, report) => {
    if (e) return fn(e)

    const startts = Date.now()
    const vnode = gnnode_pgvnodeget(ss, cfg, gr, nd)
    report = gnreport_markvnode(report, startts, nd.get(KEY))

    fn(null, gr, nd, vnode, report)
  })
}

// finpnodearr is path node object array
// eslint-disable-next-line max-len, no-async-promise-executor
const gngraph_renderpath = async (ss, cfg, path) => new Promise(async (resolve, error) => {
  const reqpnodearr = gnpathdef_pathpgarrget(cfg,
    gnpathdef_prefixstrip(path, cfg.prefixdir))
  const finpnodearr = gnpathdef_arrpermittedget(ss, cfg, reqpnodearr)
  let gr = sphgraph_get()

  // converts path nodes, to meta nodes organised in graph.
  const stackarr = await gnspec_pathseedanycreate(ss, cfg, finpnodearr)
  const res = gnnode_graphset(
    gr, gngraph_createrootnode(cfg, gr, stackarr[0]))

  gr = res[0]

  gngraph_getvnode(ss, cfg, gr, true, (e, gr, ndroot, vtree, report) => {
    if (e) return error(e)

    resolve([
      gr, finpnodearr, stackarr, ndroot, vtree, report])
  })
})

// return only tree from render behaviour (for unit-tests)
const gngraph_renderpathtree = async (sess, cfg, path) => (
  await gngraph_renderpath(sess, cfg, path))[4]

//
// removed nodes correspoding to unique portion of prevstack
// call callback (allows callee to repopulate this state of graph
// construct and hydrate nodes corresponding to newstack
// move newnode positions the corresponding oldnode positions
// construct and return vnode from full graph
//
// eslint-disable-next-line max-len
const gngraph_renderdiffpath = async (ss, cfg, gr, path, prevstackarr, onpartialfn, iscontinuefn, fn) => {
  const reqpnodearr = gnpathdef_pathpgarrget(cfg, path)
  const finpnodearr = gnpathdef_arrpermittedget(ss, cfg, reqpnodearr)
  const report = gnreport_get('diffpath')
  const objarr = await gnspec_pathseedanycreate(ss, cfg, finpnodearr)
  const nd = gr.get('/')
  const ndpg = gnnode_pgget(cfg, gr, nd)
  const rkey = `/${prevstackarr[1].name}`

  if (!iscontinuefn()) return null

  // eslint-disable-next-line max-len
  gnnode_childrm(ss, cfg, gr, nd, rkey, (e, gr, nd, cnode, cindex, cdomindex) => {
    if (e) return fn(e)
    if (!iscontinuefn()) return null

    onpartialfn(gr, (e, gr) => {
      if (e) return fn(e)
      if (!iscontinuefn()) return null

      nd = gr.get(nd.get(KEY))

      gnnode_childspecresolve(ss, cfg, gr, nd, objarr[1], 0, (
        (gr, nd, childspec, childnum, nextfn) => {
          if (!iscontinuefn()) return null

          let child = null,
              res = gnnode_childsetfromspec(
                ss, cfg, gr, nd, ndpg, childspec)
          gr = res[0]
          nd = res[1]
          child = res[2]
          // adding spec adds to end of child list.. so move to
          // position of previous child
          res =
            gnnode_graphset(gr, gnnode_childkeymv(nd, cindex, cdomindex))
          gr = res[0]
          nd = res[1]

          gnnode_dfshydrateall(ss, cfg, gr, child, report, (e, gr) => {
            if (e) return fn(e)
            if (!iscontinuefn()) return null

            nextfn(e, gr, nd)
          })
        }
      ), (e, gr) => {
        // return full vnode without reconstructing data
        // eslint-disable-next-line max-len
        gngraph_getvnode(ss, cfg, gr, false, (e, gr, rootnode, vtree, report) => {
          if (e) return fn(e)
          if (!iscontinuefn()) return null

          fn(e, gr, finpnodearr, objarr, rootnode, vtree, report)
        })
      })
    })
  })
}

export {
  gngraph_create,
  gngraph_setnode,
  gngraph_setnodeedge,
  gngraph_setnodeoutedge,
  gngraph_followedge,
  gngraph_getnoderoot,
  gngraph_attacheventsalld,
  gngraph_onframekeyarr,
  gngraph_getrenderkeyarr,
  gngraph_createrootnode,
  gngraph_getpopulated,
  gngraph_getvnode,
  gngraph_renderpath,
  gngraph_renderpathtree,
  gngraph_renderdiffpath
}
