// serialised key w/ parent key
//
// https://github.com/Matt-Esch/virtual-dom/issues/378
//
// virtual-dom has un-expected buggy behaviour using slashes and colons (which
// are in fact valid id chars). the id is 'adapted' so that click/touch yields
// correct key from document
const gnkey_docdecodekey = dockey => (
  String(dockey).replace(/:/gi, '/'))

const gnkey_docencodekey = key => (
  String(key).replace(/\//gi, ':'))

const gnkey_docencodekeyarr = keyarr => (
  keyarr.map(gnkey_docencodekey).join('---'))

const gnkey_docdecodekeyarr = dockey => (
  dockey.split('---').map(gnkey_docdecodekey))

const gnkey_docdecodebasekey = dockey => (
  gnkey_docdecodekeyarr(dockey)[0])

export {
  gnkey_docdecodekey,
  gnkey_docencodekey,
  gnkey_docencodekeyarr,
  gnkey_docdecodekeyarr,
  gnkey_docdecodebasekey
}
