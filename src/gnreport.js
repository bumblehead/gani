// Filename: gani_report.js
// Timestamp: 2017.09.29-01:57:36 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

const gnreport_get = (records => key => ({
  id: ++records,
  initiator: key,
  endts: null,
  startts: Date.now(),
  timearr: [],
  patcharr: [],
  replacearr: [],
  replaceaccumarr: [],

  patchkeyobj: {},
  patchkeyarr: [],

  // dfspopulate
  tsarr: [],
  tsrmarr: [],
  tsvnodearr: [],
  delaykeyarr: []
}))(0)

const gnreport_patchget = patch => (
  (patch && patch.length)
    ? gnreport_get(patch[0][0][0])
    : gnreport_get('nokey'))

const gnreport_addreplacekey = (rec, key) => (
  rec.replacearr.push(key),
  rec)

const gnreport_addreplaceaccumkey = (rec, key) => (
  rec.replaceaccumarr.includes(key) ||
    rec.replaceaccumarr.push(key),
  rec)

const gnreport_iskeyreplaced = (rec, key) => (
  rec.replacearr.find(repkey => (
    key.startsWith(repkey))))

const gnreport_iskeypatched = (rec, key) => (
  Boolean(rec.patchkeyobj[key]))

const gnreport_isinitiator = (rec, key) => (
  rec.initiator === key)

const gnreport_isnotinitiator = (rec, key) => (
  !gnreport_isinitiator(rec, key))

const gnreport_addpatchkeyout = (rec, key) => (
  rec.patchkeyobj[key] = true,
  rec
)

const gnreport_marktime = (rec, bgnts, key) => (
  rec.tsarr.push([Date.now() - bgnts, key]),
  rec)

const gnreport_markrmtime = (rec, bgnts, key) => (
  rec.tsrmarr.push([Date.now() - bgnts, key]),
  rec)

const gnreport_markvnode = (rec, bgnts, key) => (
  rec.tsvnodearr.push([Date.now() - bgnts, key]),
  rec)

const gnreport_markdelaynode = (rec, delaynum, key) => (
  rec.delaykeyarr.push([delaynum, key]),
  rec)

const gnreport_getdelayunitarr = rec => (
  rec.delaykeyarr.sort(([delaynumb], [delaynuma]) => (
    delaynumb > delaynuma)).map(([, key]) => key))

const gnreport_addpatch = (rec, patch) => (
  rec.patcharr.push(patch),
  rec)

const gnreport_end = (rec, ts = Date.now()) => (
  rec.patchkeyarr = Object.keys(rec.patchkeyobj),
  rec.endts = ts,
  rec)

export {
  gnreport_get,
  gnreport_patchget,
  gnreport_addreplacekey,
  gnreport_addreplaceaccumkey,
  gnreport_iskeyreplaced,
  gnreport_iskeypatched,
  gnreport_isinitiator,
  gnreport_isnotinitiator,
  gnreport_addpatchkeyout,
  gnreport_marktime,
  gnreport_markrmtime,
  gnreport_markvnode,
  gnreport_markdelaynode,
  gnreport_getdelayunitarr,
  gnreport_addpatch,
  gnreport_end
}
