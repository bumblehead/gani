// Filename: gani_sess.js
// Timestamp: 2017.12.23-20:49:38 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import {
  gnenumSESSLOCALE as LOCALE,
  gnenumSESSREQURL as REQURL
} from './gnenum.js'

import {
  gnpathdef_pathisvalid,
  gnpathdef_pathnoparams
} from './gnpathdef.js'

const gnsess_create = sess => Object
  .assign({ [LOCALE]: 'baseLocale' }, sess)

const gnsess_isvalidsess = sess => Boolean(sess
  && typeof sess[LOCALE] === 'string'
  && gnpathdef_pathisvalid(sess[REQURL]))

const gnsess_isrequrlpath = (sess, path) =>
  sess[REQURL] === gnpathdef_pathnoparams(path)

export {
  gnsess_create as default,
  gnsess_create,
  gnsess_isvalidsess,
  gnsess_isrequrlpath
}
