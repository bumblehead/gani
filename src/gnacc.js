// Filename: gani_acc.js
// Timestamp: 2018.12.09-00:20:37 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>
//
// creates 'get' and 'set' methods for data
// on the given namespace

import {
  gnenumFKEY as FKEY,
  gnenumBASE as BASE,
  gnenumBASELIST as BASELIST,
  gnenumSUBJ as SUBJ,
  gnenumCACHE as CACHE,
  gnenumCHILDSUBJ as CHILDSUBJ,
  gnenumCHILDARR as CHILDARR,
  gnenumINIT as INIT,
  gnenumFULL as FULL,
  gnenumCHILD as CHILD,
  gnenumPART as PART
} from './gnenum.js'

const accessor = (ns, name = `${ns}hydrated`, acc = {}) => {
  acc.ns = ns

  acc.name = name

  acc.getns = () => ns

  acc.is = acc => acc.ns === ns

  acc.pushspec = (node, def, arr) => (
    arr = acc.getspec(node, []),
    arr.push(def),
    acc.setspec(node, arr))

  acc.mapspec = (node, fn, arr) => (
    arr = acc.getspec(node, []),
    arr = arr.map(fn),
    acc.setspec(node, arr))

  acc.hasspec = node => (
    node.has(ns))

  acc.getspec = (node, def) => (
    node.get(ns, def))

  acc.setspec = (node, def) =>(
    node.set(ns, def))

  acc.rmspec = node => (
    node.delete(ns))

  acc.has = node => (
    node.has(name))

  acc.get = (node, def) => (
    node.get(name, def))

  acc.set = (node, def) => (
    node.set(name, def))

  acc.push = (node, def, arr) => (
    arr = acc.get(node, []),
    arr.push(def),
    acc.set(node, arr))

  acc.cp = (nodesrc, nodedest) => (
    acc.assign(nodedest, acc.get(nodesrc, {})))

  acc.rm = node => (
    node.delete(name))

  acc.hasprop = (node, prop) => (
    prop in acc.get(node, {}))

  acc.getprop = (node, prop, def, val) => (
    val = acc.get(node, {})[prop],
    val === undefined ? def : val)

  acc.setprop = (node, prop, val, nodedata) => (
    nodedata = acc.get(node, {}),
    nodedata = Array.isArray(nodedata)
      ? (nodedata[prop] = val, nodedata)
      : Object.assign({}, nodedata, { [prop]: val }),
    acc.set(node, nodedata))

  acc.assign = (node, obj, nodedata) => (
    nodedata = acc.get(node, Array.isArray(obj) ? [] : {}),
    Array.isArray(nodedata)
      ? acc.set(node, obj.slice())
      : acc.set(node, Object.assign({}, nodedata, (obj || {})))
  )

  return acc
}

// subj values are locally defined on the node after any child nodes
// are first defined
//
// they can be composed with values from fkey nodes.
//
// subj values have outgoing edges and updates to them are propogated
// to other nodes
//
// special subj values,
//   `classNameNode`, when defined, becomes the className 'name' of the node
//   `classNameRoot`, when defined, appears on root className of the node
const gnacc_subj = accessor(SUBJ)

// fkey values are defined with values that are keys to other nodes
//
// ex, fkey.user = '/data/users/user-5963'
//
// named property 'user' is a controlled static value and the key which it
// references may change.
//
const gnacc_fkey = accessor(FKEY)

// cache values are only used by uipartial at this time. this is a mostly
// private namespace for direct usage by node scripts. use carefully.
//
const gnacc_cache = accessor(CACHE)

// init values are locally defined on the node before any other data
// they can be composed with values from fkey nodes.
//
// init values never have outgoing edges and updates to them do not need
// to be propogated to other nodes
const gnacc_init = accessor(INIT)

// accumulators and "upward" edge creators
// referencing data on childs. these are assembled and populated
// to accumulate values mid-hydration loop
const gnacc_childsubj = accessor(CHILDSUBJ)

// full data used with node with subj value that is related to a list of
// other values, such as a list of user data for a user list
const gnacc_full = accessor(FULL)

// original un-modified model data sometimes give by parent node
// sometimes defined through local pattern
const gnacc_base = accessor(BASE)

// array result...
const gnacc_part = accessor(PART)

// array result...
//
// used rather than `base` when loading data keys mapped to child patterns
// base and baselist cannot be merged for this purpose...
// base definition is completed after childs
const gnacc_baselist = accessor(BASELIST)

const gnacc_child = accessor(CHILD, CHILDARR)

// return the hydrated values of multiple acessors
//
// gani_acc.groupaccessor([
//   gani_acc.fkey,
//   gani_acc.init
// ]).(node)
//
// returns {
//   fkey : hydratedfkeyvalue
//   init : hydratedinitvalue
// }
//
const gnacc_groupaccessor = accessorarr => (
  node => accessorarr.reduce((accum, acc) => (
    accum[acc.ns] = node.get(acc.name),
    accum
  ), {}))

const gnacc_isvalidfkeyval = fkeyval => (
  Boolean(typeof fkeyval !== 'undefined'))

const gnacc_shallowcp = (value, type = typeof value, f) => {
  if (value === null)
    f = value
  else if (/string|number|boolean/.test(type))
    f = value
  else if (Array.isArray(value))
    f = value.slice()
  else if (type === 'object')
    f = Object.assign({}, value)

  return f
}

const gnacc_nsmap = {
  subj: gnacc_subj,
  fkey: gnacc_fkey,
  cache: gnacc_cache,
  init: gnacc_init,
  childsubj: gnacc_childsubj,
  full: gnacc_full,
  base: gnacc_base,
  part: gnacc_part,
  baselist: gnacc_baselist,
  child: gnacc_child
}

export {
  gnacc_nsmap as default,
  gnacc_subj,
  gnacc_fkey,
  gnacc_cache,
  gnacc_init,
  gnacc_childsubj,
  gnacc_full,
  gnacc_base,
  gnacc_part,
  gnacc_baselist,
  gnacc_child,
  gnacc_nsmap,
  gnacc_groupaccessor,
  gnacc_isvalidfkeyval,
  gnacc_shallowcp
}
