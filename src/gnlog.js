// Filename: gani_log.js
// Timestamp: 2018.01.18-00:10:02 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import {
  gnenumKEY as KEY
} from './gnenum.js'

const gnlog_stringify = (obj, mini = false) => (
  (typeof obj === 'object' && obj)
    ? (mini
      ? JSON.stringify(obj)
      : `\n${JSON.stringify(obj, null, '  ')}`)
    : obj)

// firefox console "bug" prints second line here w/
// unwanted indentation. breaks formatting for some logs.
//
//    console.log('line one', "\n line two")
//
// join strings found at the front of the arg list to
// get around this bug
const gnlog_mozbug = (...consoleargs) => (
  typeof consoleargs[1] === 'string'
    ? gnlog_mozbug(...[
      `${consoleargs[0]} ${consoleargs[1]}`,
      ...consoleargs.slice(2)
    ]) : consoleargs)

const gnlog = (cfg, ...args) => {
  if (cfg.islogenabled)
    console.log(...gnlog_mozbug('[...] gani:', ...args))
}

const gnlogerr = (cfg, ...args) => {
  // error gives stacktrace
  console.error(...gnlog_mozbug('[...] gani:', ...args))
}

const gnlog_pgisvalid = (cfg, pg) => (
  gnlog(cfg, `valid node pg, ${pg.type}`))

const gnlog_error = (cfg, ...args) => {
  const isnode = node => Boolean(
    typeof node === 'object' && node &&
      typeof node.has === 'function' &&
      typeof node.toJS === 'function')

  if (isnode(args[0]) && isnode(args[1])) {
    if (cfg.islogenabled) {
      console.error('errgraph: ', args[0].toJS())
      console.error('errgraph: ', args[1].toJS())
    }
    args = args.slice(0, -2)
  }

  if (cfg.islogenabled ||
      cfg.iserrorenabled) {
    console.error(...gnlog_mozbug('[!!!] gani: ', ...args))
  }
}

const gnlog_node = (cfg, graph, node, ...args) => {
  const nodekey = (
    node && typeof node.get === 'function' && node.get(KEY))

  if (typeof window === 'object') {
    window.errgraph = graph
    window.errnode = node
  }

  gnlog_error(cfg, graph, node, nodekey, ...args)
}

const gnlog_nodepatchempty = (cfg, gr, nd, valobj) => (
  gnlog_node(cfg, gr, nd, (
    'empty patch, are target values defined in node subject? ' +
      gnlog_stringify(valobj)
  )))

const gnlog_pathmatchfound = (cfg, pathspec, pathmatch) => (
  gnlog(cfg, `match is found for “${pathspec}”, “${pathmatch}”`))

const gnlog_reporttimeunit = (timearr, msg) => (
  timearr.map(([time, key]) => (
    msg.replace(/:time/, (`     ${time}`).substr(-5, 5))
      .replace(/:key/, key)
  )).join('\n'))

// only key is sent --in some contexts, node may no longer
// exist in graph if 'population' involves its removal
const gnlog_nodepopulated = (cfg, key, report, now = Date.now()) => (
  gnlog(cfg, key, '\n' + [
    gnlog_reporttimeunit(report.tsrmarr, ':timems  remove, :key'),
    gnlog_reporttimeunit(report.tsarr, ':timems  populate, :key'),
    gnlog_reporttimeunit(report.tsvnodearr, ':timems  vnode, :key'),
    gnlog_reporttimeunit(report.delaykeyarr, ':timems  delay, :key'),
    'total: :timems, :secss'
      .replace(/:time/, now - report.startts)
      .replace(/:sec/, (now - report.startts) / 1000)
  ].filter(n => n).join('\n=================\n')))

const gnlog_nodesubjpublish = (cfg, key) => (
  gnlog(cfg, `subjdom publish: ${key}`))

const gnlog_nspropresolvedundefined = (
  cfg, nd, ns, name, nskey, nsprop, fullstr
) => (
  gnlogerr(cfg, nd.get('key'), (
    'Property ":name" resolved as `undefined` from ":fullstr". '
      + 'If this was intentional, resolve `null` instead. '
      + 'Otherwise, the namespace ":nskey" or its lookup ":nsprop" is wrong. '
      + 'Current :nskey properties: :nsprops')
    .replace(/:name/, name)
    .replace(/:fullstr/, fullstr)
    .replace(/:nskey/g, nskey)
    .replace(/:nsprop/, nsprop)
    .replace(/:nsprops/, gnlog_stringify(ns, true))))

export {
  gnlog as default,
  gnlog_stringify,
  gnlog_mozbug,
  gnlog,
  gnlog_pgisvalid,

  gnlog_error,
  gnlog_reporttimeunit,

  gnlog_node,
  gnlog_nodepatchempty,
  gnlog_nodepopulated,
  gnlog_nodesubjpublish,

  gnlog_pathmatchfound,
  gnlog_nspropresolvedundefined
}
