// Filename: gnpgdatals.js
// Timestamp: 2018.01.15-23:23:17 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import {
  sphnode_assign
} from 'spectraph/src/sphnode.js'

import {
  sphgraph_setnodechild
} from 'spectraph/src/sphgraph.js'

import gnpg from './gnpg.js'
import gnpgdata from './gnpgdata.js'

import {
  gnacc_subj,
  gnacc_fkey
} from './gnacc.js'

import {
  gnnode_fromspecpginit,
  gnnode_childget,
  gnnode_childgetall,
  gnnode_childrm,
  gnnode_graphset,
  gnnode_childspecwhenparentfirst,
  gnnode_childspecfromref,
  gnnode_prophydrateinit,
  gnnode_prophydratesubj,
  gnnode_prophydratefromchildsrefresh,
  gnnode_siblingnextorprevkeyget
} from './gnnode.js'

import {
  gnpublish_persistgraph
} from './gnpublish.js'

import {
  gnspec_childprefromspec
} from './gnspec.js'

import {
  gnenumSPECTYPES,
  gnenumEVTYPEONCHILDADDED as ONCHILDADDED,
  gnenumEVTYPEONCHILDREMOVED as ONCHILDREMOVED,

  gnenumKEY as KEY,
  gnenumACTIVEKEY as ACTIVEKEY,
  gnenumISDATA as ISDATA,
  gnenumCHILD as CHILD,
  gnenumNAME as NAME
} from './gnenum.js'

const initnode = nd => (
  nd = sphnode_assign(nd, {
    [ISDATA]: true
  }),

  nd = gnacc_subj.pushspec(nd, {
    // locksequent      : true,
    [ACTIVEKEY]: '',
    childlength: '',
    childadded: '',
    childaddedfirst: '',
    childremoved: '',
    childremovedlast: ''
  }),
  nd)

const haschild = (ss, cfg, gr, ndp, cnodename) => (
  gnnode_childget(gr, ndp, cnodename))

const onchildadd = (ss, cfg, gr, ndp, ndc) => {
  const childkey = ndc.get(KEY)
  const childlength = gnnode_childgetall(ndp).count()

  ndp = gnacc_subj.assign(ndp, {
    childadded: childkey,
    childlength
  })

  if (childlength === 1) {
    ndp = gnacc_subj.setprop(
      ndp, ACTIVEKEY, childkey)
  }

  const graphsetres = gnnode_graphset(gr, ndp)
  gr = graphsetres[0]
  ndp = graphsetres[1]

  gnpublish_persistgraph(cfg, gr)

  return [gr, ndp, ndc]
}

const populatechild = (ss, cfg, gr, ndp, fulldata, fn) => {
  let childarr = ndp.get(CHILD, []),
      childlength = gnnode_childgetall(ndp).count(),
      childnode

  // eslint-disable-next-line max-len
  gnnode_childspecwhenparentfirst(ss, cfg, gr, ndp, fulldata, childarr, async (e, childspec) => {
    if (e) return fn(e)

    // childspec = childspec || (
    //   Object.keys(ndp.get('child')).length === 1
    //    && ndp.get('child')[Object.keys(ndp.get('child'))[0]])

    if (!childspec) {
      // console.log('warning function sig differs from call')
      // console.log('monkey patch wrong return')
      // fulldata: { name: 'layer-0', type: 'layer' },
      // childarr: [ { spec: 'marker', when: [Object] } ],
      const spectypes = ndp.get(gnenumSPECTYPES) || {}
      const spectypekeys = Object.keys(spectypes)

      childspec = { spec: spectypekeys[0] }
    }
    if (!childspec) {
      throw new Error('no spectype')
    }

    const childspecfull = await gnnode_childspecfromref(
      ss, cfg, gr, ndp, childspec, ndp.get(gnenumSPECTYPES))

    childspec = gnspec_childprefromspec(
      ss, cfg, gr, ndp, fulldata, childlength, childspec, childspecfull)

    childnode = gnnode_fromspecpginit(cfg, gr, childspec, ndp)
    childnode = gnacc_fkey.assign(childnode, {
      [ndp.get(NAME)]: ndp.get(KEY)
    })

    const res = sphgraph_setnodechild(
      gr, ndp, childnode, childnode.get(NAME, null))

    gr = res[0]
    ndp = res[1]
    childnode = res[2]

    gnnode_prophydrateinit(ss, cfg, gr, childnode, (e, gr, mnode) => {
      if (e) return fn(e)

      gnnode_prophydratesubj(ss, cfg, gr, mnode, (e, gr, mnode) => {
        if (e) return fn(e)

        fn(null, gr, ndp, mnode)
      })
    })
  })
}

// pass parent fkey down to list childs
//
// here definitions on the parent node are used to describe relationships
// with child subjects, using parent's 'fkeyname'
const addchilddata = (ss, cfg, gr, ndp, pg, fulldata, fn) => {
  if (!fulldata)
    throw new Error('fulldata is required')

  populatechild(ss, cfg, gr, ndp, fulldata, (e, gr, ndp, ndc) => {
    if (e) return fn(e)

    const res = pg.onchildadd(ss, cfg, gr, ndp, ndc)
    gr = res[0]
    ndp = res[1]
    ndc = res[2]

    // rebuild accumulator values...
    gnnode_prophydratefromchildsrefresh(ss, cfg, gr, ndp, (e, gr, ndp) => {
      if (e) return fn(e)

      ndc = gr.get(ndc.get(KEY))

      gnpg.evPublishPOST(ss, cfg, gr, ndp, pg, ONCHILDADDED, {
        node: ndc
      }, e => {
        if (e) return fn(e)

        fn(null, gr, ndp, ndc)
      })
    })
  })
}

const setnextactivekey = (ss, cfg, gr, nd, activekey) => {
  const nextkey = gnnode_siblingnextorprevkeyget(
    ss, cfg, gr, gr.get(activekey))

  nd = gnacc_subj.setprop(nd, ACTIVEKEY, nextkey)

  return nd
}

// remove nodefrom list...
const onrmchild = (ss, cfg, gr, nd, ckey) => {
  const activekey = gnacc_subj.getprop(nd, ACTIVEKEY)
  const childlength = gnnode_childgetall(nd).count()

  nd = gnacc_subj.assign(nd, {
    childremoved: ckey,
    childlength
  })

  if (activekey === ckey) {
    nd = setnextactivekey(ss, cfg, gr, nd, activekey)
  }

  const graphsetres = gnnode_graphset(gr, nd)
  gr = graphsetres[0]
  nd = graphsetres[1]

  gnpublish_persistgraph(cfg, gr)

  return [gr, nd]
}

const rmchild = (ss, cfg, gr, nd, pg, ckey, fn) => {
  const cnode = gr.get(ckey)

  gnnode_childrm(ss, cfg, gr, nd, ckey, (e, gr, nd) => {
    if (e) return fn(e)

    const rmchildres = pg.onrmchild(ss, cfg, gr, nd, ckey)
    gr = rmchildres[0]
    nd = rmchildres[1]

    gnnode_prophydratefromchildsrefresh(ss, cfg, gr, nd, (e, gr, ndp) => {
      if (e) return fn(e)

      gnpg.evPublishPOST(ss, cfg, gr, ndp, pg, ONCHILDREMOVED, {
        node: cnode
      }, e => {
        if (e) return fn(e)

        fn(null, gr, ndp)
      })
    })
  })
}

// returns (err, graph, pnode)
const rmchildarr = (ss, cfg, gr, nd, pg, ckeyarr, fn) => {
  (function next (x, idarr, gr, ndp) {
    if (!x--)
      return gnnode_prophydratefromchildsrefresh(ss, cfg, gr, ndp, fn)

    pg.rmchild(ss, cfg, gr, ndp, pg, idarr[x], (e, gr, ndp) => {
      if (e) return fn(e)

      next(x, idarr, gr, ndp)
    })
  }(ckeyarr.length, ckeyarr, gr, nd))
}

// rebuilds and replaces existing cnode comprehensively
// obtains and uses spec data anew and does not use
// existing node spec data
//
// if the 'type' of node changes and facilitates using a new spec
// use ths function
//
// o.setchilddata = (ss, cfg, gr, ndp, ndc, data, fn) => {
//   let childs = ndp.get(CHILD, [])
//
//   data = Object.assign(
//     gnacc_subj.get(ndc), data)
//
//   gnnode_childspecwhenparentfirst(
//     ss, cfg, gr, ndp, data, childs, (e, cspec) => {
//       if (e) return fn(e)
//
//       const spec = gnnode_childspecfromref(
//         ss, cfg, gr, ndp, cspec, ndp.get('spectypes'))
//
//       ndc = gnacc_init.rmspec(ndc)
//       ndc = gnacc_subj.rmspec(ndc)
//       ndc = sphnode_assign(ndc, spec)
//       ndc = gnacc_base.set(ndc, data)
//
//       gnnode_prophydrateinit(ss, cfg, gr, ndc, (e, gr, ndc) => {
//         if (e) return fn(e)
//
//         gnnode_prophydratesubj(ss, cfg, gr, ndc, (e, gr, ndc) => {
//           if (e) return fn(e)
//
//           gr = gr.set(ndc.get(KEY), ndc)
//
//           fn(null, gr, ndp, ndc)
//         })
//       })
//     })
// }

const hydratepartial = (ss, cfg, gr, nd, pg, fn) => {
  const graphsetres = gnnode_graphset(gr, gnacc_subj.assign(nd, {
    childlength: gnnode_childgetall(nd).count()
  }))

  gr = graphsetres[0]
  nd = graphsetres[1]
  fn(null, gr, nd)
}

export default Object.assign({}, gnpgdata, {
  type: 'gnpgdatals',
  initnode,
  haschild,
  onchildadd,
  populatechild,
  addchilddata,
  onrmchild,
  rmchild,
  rmchildarr,
  setnextactivekey,
  hydratepartial
})
