// Filename: gani_cfg.js
// Timestamp: 2018.05.05-00:25:14 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import { h } from 'inferno-hyperscript'
import {
  hypelns,
  hypelsvg
} from 'hypel'

import gnruntime from './gnruntime.js'
import gnpgdata from './gnpgdata.js'
import gnpgdatals from './gnpgdatals.js'
import gnpgui from './gnpgui.js'

import {
  gnenumCFGPROPTYPEPREFIXDIR as PREFIXDIR,
  gnenumCLASSNAMENODE
} from './gnenum.js'

import {
  gnacc_subj
} from './gnacc.js'

import {
  gnpathdef_findmatchingfullpathstr
} from './gnpathdef.js'

import {
  gnerr_cfgreqdfnnotfoundpagespecfromisopath,
  gnerr_cfgenvinvalidorigin,
  gnerr_cfgprefixdirinvalid,
  gnerr_invalidpg
} from './gnerr.js'

const isValidPrefixDir = dir => (
  typeof dir === 'string'
    && dir.endsWith('/')
    && dir.startsWith('/'))

export default (cfgglobal, cfgenv) => {
  const o = {}
  const boolOr = (arg, def) => (
    typeof arg === 'boolean' ? arg : def)

  o.defautlLocale = 'eng-US'

  o.dom = cfgenv.dom || (typeof window === 'object' && window)
  o.islogenabled = boolOr(cfgenv.islogenabled, false)
  o.iserrorenabled = boolOr(cfgenv.iserrorenabled, true)
  o.issinglepage = boolOr(cfgenv.issinglepage, false)

  o.lastrenderid = 0
  o.version = cfgenv.version || '0.0.0'
  o.startts = cfgenv.startts || Date.now()
  o.cachemapisourlspecs = {}

  // origin, eg protocol + hostname "https://my.app.org"
  o.origin = cfgenv.origin
  if (!o.origin)
    throw gnerr_cfgenvinvalidorigin()

  o.cfgenv = cfgenv

  // NEEDS MORE INFO 'prev' data for all nodes
  o.prev = {}

  // frames per millisecond, used for animated nodes
  o.fpms = cfgenv.fpms
  o.specpath = cfgenv.specpath || './spec'
  o.speccache = {}
  o[PREFIXDIR] = cfgenv[PREFIXDIR] || cfgglobal[PREFIXDIR] || ''
  if (o[PREFIXDIR] && !isValidPrefixDir(o[PREFIXDIR]))
    throw gnerr_cfgprefixdirinvalid(o[PREFIXDIR])

  // finally, create final interpreter w/ addl. gani-specific
  // behaviour for custom dsl pattern
  o.spec = gnruntime(
    cfgenv.specfn,
    cfgenv.speccb,
    cfgenv.specerrfn || (
      (sess, cfg, graph, node, err, fn) => fn(null, graph)))

  // holds basic rules for each supported path and used to detect
  // route permissions and configurations before requesting
  // or constructing page objects
  o.pathdef = cfgglobal.pathdef || null

  // creates a lookup map of page objects; throw error for invalid objects
  o.pgmap = (cfgglobal.nodes || []).concat([
    gnpgdata, // default nodes provided
    gnpgdatals,
    gnpgui
  ]).reduce((pgmap, pg) => {
    const isvalid = Boolean(pg && pg.type)
    if (!isvalid)
      throw gnerr_invalidpg(pg)

    pgmap[pg.type] = pg

    return pgmap
  }, {})

  // env-customizable initsess and initcfg,
  // called for every init sequence
  o.initsess = cfgenv.initsess || (ss => ss)
  o.initcfg = cfgenv.initcfg || (cfg => cfg)

  // probelocaleenv called before initlocale
  // and is called only when locale not found in url or session
  o.probelocaleenv = cfgenv.probelocaleenv || (
    (ss, cfg) => ss.locale || cfg.defautlLocale)

  // initlocale called after probelocaleenv
  // and is called for every init sequence
  o.initlocale = cfgenv.initlocale || (cfg => cfg)

  // after long undevelopment, added condition check for cfg.manifest.appname
  o.pagemetaget = cfgglobal.pagemetaget || ((ss, cfg, subj, pagedirarr) => ({
    description: subj.gnmetadescription
      || (cfg.manifest && cfg.manifest.appdescription),
    title: typeof subj.gnmetatitle === 'string' ? subj.gnmetatitle : [
      (pagedirarr || [])
        .map(pnode => pnode.pathvanilla.replace(/\//i, ''))
        .filter(e => e)[0],
      (cfg.manifest && cfg.manifest.appname) || 'appname'
    ].filter(e => e).join(' - ')
  }))

  // pattern files usually available from network or filesystem
  o.pagespecfromisopath = cfgenv.pagespecfromisopath || (
    (sess, cfg, isopath) => {
      throw gnerr_cfgreqdfnnotfoundpagespecfromisopath(cfg, isopath)
    }),

  // if requested path not permitted or found, return default path
  o.pagepathdefaultget = cfgglobal.pagepathdefaultget || (
    (sess, cfg) => gnpathdef_findmatchingfullpathstr(
      cfg, cfg.pathdef, `${cfg.pathdef.pagearr[0].pathvanilla}/`))

  o.pagepathdefispermitted = cfgglobal.pagepathdefispermitted || (
    (sess, cfg, pathdef) => 'permission' in pathdef === false)

  // in case there is churn around hyperscript libraries
  o.h = (h => (pg, nd, subj = gnacc_subj.get(nd, {})) => h({
    key: nd.get('key'),
    ui: (subj[gnenumCLASSNAMENODE] || nd.get('name')).replace(/_\d.*/, '')
      + '.' + pg.type
  }))(hypelns(h))

  o.hsvg = () => hypelsvg(h)

  if (typeof process === 'object'
      && typeof process.on === 'function') {
    process.on('uncaughtException', e => {
      console.error('gani exception: ', e)
    })
  }

  return o
}
