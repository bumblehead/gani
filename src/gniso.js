// Filename: gani_iso.js
// Timestamp: 2017.12.23-20:29:25 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>
//
// iso behaviours aren't well developed. file will likely change, some thoughts,
//
// there are three combinations of lang-locale,
//
//  1. lang, files featuring differences in language only
//  2. locale, files featuring differences in locale only
//  3. lang & locale, files featuring differences langugage and locale
//
//  where multiple language and locale are featured at one location,
//  a file must exist for *every* combination of language and locale
//  which we seek to avoid where possible in favor of solutions that
//  allow optimising for files featuring lang or locale changes only
//
//  optimised cases would require fewer files that are generated in
//  shorter time frame
//
//  part of the solution here is to use a template names such as
//  Lang.json, Locale.json or LangLocale.json, which can be converted
//  to a final iso name, like US.json, eng.json or eng-US.json
//
//  improved iso support should be added later after more tests
//  and documentation are added

import {
  gnerr_isopatterninvalid,
  gnerr_isopathinvalid
} from './gnerr.js'

import {
  gnenumISOLANG,
  gnenumISOREGION,
  gnenumISOLOCALERe
} from './gnenum.js'

const slcharcode = 47
const dotcharcode = 46
const dummydomain = new URL('http://dummy.com')
const pathjoin = (basepath, subpath) => {
  const basepathstartchar = basepath.charCodeAt(0)
  const basepathendchar = basepath.charCodeAt(basepath.length - 1)
  const basepathdir = basepathendchar === slcharcode ? basepath : basepath + '/'
  const pathname = new URL(
    './' + subpath, new URL(basepathdir, dummydomain)).pathname

  return (basepathstartchar === slcharcode
    ? pathname
    : (basepathstartchar === dotcharcode)
      ? '.' + pathname
      : pathname.slice(1))
}

// ex,
//  'eng-US' -> ['eng', 'US']
//  'eng' -> ['eng', 'ZZ']
//  'US' -> ['zzz', 'US']
//  '' -> ['zzz', 'ZZ']
const localeIdParse = localeId => {
  const charCodeHyphen = 45

  return localeId.charCodeAt(3) === charCodeHyphen
    ? [localeId.slice(0, 3), localeId.slice(4)]
    : localeId.length === 3
      ? [localeId, null]
      : [null, localeId]
  //  ? [localeId, pgEnumLOCALETYPEUNIVERSALREGION]
  // : [pgEnumLOCALETYPEUNIVERSALLANG, localeId]
}

const localeIdBaseCreate = localeId => {
  const charCodeHyphen = 45

  return localeId.charCodeAt(3) === charCodeHyphen
    ? 'baseLangRegion.json'
    : localeId.length === 3
      ? 'baseLang.json'
      : 'baseRegion.json'
}

// ex,
//  'eng-US' -> 'eng-US'
//  'eng' -> 'eng-ZZ'
//  'US' -> 'zzz-US'
//  '' -> 'zzz-ZZ'
// const pgLocaleIdResolve = localeId => {
//   const charCodeHyphen = 45
//
//   return !localeId || (localeId.charCodeAt(3) === charCodeHyphen)
//     ? localeId || pgEnumLOCALETYPEUNIVERSALID
//     : localeId.length === 3
//       ? localeId + '-' + pgEnumLOCALETYPEUNIVERSALREGION
//       : pgEnumLOCALETYPEUNIVERSALLANG + '-' + localeId
// }

// input iso path and sess { lang : 'eng', region : 'US' }: (old)
//   /page/data/type-countries-arr/baseLangRegion.json
// return a new path:
//   /page/data/type-countries-arr/eng-US.json
const gniso_getpathiso = (ss, path, ll = ss.locale) => {
  if (!gnenumISOLOCALERe.test(path))
    throw gnerr_isopathinvalid(path)

  return path.replace(gnenumISOLOCALERe, (a, b) => {
    if (b === gnenumISOLANG)
      return localeIdParse(ll)[0]
    if (b === gnenumISOREGION)
      return localeIdParse(ll)[1]
    else // == gnenumISOLOCALE)
      return ll
  })
}

// ex,
//  'eng-US' -> ['eng', 'US']
//  'eng' -> ['eng', 'ZZ']
//  'US' -> ['zzz', 'US']
//  '' -> ['zzz', 'ZZ']
const gniso_localeIdParse = localeId => {
  const charCodeHyphen = 45

  return localeId.charCodeAt(3) === charCodeHyphen
    ? [localeId.slice(0, 3), localeId.slice(4)]
    : localeId.length === 3
      ? [localeId, null]
      : [null, localeId]
}

const gniso_patternfromfullpath = async (sess, cfg, path) => {
  const pathiso = gniso_getpathiso(sess, path)
  const pattern = await cfg.pagespecfromisopath(sess, cfg, pathiso)
  if (!pattern || typeof pattern !== 'object')
    throw gnerr_isopatterninvalid(pathiso)

  return pattern
}

const gniso_patternfromfullpathcache = async (sess, cfg, path) => (
  cfg.cachemapisourlspecs[path] || (
    cfg.cachemapisourlspecs[path] = gniso_patternfromfullpath(sess, cfg, path)))

const gniso_getlocalebest = (sessLocale, manifestLocales) => {
  // manifestLocales csv w/ no commas indicates a single locale
  if (manifestLocales.indexOf(',') === -1)
    return manifestLocales

  const localeParsed = gniso_localeIdParse(sessLocale)
  const localeParsedLang = localeParsed[0]
  const localeParsedRegion = localeParsed[1]
  const manifestLocalesEndc = ',' + manifestLocales + ','
  const getexactmatch = locale => (
    locale && manifestLocalesEndc.indexOf(',' + locale + ',') > -1
      ? locale
      : null)

  return getexactmatch(sessLocale)
    || getexactmatch(localeParsedLang)
    || getexactmatch(localeParsedRegion)
    || (localeParsedLang && (manifestLocalesEndc
      .match(new RegExp(`(?!,)${localeParsedLang}-\\w*`)) || [])[0])
    || (localeParsedRegion && (manifestLocalesEndc
      .match(new RegExp(`(?!,)\\w-${localeParsedRegion}*`)) || [])[0])
    || manifestLocales.split(',')[0]
}

// return the spec/pattern definition from the given path
const gniso_patternfrompath = async (sess, cfg, path) => {
  const localeBest = gniso_getlocalebest(sess.locale, cfg.manifest.locales)
  const localePath = String(path).startsWith('/')
    ? path
    : pathjoin(path, localeIdBaseCreate(localeBest))

  return gniso_patternfromfullpathcache(sess, cfg, localePath)
}

// match 639-1 two-letter language code format. ex,
// ```
// 'eng,jap-JP,es|spa-ES,chi-ES'
//   .replace(ISO639_1_re, '$1')
// 'en,ja-JP,es-ES,ch-ES'
// ```
// const gniso_localesspec_ISO639_1_Re = /(?!,)([a-z]{2})([^-,]*)/g
//
// match 639-2 three-letter language code format. ex,
// ```
// 'eng,jap-JP,es|spa-ES,chi-ES'
//   .replace(ISO639_2_re, '$2')
// 'eng,jap-JP,spa-ES,chi-ES'
// ```
// const gniso_localesspec_ISO639_2_Re = /(?!,)([a-z]{2})\|([a-z]{3})/g
//
// match 639-1+639-2 two and three-letter language code format groups. ex,
// ```
// 'eng,jap-JP,es|spa-ES,chi-ES'
//   .match(ISO639_X_re)
// [ 'es|spa-ES' ]
// ```
// const gniso_localesspec_ISO639_X_Re = /(?!,)([a-z]{2}\|[a-z]{3})/g
//
// return mapping from mixed ISO639 1 and 2 groupings
// ```
// ('eng,jap-JP,es|spa-ES,chi-ES') => ({
//   'en': 'eng',
//   'ja-JP': 'jap-JP',
//   'es-ES': 'spa-ES',
//   'ch-ES': 'chi-ES'
// })
// ```
const gniso_localesspecDiffMapCreate = (specstr, charCodeBar = 124) => (
  specstr.split(',').reduce((acc, l) => {
    if (l.charCodeAt(2) === charCodeBar)
      acc[l.slice(0, 2) + l.slice(6)] = l.slice(3)
    else
      acc[l.slice(0, 2) + l.slice(3)] = l

    return acc
  }, {}))

export {
  gniso_localesspecDiffMapCreate,

  gniso_getpathiso,
  gniso_getlocalebest,
  gniso_patternfromfullpath,
  gniso_patternfromfullpathcache,
  gniso_patternfrompath,
  gniso_localeIdParse
}

