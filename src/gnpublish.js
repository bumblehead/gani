// Filename: gani_publish.js
// Timestamp: 2018.12.09-01:17:42 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import fnguard from 'fnguard'
import { render } from 'inferno'
import { hydrate } from 'inferno-hydrate'

import {
  gnenumSUBJ as SUBJ
} from './gnenum.js'

import {
  gnreport_iskeyreplaced,
  gnreport_addreplacekey
} from './gnreport.js'

import {
  gnpatch_unitsfromkeynsarr,
  gnpatch_unitapplydeepall
} from './gnpatch.js'

import {
  gngraph_getvnode,
  gngraph_getrenderkeyarr
} from './gngraph.js'

import {
  gnnode_childrmall,
  gnnode_dfsattach,
  gnnode_pgonsubj
} from './gnnode.js'

import {
  gnlog_nodepopulated,
  gnlog_nodesubjpublish
} from './gnlog.js'

const optfn = () => {}

const gnpublish_getrootvtree = (cfg, dom = cfg.dom, doc) => (
  doc = dom && dom.window.document || document,
  doc.body.firstElementChild || doc.body)

const gnpublish_persistgraph = (cfg, graph) => (
  cfg.graph = graph)

const gnpublish_vtree = (ss, cfg, gr, vtree, dom) => {
  const rootvdom = gnpublish_getrootvtree(cfg, cfg.dom || dom)

  // inferno.hydrated must be applied to pre-existing nodetree
  // that was server-rendered. Assume server rendered content
  // if root node has any children (incl. whitespace or comments)
  if (!cfg.ishydrated && rootvdom.hasChildNodes()) {
    cfg.ishydrated = true

    hydrate(vtree, rootvdom)
  }

  // later when testing with real browser, use hydrate or render not both
  render(vtree, rootvdom)
  gnpublish_persistgraph(cfg, gr)
  cfg.renderkeyarr = gngraph_getrenderkeyarr(ss, cfg, gr)
}

const gnpublish_renderroot = (ss, cfg, gr, fn) => (
  gngraph_getvnode(ss, cfg, gr, false, fn))

const gnpublish_domgraph = (ss, cfg, gr, fn) => (
  gnpublish_renderroot(ss, cfg, gr, (e, gr, nd, vtree, report) => {
    if (e) return fn(e)

    gnpublish_vtree(ss, cfg, gr, vtree)

    fn(null, gr, report, vtree)
  }))

// keyvalarr === [ [ nodekey, valobj ] ]
const gnpublish_patchgraph = (ss, cfg, gr, keynsarr, fn) => {
  fnguard.isobj(ss, cfg).isarr(keynsarr).isany(fn)

  gnpatch_unitsfromkeynsarr(ss, cfg, gr, keynsarr, (e, patch, gr) => {
    if (e) return fn(e)

    // if (patch.length === 0) {
    //   gani_log.errornode_patchempty(cfg, graph, graph.get(nodekey), valobj)
    // }
    gnpatch_unitapplydeepall(ss, cfg, gr, patch, fn)
  })
}

//
// 1. update subj
// 2. patch dependent values in graph
//
// does not update document
// does not persist graph
//
// returns graph changes that will not appear in the rest of the app
// keyvalarr === [ [ nodekey, valobj ] ]
const gnpublish_graphsubjsilent = (ss, cfg, gr, ndkey, subj, fn = optfn) => {
  fnguard.isobj(ss, cfg, subj).isstr(ndkey)

  gnpublish_patchgraph(ss, cfg, gr, [[ndkey, subj]], (e, gr, rep) => {
    if (e) return fn(e)

    gnlog_nodepopulated(cfg, ndkey, rep)

    fn(null, gr, rep)
  })
}

const gnpublish_domgraphupdate = (ss, cfg, gr, ndkey, report, fn) => {
  if (report.replacearr.length) {
    gnlog_nodesubjpublish(cfg, ndkey)
    gnpublish_domgraph(ss, cfg, gr, fn)
  } else {
    fn(null, gr, report)
  }
}

//
// 1. update subj
// 2. patch dependent values in graph
// 3. if necessary, *apply graph to dom*
// 4. persist graph
//
// keyvalarr === [ [ nodekey, valobj ] ]
// eslint-disable-next-line max-len
const gnpublish_graphsubj = (ss, cfg, gr, keynsarr, fn = optfn, ispersist = true) => {
  fnguard.isobj(ss, cfg).isarr(keynsarr)

  if (!keynsarr.length)
    return fn(null, gr)

  gnpublish_patchgraph(ss, cfg, gr, keynsarr, (e, gr, rep) => {
    if (e) return fn(e)

    rep.patchkeyarr.map(patchkey => {
      // note, pgonsubj mutates dom
      if (!gnnode_pgonsubj(ss, cfg, gr, gr.get(patchkey))
        && !gnreport_iskeyreplaced(rep, patchkey)
      ) {
        rep = gnreport_addreplacekey(rep, patchkey)
      }
    })

    // publish against first key
    gnpublish_domgraphupdate(ss, cfg, gr, keynsarr[0][0], rep, (e, gr) => {
      if (e) return fn(e)

      // log against first key
      gnlog_nodepopulated(cfg, keynsarr[0][0], rep)

      rep.replacearr.map(key => (
        gnnode_dfsattach(ss, cfg, gr, gr.get(key))))
      rep.patchkeyarr.map(patchkey => (
        gnnode_pgonsubj(ss, cfg, gr, gr.get(patchkey))))

      if (ispersist)
        gr = gnpublish_persistgraph(cfg, gr)

      fn(null, gr, rep)
    })
  })
}

const gnpublish_subj = (ss, cfg, ndkey, subj, fn = optfn) => (
  gnpublish_graphsubj(ss, cfg, cfg.graph, [[ndkey, SUBJ, subj]], fn))

// not sure if these are needed
const gnpublish_graphrmchilds = async (ss, cfg, gr, ndkey, fn) => {
  fnguard.isobj(ss, cfg).isstr(ndkey)

  gnnode_childrmall(ss, cfg, gr, gr.get(ndkey), fn)
}

const gnpublish_rmchilds = (ss, cfg, ndkey, fn) => (
  gnpublish_graphrmchilds(ss, cfg, cfg.graph, ndkey, fn))

export {
  gnpublish_subj as default,
  gnpublish_subj as gnpublish,
  gnpublish_getrootvtree,
  gnpublish_persistgraph,
  gnpublish_vtree,
  gnpublish_renderroot,
  gnpublish_domgraph,
  gnpublish_patchgraph,
  gnpublish_graphsubjsilent,
  gnpublish_domgraphupdate,
  gnpublish_graphsubj,
  gnpublish_subj,
  gnpublish_graphrmchilds,
  gnpublish_rmchilds
}
