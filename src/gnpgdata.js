// Filename: data.js
// Timestamp: 2018.05.05-00:28:54 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import {
  gnenumEVTYPEONSUBJ as ONSUBJ,
  gnenumISDATA as ISDATA
} from './gnenum.js'

import gnpg from './gnpg.js'

const initnode = node =>
  node.set(ISDATA, true)

const resetnode = node => node

// consider moving onsubj calls to be blocking
const onsubj = (ss, cfg, gr, nd, pg, prev, subj) => {
  pg.evPublishPOST(ss, cfg, gr, nd, pg, ONSUBJ, {
    prev,
    subj
  })

  return true
}

export default Object.assign({}, gnpg, {
  type: 'gnpgdata',
  initnode,
  resetnode,
  onsubj
})
