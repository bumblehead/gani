// Filename: gani_express.js
// Timestamp: 2018.12.09-00:20:03 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import url from 'node:url'
import fs from 'node:fs/promises'
import acceptLanguageParser from 'accept-language-parser'
import infernoserver from 'inferno-server'

import {
  gninit_run0,
  gninit_run1
} from './gninit.js'

import {
  gnsess_isvalidsess
} from './gnsess.js'

import {
  gnerr_cfginvalidsess
} from './gnerr.js'

import {
  gnnode_metaget,
  gnnode_dfsfind,
  gnnode_promisify
} from './gnnode.js'

import {
  gngraph_renderpath
} from './gngraph.js'

import {
  gniso_localesspecDiffMapCreate
} from './gniso.js'

import {
  gnenumPATHROOTDEFAULT
} from './gnenum.js'

import {
  gnacc_subj
} from './gnacc.js'

const subjget = gnacc_subj.get

const gnsrv_redirectarr = []

const gnsrv_routevdom = async (ss, cfg, uri, acceptlangs) => {
  if (!gnsess_isvalidsess(ss))
    throw gnerr_cfginvalidsess(ss)

  const renderpathres = await gngraph_renderpath(ss, cfg, uri, acceptlangs)
  const [gr, pnodearr, , ndroot, vtree] = renderpathres

  return [gr, ndroot, pnodearr, infernoserver.renderToString(vtree)]
}

const gnsrv_addViewRoutingKoa = (cfgseed, uri, pgspec, init_run1ctxfn) => {
  // eslint-disable-next-line max-len
  cfgseed.cfgenv.attachRoute('get', cfgseed, uri, pgspec, init_run1ctxfn, async cfg => {
    const ss = cfg.ss
    const routeres = await gnsrv_routevdom(ss, cfg, ss.requrl)
    const gr = routeres[0]
    const ndroot = routeres[1]
    const pnodearr = routeres[2]
    const html = routeres[3]

    const [nd] = await gnnode_promisify(gnnode_dfsfind)(ss, cfg, gr, ndroot, (
      (ss, cfg, gr, nd) => gnnode_metaget(nd)
    ))

    const ndmeta = cfg.pagemetaget(ss, cfg, nd ? subjget(nd) : {}, pnodearr)

    return [ss, cfg, html, ndmeta, nd]
  })
}

// pending removal
const gnsrv_addviewroutingarr = (cfgseed, init2fn) => {
  cfgseed.pathdef.pagearr.map(pg => {
    gnsrv_addViewRoutingKoa(cfgseed, pg.path, pg, init2fn)
  })
}

const gnsrv_attach = (cfgseed, init2fn) => {
  if (cfgseed.cfgenv.isRoutesAttach
      && typeof cfgseed.cfgenv.attachRoute === 'function') {
    gnsrv_addviewroutingarr(cfgseed, init2fn)
  }
}

const gnsrvpagespecfromisopath = async (sess, cfg, isopath) => {
  // "$ROOTSPECS/spec/manifest.json" -> "../../../build/spec/manifest.json"
  if (!isopath.startsWith(gnenumPATHROOTDEFAULT)) {
    throw new Error('ivalid iso path')
  }

  if (!cfg.cfgenv.buildDir) {
    throw new Error('buildDir must be defined as env cfg')
  }

  isopath = new url.URL(
    // strip begin slash, eg  '/iso/path.json' => iso/path.json
    isopath.slice(gnenumPATHROOTDEFAULT.length + 1),
    url.pathToFileURL(cfg.cfgenv.buildDir))

  isopath = await fs.readFile(isopath, { encoding: 'utf8' })

  return JSON.parse(isopath)
}

const gnsrvprobelocaleenv = (sess, cfg, ctx) => {
  const manifestlocales = cfg.manifest.locales // comma-separated list
  const manifestlocaleslist = manifestlocales.split(',')

  // if only one locale supported, return it (disabled for testing)
  if (ctx.disablednow && manifestlocaleslist.length === 1) {
    return manifestlocaleslist[0]
  }

  // accept-language ex,
  //   'en-GB,en-US;q=0.9,fr-CA;q=0.7,en;q=0.8'
  //   'ja-JP,js;q=0.9'
  const localeAccept = ctx.get('accept-language')
  const localeMap = gniso_localesspecDiffMapCreate(manifestlocales)
  const localeBest6391 = acceptLanguageParser
    .pick(Object.keys(localeMap), localeAccept)
  const localeBest6392 = localeMap[localeBest6391]
    || cfg.defautlLocale.slice(0, 3) // 'eng-US' -> 'eng'

  return localeBest6392
}

// return full initialized cfg w/ requrl usable for rendering paths
const gnsrv_initall = async (cfgglobal, cfgenv, requrl, localeId) => {
  const cfg = await gninit_run0(cfgglobal, Object.assign({
    probelocaleenv: () => localeId,
    pagespecfromisopath: gnsrvpagespecfromisopath
  }, cfgenv))

  return gninit_run1(cfg, {
    requrl: requrl
    // requrl: ctx.originalUrl,
    // token: ctx.cookies && ctx.cookies.sesstoken
  })
}

const gnsrv_start = async (globalcfg, cfgenv) => {
  if (Array.isArray(cfgenv.redirectarr)) {
    gnsrv_redirectarr.push(...cfgenv.redirectarr)
  }

  const cfgseed = await gninit_run0(globalcfg, Object.assign({
    // override these w/ custom versions when calling gnsrv_start(…)
    probelocaleenv: gnsrvprobelocaleenv,
    pagespecfromisopath: gnsrvpagespecfromisopath
  }, cfgenv))

  return gnsrv_attach(cfgseed, async (cfg, ctx) => {
    cfg = await gninit_run1(cfg, {
      requrl: ctx.originalUrl,
      token: ctx.cookies && ctx.cookies.sesstoken
    }, ctx)

    return cfg
  })
}

export {
  gnsrv_redirectarr,
  gnsrv_routevdom,
  gnsrv_addViewRoutingKoa,
  gnsrv_addviewroutingarr,
  gnsrv_attach,
  gnsrv_start,
  gnsrv_initall
}
