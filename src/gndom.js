// Filename: gani_ev.js
// Timestamp: 2018.12.09-00:20:18 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import {
  gnenumKEY as KEY
} from './gnenum.js'

import {
  // gnkey_docdecodekey,
  gnkey_docencodekey,
  gnkey_docencodekeyarr,
  gnkey_docdecodekeyarr,
  gnkey_docdecodebasekey
} from './gnkey.js'

const cfgwin = cfg => cfg.dom.window

const cfgdoc = cfg => cfg.dom.window.document

const gndom_evcreate = (nodekey, type) => ({
  isfake: true,
  type,
  target: { id: nodekey }
})

const gndom_evresolve = (cfg, ev) => (
  ev || cfgwin(cfg).event)

const gndom_evtypeget = (cfg, ev) => (
  gndom_evresolve(cfg, ev).type)

const gndom_elemgetclickable = (elem, elemtag = elem && elem.tagName) => {
  if (!elemtag)
    return

  return elemtag === 'A' || elemtag === 'BUTTON'
    ? elem : gndom_elemgetclickable(elem.parentNode)
}

const gndom_elemgetlabel = (elem, elemtag = elem && elem.tagName) => {
  if (!elemtag)
    return

  return elemtag === 'LABEL'
    ? elem : gndom_elemgetlabel(elem.parentNode)
}

const gndom_evelemget = ev => (
  ev && ev.target)

const gndom_evlinkelemget = (cfg, ev) => {
  const evres = gndom_evresolve(cfg, ev)
  const evtype = gndom_evtypeget(cfg, ev)
  const evtarget = gndom_evelemget(evres)

  return evtype === 'click'
    ? gndom_elemgetclickable(evtarget) || evtarget
    : evtarget
}

const gndom_keyfromid = id => (
  gnkey_docdecodebasekey(id))

const gndom_evidget = (cfg, ev, elem) => (
  elem = gndom_evlinkelemget(cfg, ev),
  elem ? elem.id : null)

const gndom_evidkeyarrget = (cfg, ev) => (
  gnkey_docdecodekeyarr(gndom_evidget(cfg, ev)))

const gndom_evkeyget = (cfg, ev, id) => (
  id = gndom_evidget(cfg, ev),
  id && gndom_keyfromid(id))

const gndom_inputfromname = (cfg, name) => (
  cfgdoc(cfg).querySelector(`input[name="${name}"]`))

const gndom_nodeelemid = (nd, id, t) => (
  t = typeof id, t === 'string' || t === 'number'
    ? gnkey_docencodekeyarr([nd.get(KEY), id])
    : gnkey_docencodekey(nd.get(KEY)))

const gndom_nodeelem = (cfg, nd, id) => (
  cfgdoc(cfg).getElementById(gndom_nodeelemid(nd, id)))

const gndom_nodepartelem = (cfg, nd, partkey) => (
  cfgdoc(cfg).getElementById(
    gnkey_docencodekeyarr([nd.get('key'), partkey])))

const gndom_elemclasstoggle = (elem, togglename, togglebool) => {
  const classnameold = `${togglename}-${!togglebool}`
  const classnamenew = `${togglename}-${Boolean(togglebool)}`

  return (elem && (
    elem.classList.replace(classnameold, classnamenew)
      || elem.classList.add(classnamenew)))
}

// a little bit slower than class, but works with any togglevalue
const gndom_elemclassupdate = (elem, togglename, togglevalue) => {
  const classnameold = Array.from(elem.classList)
    .find(cl => cl.startsWith(togglename))
  const classnamenew = `${togglename}-${togglevalue}`

  return (elem && (
    elem.classList.replace(classnameold, classnamenew)
      || elem.classList.add(classnamenew)))
}

const gndom_nodeelemclasstoggle = (cfg, nd, togglename, togglebool) => (
  gndom_elemclasstoggle(gndom_nodeelem(cfg, nd), togglename, togglebool))

const gndom_nodeelemclassupdate = (cfg, nd, togglename, togglevalue) => (
  gndom_elemclassupdate(gndom_nodeelem(cfg, nd), togglename, togglevalue))

const gndom_nodeelemtextcontentset = (cfg, nd, id, str) => {
  const elem = gndom_nodeelem(cfg, nd, id)

  if (elem) elem.textContent = str
}

const gndom_nodeeleminnerhtmlset = (cfg, nd, id, htmlstr) => {
  const elem = gndom_nodeelem(cfg, nd, id)

  if (elem) elem.innerHTML = htmlstr
}

const gndom_nodeelemdisabledset = (cfg, nd, id, disabled) => {
  const elem = gndom_nodeelem(cfg, nd, id)

  if (elem) elem.disabled = Boolean(disabled)
}

const gndom_nodeelemvalueset = (cfg, nd, value) => {
  const elem = gndom_nodeelem(cfg, nd)

  if (elem) elem.value = value
}

const gndom_nodeelemvalueget = (cfg, nd, id) => {
  const elem = gndom_nodeelem(cfg, nd, id)

  return elem ? elem.value : null
}

const gndom_nodeformname = nd => (
  `${nd.get('key')}-form`)

const gndom_nodeform = (cfg, nd) => (
  cfgdoc(cfg).forms[gndom_nodeformname(nd)])

const gndom_nodeforminputs = (cfg, nd) => {
  const formelem = gndom_nodeform(cfg, nd)
  const forminputs = formelem && [].slice.call(
    formelem.getElementsByTagName('input'), 0)

  if (!formelem) {
    throw new Error('form elem not found')
  }

  return forminputs
}

const gndom_winyoffset = cfg => (
  cfgwin(cfg).pageYOffset || cfgdoc(cfg).body.scrollTop)

const gndom_docactiveelem = cfg => (
  cfgdoc(cfg).activeElement)

export {
  gndom_evcreate,
  gndom_evresolve,
  gndom_evtypeget,
  gndom_evelemget,
  gndom_keyfromid,
  gndom_evidget,
  gndom_evidkeyarrget,
  gndom_evkeyget,
  gndom_evlinkelemget,
  gndom_inputfromname,
  gndom_elemgetclickable,
  gndom_elemgetlabel,
  gndom_elemclasstoggle,
  gndom_elemclassupdate,
  gndom_nodeelemid,
  gndom_nodepartelem,
  gndom_nodeelem,
  gndom_nodeelemtextcontentset,
  gndom_nodeeleminnerhtmlset,
  gndom_nodeelemclasstoggle,
  gndom_nodeelemclassupdate,
  gndom_nodeelemdisabledset,
  gndom_nodeelemvalueget,
  gndom_nodeelemvalueset,
  gndom_nodeformname,
  gndom_nodeform,
  gndom_nodeforminputs,
  gndom_winyoffset,
  gndom_docactiveelem
}
