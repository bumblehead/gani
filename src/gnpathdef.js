// Filename: gani_pathdef.js
// Timestamp: 2017.12.23-14:33:14 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>
//
// after primitive path string, pathdef is an early node form including a path
// and other information, such as rules or permissions that must associate with
// the path, any available child paths, routing details etc
//
// pathdef objects are nested in tree-like way
//
// '/account/:home?' for example, might result in a pathdef hierarchy like this
//
// { ...rootdef, pagearr : [
//   { ...accountdef, pagearr : [
//     { ...accounthomedef, pagearr : [] },
//     { ...accountprofiledef, pagearr : [] },
//     { ...accounteditdef, pagearr : [] }
//   ] }
// ] }
//

import { pathToRegexp } from 'path-to-regexp'

import {
  gnlog_pathmatchfound
} from './gnlog.js'

import {
  gnerr_routesinvalid,
  gnerr_pathdefitemnotfound,
  gnerr_pathdeffullpathnotfound
} from './gnerr.js'

import {
  gnenumPATHROOTDEFAULT
} from './gnenum.js'

const gnpathdef_charcodeFORWARDSLASH = 47

const gnpathdef_pathisvalid = path =>
  typeof path === 'string' &&
    path.charCodeAt(0) === gnpathdef_charcodeFORWARDSLASH

// remove hash or question mark and all that follow
const gnpathdef_pathnoparams = (path, i = path.search(/[?#]/)) =>
  i > -1 ? path.slice(0, i) : path

// uriRule ex,
//
//   [ "", "page-root" ],
//   [ "/", "page-home" ],
//   [ "/faq", "page-faq" ],
//   [ "/account/:page?", "page-acct", { pagearr : [ ...uriRuleArr ] } ],
//
// returns a tree-like system of pathdef, where each pathdef 'pagearr'
// includes nested pathdef objects
//
// return ex,
// {
//   path: "/doc.gani/:page?",
//   pathvanilla: "/doc.gani",
//   pathre: {
//     keys: [ {
//       name: "page",
//       delimiter: "/",
//       optional: true,
//       repeat: false
//     } ]
//   },
//   pathpattern: "gitlabdir",
//   pagearr: [ {
//     path: "/overview/",
//     pathvanilla: "/overview",
//     pathre: {
//       keys: []
//     },
//     pathpattern: "page-overview"
//   } ]
// }
//
const gnpathdef_create = uriRule => {
  const rulepath = uriRule[0]
  const pathpattern = uriRule[1]
  const urirule = uriRule[2]
  const pathvanilla = rulepath.match(/(\/?[^/]*)/)[0]

  if (!uriRule[1]) {
    throw new Error(`[!!!] view undefined: ${uriRule}`)
  }

  return Object.assign({}, urirule || {}, {
    path: rulepath,
    pathvanilla,
    pathre: pathToRegexp(rulepath).regexp,
    pathpattern
  })
}

// use an option on the manfest to determine how paths should be un-packed
// eg, flat: page-home-footer-nav-img
//     nested: page-home/fooer-nav-img
const gnpathdef_patternpathfrompath = (manifest, routepath, routepathp) => {
  // '/' => 'pg-index'
  // '/about/' => 'root-about'
  // '/blog/all/' => 'root-blog-all'
  if (routepath === routepathp) {
    routepath += 'pg-index'
  }

  const pattern = routepath
    .replace(/^\//, 'root-')
    .replace(/\/$/, '')
    .replace(/\//g, '-')
    .replace(/:/, '')

  return pattern
}

const gnpathdef_createfrommanifestroutes = (mroutes, parent, routes = []) => {
  if (!mroutes.length)
    return routes

  const path = mroutes[0]
  const pathchilds = Array.isArray(mroutes[1]) && mroutes[1]
  const subpath = (
    parent !== '/'
      && typeof path === 'string'
      && path.startsWith(parent)
  ) ? '/' + path.slice(parent.length) : path

  // use an option on the manfest to determine how paths should be un-packed
  // eg, flat: page-home-footer-nav-img
  //     nested: page-home/fooer-nav-img
  if (typeof subpath === 'string') {
    const pathdef = gnpathdef_create([
      // if "/blog/" has pathchilds ["/blog/blog-1/"]
      // then match "/blog/:item?"
      // older koa pathtore used syntax ':item?' and changed to '{:item}'
      pathchilds ? subpath + '{:item}' : subpath,

      gnpathdef_patternpathfrompath({}, path, parent)
    ])

    if (pathchilds) {
      pathdef.pagearr = gnpathdef_createfrommanifestroutes(
        pathchilds[0], path)
    }

    routes.push(pathdef)
  }

  if (Array.isArray(pathchilds)) {
    return gnpathdef_createfrommanifestroutes(
      mroutes.slice(2), parent, routes)
  } else {
    return gnpathdef_createfrommanifestroutes(
      mroutes.slice(1), parent, routes)
  }
}

const gnpathdef_createfrommanifest = manifest => {
  // first 'pathdefroot' is a legacy could possibly be removed if time
  // to refactor
  const pathdefroot = gnpathdef_create(['', 'root'])

  if (!Array.isArray(manifest.routes))
    throw gnerr_routesinvalid(manifest.routes)

  pathdefroot.pagearr = gnpathdef_createfrommanifestroutes(
    manifest.routes, '/')

  return pathdefroot
}

// does pathstr match the given pathdef?
const gnpathdef_ismatchpathstr = (pathdef, pathstr) => (
  pathdef.pathre.test(pathstr))

// ('/blog', [
//    { path: '/', ...pathdef1 },
//    { path: '/blog/:item?', ...pathdef2 }
// ]) -> ({ path: '/blog/:item?', ...pathdef2 })
//
// return first pathdef from list matching with pathstr
const gnpathdef_findmatchpathstr = (pathstr, pathdefarr = []) => {
  const pathdefarrlen = pathdefarr.length
  const pathdef = pathdefarr[pathdefarrlen - 1]

  if (!pathdefarr.length)
    return null

  return gnpathdef_ismatchpathstr(pathdef, pathstr)
    ? pathdef
    : gnpathdef_findmatchpathstr(pathstr, pathdefarr.slice(0, -1))
}

const gnpathdef_querystringre = /\?.*$/
const gnpathdef_tokenresolve = token => (
  token = token.replace(gnpathdef_querystringre, ''),
  token === '/' ? token : `/${token}`)

// from nested definitions in pathdefarr, return a flat list of pathdef
// matching the pathstr in pathstrarr.
//
// ['/account', '/edit'] <= for path /account/edit
//
// index position of items correspond to their depth in pathdef
// if more than one path appears, views on the path must be
// childed to the parent view
//
// function not originally created with useful comments or tests, and
// some conditions are now un-familiar :/
//
// ex,
// (cfg, [ '/blog', '/blog-all' ], [ ...blogdef, ...blogalldef ], rootdef)
//
const gnpathdef_findmatchpatharr = (cfg, patharr, pathdefarr, routesobj) => {
  if (!Array.isArray(patharr)) {
    throw new Error(
      `invalid patharr must be list of path tokens, pathharr: "${patharr}"`)
  }

  const nextpatharr = patharr.slice(1)
  const pathitem = gnpathdef_tokenresolve(patharr[0])
  const pathdef = gnpathdef_findmatchpathstr(
    pathitem, pathdefarr)

  let arr = []

  if (pathdef) {
    arr = routesobj || []
    const match = patharr.join('').match(pathdef.pathre)

    if (match && match[1]) {
      pathdef.matchedpathid = [pathdef.path, match[1]]

      gnlog_pathmatchfound(cfg, pathdef.path, match[1])
    }

    arr.push(pathdef)
    if (pathdef.pagearr && nextpatharr.length) {
      arr = arr.concat(
        gnpathdef_findmatchpatharr(cfg, nextpatharr, pathdef.pagearr))
    } else if (pathdef.pagearr) {
      arr = arr.concat(
        gnpathdef_findmatchpatharr(cfg, ['/'], pathdef.pagearr))
    }
  } else if (pathdefarr && pathdefarr.length) {
    throw gnerr_pathdefitemnotfound(cfg, pathitem, pathdef)
  }

  return arr
}

// any re must return either endslash or openslash
// and cannot return both. the re returns with endslash
// all path tokens will have an implied openslash which
// should be affixed outside this function
//
// ex,
// ('/me/too/three') => ['me/', 'too/', 'three']
// ('/me/too/three/') => ['me/', 'too/', 'three/']
// ('/' => ['/'])
const gnpathdef_tokensre = /(?![/]+)(?!$)[^/]*\/?/gi
const gnpathdef_pathstrtokens = pathstr => (
  pathstr === '/'
    ? [pathstr]
    : String(pathstr).match(gnpathdef_tokensre))

//
// call findmatchpatharr using a fullpathstr
//
const gnpathdef_findmatchingfullpathstr = (cfg, pathdef, fullpathstr) => {
  try {
    return gnpathdef_findmatchpatharr(
      cfg, gnpathdef_pathstrtokens(fullpathstr), pathdef.pagearr, [pathdef])
  } catch {
    throw gnerr_pathdeffullpathnotfound(cfg, fullpathstr, [])
  }
}

//
// return true if path tokens are found in pathdefarr
//
const gnpathdef_haspatharr = (pathdefarr, patharr) => {
  // if (!Array.isArray(patharr))
  //   throw new Error(
  //    `invalid patharr must be list of path tokens, pathharr: "${patharr}"`)

  const nextpatharr = patharr.slice(1)
  const pathtoken = gnpathdef_tokenresolve(patharr[0])
  const pattern = gnpathdef_findmatchpathstr(pathtoken, pathdefarr)

  let arr = true

  if (pattern) {
    if (pattern.pagearr && nextpatharr.length) {
      arr = gnpathdef_haspatharr(pattern.pagearr, nextpatharr)
    } else if (pattern.pagearr) {
      arr = gnpathdef_haspatharr(pattern.pagearr, '/')
    }
  } else {
    arr = false
  }

  return arr
}

const gnpathdef_haspathstr = (pathdefarr, pathstr) => (
  gnpathdef_haspatharr(pathdefarr, gnpathdef_pathstrtokens(pathstr)))

// check list of path defintions for 'permission' to render
//
// actual permissions are determined by user-defined function
// 'ispathdefpermitted'
//
// if not permitted, user-defined getdefaultpathdef is called
// app may return different default paths depending upon session,
// locale or other factors and so host-application is better
// suited return preferred default pathdefarr
//
const gnpathdef_arrpermittedis = (sess, cfg, pathdefarr) => (
  pathdefarr.every(pathdef => (
    'permission' in pathdef
      ? cfg.pagepathdefispermitted(sess, cfg, pathdef)
      : true
  )))

const gnpathdef_arrpermittedget = (sess, cfg, pathdefarr) => (
  gnpathdef_arrpermittedis(sess, cfg, pathdefarr)
    ? pathdefarr
    : cfg.pagepathdefaultget(sess, cfg))

// if 'top' routetree namespace does not have path,
// return empty page arr
const gnpathdef_pathpgarrget = (cfg, fullpath) => (
  gnpathdef_findmatchingfullpathstr(cfg, cfg.pathdef, fullpath))

const gnpathdef_prefixstrip = (path, prefixdir) => (
  (prefixdir && path && path.startsWith(prefixdir))
    ? path.replace(prefixdir, '/')
    : path)

// for details on prefixdir, see enums file
const gnpathdef_prefixresolverootspec = (pathrootspec, prefixdir) => (
  pathrootspec.replace(
    gnenumPATHROOTDEFAULT, prefixdir.endsWith('/')
      ? prefixdir.slice(0, -1) : prefixdir))

export {
  gnpathdef_createfrommanifestroutes,
  gnpathdef_createfrommanifest,
  gnpathdef_ismatchpathstr,
  gnpathdef_findmatchpathstr,
  gnpathdef_findmatchpatharr,
  gnpathdef_pathstrtokens,
  gnpathdef_findmatchingfullpathstr,
  gnpathdef_haspatharr,
  gnpathdef_haspathstr,

  gnpathdef_prefixstrip,
  gnpathdef_prefixresolverootspec,

  gnpathdef_pathnoparams,
  gnpathdef_pathisvalid,
  gnpathdef_pathpgarrget,
  gnpathdef_arrpermittedis,
  gnpathdef_arrpermittedget
}
