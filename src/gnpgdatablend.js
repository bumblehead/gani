// Filename: gnpgdatablend.js
// Timestamp: 2018.05.05-00:26:36 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import gnpgdata from './gnpgdata.js'

import {
  gnenumISDATA as ISDATA,
  gnenumISDATABLEND as ISDATABLEND
} from './gnenum.js'

const initnode = nd => nd
  .set(ISDATA, true)
  .set(ISDATABLEND, true)

const iscompositechanged = (ss, cfg, gr, nd, prev, subj) => (
  String(prev.value) !== String(subj.value))

export default Object.assign({}, gnpgdata, {
  type: 'gnpgdatablend',
  initnode,
  iscompositechanged
})

