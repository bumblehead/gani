import {
  gnlog_mozbug,
  gnlog_stringify
} from './gnlog.js'

import {
  gnenumKEY
} from './gnenum.js'

const gnerr_tryc = (fn, ...args) => {
  try {
    return fn(...args.slice(0, -1))
  } catch (e) {
    return args.slice(-1)[0](e)
  }
}

const gnerr = (cfg, ...args) => {
  // lastarg typically a string or an existing error
  const lastarg = args.slice(-1)[0]
  const errinst = lastarg instanceof Error
    ? lastarg : new Error(lastarg)

  if (cfg.islogenabled ||
      cfg.iserrorenabled) {
    console.error(...gnlog_mozbug('[!!!] gani: ', ...args))
  }

  return errinst
}

const gnerr_cfgenvinvalidorigin = () => new Error(
  `[!!!] envcfg must define an "origin"`)

const gnerr_cfginvalidsess = sess => new Error(
  "[!!!] cfg must define a valid session with locale and requrl properties,"
    + ` current session: ${gnlog_stringify(sess)}`)

const gnerr_rootnodenotfound = gr => (
  gnerr(`root node, key '/', not found: ${gnlog_stringify(gr.toJS())}`))

const gnerr_cfgreqdfnnotfound = (cfg, fnname) => (
  gnerr(cfg, `required function not defined, "${fnname}"`))

const gnerr_cfgreqdfnnotfoundpagespecfromisopath = cfg => (
  gnerr_cfgreqdfnnotfound(cfg, 'pagespecfromisopath'))

// was useful for browser troubleshooting. commented-out but possibly removable
// const gnerrnode = (graph, node, ...args) => {
//   if (typeof window === 'object') {
//     window.errgraph = graph
//     window.errnode = node
//   }
//   console.error('errgraph: ', graph.toJS())
//   console.error('errnode: ', node.toJS())
//
//   return gnerr(gnlog_getnodekey(node), ...args)
// }

const gnerrinst = (graph, node, msg) => (
  Object.assign(new Error((
    node ? node.get(gnenumKEY) + '\n' + msg : msg
  )), {
    graph: graph && graph.toJS(),
    node: node && node.toJS()
  }))

// ex refspec, { spec: user }
// ex spectypes, { user: { ... } }
const gnerrnode_childspecnotfound = (gr, nd, refspec, spectypes) => (
  gnerrinst(gr, nd, [
    `childspec not found in spectypes: ${gnlog_stringify(refspec)}`,
    `spectypes: ${gnlog_stringify(spectypes)}`
  ].join('\n')))

// one problem that may occur if a pattern defines two childs of the same
// name dfs node removal here will remove the single node, returning null
// from graph on second reference found in child arr
//
// multiple childs should never be defined on the same name. gani does
// not check for multiple occurrences of a name, and instead an external
// tool should be used to analyze and verify the this occurrence
//
const gnerr_nodedfskeynotfound = (gr, nd, key) => (
  gnerrinst(gr, nd, [
    `dfs key not found:  "${key}",`,
    '\n',
    'can occur when multiple childs share same name ',
    'and node is removed on first occurrence of name'
  ].join('')))

const gnerr_nodespecchildinvalid = (gr, nd, childspec) => (
  gnerrinst(gr, nd, (
    'invalid childspec: (must be an object w/ name): ' +
      gnlog_stringify(childspec))))

const gnerr_nodespecobjinvalid = (gr, nd, specobj, obj) => (
  gnerrinst(gr, nd, (
    `invalid specobj (must be an object): “${gnlog_stringify(obj)}”, ` +
      + `associated with spec “${gnlog_stringify(specobj)}”`
  )))

const gnerrnode_edgenodenotfound = (gr, nd, key, edgespec) => {
  const msg = [
    `edgenode not found, key “${key}”, `,
    `verify fkey or path \n“${gnlog_stringify(edgespec)}”`
  ].join('')

  return gnerrinst(gr, nd, msg)
}

const gnerrnode_keynodenotfound = (gr, nd, nodekey) => {
  const msg = `node not found, “${nodekey}”`

  return gnerrinst(gr, nd, msg)
}

const gnerrnode_parentkeynotfound = (gr, nd, spec) => {
  const msg = `parent node not found, “${gnlog_stringify(spec)}”`

  return gnerrinst(gr, nd, msg)
}

const gnerr_nodensnotfoundedge = (gr, nd, proptype, edgespec) => (
  gnerrinst(gr, nd, (
    `prop "type" not found: “${proptype}”, “${gnlog_stringify(edgespec)}”`
      + '\n\nusing a different "type" may resolve this issue')))

const gnerr_nodensnotfound = (gr, nd, namespace) => (
  gnerrinst(gr, nd, `required namespace not found: “${namespace}”`))

const gnerr_nodespecfullkeyinvalid = (gr, nd, fullsubj, spec, part, key) => (
  gnerrinst(gr, nd, [
    `invalid key: ${key},`,
    '`part.fullkey` should be found in `full`',
    '\n',
    `part: ${gnlog_stringify(part)},`,
    `full: ${gnlog_stringify(fullsubj)},`,
    `partspec: ${gnlog_stringify(spec)}`
  ].join('\n')))

const gnerr_nodechildnameinvalid = (gr, nd, ndname) => (
  gnerrinst(gr, nd, `invalid node name: "${ndname}"`))

const gnerr_nodespecnameinvalid = (gr, nd, ndspec) => (
  gnerrinst(gr, nd, (
    'node must have an unique name property: ' +
      `“${gnlog_stringify(ndspec)}”`)))

const gnerrnode_foreignnamespacenotfound = (gr, nd, nsopts) => (
  gnerrinst(gr, nd, `foreign namespace not found, ${gnlog_stringify(nsopts)}`))

const gnerrnode_pathrefnotfound = (gr, nd, pathrefprop, pathrefprops) => (
  gnerrinst(gr, nd, (
    `pathrefprop not found: “${pathrefprop}”,`
      + gnlog_stringify(pathrefprops))))

const gnerrnode_pathnotfound = (gr, nd, nodepath) => (
  gnerrinst(gr, nd, `data path not found: ${nodepath}`))

const gnerrnode_nskeyrequired = (gr, nd, props) => {
  const msg = [
    `'nskey' not found, ${gnlog_stringify(props)}`,
    '',
    'is nsprop valid?',
    `  prop: ${props.fullstr}`,
    '',
    'valid nsprop ex,',
    '  prop: [id].subj.prop'
  ].join('\n')

  return gnerrinst(gr, nd, msg)
}

const gnerrnode_nspropresolvedundefined = (
  gr, nd, ns, name, nskey, nsprop, fullstr
) => (
  gnerrinst(gr, nd, (
    'Property ":name" resolved as `undefined` from ":fullstr". '
      + 'If this was intentional, resolve `null` instead. '
      + 'Otherwise, the namespace ":nskey" or its lookup ":nsprop" is wrong. '
      + 'Current :nskey properties: :nsprops')
    .replace(/:name/, name)
    .replace(/:fullstr/, fullstr)
    .replace(/:nskey/g, nskey)
    .replace(/:nsprop/, nsprop)
    .replace(/:nsprops/, gnlog_stringify(ns, true))))

const gnerr_nodenotfound = (gr, ndkey) => (
  gnerrinst(gr, null, `node not found: ${ndkey}`))

const gnerr_nodechildnotfoundindex = (gr, nd, i) => (
  gnerrinst(gr, nd, `child key not found, index "${i}"`))

const gnerr_nodechildnotfoundvnode = (gr, nd, ndname) => (
  gnerrinst(gr, nd, (
    `childkey not found, “${ndname}” ${gnlog_stringify(nd.get('childkey'))}`)))

const gnerr_nodekeyinvalid = (cfg, gr, nd, key) => (
  gnerrinst(gr, nd, `invalid key: ${key}`))

const gnerr_nodepropselecttypeinvalid = (cfg, gr, nd, key) => (
  gnerrinst(gr, nd, `invalid selecttype: ${key}`))

const gnerr_nodeonsubjmustboolean = (gr, nd, val) => (
  gnerrinst(gr, nd, `onsubj function must return a boolean, returned "${val}"`))

const gnerr_nodepgnotfound = (gr, nd) => (
  gnerrinst(gr, nd, `page not found “${nd.get('node')}”, ${nd.get('name')}`))

const gnerr_nodecompositevalueinvalid = (gr, nd) => (
  gnerrinst(gr, nd, 'blend must have subject named-property “value”'))

const gnerr_nodeevhandlermustreturngraph = (gr, nd, eventhandler, returned) => (
  gnerrinst(gr, nd, `event handler must return graph; `
    + gnlog_stringify(eventhandler) + `;\nreturned “${returned}”`))

const gnerr_nodeevhandlermustbelist = (gr, nd, event, eventhandler) => (
  gnerrinst(gr, nd, `event handler must be defined as list, “${event}”: `
    + gnlog_stringify(eventhandler)))

const gnerr_isopatterninvalid = isopathtpl => new Error(
  `[!!!] invalid iso pattern: pattern must be object. path: ${isopathtpl}`)

const gnerr_isopathinvalid = isopathtpl => new Error(
  `[!!!] invalid iso path: must use baseLangLocale naming. path: ${isopathtpl}`)

// see `pagespecfromisopath`
// routes are defined in the manifest, for example,
// ```json
// {
//   ...manifest,
//   "routes": [ "/", "/about/", "/blog/" ]
// }
// ```
const gnerr_routesinvalid = routes => new Error(
  `[!!!] invalid routes: "${routes}", array required.`)

const gnerr_invalidpg = pg => new Error(
  `[!!!] invalid pg: ${pg}`)

const gnerr_cfgprefixdirinvalid = prefixdir => new Error(
  `[!!!] invalid prefixdir "${prefixdir}"; `
    + 'prefixdir must be string with begin-slash and end-slash chars.'
    + ' ex, "/myprefixdir/"')

const gnerr_pgreqdfnnotfound = (cfg, fnname) => (
  gnerr(cfg, `pg required function not found: "${fnname}"`))

const gnerr_pgreqdfnnotfoundvnode = cfg => (
  gnerr_pgreqdfnnotfound(cfg, 'getvnode'))

const gnerr_pathdefitemnotfound = (cfg, pathitem, pathdefarr) => (
  gnerr(cfg, (
    `pathdef not found for pathitem "${pathitem}", `
      + gnlog_stringify(pathdefarr))))

const gnerr_pathdeffullpathnotfound = (cfg, fullurl, pathdefarr) => (
  gnerr(cfg, (
    `pathdef not found for fullurl "${fullurl}", `
      + gnlog_stringify(pathdefarr))))

export {
  gnerr_tryc,
  gnerr,
  gnerr_invalidpg,

  gnerr_nodenotfound,
  gnerr_nodekeyinvalid,
  gnerr_nodensnotfoundedge,
  gnerr_nodensnotfound,
  gnerr_nodepgnotfound,
  gnerr_nodeonsubjmustboolean,
  gnerr_nodedfskeynotfound,
  gnerr_nodespecnameinvalid,
  gnerr_nodespecfullkeyinvalid,
  gnerr_nodespecobjinvalid,
  gnerr_nodespecchildinvalid,

  gnerr_nodechildnameinvalid,
  gnerr_nodechildnotfoundindex,
  gnerr_nodechildnotfoundvnode,

  gnerr_nodepropselecttypeinvalid,
  gnerr_nodecompositevalueinvalid,
  gnerr_nodeevhandlermustreturngraph,
  gnerr_nodeevhandlermustbelist,

  // gnerrnode_notfound as gnerr_nodenotfound,
  gnerr_pgreqdfnnotfound,
  gnerr_pgreqdfnnotfoundvnode,

  gnerr_rootnodenotfound,

  gnerr_cfgreqdfnnotfoundpagespecfromisopath,
  gnerr_cfgprefixdirinvalid,

  gnerrnode_childspecnotfound,

  gnerrnode_edgenodenotfound,
  gnerrnode_keynodenotfound,
  gnerrnode_parentkeynotfound,
  gnerrnode_foreignnamespacenotfound,
  gnerrnode_pathrefnotfound,

  gnerrnode_pathnotfound,
  gnerrnode_nskeyrequired,
  gnerrnode_nspropresolvedundefined,


  // newer errors added after esm refactor
  gnerr_isopatterninvalid,
  gnerr_isopathinvalid,
  gnerr_routesinvalid,

  gnerr_cfgenvinvalidorigin,
  gnerr_cfginvalidsess,

  gnerr_pathdefitemnotfound,
  gnerr_pathdeffullpathnotfound
}
