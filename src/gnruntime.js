// specmob object can be augmented w/ special type methods here
//
// JSON 'type':'mymethodname'
// here: retmymethodname : (sess, cfg, graph, node, spec, fn) => {}

import specmob from 'specmob'

import {
  gnpublish_subj,
  gnpublish_graphsubj
} from './gnpublish.js'

import {
  gnspec_nspropstrparse,
  gnspec_nspropexpand
} from './gnspec.js'

import {
  gnnode_childgetall,
  gnnode_pathresolve,
  gnnode_fromrelpath
} from './gnnode.js'

import {
  gnerr_tryc as tryc,
  gnerrnode_keynodenotfound,
  gnerrnode_parentkeynotfound,
  gnerrnode_pathnotfound,
  gnerrnode_nskeyrequired,
  gnerrnode_nspropresolvedundefined
} from './gnerr.js'

import {
  gnacc_nsmap,
  gnacc_child,
  gnacc_full
} from './gnacc.js'

import {
  gnenumSORTBY,
  gnenumSORTDIRECTION,
  gnenumSORTORDERDESC,
  gnenumSPECPROPNAMETYPE,
  gnenumSUBJ,
  gnenumKEY
} from './gnenum.js'

// detect namespace lookup strings
const gnrun_nsre = /^(child)?(node|subj|base|init|fkey|part)(\.|\b)/
const gnrun_fn = {}
const gnrun_cb = {}

gnrun_fn.getchildkeys = ([parentkey], ss, cfg, gr, nd) => {
  const nddata = gr.get(parentkey)
  if (!nddata) {
    throw gnerrnode_keynodenotfound(gr, nd, parentkey)
  }

  return nddata ? gnnode_childgetall(nddata).toArray() : []
}

// Publish subject values to all childs.
//
// ex, mark all items in a todo list as 'completed'
// ```
// d.cb('applysubj')('/datatodos', {
//   iscompleted: d.fkey('./')('value')
//     .filterin(d.fn('isnottrue')('this'))
// })
// ```
gnrun_cb.applysubjtochilds = ([key, optsns], fn, ss, cfg, gr, nd) => {
  const keys = tryc(gnrun_fn.getchildkeys, [key], ss, cfg, gr, nd, fn)
  // pushes parent to front at patch 'initiator' position
  const keyvalarr = keys.reduce((prev, key) => {
    prev.push([key, gnenumSUBJ, optsns])

    return prev
  }, [[key, gnenumSUBJ, { childlength: keys.length }]])

  gnpublish_graphsubj(ss, cfg, gr, keyvalarr, (err, gr, rep) => {
    fn(err, gr, rep)
  })
}

// const gnrun_create = (speccfg, p = {}, o) => {
const gnrun_create = (specfn, speccb, specerrfn, p = {}, o) => {
  o = Object.assign({}, p)

  //
  // "gn:type" : "childcount",
  // "name" : "val",
  // "path" : "datausers"
  //
  o.retchildcount = (ss, cfg, gr, nd, ns, spec, fn) => {
    const pnode = tryc(o.getdatanode, ss, cfg, gr, nd, spec, fn)
    const child = pnode && gnacc_child.get(pnode)
    const count = child ? child.count() : 0

    o.valordefval(ss, cfg, gr, nd, ns, spec, count, fn)
  }

  // return named-property value from data node
  //
  // "fkey" : {
  //   "blogkey" : "/path/to/ref"
  // },
  //
  // "subj" : [{
  //   "gn:type" : "nsprop",
  //   "prop" : "base.rotation",
  //   "name" : "rotation",
  // },{
  //   "gn:type" : "nsprop",
  //   "prop" : "[fkey.blogkey].subj.title",
  //   "name" : "title"
  // }]
  //
  o.retnsprop = (ss, cfg, gr, nd, ns, spec, fn) => {
    const props = gnspec_nspropexpand(spec)

    // if foreign namespace...
    if (props.fnsstr) {
      const datanode = tryc(o.getdatanode, ss, cfg, gr, nd, props, fn)
      const nskey = props.nskey

      if (!nskey) {
        // if all props are generated from nsstr, this condition
        // cannot occur, because nsstr must parse nskey
        return fn(gnerrnode_nskeyrequired(gr, nd, props))
      }

      ns = datanode ? gnacc_nsmap[nskey].get(datanode, {}) : ''
    }

    o.retobjprop(ss, cfg, gr, nd, ns, props, (e, res, gr) => {
      if (res === undefined) {
        return fn(gnerrnode_nspropresolvedundefined(
          gr, nd, ns, props.name, props.nskey, props.nsprop, props.fullstr))
      }

      fn(e, res, gr)
    })
  }

  // atypical lookup properties... requires index on top-level
  o.retfullelem = (sess, cfg, graph, node, ns, spec, fn) => {
    o.valordefval(sess, cfg, graph, node, ns, spec, (
      gnacc_full.get(node)[spec.index]
    ), fn)
  }

  o.getdatanodeopt = (ss, cfg, gr, nd, spec) => {
    const path = gnnode_pathresolve(ss, cfg, gr, nd, spec)

    return path && gnnode_fromrelpath(gr, nd, path)
  }

  o.getdatanode = (ss, cfg, gr, nd, spec) => {
    const datanode = o.getdatanodeopt(ss, cfg, gr, nd, spec)

    // should be removed and hadled at parent
    if (!datanode)
      throw gnerrnode_pathnotfound(gr, nd, spec.path)

    return datanode
  }

  o.retpublishprop = (ss, cfg, gr, nd, ns, spec, fn) => {
    const nodekey = nd.get(gnenumKEY)

    o.getopts(ss, cfg, gr, nd, ns, spec, (e, opts) => {
      if (e) return fn(e)

      gnpublish_subj(ss, cfg, nodekey, opts, (e, gr) => {
        if (e) return fn(e)

        fn(null, gr, gr.get(nodekey))
      })
    })
  }

  o.retdatafirstchildkey = (ss, cfg, gr, nd, ns, spec, fn) => {
    const datanode = tryc(o.getdatanode, ss, cfg, gr, nd, spec, fn)
    if (!datanode)
      return fn(gnerrnode_pathnotfound(gr, nd, spec.path))

    o.valordefval(ss, cfg, gr, nd, ns, spec, (
      gnnode_childgetall(datanode).get(0)
    ), fn)
  }

  // {
  //   "gn:type": "childkeys",
  //   args: ["/datausers"],
  //   options: {
  //     sortBy: 'subj.date',
  //     sortOrder: 'asc'
  //   }
  // }
  o.retchildkeys = (ss, cfg, gr, nd, ns, spec, fn) => {
    const specargs = spec.args || []
    const specopts = spec.options || {}
    const parentkey = specargs[0]
    const parentnode = gr.get(parentkey)
    const sortpropname = specopts[gnenumSORTBY]
    const sortdir = specopts[gnenumSORTDIRECTION]
    const sortorder = sortdir === gnenumSORTORDERDESC ? 1 : -1

    if (!parentkey)
      return fn(gnerrnode_parentkeynotfound(gr, nd, spec))

    if (!parentnode)
      return fn(gnerrnode_keynodenotfound(gr, nd, parentkey))

    let keys = gnnode_childgetall(parentnode).toArray()

    if (sortpropname) { // sort
      const propstrparsed = gnspec_nspropstrparse(sortpropname)
      const nskey = propstrparsed.nskey
      const nsprop = propstrparsed.nsprop
      const getprop = gnacc_nsmap[nskey].getprop
      const nodeprop = key => getprop(gr.get(key), nsprop)

      keys = keys.sort((keya, keyb) => (
        nodeprop(keya) > nodeprop(keyb) ? sortorder : -sortorder))
    }

    fn(null, keys, gr)
  }

  return specmob({
    speccb: Object.assign(gnrun_cb, speccb),
    specfn: Object.assign(gnrun_fn, specfn),
    typeprop: gnenumSPECPROPNAMETYPE,
    specerrfn,
    nsre: gnrun_nsre
  }, o)
}

export {
  gnrun_create as default,
  gnrun_nsre,
  gnrun_fn,
  gnrun_cb
}
