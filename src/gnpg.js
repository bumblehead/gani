// Filename: gani_ui.js
// Timestamp: 2018.12.10-09:23:10 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>
//
// basic methods for all ui nodes. use as base and redefine functions as needed

import {
  gnacc_fkey,
  gnacc_init,
  gnacc_full,
  gnacc_subj,
  gnacc_groupaccessor
} from './gnacc.js'

import {
  gnenumKEY as KEY
} from './gnenum.js'

import {
  gnrun_retobj,
  gnrun_retopt
} from './gnrun.js'

import {
  gnerr_nodeevhandlermustbelist,
  gnerr_nodeevhandlermustreturngraph
} from './gnerr.js'

import {
  sphgraph_is as graph_is
} from 'spectraph/src/sphgraph.js'

import {
  gnpublish_subj
} from './gnpublish.js'

const optfn = () => {}

// assumes that keyOrNode is a key or a node
// allows default runtime error to occur if node lookups fail
const gnpg_keygetfrompolymorph = keyOrNode => (
  typeof keyOrNode === 'string'
    ? keyOrNode
    : keyOrNode.get(KEY))

const getsubj = (nd, def = {}) => (
  gnacc_subj.get(nd, def))

const initnode = node => node

const resetnode = node => node

// contentrm(sess, cfg, graph, node)
const oncontentrm = () => null

// onattach(sess, cfg, graph, node, pg)
const onattach = () => null

const onrmchild = (sess, cfg, graph, node, cnode) => (
  [graph, node, cnode])

const onchildadd = (sess, cfg, graph, node, cnode) => (
  [graph, node, cnode])

// partial inheritor should define own method here
const hydratepartial = (ss, cfg, gr, nd, pg, fn) => (
  fn(null, gr, nd))

// preprocessing data
const onprerender = (ss, cfg, gr, nd, fn) => (
  fn(null, gr, nd))

const getbaseadapted = (ss, cfg, gr, nd, pg, fn) => (
  fn(null, gr, nd))

const prevset = (cfg, nd, subj) => (
  cfg.prev[nd.get(KEY)] = Object.assign({}, subj))

const prevget = (cfg, nd) => (
  cfg.prev[nd.get(KEY)] ||
    prevset(cfg, nd, getsubj(nd, {})))

const prevassign = (cfg, nd, subj) => (
  Object.assign(prevget(cfg, nd), subj))

const evonframe = (ss, cfg, gr, nd, pg) => pg.onframe && (
  pg.onframe(ss, cfg, gr, nd, pg, pg.prevget(cfg, nd)))

const onev = (sess, cfg, graph, node, evpattern, fn) => (
  evpattern && gnrun_retobj(
    sess, cfg, graph, node,
    gnacc_init.get(node, {}), evpattern, fn))

const evActionSpecsApply = (ss, cfg, gr, nd, pg, ns, actionspecs, fn) => {
  if (actionspecs.length <= 0)
    return fn(null, cfg.graph = gr) // persist graph

  const actionspec = actionspecs[0]
  const evactionfn = Array.isArray(actionspec)
    ? gnrun_retobj
    : gnrun_retopt

  evactionfn(ss, cfg, gr, nd, Object.assign(gnacc_groupaccessor([
    gnacc_fkey,
    gnacc_init,
    gnacc_full,
    gnacc_subj
  ])(nd), ns), actionspec, (err, grnew) => {
    if (err) return fn(err)

    if (!graph_is(grnew)) {
      // wip if event mutates graph, graph must be returned
      // this allows progressive graph updates using sequences of events
      // BUT it becomes confusing if some functions return any 'value'
      // but other functions return graph.
      //
      // maybe async specfn could return [value, graph] and always return both?
      //
      // IDEA IFF its a two-item list AND IFF
      // first item is graph, then it is multi-value return
      // cannot image a non-multi return situation that would
      // return array with graph as first item
      throw gnerr_nodeevhandlermustreturngraph(gr, nd, actionspec, grnew)
    }

    evActionSpecsApply(ss, cfg, grnew, nd, pg, ns, actionspecs.slice(1), fn)
  })
}

// call evPublishPOST() after action changes published from node
// an evPublishPRE() wold be called before changes published
//
// when a node publishes changes which remove that same node from graph,
// actionspecs will not be applied for that node
//
// for example, if checking a todo-list-item removes that item
// from todolist, changes specified at POST are not applied
const evPublishPOST = (ss, cfg, gr, nd, pg, actionstr, ns = {}, fn = optfn) => {
  nd = gr.get(gnpg_keygetfrompolymorph(nd))

  // node does not exist in graph
  if (!nd) return fn(null, gr)

  const onaction = nd.get(actionstr)
  if (!onaction)
    return fn(null)

  if (!Array.isArray(onaction))
    throw gnerr_nodeevhandlermustbelist(gr, nd, actionstr, onaction)

  evActionSpecsApply(ss, cfg, gr, nd, pg, ns, onaction, fn)
}

const publish = (ss, cfg, nd, subj, fn = optfn) => {
  const ndkey = gnpg_keygetfrompolymorph(nd)

  gnpublish_subj(ss, cfg, ndkey, subj, fn)
}

export default {
  type: 'gnpg',
  gnpg_keygetfrompolymorph,
  getsubj,
  initnode,
  resetnode,
  oncontentrm,
  onattach,
  onrmchild,
  onchildadd,
  hydratepartial,
  onprerender,
  getbaseadapted,
  prevset,
  prevget,
  prevassign,
  evonframe,
  onev,
  evActionSpecsApply,
  evPublishPOST,
  publish
}
