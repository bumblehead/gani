// "init" sequence here should work in both browser and server
//
// run0
//   * gninit_run0_0_seedenv
//     * create cfg composing browser|server env hooks with global details
//   * gninit_run0_1_loadmanifest
//     * load manifest using seeded browser or server cfg details
//     * create route details from manifest and cfg
// run1
//   * gninit_run1_2_seedsess
//     * define cfg and sess details for specific requests: token, url etc
//   * gninit_run1_3_probelocale
//     * probe locale from request url or sess
//
//     * give user a chance to define their own...
//     * if _still_ no locale, then use gani's probing
//     * probe locale or (browser|servreq)env details
//     * should this be defined at gani or client?

import gncfg from './gncfg.js'

import {
  gnpathdef_createfrommanifest
} from './gnpathdef.js'

import {
  gnsess_create,
  gnsess_isvalidsess
} from './gnsess.js'

import {
  gnenumISOLOCALEPATHRe,
  gnenumISOLOCALEIDREDIRECT,
  gnenumPATHROOTSPECSMANIFEST
} from './gnenum.js'

import {
  gnerr_cfginvalidsess
} from './gnerr.js'

const gninit_run0_0_seedenv = (cfgglobal, cfgenv) => (
  gncfg(cfgglobal, cfgenv))

// get manfest and unpack route specs generating path rules/permissions
const gninit_run0_1_loadmanifest = async cfg => {
  cfg.manifest = await cfg.pagespecfromisopath(
    {}, cfg, gnenumPATHROOTSPECSMANIFEST)
  cfg.pathdef = gnpathdef_createfrommanifest(cfg.manifest)

  return cfg
}

const gninit_run1_2_seedsess = async (cfg, sessseed) => (
  gnsess_create({
    locale: cfg.defaultLocale,
    requrl: sessseed.requrl
  }))

// return with locale from url  ex,
//
// ({ manifest: { locales: 'eng-US' } }, '/home/') => null
// ({ manifest: { locales: 'eng-US' } }, '/eng-US/home/') => 'eng-US'
// ({ manifest: { locales: 'eng-US' } }, '/jap-JA/home/') => 'jap-JA:REDIRECT'
const gninit_run1_3_probelocaleurl = (ss, cfg) => {
  const url = ss.requrl
  const urllocale = (url.match(gnenumISOLOCALEPATHRe) || [])[1]
  if (!urllocale)
    return null

  const manifestlocales = cfg.manifest.locales // comma-separated list
  const manifestlocalefirst = manifestlocales.split(',')[0]
  return manifestlocales.includes(urllocale)
    ? urllocale
    : manifestlocalefirst + gnenumISOLOCALEIDREDIRECT
}

// to get the default locale,
//  * IF url contains locale-specific path, eg /eng-US/path/
//    * parse locale from url
//    * IF locale is supported
//      * RETURN locale
//    * ELSE
//      * ERROR or REDIRECT to supported locale
//  * ELIF session contains locale
//    * IF locale is supported
//      * IF locale is manifest default
//        * RETURN locale
//      * ELSE
//        * REDIRECT to locale-specific path
//    * ELSE
//      * ERROR or REDIRECT to supported locale
//
// result is passed to probe-hook
//  * IF locale was found return it
//  * ELSE
//    * parse best locale from manifest matching navigator or request header
//    * IF locale found
//      * IF locale is manifest default
//        * RETURN locale
//      * ELSE
//        * REDIRECT to locale-specific path
//    * ELSE
//      * RETURN first manifest locale as default
const gninit_run1_3_probelocale = (ss, cfg) => {
  const localeurl = gninit_run1_3_probelocaleurl(ss, cfg)
  if (localeurl)
    return localeurl

  // for now, throw error and resolve at that time error first-occurs
  throw new Error('hey now') // return 'eng'
}

const gninit_run0 = async (cfgglobal, cfgenv) => (
  gninit_run0_1_loadmanifest(
    gninit_run0_0_seedenv(cfgglobal, cfgenv)))

const gninit_run1 = async (cfg, seed, ctxOrWin, ss) => {
  if (seed.requrl === undefined) {
    throw new Error('requrl must be defined at seed')
  }

  ss = await gninit_run1_2_seedsess(cfg, {
    requrl: seed.requrl,
    token: seed.token // could be a cookie or session value
  })

  if (!(ss && ss.requrl))
    throw gnerr_cfginvalidsess(ss)

  ss.locale = gninit_run1_3_probelocaleurl(ss, cfg)
  // calls gani-defined probing
    || cfg.probelocaleenv(ss, cfg, ctxOrWin)

  // calls user-defined probing
  ss = await cfg.initlocale(ss, cfg, ctxOrWin)

  ss = await cfg.initsess(ss, cfg, ctxOrWin)
  cfg = await cfg.initcfg(cfg, ss, ctxOrWin)

  if (!gnsess_isvalidsess(ss))
    throw gnerr_cfginvalidsess(ss)

  return Object.assign({ ss }, cfg)
}


export {
  gninit_run0_0_seedenv,
  gninit_run0_1_loadmanifest,
  gninit_run1_2_seedsess,
  gninit_run1_3_probelocale,
  gninit_run0,
  gninit_run1
}
