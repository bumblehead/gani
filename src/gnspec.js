// adapt and construct new patterns

import nsstr from 'nsstr'

import gnacc from './gnacc.js'

import {
  gnenumSPECPROPNAMETYPE,
  gnenumTYPE,
  gnenumFKEYBASEELEM,
  gnenumSPECPROPTYPELOCALREF,
  gnenumFKEY as FKEY,
  gnenumNAMEKEY as NAMEKEY,
  gnenumISPATHNODE as ISPATHNODE,
  gnenumDATAKEYPROP as DATAKEYPROP,
  gnenumMAPSRCKEY as MAPSRCKEY,
  gnenumUNSAFE as UNSAFE,
  gnenumPATHROOTSPECS
} from './gnenum.js'

import {
  gnerr_nodespecchildinvalid,
  gnerr_nodespecobjinvalid
} from './gnerr.js'

import {
  gnrun_getfn,
  gnrun_getargs
} from './gnrun.js'

import {
  gniso_patternfrompath
} from './gniso.js'

const gnspec_nameisvalid = (key, type) => (
  type = typeof key,
  type === 'number' ||
    (type === 'string' && key.length > 0))

const gnspec_keyisvalid = (key, type) => (
  type = typeof key,
  type === 'number' ||
    (type === 'string' && key.length > 0))

// pathrule's are incomplete patterns with reference to more-complete
// patterns that can be resolved
const gnspec_traitispathrule = spec => (
  spec[ISPATHNODE])

const gnspec_atomisvalid = spec => Boolean(
  typeof spec === 'object' && spec &&
    typeof spec.name === 'string' && spec.name)

const gnspec_traitisvalid = spec => Boolean(
  typeof spec === 'object' && spec !== null)

const gnspec_atomisdynamic = spec => (
  spec[gnenumSPECPROPNAMETYPE] && !spec.spread)

// deserialise the nspropstr to definitions on named-properties
const gnspec_nspropstrparse = (nspropstr, parsed) => (
  parsed = nsstr(nspropstr),
  parsed.path = parsed.fnspath,
  parsed.prop = parsed.fnsstr ? parsed.nsprop : parsed.nsstr,
  parsed)

// expands the below forms by merging the parsed
// prop values results with the given opts
//
// "gn:type" : "nsprop",
// "prop" : "base.rotation",
//
// "gn:type" : "nsprop",
// "prop" : "[fkey.blogkey].subj.title",
//
const gnspec_nspropexpand = opts => {
  if (opts.isexpanded)
    return opts

  return Object.assign({
    isexpanded: true
  }, opts, opts.prop ? gnspec_nspropstrparse(opts.prop) : {})
}

const gnspec_nspropexpandall = optsarr => {
  if (!optsarr.length || optsarr[0].isexpanded)
    return optsarr

  return optsarr.map(gnspec_nspropexpand)
}

// const gnspec_getrefspecchild = refspec => Object.assign({
const gnspec_childrefcreate = refspec => Object.assign({
  [NAMEKEY]: refspec[NAMEKEY]
}, refspec.spec, {
  name: String(refspec.name || refspec.spec.name)
})

const gnspec_childrefget = (meta, index) => (
  meta[NAMEKEY]
    ? gnspec_childrefcreate(meta, index)
    : meta.spec || meta)

// returns a unique name from a unique value on the data
const gnspec_childprenamefromnode = (ss, cfg, gr, nd, spec, fkey, obj, i) => {
  const keyprop = spec[DATAKEYPROP]
  let name

  if (gnspec_keyisvalid(keyprop)) {
    name = gnspec_nameisvalid(obj[keyprop])
      && String(obj[keyprop])
  } else if (gnspec_traitisvalid(keyprop)) {
    // keyprop is a pattern, resolve
    name = gnrun_getfn(cfg, keyprop.fnname)(
      [...gnrun_getargs(ss, cfg, gr, nd, keyprop, { base: obj }), fkey],
      ss, cfg, gr, nd)
  }

  // fallback to dangerous, using index in name...
  return name || `${UNSAFE + i}-${spec.name}`
}

// Observe... separate node hierarchies may generate 'safe' names
// which collide later when joined at separate node hierarchies with
// other 'safe' names that resolve to same value.
//
// if problem surfaces and requires solution, this function should be updated
//
const gnspec_childnamecreate = (spec, key, i) => {
  const specname = spec.name
  const keybasenamei = key.lastIndexOf('/')
  const keybasename = keybasenamei > -1 ? key.slice(keybasenamei + 1) : key
  const specnamekey = specname + '_' + keybasename
  // dangerous, using index in name...
  return specname.startsWith(UNSAFE)
    ? `${UNSAFE + i}_${specnamekey}`
    : specnamekey
}

// Summary of fkey+datakey
//
// returns "fkey" object (not "fkeyhydrated") for the childpre spec
//
// `fkey` uses a necessarily complex behaviour with `baselist` datakey.
// A node with 'baselist' data generates child nodes by mapping baselist
// elements (datakey) to template child specs.
//
// Simplified description of what happens,
// ```
// childspecs = baselist.map(datakey => specwithdata(childspec, datakey))
// ```
//
// One eventually wants to link nodes with data. This is difficult because data
// are variable, eg
// ```
// { name: 'email1', node: 'uitext', subj: { value: '[/datausers/172].email' } }
// { name: 'email2', node: 'uitext', subj: { value: '[/datausers/837].email' } }
// ```
//
// Using 'fkey' a friendly-named reference, such as 'datauser', can be used
// inside the node and deeply inside any child nodes, as they inherit the
// 'fkey' (eg, iow fkey is scoped to the subtree)
// ```
// {
//   name: 'email1',
//   node: 'uitext',
//   fkey: { datauser: '/datausers/172' },
//   subj: { value: '[fkey.datauser].email' }
// }
// ```
//
// Programmatically building fkey values has challenges,
//  * App-author should control name of property, here 'datauser'
//    * should be simple enough to be hand-applied in literal JSON
//    * should allow an abstraction so App-tooling can guarantee uniqueness
//  * spec should not embed implied-dependencies on base data
//  * fkey resolution should be local w/ only datakey and no hard parent link
//    * `resolve(childspec, datakey)`
//  * fkey details should be self-contained, relative, for loose node coupling
//  * fkey definition-sequence must not overwrite parent fkeys
//  * resolution sequence should be O(n) and avoid looping
//    * looping okay in first iteration, but should be possible to factor out
//
// --------------------------------------------------------
// Here's how it works currently, in tandem with spec tooling
//
// Two supported scenarious:
//   1. variable or query-generated key,
//   2. human-generated stringy key
//
//
// 1. query for fkey
//    ```
//    { fkey: d.nsobject(
//        dataTodoKeyQuery, '[./].baselem',
//        'dataTodoKeyStringy', '[./].baselem') }
//    ```
//
//    query resolves fkey, auto-generating '[./].baseelem' for baselist child
//    ```
//    { fkey: {
//        '[./].baseelem': ['datanodeVariable', 'datanodeStringy'],
//        datatodoVariable: 'baseelem4827',
//        dataTodoKeyStringy: '[./].baselem'
//    } }
//    ```
//
//    note:
//     * dataTodoKeyQuery resolves 'baselem-4827' for every usage in subtree,
//     * tooling always auto-defines fkey '[./].baseelem' for any baselist child
//     * `[./].baseelem` gives lookup for properties that reference baselem
//
//    client resolves fkey to this, `resolve(fkeynamespace, datakey)`,
//    ```
//    { fkey: {
//      '[./].baseelem': null,
//      baseelem4827: '/datatodos/1729362148779',
//      datatodoVariable: '/datatodos/1729362148779',
//      dataTodoKeyStringy: '/datatodos/1729362148779'
//    } }
//    ```
//
// DONE:
//   * '[./].baseelem' hydrates as `null` to mute inheritance,
//   * 'baseelem4827' can be auto-created for child, if `dataTodoKeyQuery` used,
//   * query tool ensures dataTodoKeyQuery resolves "baselem-4827" for subtree,
//   * childs can resolve fkey "baselem-4827",
//   * foreign-nodes can resolve fkey
const gnspec_childprefkeyfromdatakey = (fkey, datakey) => {
  if (typeof datakey !== 'string')
    throw new Error('datakey must be a string')

  if (!fkey)
    return {}

  if (gnenumFKEYBASEELEM in fkey === false
    || fkey[gnenumFKEYBASEELEM] === null
  ) {
    return fkey
  }

  return fkey[gnenumFKEYBASEELEM].reduce((fkeyhydrated, baseelemrefkey) => {
    fkeyhydrated[baseelemrefkey] = datakey
    if (fkey[baseelemrefkey] !== gnenumFKEYBASEELEM) {
      fkeyhydrated[fkey[baseelemrefkey]] = datakey
    }

    return fkeyhydrated
  }, {
    [gnenumFKEYBASEELEM]: null
  })
}

// must not mutate original values
//
// `fkeyhydrated` is used (rather than `fkey`) so that keys mapped
// from parent are available for `fkey` definitions
const gnspec_childprefromspecobj = (ss, cfg, gr, nd, spec, fkey, obj, i) => {
  if (!gnspec_atomisvalid(spec))
    throw gnerr_nodespecchildinvalid(gr, nd, spec)
  if (!gnspec_traitisvalid(obj))
    throw gnerr_nodespecobjinvalid(gr, nd, spec, obj)

  const init = Array.isArray(spec.init) ? spec.init.slice() : []
  const subj = Array.isArray(spec.subj) ? spec.subj.slice() : []
  const name = gnspec_childprenamefromnode(
    ss, cfg, gr, nd, spec, fkey, obj, i)

  // if non-dynamic type exists, assume it is found at front of list always
  // `{ spread: true }` is legacy-thing for specmob, indicating non-dynamic vals
  const subjfull = (subj.length && gnenumTYPE in subj[0] === false)
    ? [Object.assign({ spread: true }, obj, subj[0])].concat(subj.slice(1))
    : [Object.assign({ spread: true }, obj)].concat(subj)

  return Object.assign({}, spec, {
    basehydrated: obj,
    name,
    init,
    subj: subjfull
  })
}

// for a list of layers... each layer has unique id (key) defined by parent
//
// 'mapsrckey' associated source data node w/ layer pattern here,
// used when selection or other state obtained from external data node
const gnspec_childprefromspeckey = (ss, cfg, gr, ndp, spec, fkey, dkey, i) => (
  // `fkeyhydrated` is used (rather than `fkey`) so that keys mapped
  // from parent are available for `fkey` definitions
  Object.assign({ [MAPSRCKEY]: dkey }, spec, {
    name: gnspec_childnamecreate(spec, dkey, i),
    // TODO handle array fkey, legacy variant like this:
    // ```
    // (!Array.isArray(spec.fkey) && spec.fkey) || {}, { [fkey]: key })
    // ```
    fkeyhydrated: gnspec_childprefkeyfromdatakey(fkey, dkey)
  })
)

const gnspec_childprefromspec = (ss, cfg, gr, nd, data, i, specref, spec) => (
  typeof data === 'string'
    ? gnspec_childprefromspeckey(ss, cfg, gr, nd, spec, specref.fkey, data, i)
    : gnspec_childprefromspecobj(ss, cfg, gr, nd, spec, specref.fkey, data, i))

const gnspec_datakeyset = (spec, key, datakey, dataval) => (
  spec[key] = spec[key] || {},
  spec[key] = Array.isArray(spec[key])
    ? (spec[key].push({ datakey: dataval }), spec[key])
    : (spec[key][datakey] = dataval, spec[key]),
  spec)

const gnspec_datakeyhydratedset = (spec, key, datakey, dataval) => (
  gnspec_datakeyset(spec, gnacc[key].name, datakey, dataval))

const gnspec_datakeysetdeep = (spec, key, datakey, dataval) => (
  spec = gnspec_datakeyset(spec, key, datakey, dataval),
  spec = gnspec_datakeyhydratedset(spec, key, datakey, dataval),
  spec)

const gnspec_nsspecreduce = (nsspec, subj, acc = [], accumfn, fn) => {
  const proplen = Object.keys(subj || {}).length
  const nsspecarr = Array.isArray(nsspec) ? nsspec : [nsspec]

  return (function next (nsspecarr, acc) {
    nsspec = nsspecarr[0]

    if (!nsspec || proplen === acc.length)
      return fn(null, acc)

    const specnames = gnspec_atomisdynamic(nsspec)
      ? [nsspec.name]
      : Object.keys(nsspec)

    if (gnspec_atomisdynamic(nsspec) && nsspec['edgein:disable'])
      return next(nsspecarr.slice(1), acc)

    return (function reduce (acc, subj, nsspec, specnames) {
      const specname = specnames[0]
      if (!specname)
        return next(nsspecarr.slice(1), acc)

      accumfn(acc, subj, nsspec, specname, (e, acc) => {
        if (e) return fn(e)

        reduce(acc, subj, nsspec, specnames.slice(1))
      })
    })(acc, subj, nsspec, specnames)
  }(nsspecarr, acc))
}


// 'prenode' is the early node stage
//
//  * stage 1: 'paths', a path, such as '/main'
//
//  * stage 2: 'pathdefs', a path definition that includes the path and other
//             information, such as rules or permissions which might
//             associate with the path, available child paths,
//             routing-details etc.
//
//  * stage 3: 'prenodes', completed patterns that define nodes,
//             originating from path definitions
//
//  * stage 4: 'nodes', completed and constructed from prenode-patterns
//
//
// perhaps the steps could be shown as below,
//
//   path => pathdef(s) => prenodes => nodes => vnodes
//
//
// ---------------------------------------------------------------------------
//
// these functions have been difficult to organise for many reason...
// names such as 'node', 'pattern' and 'path' already have specific meanings
// within gani and usage of those words here is confusing. incredible how
// labelling becomes difficult
//
// the relationhip these functions have with configuration and node files
// tends to result in circular dependencies to those files
//
// writing descriptive names for these functions is tough, for example, at time
// of this writing, gani_cfg includes functions with these signatures (yikes)

// if given pattern references an external pattern, handles the
// condition of obtaining the final pattern
const gnspec_pathseedcreate = async (ss, cfg, pathrule) => {
  const pathpattern = gnenumPATHROOTSPECS + pathrule.pathpattern

  // condition not used by web site, no test hitting this. to be removed?
  if (!pathpattern)
    return pathrule

  const prenodepattern = await gniso_patternfrompath(ss, cfg, pathpattern)
  return Object.assign(prenodepattern, {
    [ISPATHNODE]: true
  })
}

// recursively construct pattern nodes from path nodes
// until empty path node list
//
// maybe the entire list should be returned.... callers wanting the primary
// spec could find it at the head of the list.
//
// async-style callback patterns to be async accessed
const gnspec_pathseedstackcreate = async (ss, cfg, rules, obj, parent, arr) => {
  if (!rules[0])
    return arr || []

  const prenodespec = await gnspec_pathseedcreate(ss, cfg, rules[0])

  let spec = Object.assign({}, prenodespec)

  if (rules[0].matchedpathid) {
    let [matchkey, matchval] = rules[0].matchedpathid

    spec = gnspec_datakeysetdeep(spec, FKEY, matchkey, matchval)
  }

  if (parent) {
    parent.child = (parent.child || [])
      // only one path may exist. replaces any existing 'pathnode'
      // with _this_ path node
      .map(ospec => gnspec_traitispathrule(ospec) ? spec : ospec)
  } else {
    obj = spec
  }

  arr = arr || []
  arr.push(spec)

  return gnspec_pathseedstackcreate(
    ss, cfg, rules.slice(1), obj, spec, arr)
}

const gnspec_traitisexternal = spec => (
  spec && spec[gnenumSPECPROPNAMETYPE] === gnenumSPECPROPTYPELOCALREF)

// return full trait from local-ref,
// 'trait' to distinguish from other resolvable-types
//
// ex,
// ```
// {
//   'gn:type': "local-ref",
//   path: "../root-data-errors/eng-US.json"
// }
// ```
const gnspec_traitresolve = async (ss, cfg, spec) => (
  cfg.pagespecfromisopath(
    ss, cfg, spec.path.startsWith('../')
      ? gnenumPATHROOTSPECS + spec.path.slice(3)
      : spec.path))

const gnspec_traitlistresolve = (ss, cfg, specs, fn) => (
  (async function nextspec (specs, specsresolved) {
    if (!specs.length)
      return fn(null, specsresolved)

    if (gnspec_traitisexternal(specs[0]))
      specsresolved.push(await gnspec_traitresolve(ss, cfg, specs[0]))

    nextspec(specs.slice(1), specsresolved)
  }(specs, []))
)

const gnspec_pathseedanycreate = async (sess, cfg, pathdefarr) => {
  // this should be simplified/improved with more tests
  // maybe the empty-list condition does not need to be supported
  return pathdefarr.length
    ? await gnspec_pathseedstackcreate(sess, cfg, pathdefarr)
    : [await gnspec_pathseedcreate(sess, cfg, { pathpattern: 'root' })]
}

export {
  gnspec_keyisvalid,
  gnspec_traitisvalid,
  gnspec_traitisexternal,
  gnspec_traitispathrule,
  gnspec_traitresolve,
  gnspec_traitlistresolve,
  gnspec_atomisvalid,
  gnspec_atomisdynamic,
  gnspec_nsspecreduce,

  gnspec_nspropstrparse,
  gnspec_nspropexpand,
  gnspec_nspropexpandall,

  gnspec_childrefcreate,
  gnspec_childrefget,
  gnspec_childnamecreate,

  gnspec_childprenamefromnode,
  gnspec_childprefromspecobj,
  gnspec_childprefromspeckey,
  gnspec_childprefromspec,
  gnspec_childprefkeyfromdatakey,

  gnspec_datakeyset,
  gnspec_datakeyhydratedset,

  gnspec_datakeysetdeep,

  gnspec_pathseedcreate,
  gnspec_pathseedstackcreate,
  gnspec_pathseedanycreate
}
