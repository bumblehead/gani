// Filename: uiobjls.js
// Timestamp: 2018.12.09-00:26:54 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>
//
// a multi-value uinode that manages node-lists rather
// than lists of data. reuses 'partial' pipeline used by
// uidropdown and uinav. allows for selection of and return
// of values from list of nodes.
//
// example node list /content/video
//                   /content/ads1
//                   /content/ads2
//

import gnpguipart from '../gnpguipart.js'

import {
  gndom_evidkeyarrget,
  gndom_elemclasstoggle
} from '../gndom.js'

// returns the 'full' value referenced by part patterns
//
const getfullelemdata = (ss, cfg, gr, nd, fullarr, baselist, x, subj) => (
  Object.assign({}, subj, {
    datakey: baselist[x],
    uikey: fullarr[x]
  }))

const getvnode = (ss, cfg, gr, nd, pg, subj) => {
  const { div, button, ul, li, a, span } = cfg.h(pg, nd)
  const labelprimary = subj.labelprimary
  const islistitemlink = subj.islistitemlink
  const target = islistitemlink === true ? a : button

  return (
    div('.:ui', [
      labelprimary &&
        span('.:ui-label-primary', labelprimary),

      ul('.:ui-list', (
        pg.mapparts(nd, true, ({ dockey, partkey, uikey, isactive }) => (
          li('.:ui-list-item', [
            target(`.:ui-list-item-target.st-isactive-${isactive}`, {
              id: dockey,
              href: `#${partkey}`
            }, [pg.getkeyvnode(ss, cfg, gr, uikey)])
          ])
        ))
      ))
    ])
  )
}

// returns the given number or '0'
const getasminselectnum = num => (
  Number(typeof num === 'number' && num))

const setactive = (ss, cfg, gr, nd, pg, { value }) => (
  pg.mapparts(nd, true, ({ dockey, partkey }) => gndom_elemclasstoggle(
    document.getElementById(dockey), 'st-isactive', partkey === value)))

const getpartkeysasretarr = (ss, cfg, gr, nd, keyarr, fn) => (
  fn(null, keyarr, gr))

const onclick = (ss, cfg, gr, nd, pg, prev, subj, ev) => {
  const keyarr = gndom_evidkeyarrget(cfg, ev)
  const key = keyarr[0]
  const ckey = keyarr[1]
  const minnum = pg.getasminselectnum(subj.minselectnum)

  // always searches for this on partkey
  if (ckey) {
    ev.preventDefault()

    pg.getpartkeyasdata(ss, cfg, gr, nd, pg, ckey, (e, gr, value) => {
      if (!minnum || minnum <= value.length) {
        pg.setactive(ss, cfg, gr, nd, pg, { value })
        pg.publish(ss, cfg, key, { value })
      }
    })
  }
}

export default Object.assign({}, gnpguipart, {
  type: 'uiobjls',
  getfullelemdata,
  getasminselectnum,
  setactive,
  getpartkeysasretarr,
  getvnode,
  onclick
})


