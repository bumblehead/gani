// Filename: uiobj.js
// Timestamp: 2017.12.06-17:24:47 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>
//
// consider including functionality here among all nodes
//
// would likely require usage of local 'touch' property, which would
// subscribe to multiple+ external subjects (incoming edges only)
//
// node pattern not supported yet
//

import gnpgui from '../gnpgui.js'

const itemgetvnode = (ss, cfg, gr, nd, pg, subj) => {
  const { div, span } = cfg.h(pg, nd)
  const {
    labelprimary,
    labelsecondary
  } = subj

  return (
    div('.:ui', [
      labelprimary &&
        span('.:ui-label-primary', labelprimary),
      labelsecondary &&
        span('.:ui-label-primary', labelsecondary),

      pg.getchildvnodearr(ss, cfg, gr, nd, vnode => (
        div('.:ui-content', [vnode])
      ))
    ])
  )
}

const getvnode = (ss, cfg, gr, nd, pg, subj) => {
  const { div, span } = cfg.h(pg, nd)
  const { labelprimary } = subj

  return (
    div('.:ui', [
      labelprimary &&
        span('.:ui-label-primary', labelprimary),

      pg.getchildvnodearr(ss, cfg, gr, nd, vnode => (
        div('.:ui-content', vnode)
      ))
    ])
  )
}

export default Object.assign({}, gnpgui, {
  type: 'uiobjcontent',
  getvnode,
  item: Object.assign({}, gnpgui, {
    type: 'uiobjcontentitem',
    getvnode: itemgetvnode
  })
})

