// Filename: uisubmit.js
// Timestamp: 2018.05.05-00:31:44 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import gnpgui from '../gnpgui.js'

import {
  gndom_nodeelemid
} from './gndom.js'

import {
  gnenumEVTYPEONCLICK as ONCLICK
} from '../gnenum.js'

const getvnode = (ss, cfg, gr, nd, pg, subj) => {
  const { label, span, input } = cfg.h(pg, nd)
  const {
    labelprimary,
    labelsecondary,
    labeltertiary,
    value
  } = subj

  return (
    label('.:ui', {
      for: gndom_nodeelemid(nd)
    }, [
      labelsecondary &&
        span('.:ui-label-secondary', labelsecondary),
      labeltertiary &&
        span('.:ui-label-tertiary', labeltertiary),

      input('.:ui-button#:key', {
        type: 'submit',
        name: nd.get('name'),
        value: value || labelprimary
      })
    ])
  )
}

const onclick = (ss, cfg, gr, nd, pg, prev, subj, ev) => {
  if (nd.has(ONCLICK)) {
    ev.preventDefault()
    pg.evPublishPOST(ss, cfg, gr, nd, pg, ONCLICK)
  }
}

export default Object.assign({}, gnpgui, {
  type: 'uisubmit',
  getvnode,
  onclick
})
