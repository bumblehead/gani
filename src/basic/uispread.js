// Filename: uispread.js
// Timestamp: 2018.01.20-02:25:30 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import gnpgui from '../gnpgui.js'

const getvnode = (ss, cfg, gr, nd, pg) => {
  const { div } = cfg.h(pg, nd)

  return (
    div('.:ui#:key', [
      pg.getchildvnodearr(ss, cfg, gr, nd)
    ])
  )
}

export default Object.assign({}, gnpgui, {
  type: 'uispread',
  getvnode
})
