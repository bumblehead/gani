// Filename: uilabel.js
// Timestamp: 2018.05.05-00:29:43 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import gnpgui from '../gnpgui.js'

import {
  gndom_nodeelemtextcontentset
} from '../gndom.js'

const getvnode = (ss, cfg, gr, nd, pg, subj) => {
  const { div, span, a } = cfg.h(pg, nd)
  const {
    labelanchor,
    labelprimary,
    labelsecondary,
    label
  } = subj

  return (
    div('.:ui', [
      labelanchor &&
        a('.:ui-label-anchor', {
          id: labelanchor,
          href: `#${labelanchor}`
        }, ''),
      labelprimary &&
        span('.:ui-label-primary', labelprimary),
      labelsecondary &&
        span('.:ui-label-secondary', labelsecondary),
      span('.:ui-label#:key', String(label))
    ])
  )
}

const onsubj = (ss, cfg, gr, nd, pg, prev, { label }) => {
  if (label !== prev.label) {
    gndom_nodeelemtextcontentset(cfg, nd, null, label)
  }

  return true
}

export default Object.assign({}, gnpgui, {
  type: 'uilabel',
  getvnode,
  onsubj
})

