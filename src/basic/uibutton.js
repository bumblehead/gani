// Filename: uibutton.js
// Timestamp: 2018.05.05-00:31:00 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import gnpgui from '../gnpgui.js'

import {
  gnenumEVTYPEONCLICK as ONCLICK,
  gnenumKEY as KEY
} from '../gnenum.js'

const getvnode = (ss, cfg, gr, nd, pg, subj) => {
  const { label, span, button } = cfg.h(pg, nd)
  const {
    labelprimary,
    labelsecondary,
    labeltertiary,
    labelbutton,
    value
  } = subj

  return (
    label('.:ui', {
      for: nd.get(KEY)
    }, [
      labelprimary &&
        span('.:ui-label-primary', labelprimary),
      labelsecondary &&
        span('.:ui-label-secondary', labelsecondary),
      labeltertiary &&
        span('.:ui-label-tertiary', labeltertiary),
      button('.:ui-button#:key', {
        type: 'button',
        name: nd.get('name'),
        value
      }, labelbutton)
    ])
  )
}

const onclick = (ss, cfg, gr, nd, pg, prev, subj, ev) => {
  if (nd.has(ONCLICK)) {
    ev.preventDefault()
    pg.evPublishPOST(ss, cfg, gr, nd, pg, ONCLICK)
  }
}

export default Object.assign({}, gnpgui, {
  type: 'uibutton',
  getvnode,
  onclick
})
