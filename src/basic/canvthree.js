// Filename: canvthree.js
// Timestamp: 2018.12.09-00:09:35 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import canv from './canv.js'

const getglrenderer = (cfg, THREE, canvas) => (
  new THREE.WebGLRenderer({
    preserveDrawingBuffer: true,
    autoClearColor: false,

    canvas,
    alpha: true,
    antialias: true
  }))

// used to return new THREE.VREffect(renderer)
// pending removal
const getrenderereffect = (cfg, nd, renderer) => (
  renderer)

const getperspectivecamera = (cfg, nd, pg, fov = 64, clippingdist = 1000) => {
  const parentelem = pg.getresetglcanvas(cfg, nd, pg).parentElement
  const parentelemwh = [parentelem.offsetWidth, parentelem.offsetHeight]
  const aspect = parentelemwh[0] / parentelemwh[1]
  const cam = new cfg.THREE.PerspectiveCamera(fov, aspect, 1, clippingdist)

  return cam
}

const getlinesegmenthelper = (THREE, mesh, color = 0x5f5f5f, opacity = 1) => (
  new THREE.LineSegments(
    new THREE.EdgesGeometry(mesh.geometry),
    new THREE.LineBasicMaterial({ color, opacity, linewidth: 2 })))

const getedgehelper = (THREE, mesh, color, opacity = 1, scale = 1.03) => {
  const linesegmenthelper = getlinesegmenthelper(THREE, mesh, color, opacity)

  linesegmenthelper.geometry.scale(scale, scale, scale)

  return linesegmenthelper
}

const glrender = ({ glrenderer, glscene, camera }) => glrenderer
  && glrenderer.render(glscene, camera)

const render = renderobj => (
  glrender(renderobj))

export default Object.assign({}, canv, {
  type: 'canvthree',
  getglrenderer,
  getrenderereffect,
  getperspectivecamera,
  getedgehelper,
  glrender,
  render
})
