// Filename: uiactivebox.js
// Timestamp: 2018.12.09-00:10:21 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import gnpgui from '../gnpgui.js'

import {
  gndom_nodeelem,
  gndom_elemclasstoggle
} from '../gndom.js'

const getvnode = (ss, cfg, gr, nd, pg, subj) => {
  const { div, span } = cfg.h(pg, nd)
  const label = subj.label
  const isactive = subj.isactive

  return (
    div(`.:ui.st-isactive-${Boolean(isactive)}#:key`, [
      label &&
        span('.:ui-label-primary', label),

      pg.getchildvnodearr(ss, cfg, gr, nd)
    ])
  )
}

const onsubj = (ss, cfg, gr, nd, pg, prev, { isactive }) => (
  gndom_elemclasstoggle(
    gndom_nodeelem(cfg, nd), 'st-isactive', isactive),
  true)

export default Object.assign({}, gnpgui, {
  type: 'uiactivebox',
  getvnode,
  onsubj
})
