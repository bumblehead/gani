// Filename: uicheckbox.js
// Timestamp: 2018.12.09-00:10:39 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import uisvgsymbol from './uisvgsymbol.js'
import gnpgui from '../gnpgui.js'

import {
  gndom_nodeelem,
  gndom_nodeelemid,
  gndom_elemclasstoggle
} from '../gndom.js'

import {
  gnenumEVTYPEONCHANGE as ONCHANGE,
  gnenumKEY as KEY
} from '../gnenum.js'

const getvnode = (ss, cfg, gr, nd, pg, subj) => {
  const { div, input, label, span } = cfg.h(pg, nd)
  const {
    labelprimary,
    labelsecondary,
    labeltertiary,
    svgchecked,
    svgunchecked,
    isdisabled,
    value
  } = subj

  return (
    label('.:ui', {
      for: gndom_nodeelemid(nd)
    }, [
      labelprimary &&
        span('.:ui-label-primary', labelprimary),

      div(`.:ui-group.st-ischecked-${value}#:key---group`, [
        input('.:ui-group-input#:key', {
          type: 'checkbox',
          name: nd.get(KEY),
          disabled: isdisabled && 'disabled',
          defaultChecked: Boolean(value)
        }),

        span('.:ui-group-ico'),

        svgchecked &&
          div('.:ui-group-svgchecked', [
            uisvgsymbol.getvnode(ss, cfg, gr, nd, svgchecked)
          ]),

        svgunchecked &&
          div('.:ui-group-svgunchecked', [
            uisvgsymbol.getvnode(ss, cfg, gr, nd, svgunchecked)
          ])
      ]),

      labelsecondary &&
        span('.:ui-label-secondary', labelsecondary),
      labeltertiary &&
        span('.:ui-label-tertiary', labeltertiary)
    ])
  )
}

const oncontentload = (ss, cfg, gr, nd, pg, prev, { value }) => {
  const elemcb = gndom_nodeelem(cfg, nd)

  if (elemcb) elemcb.checked = value
}

const onchange = (ss, cfg, gr, nd, pg) => {
  const elemcb = gndom_nodeelem(cfg, nd)
  const ischecked = Boolean(elemcb.checked)

  gndom_elemclasstoggle(elemcb, 'st-ischecked', ischecked)
  pg.publish(ss, cfg, nd, { value: ischecked }, (e, gr) => {
    pg.evPublishPOST(ss, cfg, gr, nd, pg, ONCHANGE)
  })
}

const onsubj = (ss, cfg, gr, nd, pg, prev, subj) => {
  const elemcb = gndom_nodeelem(cfg, nd)
  const ischecked = Boolean(subj.value)

  gndom_elemclasstoggle(elemcb, 'st-ischecked', ischecked)
  elemcb.checked = ischecked
  elemcb.disabled = Boolean(subj.isdisabled)

  return true
}

export default Object.assign({}, gnpgui, {
  type: 'uicheckbox',
  getvnode,
  oncontentload,
  onchange,
  onsubj
})
