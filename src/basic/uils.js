// Filename: uils.js
// Timestamp: 2017.12.06-17:24:21 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import gnpgui from '../gnpgui.js'

// ex, ('uichildpg st-isexcerpt-false') => 'st-isexcerpt-false'
const stripuiclass = className => (
  className && className.replace(/ui[^ ]*/g, '').trim())

// ex, ('className1 className2') => '.className1.className2'
const querifyclass = className => className
  ? '.' + className.replace(/ /g, '.')
  : ''

// returns className query string from vnode
// useful to render each child className to the containing list item
// allows child context to style list item
const vnodeclass = vnode => (
  querifyclass(stripuiclass(vnode.className)))

const getvnode = (ss, cfg, gr, nd, pg, subj) => {
  const { ul, li, div } = cfg.h(pg, nd)
  const {
    label,
    labelsecondary
  } = subj

  return (
    div('.:ui-root', [
      label &&
        div('.:ui-label-primary', label),

      labelsecondary &&
        div('.:ui-label-secondary', labelsecondary),

      ul('.:ui-list', pg.getchildvnodearr(ss, cfg, gr, nd, vnode => (
        li(`.:ui-listitem${vnodeclass(vnode)}`, [vnode])
      )))
    ])
  )
}

export default Object.assign({}, gnpgui, {
  type: 'uils',
  getvnode
})
