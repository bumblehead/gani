// Filename: uiform.js
// Timestamp: 2018.05.05-00:33:01 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>
//
// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/form
//
// <legend>Title</legend>

import gnpgui from '../gnpgui.js'

import {
  gndom_nodeforminputs,
  gndom_nodeelemdisabledset
} from '../gndom.js'

import {
  gnenumEVTYPEONSUBMIT as ONSUBMIT,
  gnenumKEY as KEY
} from '../gnenum.js'

const getformname = nd =>
  nd.get(KEY) + '-form'

const getfirstvisibleinput = (cfg, nd) => (
  gndom_nodeforminputs(cfg, nd).find(
    elem => elem.clientHeight))

const focusfirstinput = (cfg, nd) => {
  const formelem = getfirstvisibleinput(cfg, nd)

  if (formelem) {
    formelem.blur()
    formelem.focus()
  }
}

const getvnode = (ss, cfg, gr, nd, pg, subj) => {
  const { div, h3, h4, form, fieldset, legend } = cfg.h(pg, nd)
  const {
    labelprimary,
    labelsecondary,
    labellegend,
    action,
    method,
    isfieldsetdiv,
    isdisabled
  } = subj
  // https://bugs.chromium.org/p/chromium/issues/detail?id=262679
  //
  // chrome is unable to flex style fieldset
  const fset = isfieldsetdiv ? div : fieldset

  return (
    div('.:ui', [
      labelprimary &&
        h3('.:ui-label-primary', labelprimary),

      labelsecondary &&
        h4('.:ui-label-secondary', labelsecondary),

      form('.:ui-form#:key', {
        name: getformname(nd),
        action, // ex, '/login',
        method // ex, 'post'
      }, [
        fset('.:ui-form-fieldset#:key---fieldset', {
          disabled: isdisabled && 'disabled'
        }, [
          labellegend &&
            legend('.:ui-form-fieldset-legend', labellegend),

          pg.getchildvnodearr(ss, cfg, gr, nd)
        ])
      ])
    ])
  )
}

const onsubmit = (ss, cfg, gr, nd, pg, prev, subj, ev) => {
  const onsubmit = nd.get(ONSUBMIT)

  if (onsubmit) {
    gndom_nodeelemdisabledset(cfg, nd, 'fieldset', true)
    ev.preventDefault()

    pg.evPublishPOST(ss, cfg, gr, nd, pg, ONSUBMIT, {}, () => {
      gndom_nodeelemdisabledset(cfg, nd, 'fieldset', false)
    })
  }
}

const onsubj = (ss, cfg, gr, nd, pg, prev, subj) => (
  gndom_nodeelemdisabledset(cfg, nd, 'fieldset', subj.isdisabled))

const oncontentload = (ss, cfg, gr, nd, pg, prev, subj) => {
  // causes unexpected problems when forms are
  // rendered out-of-view
  // o.focusfirstinput(sess, cfg, graph, node)
  gndom_nodeelemdisabledset(cfg, nd, 'fieldset', subj.isdisabled)
}

export default Object.assign({}, gnpgui, {
  type: 'uiform',
  getfirstvisibleinput,
  focusfirstinput,
  getvnode,
  onsubmit,
  onsubj,
  oncontentload
})
