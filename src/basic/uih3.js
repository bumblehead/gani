import gnpgui from '../gnpgui.js'

const getvnode = (ss, cfg, gr, nd, pg, subj) => {
  const { h3 } = cfg.h(pg, nd)

  return (
    h3('.:ui#:key', 'label' in subj
      ? subj.label
      : [pg.getchildvnodearr(ss, cfg, gr, nd)]
    ))
}

export default Object.assign({}, gnpgui, {
  type: 'uih3',
  getvnode
})
