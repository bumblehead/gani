// Filename: uitext.js
// Timestamp: 2018.05.06-16:32:29 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import gnpgui from '../gnpgui.js'

import {
  gndom_nodeelemid,
  gndom_nodeelemvalueget,
  gndom_nodeelemvalueset
} from '../gndom.js'

import {
  gnenumKEY as KEY,
  gnenumEVTYPEONCHANGE as ONCHANGE,
  gnenumEVTYPEONKEYUP as ONKEYUP
} from '../gnenum.js'

const getvnode = (ss, cfg, gr, nd, pg, subj) => {
  const { input, label, span } = cfg.h(pg, nd)
  const {
    labelprimary,
    labelsecondary,
    labeltertiary,
    step,
    value,
    inputtype,
    disabled,
    name,
    hint,
    maxlength
  } = subj

  return (
    label('.:ui', {
      for: gndom_nodeelemid(nd)
    }, [
      labelprimary &&
        span('.:ui-label-primary', labelprimary),
      labelsecondary &&
        span('.:ui-label-secondary', labelsecondary),

      input('.:ui-input#:key', {
        placeholder: hint || '',
        type: inputtype || 'text',
        name: name || nd.get('name'),
        step: typeof step === 'number' ? step : 'any',
        disabled: disabled && 'disabled',
        defaultValue: value,
        autocomplete: 'off',
        maxlength: maxlength || 400
      }),

      labeltertiary &&
        span('.:ui-label-tertiary', labeltertiary)
    ])
  )
}

const getval = (ss, cfg, gr, nd, pg, subj, fn) => {
  const elemval = gndom_nodeelemvalueget(cfg, nd)
  let val = elemval

  if (subj.inputtype === 'number') {
    if (val === '') {
      // do nothing
      // setting as same value results in no action
      val = subj.value
    } else {
      val = subj.numbertype === 'int'
        ? Number.parseInt(val, 10)
        : Number.parseFloat(val)
    }
  }

  fn(null, val)
}

const getvalifmodified = (ss, cfg, gr, nd, pg, subj, fn) => (
  pg.getval(ss, cfg, gr, nd, pg, subj, (e, value) => (
    value === subj.value || fn(null, value)
  )))

const onchange = (ss, cfg, gr, nd, pg, prev, subj) => {
  const ndkey = nd.get(KEY)

  pg.getvalifmodified(ss, cfg, gr, nd, pg, subj, (e, value) => (
    pg.publish(ss, cfg, ndkey, { value }, (e, gr) => (
      pg.evPublishPOST(ss, cfg, gr, nd, pg, ONCHANGE)
    ))
  ))
}

const onkeyup = (ss, cfg, gr, nd, pg, prev, subj) => {
  const ndkey = nd.get(KEY)

  pg.getvalifmodified(ss, cfg, gr, nd, pg, subj, (e, value) => (
    pg.publish(ss, cfg, ndkey, { value }, (e, gr) => (
      pg.evPublishPOST(ss, cfg, gr, nd, pg, ONKEYUP)
    ))
  ))
}

const onsubj = (ss, cfg, gr, nd, pg, prev, subj) => {
  if (prev.value !== subj.value) {
    gndom_nodeelemvalueset(cfg, nd, subj.value)
  }

  return true
}

export default Object.assign({}, gnpgui, {
  type: 'uitext',
  getvnode,
  getval,
  getvalifmodified,
  onchange,
  onkeyup,
  onsubj
})
