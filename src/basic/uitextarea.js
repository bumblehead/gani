// Filename: uitextarea.js
// Timestamp: 2017.12.06-17:27:08 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import uitext from './uitext.js'

import {
  gndom_nodeelemvalueget
} from '../gndom.js'

const getvnode = (ss, cfg, gr, nd, pg, subj) => {
  const { textarea, label, span } = cfg.h(pg, nd)
  const {
    labelprimary,
    labelsecondary,
    labeltertiary,
    maxlength,
    value,
    disabled,
    name,
    hint
  } = subj

  return (
    label('.:ui', {
      for: nd.get('key')
    }, [
      labelprimary &&
        span('.:ui-label-primary', labelprimary),
      labelsecondary &&
        span('.:ui-label-secondary', labelsecondary),

      textarea('.:ui-input#:key', {
        placeholder: hint || '',
        name: name || nd.get('name'),
        rows: '4',
        cols: '50',

        disabled: disabled && 'disabled',
        value,
        autocomplete: 'off',
        maxlength: maxlength || 400
      }),

      labeltertiary &&
        span('.:ui-label-tertiary', labeltertiary)
    ])
  )
}

const getval = (ss, cfg, gr, nd, pg, fn) => {
  const val = gndom_nodeelemvalueget(cfg, nd)

  fn(null, val)
}

export default Object.assign({}, uitext, {
  type: 'uitextarea',
  getvnode,
  getval
})
