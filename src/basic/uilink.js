// Filename: uilink.js
// Timestamp: 2018.12.09-00:27:43 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import uisvgsymbol from './uisvgsymbol.js'
import gnpgui from '../gnpgui.js'

import {
  gnenumEVTYPEONCLICK as ONCLICK
} from '../gnenum.js'

import {
  gndom_nodeelemclasstoggle
} from '../gndom.js'

const isactive = ({ activevalue, value }) => (
  typeof activevalue !== 'undefined' && activevalue === value)

const getvnode = (ss, cfg, gr, nd, pg, subj) => {
  const { a, span, img } = cfg.h(pg, nd)
  const title = subj.title || subj.label
  const {
    label,
    labelsecondary,
    labeltertiary,
    labelalt,
    target,
    imgsrc,
    href,
    svgsymbol
  } = subj

  return (
    a(`.:ui.st-isactive-${isactive(subj)}#:key`, {
      href,
      title,
      target
    }, [
      label &&
        span('.:ui-label-primary', label),
      labelsecondary &&
        span('.:ui-label-secondary', labelsecondary),

      imgsrc &&
        img('.:ui-img', {
          src: imgsrc,
          alt: labelalt
        }),

      svgsymbol &&
        uisvgsymbol.getvnode(ss, cfg, gr, nd, { svgsymbol }),

      labeltertiary &&
        span('.:ui-label-tertiary', labeltertiary)
    ])
  )
}

const onclick = (ss, cfg, gr, nd, pg, prev, subj, ev) => {
  if (nd.has(ONCLICK)) {
    ev.preventDefault()

    pg.evPublishPOST(ss, cfg, gr, nd, pg, ONCLICK)
  }
}

const onsubj = (ss, cfg, gr, nd, pg, prev, subj) => {
  if (subj.activevalue !== prev.activevalue) {
    gndom_nodeelemclasstoggle(cfg, nd, 'st-isactive', isactive(subj))

  }

  return true
}

export default Object.assign({}, gnpgui, {
  type: 'uilink',
  getvnode,
  onclick,
  onsubj
})
