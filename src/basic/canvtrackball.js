// Filename: canvtrackball.js
// Timestamp: 2018.12.09-00:09:57 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>
//
// externally, state is represented as degrees
// internally, uses radians
//
// TODO reduce lookups in this file. import full functions

import canvthree from './canvthree.js'
import canv from './canv.js'

const COLORRGBGREEN = 'rgb(78, 248, 78)'
const COLORRGBRED = 'rgb(255, 0, 0)'
const COLORRGBCYAN = 'rgb(0, 255, 255)'
const COLORRGBCYANDARK = 'rgb(232, 248, 248)'
const COLORRGBGRAYDARK = 'rgb(43, 51, 63)'
const COLORDEFAULTS = {
  ycolor: COLORRGBGREEN,
  xcolor: COLORRGBRED,
  zcolor: COLORRGBCYAN,
  fgcolor: COLORRGBCYANDARK,
  bgcolor: COLORRGBGRAYDARK
}

const pixels_as_degreesarr = (arr, pw) => (
  arr.map(xy => canv.pixels_as_degrees(xy, pw)))

const getismove = (ss, cfg, gr, nd, prev) => (
  cfg.touchboom.coordsismove(prev))

const getlocalrotation = (ss, cfg, gr, nd, prev) => {
  const totalxy = cfg.touchboom.coordsgettotal(prev)
  const degxy = pixels_as_degreesarr(totalxy, prev.pw)

  return { rotationxy: degxy }
}

const publishscene = (ss, cfg, gr, nd, pg, prev) => (
  pg.publish(ss, cfg, nd, (
    getlocalrotation(ss, cfg, gr, nd, prev))))

const applyscenerotation = ({ bodygroup, headgroup, rotationxy }) => (
  headgroup.rotation.x = canv.degrees_as_radians(rotationxy[1]),
  bodygroup.rotation.y = canv.degrees_as_radians(rotationxy[0]))

const applyscenerotationpatch = (ss, cfg, gr, nd, pg, prev, rotation) => {
  applyscenerotation({
    bodygroup: prev.bodygroup,
    headgroup: prev.headgroup,
    rotationxy: rotation.rotationxy
  })

  if (!prev.throttledpublish) {
    prev.throttledpublish = cfg.lockfnthrottling({ ms: 180 }, pg.publish)
  }

  prev.throttledpublish(ss, cfg, nd, rotation)
}

const isresized = (ss, cfg, gr, nd, pg, prev) => {
  const curwh = canv.getglcanvaswh(cfg, nd)
  const oldwh = prev.wharr || []

  return curwh[0] !== oldwh[0] || curwh[1] !== oldwh[1]
}

const getsphere = (THREE, opacity, color, radius) => (
  new THREE.Mesh(
    new THREE.SphereGeometry(radius, 32, 32),
    new THREE.MeshBasicMaterial({
      wireframe: true,
      color,
      opacity,
      transparent: true,
      side: THREE.BackSide
    })))

const getwrapcircle = (THREE, opacity, color, radius) => (
  new THREE.Mesh(
    new THREE.TorusGeometry(radius, 0.02, 30, 100),
    new THREE.MeshBasicMaterial({
      wireframe: true,
      color,
      opacity,
      transparent: true
    })))

const getgridhelper = (THREE, size = 200, step = 10, x, y, gridhelper) => (
  gridhelper = new THREE.GridHelper(size, step),
  gridhelper.position.y = -50,
  gridhelper)

const gettrackball = (THREE, node, cfg) => {
  const headgroup = new THREE.Group()
  const wrapcircle = getwrapcircle(THREE, 0.1, cfg.xcolor, 11)
  const wrapcircle_y90 = getwrapcircle(THREE, 1, cfg.ycolor, 11)
  const wrapcircle_z90 = getwrapcircle(THREE, 1, cfg.zcolor, 11)
  const eyecircle1 = getwrapcircle(THREE, 0.2, cfg.fgcolor, 6)
  const eyecircle2 = getwrapcircle(THREE, 0.2, cfg.fgcolor, 5)
  const sphere = getsphere(THREE, 0.7, cfg.fgcolor, 20 / 2)

  headgroup.name = 'headgroup'

  wrapcircle_y90.rotation.y = Math.PI / 2
  wrapcircle_z90.rotation.x = Math.PI / 2

  eyecircle1.position.set(0, 0, 8.1)
  eyecircle2.position.set(0, 0, 8.9)

  headgroup.add(sphere)
  headgroup.add(wrapcircle_y90)
  headgroup.add(wrapcircle_z90)

  headgroup.add(wrapcircle)
  headgroup.add(eyecircle1)
  headgroup.add(eyecircle2)

  return headgroup
}

const getcoords = (cfg, nd, rotationxy) => {
  const canvas = canv.getglcanvas(cfg, nd)
  const pw = canv.pixel_weightelem(canvas)
  const autoweight = canv.key_weight(canvas)

  return cfg.touchboom.coords([{
    autoweight,
    bgn: canv.degrees_as_pixels(rotationxy[0], pw)
  }, {
    autoweight,
    bgn: canv.degrees_as_pixels(rotationxy[1], pw),
    min: canv.degrees_as_pixels(-90, pw),
    max: canv.degrees_as_pixels(90, pw)
  }])
}

const getscene = (ss, cfg, gr, nd, pg, prev, subj) => {
  const THREE = cfg.THREE
  const canvas = canv.getglcanvas(cfg, nd)
  const wharr = canv.getelemwh(canvas)
  const glscene = new THREE.Scene()
  const camera = new THREE.PerspectiveCamera(60, wharr[0] / wharr[1], 1, 10000)
  const glrenderer = canvthree.getglrenderer(cfg, THREE, canvas)
  const headgroup = gettrackball(
    THREE, nd, Object.assign({}, COLORDEFAULTS, prev))
  const bodygroup = new THREE.Group()
  const scenegroup = new THREE.Group()
  const pw = canv.pixel_weightelem(canvas)

  bodygroup.name = 'bodygroup'
  bodygroup.add(headgroup)
  scenegroup.add(bodygroup)

  scenegroup.rotation.y = -THREE.MathUtils.degToRad(90)
  scenegroup.name = 'scenegroup'

  glscene.add(scenegroup)

  camera.position.set(0, 0, -44)
  camera.lookAt(glscene.position)

  glscene.rotation.y += -Math.PI / 2

  applyscenerotation({
    bodygroup,
    headgroup,
    rotationxy: subj.rotationxy
  })

  return {
    coords: getcoords(cfg, nd, subj.rotationxy),
    wharr,
    headgroup,
    bodygroup,
    glrenderer,
    camera,
    glscene,
    pw
  }
}

const disposescene = (ss, cfg, gr, nd, prev) => {
  prev.glscene = canv.disposeNode(prev.glscene)
  prev.headgroup = null
  prev.bodygroup = null
  prev.glrenderer = null
  prev.camera = null
  prev.glscene = null

  const canvas = canv.getglcanvas(cfg, nd)
  if (canvas && canvas.parentNode) {
    canvas.parentNode
      .replaceChild(canvas.cloneNode(), canvas)
  }

  return prev
}

const rebuildscene = (ss, cfg, gr, nd, pg, prev, frameobj) => {
  prev = disposescene(ss, cfg, gr, nd, prev)

  prev.wharr = canv.getglcanvaswh(cfg, nd)

  return Object.assign(
    prev, pg.getscene(ss, cfg, gr, nd, pg, prev, frameobj))
}

const oncontentload = (ss, cfg, gr, nd, pg, prev, frameobj) => {
  // key weight after properties are separated
  prev = Object.assign(prev, frameobj)
  prev = pg.rebuildscene(ss, cfg, gr, nd, pg, prev, frameobj)

  const canvas = canv.getglcanvas(cfg, nd)

  prev = cfg.touchboom.attach(prev, canvas, {
    oneventfn: (prev, etype) => {
      if (etype === 'start') {
        canvas.focus()
      } else if (etype === 'moveend') {
        publishscene(ss, cfg, gr, nd, pg, prev)
      }
    },
    oninertiafn: prev => {
      applyscenerotationpatch(ss, cfg, gr, nd, pg, prev, (
        getlocalrotation(ss, cfg, gr, nd, prev)
      ))
    }
  })

  prev.wharr = canv.getglcanvaswh(cfg, nd)
}

const onblur = (ss, cfg, gr, nd, pg, prev) => (
  cfg.touchboom.coordsmoveend(prev))

const onfocusout = (ss, cfg, gr, nd, pg, prev) => (
  cfg.touchboom.coordsmoveend(prev))

const onresize = (ss, cfg, gr, nd, pg, prev, subj) => {
  if (isresized(ss, cfg, gr, nd, pg, prev)) {
    pg.rebuildscene(ss, cfg, gr, nd, pg, prev, subj)
  }
}

const onframe = (ss, cfg, gr, nd, pg, prev) => {
  if (prev.glscene && canv.isalive(cfg, nd)) {
    pg.render(prev)
  }
}

const onsubj = (ss, cfg, gr, nd, pg, prev, subj) => {
  const rotationxy = subj.rotationxy

  if (prev.glscene) {
    prev.rotationxy = rotationxy.slice()

    applyscenerotation({
      bodygroup: prev.bodygroup,
      headgroup: prev.headgroup,
      rotationxy
    })

    if (!cfg.touchboom.coordsismove(prev)) {
      prev.coords = getcoords(cfg, nd, rotationxy)
    }

    return true
  }

  return false
}

export default Object.assign({}, canvthree, {
  type: 'canvtrackball',
  getismove,
  getlocalrotation,
  publishscene,
  isresized,
  getsphere,
  getwrapcircle,
  getgridhelper,
  gettrackball,
  getcoords,
  getscene,
  disposescene,
  rebuildscene,
  applyscenerotationpatch,
  applyscenerotation,
  oncontentload,
  onblur,
  onfocusout,
  onresize,
  onframe,
  onsubj
})
