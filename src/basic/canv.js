// Filename: canv.js
// Timestamp: 2017.12.16-22:20:25 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>
//
// common methods fo canvas nodes
//
// canvas does not render childs as dom nodes,
// childs to be rendered on canvas
//

import gnpgui from '../gnpgui.js'

import {
  gndom_nodeelem
} from '../gndom.js'

const key_weight = (elem = window) => (
  0.03 * Math.min(
    ...(
      elem === window
        ? [elem.innerWidth, elem.innerHeight]
        : [elem.clientWidth, elem.clientHeight])))

const pixel_weightarea = ([w, h]) => (
  180 / Math.max(w, h))

const pixel_weightelem = (elem = window) => (
  pixel_weightarea(
    elem === window
      ? [elem.innerWidth, elem.innerHeight]
      : [elem.clientWidth, elem.clientHeight]))

const pixels_as_degrees = (p, pw) => (
  p * (pw || pixel_weightelem()))

const degrees_as_pixels = (d, pw) => (
  d / (pw || pixel_weightelem()))

const degrees_as_radians = d => (
  d * (Math.PI / 180))

const radians_as_degrees = r => (
  r * (180 / Math.PI))

const radians_as_pixels = (r, pw) => (
  degrees_as_pixels(radians_as_degrees(r), pw))

const pixels_as_radians = (p, pw) => (
  degrees_as_radians(pixels_as_degrees(p, pw)))

// awkward canvas when container is resized to proportions
// different from those used on initial canvas 'render'
//
// canvas kept inside container w/ overflow:hidden
// canvas resized to 'fit' container on resize
const getvnode = (ss, cfg, gr, nd, pg) => {
  const { div, canvas } = cfg.h(pg, nd)

  return (
    div('.:ui#:key', [
      canvas('.:ui-glcanvas#:key---glcanvas', {
        tabindex: '0' // focusable
      })
    ])
  )
}

const isvalidglcanvas = canvas => (
  canvas &&
    canvas.offsetWidth &&
    canvas.offsetHeight)

const setcanvaswh = (canvas, [w, h]) => {
  if (!canvas)
    return canvas

  canvas.width = w
  canvas.height = h

  canvas.style.width = `${w}px`
  canvas.style.height = `${h}px`

  return canvas
}

// stylesheet should define canvas to 100% width/height
// to obtain maximum possible offsetWidth/Height
const resetcanvas = canvas => {
  if (!canvas)
    return canvas

  canvas.style.width = ''
  canvas.style.height = ''

  setcanvaswh(canvas, [
    canvas.offsetWidth,
    canvas.offsetHeight
  ])

  return canvas
}

const getglcanvas = (cfg, nd) => {
  const glcanvas = gndom_nodeelem(cfg, nd, 'glcanvas')
  const glcanvasres = glcanvas && resetcanvas(glcanvas)

  return glcanvasres
}

const getresetglcanvas = (cfg, nd) => (
  resetcanvas(getglcanvas(cfg, nd)))

const getelemwh = elem => elem
  ? [elem.offsetWidth, elem.offsetHeight]
  : []

const getglcanvaswh = (cfg, nd) => (
  getelemwh(gndom_nodeelem(cfg, nd, 'glcanvas')))

const iscanvaswh = (cfg, nd) => {
  const wh = getglcanvaswh(cfg, nd)

  return Boolean(wh[0] && wh[1])
}

const isalive = (cfg, nd) => (
  Boolean(gndom_nodeelem(cfg, nd)))

const disposeMaterial = material => {
  [
    'map',
    'lightMap',
    'bumpMap',
    'normalMap',
    'specularMap',
    'envMap'
  ].forEach(n => {
    const prop = material[n]
    if (prop)
      prop.dispose()
  })

  material.dispose() // disposes any programs associated with the material

  return material
}

const disposeNode = (THREE, parentObject) => {
  if (!parentObject) return null

  parentObject.traverse(node => {
    if (node instanceof THREE.Mesh) {
      if (node.geometry) node.geometry.dispose()
      if (node.material) {
        if (node.material instanceof THREE.MeshFaceMaterial ||
            node.material instanceof THREE.MultiMaterial)
          node.material.materials.forEach(disposeMaterial)
        else
          disposeMaterial(node.matrial)
      }
    }
  })

  parentObject.children.map(child => parentObject.remove(child))
}

export default Object.assign({}, gnpgui, {
  type: 'canv',
  key_weight,
  pixel_weightarea,
  pixel_weightelem,
  pixels_as_degrees,
  pixels_as_radians,
  degrees_as_pixels,
  degrees_as_radians,
  radians_as_pixels,
  getvnode,
  isvalidglcanvas,
  setcanvaswh,
  resetcanvas,
  getresetglcanvas,
  getelemwh,
  getglcanvas,
  getglcanvaswh,
  iscanvaswh,
  isalive,
  disposeMaterial,
  disposeNode
})
