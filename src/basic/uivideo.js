// Filename: uivideo.js
// Timestamp: 2018.12.09-00:25:04 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import lockfnthrottling from 'lockfn/throttling'
import gnpgui from '../gnpgui.js'

// https://developer.mozilla.org/en-US/docs/Web/API/HTMLMediaElement/readyState
//
// 0 No information is available about the media resource.
// 1 Enough of the media resource has been retrieved that the metadata
//   attributes are initialized. Seeking will no longer raise an exception.
// 2 Data is available for the current playback position, but not enough to
//   actually play more than one frame.
// 3 Data for the current playback position as well as for at least a
//   little bit of time into the future is available (in other words,
//   at least two frames of video, for example).
//
// const HAVE_NOTHING = 0
// const HAVE_METADATA = 1
// const HAVE_CURRENT_DATA = 2
// const HAVE_FUTURE_DATA = 3
const HAVE_ENOUGH_DATA = 4

const BUFFERING = 'buffering'
const BUFFERINGOUT = 'bufferingout'

const mediaevents = [
  'ended',
  'play',
  'pause',
  'abort',
  'seeking',
  'waiting',
  'canplay',
  'loadedmetadata',
  'timeupdate'
]

const get_buffer_state = (opt = {}) => ({
  buf_load_percent: opt.load_percent || 0.0,
  buf_seek_percent: opt.seek_percent || 0.0,
  buf_timess_duration: opt.timess_duration || 0,
  buf_timess_current: opt.timess_current || 0
})

const getdurationss = videoelem => (
  (videoelem && !Number.isNaN(videoelem.duration)) ? videoelem.duration : 0)

const getperss = (videoelem, per) => (
  Math.floor(getdurationss(videoelem) * per))

// http://stackoverflow.com/questions/9754527/ \
//   what-does-the-video-buffered-length-exactly-tells
//
// length returns how many "parts" of the media are being buffered.
const buffer_is_buffered_parts = videoelem => (
  videoelem.buffered.length >= 1)

// to be enabled later
const buffer_is_not_time_buffered = (videoelem, time) => {
  const { buffered } = videoelem

  return (!(buffered.start(0) <= time && time <= buffered.end(0)))
}

// videoelem.duration is *sometimes* defined as NaN...
// cfg.duration is defined on metadata event and will not be NaN
const gettimeatposition = (cfg, videoelem, positionper) => (
  positionper * cfg.duration)

// https://developer.mozilla.org/en-US/Apps/Build/ \
//   Audio_and_video_delivery/buffering_seeking_time_ranges
// http://stackoverflow.com/questions/5029519/html5-video-percentage-loaded
//
const get_buffer_state_videoelem = videoelem => {
  let range = 0,
      bf = videoelem.buffered,
      time = videoelem.currentTime,
      stateobj = get_buffer_state({
        timess_duration: videoelem.duration,
        timess_current: time
      })

  if (buffer_is_buffered_parts(videoelem)) {
    range = (function (range, bufbgn, bufend) {
      bufbgn = bf.start(range)
      bufend = bf.end(range)

      // buffer is not within range
      return (!(bufbgn <= time && time <= bufend))
        ? 0 : range
    }(0))

    let videoelemduration = videoelem.duration || 0.000000001,
        videoelemcurrentTime = videoelem.currentTime || 0,
        loadStartPercentage = bf.start(range) / videoelemduration,
        loadEndPercentage = bf.end(range) / videoelemduration

    stateobj.buf_duration_ss = videoelemduration
    stateobj.buf_load_percent = loadEndPercentage - loadStartPercentage
    stateobj.buf_seek_percent = videoelemcurrentTime / videoelemduration
  }

  return stateobj
}

const getvideoelem = (nd, pg) => (
  pg.gEBId(nd, 'video'))

const getsourceelem = (nd, pg) => {
  const videoelem = pg.getvideoelem(nd, pg)

  return videoelem
    ? videoelem.childNodes[0]
    : null
}

const setcurrenttime = (nd, pg, currenttime) => {
  const videoelem = pg.getvideoelem(nd, pg)

  if (typeof currenttime !== 'number' || Number.isNaN(currenttime)) {
    return console.error('invalid currenttime', currenttime)
  }

  if (videoelem) {
    videoelem.currentTime = currenttime
  }
}

const getvnode = (ss, cfg, gr, nd, pg, subj) => {
  const { div, video, source } = cfg.h(pg, nd)
  const {
    poster,
    file_url,
    autoplay,
    mimetype
  } = subj

  return (
    div('.:ui', [
      video('.:ui-video#:key---video', {
        // this requires cors service headers
        // and for us means the request must be http
        crossOrigin: 'anonymous',
        poster: poster || undefined,
        playsinline: 'playsinline',
        control: true,

        // autoplay 'false' has unintended consequence --autoplays video
        // do not define autoplay if autoplay is not wanted
        autoplay: autoplay || undefined,
        // https://developer.apple.com/library/prerelease/content/ \
        //  releasenotes/General/WhatsNewInSafari/Articles/Safari_10_0.html
        //
        // webkit-playsinline should allow browser to play video
        'webkit-playsinline': ''
      }, [
        file_url &&
          source('.:ui-video-src', {
            src: file_url,
            type: mimetype
          })
      ])
    ])
  )
}

const loadsrc = (ss, cfg, gr, nd, pg, src) => {
  const videoelem = pg.getvideoelem(nd, pg)
  const sourceelem = pg.getsourceelem(nd, pg)

  if (sourceelem) {
    sourceelem.src = src
    pg.prevget(cfg, nd, pg).reloadshouldplay = src
    videoelem.load()
  }
}

const timeupdate = (ss, cfg, gr, nd, pg) => {
  const videoelem = pg.getvideoelem(nd, pg)
  const subj = pg.getsubj(nd)

  if (!subj.isseeking && videoelem.readyState === HAVE_ENOUGH_DATA) {
    pg.publish(ss, cfg, nd, (
      pg.get_buffer_state_videoelem(videoelem)))
  }
}

const onended = (ss, cfg, gr, nd, pg, prev) => {
  prev.postplaypause = 'pause'
  prev.playpause = 'pause'
  pg.publish(ss, cfg, nd, {
    postplaypause: 'pause',
    playpause: 'pause'
  }, (e, gr) => (
    pg.evPublishPOST(ss, cfg, gr, nd, pg, 'onended')))
}

const onplay = (ss, cfg, gr, nd, pg, prev) => (
  pg.publish(ss, cfg, nd, { playpause: 'play' }, () => (
    prev.reloadshouldplay = false)))

const onpause = (ss, cfg, gr, nd, pg) => (
  pg.publish(ss, cfg, nd, { playpause: 'pause' }))

const oncanplay = (ss, cfg, gr, node, pg, prev, e, videoelem) => {
  videoelem.width = videoelem.videoWidth
  videoelem.height = videoelem.videoHeight

  if (prev.reloadshouldplay && videoelem.paused) {
    videoelem.play()
  }

  pg.publish(ss, cfg, node, Object.assign({
    canplay: true
  }, pg.get_buffer_state_videoelem(videoelem)))
}

const ontimeupdate = (ss, cfg, gr, node, pg, prev, e) => (
  prev.timeupdatethrottle && prev.timeupdatethrottle(() => {
    pg.timeupdate(ss, cfg, gr, node, prev, e)
  }))

const addlisteners = (ss, cfg, prev, videoelem, fn) => {
  if (prev.listener) {
    mediaevents.map(evname => (
      videoelem.removeEventListener(evname, prev.listener)))
  }

  prev.timeupdatethrottle = cfg.lockfnthrottling.getNew({
    isendthrottlecall: false,
    ms: 400
  })
  prev.listener = fn

  mediaevents.map(evname => (
    videoelem.addEventListener(evname, prev.listener)))
}

const oncontentload = (ss, cfg, gr, nd, pg, prev) => {
  const nodekey = nd.get('key')
  const videoelem = pg.getvideoelem(nd, pg)

  // buffering state
  prev.bufferingDetected = false
  prev.lastPlayPos = 0

  if (videoelem) {
    pg.addlisteners(ss, cfg, prev, videoelem, e => {
      const node = cfg.graph.get(nodekey)
      const handler = pg[`on${e.type}`]

      if (node && handler)
        handler(ss, cfg, gr, node, prev, e, videoelem)
    })

    if (videoelem.readyState === HAVE_ENOUGH_DATA) {
      pg.oncanplay(ss, cfg, gr, nd, pg, prev, {}, videoelem)
    }
  }
}

const play = (ss, cfg, gr, nd, pg) => {
  const videoelem = pg.getvideoelem(nd, pg)

  if (videoelem)
    videoelem.play()
}

const pause = (ss, cfg, gr, nd, pg) => {
  const videoelem = pg.getvideoelem(nd, pg)

  if (videoelem)
    videoelem.pause()
}

const reload = (ss, cfg, gr, nd, pg) => {
  const videoelem = pg.getvideoelem(nd, pg)

  if (videoelem) {
    pg.publish(ss, cfg, nd, { canplay: false }, () => {
      videoelem.load()
    })
  }
}

const volume = (ss, cfg, gr, nd, pg, prev, subj) => {
  const videoelem = pg.getvideoelem(nd, pg)

  if (videoelem)
    (videoelem.volume = Number(subj.volume))
}

const ispaused = (nd, pg) => {
  const videoelem = pg.getvideoelem(nd)

  return Boolean(videoelem && videoelem.paused)
}

const isplaypausestate = (nd, pg, playpausestate, ispaused) => (
  ispaused = pg.ispaused(nd, pg),
  Boolean((playpausestate === 'play' && !ispaused) ||
          (playpausestate === 'pause' && ispaused)))

const onbuffering = (ss, cfg, gry, nd, pg) => (
  pg.publish(ss, cfg, nd, { isbuffering: true }))

const onbufferingended = (ss, cfg, gry, nd, pg) => (
  pg.publish(ss, cfg, nd, { isbuffering: false }))

// checking offset, e.g. 1 / 50ms = 0.02
// isplaycontinue(videoelem, lastPlayPos, offsetms)
const isplaycontinue = () => true
// console.log('isplaycontinue',
//            Boolean(videoelem.currentTime > (lastPlayPos + (1/offsetms))),
//            videoelem.currentTime, (lastPlayPos + (1/offsetms)))
//
// return Boolean(videoelem.currentTime > (lastPlayPos + (1/offsetms)))

const isbuffering = (videoelem, pg, lastPlayPos, offsetms) => (
  Boolean(
    videoelem && !videoelem.paused &&
      !pg.isplaycontinue(videoelem, lastPlayPos, offsetms)))

const isnotbuffering = (videoelem, pg, lastPlayPos, offsetms) => (
  Boolean(
    videoelem && !videoelem.paused &&
      pg.isplaycontinue(videoelem, lastPlayPos, offsetms)))

// http://stackoverflow.com/questions/21399872/ \
//   how-to-detect-whether-html5-video-has-paused-for-buffering
const checkvideo = lockfnthrottling({
  isendthrottlecall: false,
  ms: 60
}, (ss, cfg, gr, nd, pg, prev, fn) => {
  let videoelem = pg.getvideoelem(nd, pg),
      datenow = Date.now(),
      offsetms = datenow - (prev.lastdate || 60),
      { bufferingDetected,
        lastPlayPos } = prev

  if (!videoelem) {
    return null
  }

  if (!bufferingDetected
      && pg.isbuffering(videoelem, pg, lastPlayPos, offsetms)) {
    fn(BUFFERINGOUT)
    prev.bufferingDetected = true
  } else if (bufferingDetected
             && pg.isnotbuffering(videoelem, pg, lastPlayPos, offsetms)) {
    fn(BUFFERING)
    prev.bufferingDetected = false
  }

  prev.lastdate = datenow
  prev.lastPlayPos = videoelem.currentTime
})

const onframe = (ss, cfg, gr, nd, pg, prev, subj) => {
  // polling the video element to discover buffer state is change
  // if buffering, video is in a 'loading' state
  // prev.lastPlayPos = o.getvideoelem(node).currentTime
  pg.checkvideo(ss, cfg, gr, nd, pg, prev, event => {
    if (event === BUFFERING) {
      pg.onbufferingended(ss, cfg, gr, nd, prev, subj)
    } else if (event === BUFFERINGOUT) {
      pg.onbuffering(ss, cfg, gr, nd, prev, subj)
    }
  })
}

const onsubj = (ss, cfg, gr, nd, pg, prev, subj) => {
  const { postplaypause, buf_timess_current, volume } = subj

  if (prev.volume !== volume) {
    prev.volume = volume

    pg.volume(ss, cfg, gr, nd, pg, prev, subj)
  } else if (prev.postplaypause !== postplaypause &&
             !pg.isplaypausestate(nd, pg, postplaypause)) {
    prev.postplaypause = postplaypause

    if (!/play|pause/.test(postplaypause)) {
      console.log(nd, `invalid playpause state: ${postplaypause}`)
    } else if (/play/.test(postplaypause)) {
      pg.play(ss, cfg, gr, nd, pg, prev, subj)
    } else if (/pause/.test(postplaypause)) {
      pg.pause(ss, cfg, gr, nd, pg, prev, subj)
    }
  } else if (prev.buf_timess_current !== buf_timess_current) {
    pg.setcurrenttime(nd, pg, buf_timess_current)

    prev.buf_timess_current = buf_timess_current
  }

  return true
}

export default Object.assign({}, gnpgui, {
  type: 'uivideo',
  getvnode,
  get_buffer_state,
  getdurationss,
  getperss,
  buffer_is_buffered_parts,
  buffer_is_not_time_buffered,
  get_buffer_state_videoelem,
  gettimeatposition,
  getvideoelem,
  getsourceelem,
  setcurrenttime,
  loadsrc,
  timeupdate,
  onended,
  onplay,
  onpause,
  oncanplay,
  ontimeupdate,
  addlisteners,
  oncontentload,
  play,
  pause,
  reload,
  volume,
  ispaused,
  isplaypausestate,
  onbuffering,
  onbufferingended,
  isplaycontinue,
  isbuffering,
  isnotbuffering,
  checkvideo,
  onframe,
  onsubj
})
