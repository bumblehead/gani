// Filename: uierror.js
// Timestamp: 2018.12.09-00:11:01 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>
//
// show error or other type of message
//
// click ok/close link to make the error fade away

import {
  gndom_evidkeyarrget,
  gndom_nodeelemtextcontentset,
  gndom_nodeelemclasstoggle
} from '../gndom.js'

import uisvgsymbol from './uisvgsymbol.js'
import gnpgui from '../gnpgui.js'

const getvnode = (ss, cfg, gr, nd, pg, subj) => {
  const { div, span, a } = cfg.h(pg, nd)
  const {
    labelprimary,
    labelsecondary,
    error,
    svgsymbol,
    textsymbol,
    isopen
  } = subj

  return (
    div(`.:ui.st-isopen-${Boolean(isopen)}#:key`, [
      labelprimary &&
        span('.:ui-label-primary', labelprimary),
      labelsecondary &&
        span('.:ui-label-secondary', labelsecondary),

      span('.:ui-error#:key---error', String(error)),

      a('.:ui-link#:key---close', { href: '' }, [
        svgsymbol &&
          uisvgsymbol.getvnode(ss, cfg, gr, nd, { svgsymbol }),
        textsymbol &&
          span('.:ui-link-symbol', textsymbol)
      ])
    ])
  )
}

const onsubj = (ss, cfg, gr, nd, pg, prev, { error, isopen }) => (
  gndom_nodeelemtextcontentset(cfg, nd, 'error', error),
  gndom_nodeelemclasstoggle(cfg, nd, 'st-isopen', isopen))

const onclick = (ss, cfg, gr, nd, pg, prev, subj, ev) => {
  const partkey = (gndom_evidkeyarrget(cfg, ev) || [])[1]

  if (partkey === 'close') {
    ev.preventDefault()

    pg.publish(ss, cfg, nd, { isopen: false }, () => {
      gndom_nodeelemclasstoggle(cfg, nd, 'st-isopen', false)
    })
  }
}

export default Object.assign({}, gnpgui, {
  type: 'uierror',
  getvnode,
  onclick,
  onsubj
})
