// Filename: canvshapedemo.js
// Timestamp: 2018.12.09-00:06:41 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import canvthree from './canvthree.js'

const enumGeometryTypeDODECAHEDRON = 'dodecahedron'
const enumGeometryTypeTETRAHEDRON = 'tetrahedron'
const enumGeometryTypeICOSAHEDRON = 'icosahedron'

const getyawobj = (camera, THREE) => {
  const bodygroup = new THREE.Object3D()
  const headgroup = new THREE.Object3D()

  bodygroup.name = 'bodygroup'
  headgroup.name = 'headgroup'
  camera.name = 'camera'

  bodygroup.add(headgroup)
  headgroup.add(camera)

  return [bodygroup, headgroup]
}

const getscene = (ss, cfg, gr, nd, pg, prev, subj) => {
  const THREE = cfg.THREE
  const glcanvas = pg.getresetglcanvas(cfg, nd)
  const glscene = new THREE.Scene()
  const glrenderer = pg.getrenderereffect({
    isvrmodesupport: subj.isvrmodesupport,
    iswebgl: true
  }, nd, pg.getglrenderer(cfg, THREE, glcanvas))
  const camera = pg.getperspectivecamera(cfg, nd, pg)
  const light = new THREE.DirectionalLight()
  const [bodygroup, headgroup] = pg.getyawobj(camera, THREE)

  glscene.add(bodygroup)
  camera.position.set(0, 300, 400)
  light.position.set(1, 1, 1)

  glscene.add(new THREE.AmbientLight(0xffffff, 2.5))
  glscene.add(light)

  return {
    light,
    raycaster: new THREE.Raycaster(),
    lastintersected: null,
    mouse: new THREE.Vector2(),
    projection: subj.projection,
    bodygroup,
    headgroup,
    glrenderer,
    glcanvas,
    camera,
    glscene
  }
}

const getgeometrytype = (geometrytype, THREE) => {
  let geometry

  switch (geometrytype) {
  case enumGeometryTypeDODECAHEDRON:
    geometry = new THREE.DodecahedronGeometry(250)
    break
  case enumGeometryTypeTETRAHEDRON:
    geometry = new THREE.TetrahedronGeometry(250)
    break
  case enumGeometryTypeICOSAHEDRON:
    geometry = new THREE.IcosahedronGeometry(250)
    break
  default:
    geometry = new THREE.DodecahedronGeometry(250)
  }

  return geometry
}

const getshapemesh = (cfg, pg, shapedetails) => {
  const THREE = cfg.THREE
  const { color, edgecolor, shapetype, opacity = 1 } = shapedetails
  const geometry = pg.getgeometrytype(shapetype, THREE)
  const material = new THREE.MeshToonMaterial({ color, opacity })
  const mesh = new THREE.Mesh(geometry, material)

  mesh.add(pg.getedgehelper(THREE, mesh, edgecolor, opacity))

  return mesh
}

const rebuildscene = (ss, cfg, gr, nd, pg, prev, subj) => {
  pg.disposeNode(prev.glscene)

  delete prev.glscene

  prev = Object.assign(prev, subj)
  prev = Object.assign(
    prev, pg.getscene(ss, cfg, gr, nd, pg, prev, subj))

  return prev
}

const replacemesh = (ss, cfg, gr, nd, pg, prev) => {
  const THREE = cfg.THREE
  const glscene = prev.glscene
  const group = new THREE.Object3D()
  const blue = 'rgb(0, 135, 230)'
  const pink = 'rgb(255, 0, 160)'
  const orange = 'rgb(255, 171, 0)'
  const dodecamesh = pg.getshapemesh(cfg, pg, {
    shapetype: enumGeometryTypeDODECAHEDRON,
    edgecolor: blue,
    color: orange
  })
  const icosahedronmesh = pg.getshapemesh(cfg, pg, {
    shapetype: enumGeometryTypeICOSAHEDRON,
    edgecolor: orange,
    color: pink
  })
  const tetrahedronmesh = pg.getshapemesh(cfg, pg, {
    shapetype: enumGeometryTypeTETRAHEDRON,
    edgecolor: pink,
    color: blue
  })

  dodecamesh.visible = true
  icosahedronmesh.visible = false
  tetrahedronmesh.visible = false

  group.add(dodecamesh)
  group.add(icosahedronmesh)
  group.add(tetrahedronmesh)

  if (glscene) {
    prev.meshgroup = group
    prev.camera.lookAt(group.position)

    glscene.add(group)
  }

  return prev
}

const oncontentload = (ss, cfg, gr, nd, pg, prev, subj) => {
  prev = pg.rebuildscene(ss, cfg, gr, nd, pg, prev, subj)
  prev = pg.replacemesh(ss, cfg, gr, nd, pg, prev, subj)

  prev.frames = 40
  prev.count = +prev.frames
  // use 'curved' npm package
  prev.curve = cfg.curved(1 * 100, 0.018 * 100, 'end')

  prev.mastercurvearr = []

  do {
    prev.mastercurvearr.push(prev.curve(prev.count / prev.frames) / 100)
  } while (--prev.count)

  prev.curvearr = []
  prev.stagetimems = 14000
  prev.stagedate = Date.now()
}

const onframe = (ss, cfg, gr, nd, pg, prev) => {
  if (prev.meshgroup) {
    prev.count += 0.04

    if (Date.now() - prev.stagedate > prev.stagetimems) {
      prev.curvearr = prev.mastercurvearr.slice()
      prev.stagedate = Date.now()

      prev.meshgroup.children.find(({ visible }, i) => {
        if (visible) {
          prev.meshgroup.children[i].visible = false
          prev.meshgroup.children[
            i === 0 ? prev.meshgroup.children.length - 1 : --i
          ].visible = true
        }

        return visible
      })
    }

    prev.meshgroup.position.y = Math.sin(prev.count) * 10
    if (prev.curvearr.length) {
      prev.meshgroup.rotation.y += prev.curvearr.pop()
    } else {
      prev.meshgroup.rotation.y += 0.02
    }
  }

  pg.render(prev)
}

export default Object.assign({}, canvthree, {
  type: 'canvshapedemo',
  getyawobj,
  getshapemesh,
  getscene,
  getgeometrytype,
  rebuildscene,
  replacemesh,
  oncontentload,
  onframe
})
