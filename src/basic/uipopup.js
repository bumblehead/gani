// Filename: uipopup.js
// Timestamp: 2018.12.09-00:26:28 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>
//
// from "screenpop"
// https://github.com/iambumblehead/screenpop
//
// +-------------------------------------------------+
// | sp-layers                                       |
// |   +-----------------------------------------+   |
// |   | sp-layers-layer                         |   |
// |   |   +---------------------------------+   |   |
// |   |   | sp-layers-layer-screen          |   |   |
// |   |   +---------------------------------+   |   |
// |   |   +---------------------------------+   |   |
// |   |   | sp-layers-layer-content         |   |   |
// |   |   +---------------------------------+   |   |
// |   |                                         |   |
// |   +-----------------------------------------+   |
// |                                                 |
// +-------------------------------------------------+

import {
  gndom_nodeelem,
  gndom_winyoffset,
  gndom_evidkeyarrget,
  gndom_nodeelemclasstoggle
} from '../gndom.js'

import {
  gnenumKEY as KEY
} from '../gnenum.js'

import gnpgui from '../gnpgui.js'

// redefine to create window/modal buttons controls
const getvnodetop = () => ''

const getvnodebottom = () => ''

const getvnode = (ss, cfg, gr, nd, pg, subj) => {
  const { div } = cfg.h(cfg, nd)
  const isshow = Boolean(subj.isshow)
  const isload = Boolean(subj.isload)

  return (
    div('.:ui', [
      div(`.:ui-layer.st-isshow-'${isshow}.st-isload-${isload}#:key---layer`, [
        div('.:ui-layer-screen#:key---screen'),
        div('.:ui-layer-load#:key---load'),
        div('.:ui-layer-content#:key---content', [
          // for popup window controls
          pg.getvnodetop(ss, cfg, gr, nd, pg, subj),
          div('.:ui-layer-content-middle', [
            pg.getoptchildvnode(ss, cfg, gr, nd, 'content')
          ]),
          pg.getvnodebottom(ss, cfg, gr, nd, pg, subj)
        ])
      ])
    ])
  )
}

const getlayerelem = (cfg, nd) => gndom_nodeelem(cfg, nd, 'layer')
const getcontentelem = (cfg, nd) => gndom_nodeelem(cfg, nd, 'content')
const getloadelem = (cfg, nd) => gndom_nodeelem(cfg, nd, 'load')
const getscroll = cfg => gndom_winyoffset(cfg)

const getContentT = (cfg, nd, pg) => {
  const contentElem = pg.getcontentelem(cfg, nd)
  if (!contentElem)
    return null

  const top = (contentElem.style.margin.match(/^\d*/) || [])[0]
  const topNum = top && Number.parseFloat(top)

  return Number.isNaN(topNum) ? null : topNum
}

const center = (cfg, nd, pg) => {
  const contentelem = pg.getcontentelem(cfg, nd)
  if (!contentelem)
    return nd

  // use domwh npm package
  const contentWH = cfg.domwh(contentelem)
  const windowWH = cfg.domwh.window()

  // restrict top and left from outside of window
  const posT = Math.max((windowWH[1] - contentWH[1]) / 2, 0)
  const posL = Math.max((windowWH[0] - contentWH[0]) / 2, 0)

  contentelem.style.margin = ':tpx :rlpx 0'
    .replace(/:t/, posT + pg.getscroll())
    .replace(/:rl/gi, posL)

  return nd
}

const centerVert = (cfg, nd, pg) => {
  let contentelem = pg.getcontentelem(cfg, nd, pg),
      hintsize = nd.data.subj.getprop(nd, 'hintsize', 0),
      contentH,
      windowH,
      posT,
      scroll,
      top,
      topold

  if (contentelem) {
    // use domwh npm package
    [, contentH] = cfg.domwh(contentelem)
    ;[, windowH] = cfg.domwh.window()
    topold = pg.getContentT(cfg, nd, pg)
    scroll = pg.getscroll()
    posT = (windowH - contentH) / 2

    if (topold + contentH - hintsize <= scroll) {
      // if bottom 'hint' area is not fully visible...
      top = scroll - contentH + hintsize
    } else if (topold >= scroll && posT + scroll < topold) {
      // if top is below top of viewable area &&
      // if top is below the position it would own if 'centered'...
      // if top is not above visible area when centered
      top = (posT > 0) ? posT + scroll : scroll
    }

    if (typeof top === 'number') {
      contentelem.style.marginTop = ':tpx'.replace(/:t/, top)
    }
  }
}

const pop = (ss, cfg, gr, nd, pg) => {
  const key = nd.get(KEY)

  gndom_nodeelemclasstoggle(cfg, nd, 'st-isload', false)
  gndom_nodeelemclasstoggle(cfg, nd, 'st-isshow', true)

  pg.publish(ss, cfg, key, {
    isload: false,
    isshow: true
  }, (e, gr) => pg.center(gr.get(key), pg))
}

const close = (ss, cfg, gr, nd, pg) => {
  gndom_nodeelemclasstoggle(cfg, nd, 'st-isshow', false)

  pg.publish(ss, cfg, nd, { isshow: false }, (e, gr) => {
    pg.evPublishPOST(ss, cfg, gr, nd, pg, 'onclose')
  })
}

const onclick = (ss, cfg, gr, nd, pg, prev, subj, ev) => {
  const partkey = (gndom_evidkeyarrget(cfg, ev) || [])[1]

  if (partkey === 'load' ||
      partkey === 'screen') {
    pg.close(ss, cfg, gr, nd, pg, subj)
  }
}

const onresize = (ss, cfg, gr, nd, pg) => (
  pg.center(cfg, nd, pg))

export default Object.assign({}, gnpgui, {
  type: 'uipopup',
  getvnodetop,
  getvnodebottom,
  getvnode,
  getlayerelem,
  getcontentelem,
  getloadelem,
  getscroll,
  getContentT,
  center,
  centerVert,
  pop,
  close,
  onclick,
  onresize
})
