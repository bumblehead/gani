// Filename: uiimg.js
// Timestamp: 2017.12.06-17:22:04 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import gnpgui from '../gnpgui.js'

const getvnode = (ss, cfg, gr, nd, pg, subj) => {
  const { div, img, span } = cfg.h(pg, nd)
  const imgsrc = subj.imgsrc
  const labelprimary = subj.labelprimary

  return (
    div('.:ui', [
      labelprimary &&
        span('.:ui-label-primary', labelprimary),
      img('.:ui-img', {
        src: imgsrc,
        alt: labelprimary
      })
    ])
  )
}

export default Object.assign({}, gnpgui, {
  type: 'uiimg',
  getvnode
})
