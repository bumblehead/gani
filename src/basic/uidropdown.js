// Filename: uidropdown.js
// Timestamp: 2018.05.05-00:30:30 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import gnpguipart from '../gnpguipart.js'

import {
  gndom_nodeelem,
  gndom_nodeelemid
} from '../gndom.js'

import {
  gnenumEVTYPEONCHANGE as ONCHANGE
} from '../gnenum.js'

const getvnode = (ss, cfg, gr, nd, pg, subj) => {
  const { label, select, option, span } = cfg.h(pg, nd)
  const {
    disabled,
    labelprimary,
    labelsecondary,
    labeltertiary,
    labelhint,
    isemptyvalue
  } = subj

  return (
    label('.:ui', {
      for: gndom_nodeelemid(nd)
    }, [
      labelprimary &&
        span('.:ui-label-primary', labelprimary),
      labelsecondary &&
        span('.:ui-label-secondary', labelsecondary),
      select('.:ui-select#:key', {
        name: nd.get('name'),
        disabled: disabled ? 'disabled' : ''
      }, [
        isemptyvalue &&
          option('.:ui-select-option', {
            value: ''
          }, labelhint || '---'),
        pg.mapparts(nd, false, ({ isactive, partkey, label }) => (
          option('.:ui-select-option', {
            value: partkey,
            selected: isactive ? 'selected' : ''
          }, label)
        ))
      ]),
      labeltertiary &&
        span('.:ui-label-tertiary', labeltertiary)
    ])
  )
}

const onchange = (ss, cfg, gr, nd, pg) => (
  pg.getlivekeys(ss, cfg, gr, nd, pg, (e, keyarr) => (
    pg.getpartkeysasdata(ss, cfg, gr, nd, pg, keyarr, (e, gr, value) => (
      pg.publish(ss, cfg, nd, { value }, (e, gr) => (
        pg.evPublishPOST(ss, cfg, gr, nd, pg, ONCHANGE)))
    ))
  ))
)

const setactive = (ss, cfg, gr, nd, pg, partkey) => {
  const elem = gndom_nodeelem(cfg, nd)

  Array.from(elem.options || [], option => {
    option.selected = option.value === partkey
  })
}

const onsubj = (ss, cfg, gr, nd, pg, prev, { value }) => {
  pg.getkeysaspartarr(ss, cfg, gr, nd, [value], (e, [newpartial]) => {
    if (e) return console.error(e)

    setactive(ss, cfg, gr, nd, pg, newpartial && newpartial.partkey)
  })

  return true
}

const getlivekeys = (ss, cfg, gr, nd, pg, fn) => {
  const elem = gndom_nodeelem(cfg, nd)

  fn(null, [elem.value || ''])
}

export default Object.assign({}, gnpguipart, {
  type: 'uidropdown',
  getvnode,
  onchange,
  onsubj,
  getlivekeys
})

