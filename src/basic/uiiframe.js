// Filename: uiiframe.js
// Timestamp: 2017.12.06-12:40:05 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import gnpgui from '../gnpgui.js'

const elemsrcisvalid = src => (
  typeof src === 'string' && src.length > 8)

const elemsrchas = iframe => (
  iframe && iframe.hasAttribute('src'))

const elemsrcrm = iframe => (
  iframe.removeAttribute('src'),
  iframe)

const elemsrcset = (iframe, src) => (
  src
    ? iframe.src = src
    : elemsrcrm(iframe),
  iframe)

const elemiframecreate = (cfg, onloadfn) => {
  const iframe = document.createElement('iframe')
  const allowfullscreen = Boolean(cfg.allowfullscreen)
  const wharr = cfg.wharr

  iframe.width = wharr[0]
  iframe.height = wharr[1]
  iframe.frameBorder = '0'
  iframe.allowfullscreen = ''
  elemsrcset(iframe, cfg.src)

  iframe.onerror = e => (
    typeof onloadfn === 'function' && onloadfn(e))
  iframe.onload = () => (
    typeof onloadfn === 'function' && onloadfn(null, cfg))

  iframe.setAttribute('allowfullscreen', allowfullscreen)
  iframe.setAttribute('mozallowfullscreen', allowfullscreen)
  iframe.setAttribute('webkitallowfullscreen', allowfullscreen)

  return iframe
}

export default Object.assign({}, gnpgui, {
  elemsrcisvalid,
  elemsrchas,
  elemsrcrm,
  elemsrcset,
  elemiframecreate
})
