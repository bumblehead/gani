// Filename: uiradio.js
// Timestamp: 2018.05.05-00:34:42 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>
//
// radio components simultanaeously difficult and useful
//  * reference to radio 'group' element is hard to get, needs form or event
//  * 'click' and 'change' events may affect this component
//  * default browser behaviour incl. unexpected page-scrolling on 'focus'
//

import uisvgsymbol from './uisvgsymbol.js'
import gnpguipart from '../gnpguipart.js'

import {
  gndom_evidkeyarrget,
  gndom_inputfromname,
  gndom_elemgetlabel,
  gndom_nodepartelem
} from '../gndom.js'

const getvnode = (ss, cfg, gr, nd, pg, subj) => {
  const { label, span, input, div } = cfg.h(pg, nd)
  const labelprimary = subj.label
  const labelsecondary = subj.labelsecondary
  const labeltertiary = subj.labeltertiary

  return (
    div('.:ui', [
      labelprimary &&
        span('.:ui-label-primary', labelprimary),
      labelsecondary &&
        span('.:ui-label-secondary', labelsecondary),
      labeltertiary &&
        span('.:ui-label-tertiary', labeltertiary),

      pg.mapparts(nd, true, ({
        dockey,
        partkey,
        nodekey,
        isactive,
        svgsymbol,
        label: partlabel,
        labelsecondary: partlabelsecondary
      }) => [
        label('.:ui-item', {
          for: dockey
        }, [
          input('.:ui-input', {
            id: dockey,
            type: 'radio',
            name: nodekey,
            value: partkey,
            // if this is not 'false', refreshing
            // page results in browser-cache selection
            autocomplete: 'off',
            defaultChecked: isactive
          }),
          // to style active radio. ex,
          //
          //  input[type="radio"]:checked + uiradio-item-link
          //
          div('.:ui-item-link', [
            svgsymbol &&
              uisvgsymbol.getvnode(ss, cfg, gr, nd, { svgsymbol }),
            partlabel &&
              span('.:ui-item-label-primary', partlabel),
            partlabelsecondary &&
              span('.:ui-item-label-secondary', partlabelsecondary)
          ])
        ])
      ])
    ])
  )
}

// reference to the radio button 'group' element only obtainable
// through a 'form' element wrapping the input OR through event
// emitted by the group
const onchange = (ss, cfg, gr, nd, pg, prev, subj, ev) => {
  const ndkey = nd.get('key')
  const elemctrl = (ev.target && ev.target.tagName === 'INPUT')
    ? ev.target
    : gndom_inputfromname(cfg, ndkey)
  const partkey = elemctrl.value

  pg.getpartkeyasdata(ss, cfg, gr, nd, pg, partkey, (e, gr, value) => {
    if (e) return console.error(e)

    pg.publish(ss, cfg, ndkey, { value })
  })
}

const onsubj = (ss, cfg, gr, nd, pg, prev, subj) => (
  pg.getkeysaspartarr(ss, cfg, gr, nd, [subj.value], (e, [newpartial]) => {
    if (e) return console.error(e)

    if (newpartial) {
      const elem = gndom_nodepartelem(cfg, nd, newpartial.partkey)
      if (elem) {
        (elem.focus && elem.focus({ preventScroll: true }))
        elem.checked = true
      }
    }
  }),
  true
)

//
// persistance of all values appears to pass through filter for patch process
//
const onclick = (ss, cfg, gr, nd, pg, prev, subj, ev) => {
  if (ev.target.tagName === 'INPUT' || gndom_elemgetlabel(ev.target)) {
    // https://stackoverflow.com/questions/24299567 \
    //   /radio-button-causes-browser-to-jump-to-the-top
    ev.target.focus({ preventScroll: true })
    return
  }

  const keyarr = gndom_evidkeyarrget(cfg, ev)
  const keynode = keyarr[0]
  const keypart = keyarr[1] && (
    subj.toggleaction === 'next'
      ? pg.getkeynext(ss, cfg, gr, nd, keyarr[1])
      : keyarr[1])

  if (keypart) {
    pg.getpartkeyasdata(ss, cfg, gr, nd, pg, keypart, (e, gr, value) => {
      pg.onsubj(ss, cfg, gr, nd, pg, prev, { value }, ev)

      pg.publish(ss, cfg, keynode, { value })
    })
  }
}

export default Object.assign({}, gnpguipart, {
  type: 'uiradio',
  getvnode,
  onchange,
  onsubj,
  onclick
})
