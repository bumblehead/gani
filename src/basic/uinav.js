// Filename: uinav.js
// Timestamp: 2018.12.09-00:27:07 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import gnpguipart from '../gnpguipart.js'

import {
  gndom_nodeelem,
  gndom_evidkeyarrget,
  gndom_elemclasstoggle
} from '../gndom.js'

import {
  gnenumEVTYPEONCLICK as ONCLICK
} from '../gnenum.js'

const getvnode = (ss, cfg, gr, nd, pg, subj) => {
  const { div, ul, li, a, button, span, img } = cfg.h(pg, nd)
  const label = subj.label
  const labelsecondary = subj.labelsecondary
  const islistitemlink = subj.islistitemlink
  const targetelem = islistitemlink === false ? button : a

  return (
    div('.:ui#:key', [
      label &&
        span('.:ui-label-primary', label),

      labelsecondary &&
        span('.:ui-label-secondary', labelsecondary),

      ul('.:ui-list', pg.mapparts(nd, true, ({
        dockey,
        isactive,
        label,
        target,
        imgsrc,
        href
      }) => (
        li('.:ui-listitem', [
          targetelem(`.:ui-listitem-target.st-isactive-${isactive}`, {
            target,
            id: dockey,
            href
          }, [
            imgsrc && img('.img', {
              src: imgsrc,
              alt: label
            }),

            label &&
              span('.:ui-listitem-target-label-primary', label),

            labelsecondary &&
              span('.:ui-listitem-target-label-secondary', labelsecondary)
          ])
        ]))))
    ])
  )
}

const onclick = (ss, cfg, gr, nd, pg, prev, subj, ev) => {
  const partkey = (gndom_evidkeyarrget(cfg, ev) || [])[1]

  if (partkey) {
    pg.getpartkeyasdata(ss, cfg, gr, nd, pg, partkey, (e, gr, value) => (
      pg.publish(ss, cfg, nd, { value }, (e, gr) => (
        pg.evPublishPOST(ss, cfg, gr, nd, pg, ONCLICK)))
    ))
  }
}

const setactive = (ss, cfg, gr, nd, pg, { value }) => (
  pg.mapparts(nd, false, ({ partkey }) => gndom_elemclasstoggle(
    gndom_nodeelem(cfg, nd, partkey), 'st-isactive', partkey === value)))

const onsubj = (ss, cfg, gr, nd, pg, prev, { value }) => (
  pg.setactive(ss, cfg, gr, nd, pg, { value }),
  true)

export default Object.assign({}, gnpguipart, {
  type: 'uinav',
  getvnode,
  setactive,
  onclick,
  onsubj
})
