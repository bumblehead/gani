// Filename: uisvgsymbol.js
// Timestamp: 2017.12.06-17:26:51 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>
//
// <svg>
//   <use xmlns:xlink="http://www.w3.org/1999/xlink"
//        xlink:href="./symbols.sprite.svg#symbolid"></use>
// </svg>
//

import gnpgui from '../gnpgui.js'

const getvnode = (ss, cfg, gr, nd, pg, subj) => {
  const { div, svg } = cfg.h(pg, nd)
  const { use } = cfg.hsvg(pg, nd)
  const { svgsymbol } = subj

  return (
    div('.:ui', [
      svg('', [
        use({ 'xlink:href': svgsymbol })
      ])
    ])
  )
}

export default Object.assign({}, gnpgui, {
  type: 'uisvgsymbol',
  getvnode
})
