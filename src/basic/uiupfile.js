// Filename: uiupfile.js
// Timestamp: 2017.12.06-17:27:27 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import gnpgui from '../gnpgui.js'

const getvnode = (ss, cfg, gr, nd, pg, subj) => {
  const { input, label, span } = cfg.h(pg, nd)
  const {
    primary,
    secondary,
    tertiary,
    disabled,
    value,
    hint
  } = subj

  return (
    label('.:ui', {
      for: nd.get('key'),
      attributes: { tabindex: '0' } // focusable
    }, [
      primary &&
        span('.:ui-label-primary', primary),
      secondary &&
        span('.:ui-label-secondary', secondary),
      input('.:ui-input#:key', {
        title: hint || 'upload',
        type: 'file',
        name: nd.get('name'),
        disabled: disabled && 'disabled',
        value,
        size: '19'
      }),
      tertiary &&
        span('.:ui-label-tertiary', tertiary)
    ])
  )
}

export default Object.assign({}, gnpgui, {
  type: 'uiupfile',
  getvnode
})
