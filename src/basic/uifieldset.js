// Filename: uifieldset.js
// Timestamp: 2017.12.06-17:23:10 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import gnpgui from '../gnpgui.js'

const getvnode = (ss, cfg, gr, nd, pg, subj) => {
  const { fieldset, legend } = cfg.h(pg, nd)
  const { labellegend } = subj

  return (
    fieldset('.:ui', [
      labellegend &&
        legend('.:ui-legend', labellegend),

      pg.getchildvnodearr(ss, cfg, gr, nd)
    ])
  )
}

export default Object.assign({}, gnpgui, {
  type: 'uifieldset',
  getvnode
})
