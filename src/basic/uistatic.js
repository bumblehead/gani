// Filename: uistatic.js
// Timestamp: 2017.12.06-17:26:34 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>
//
// shows static content from a markdown file given on 'subj'...
// replaces keys in subject data with content from inputs array

import gnpgui from '../gnpgui.js'

const getcontent = (ss, cfg, gr, nd, pg, subj) => {
  const content = subj.content || subj.value

  return typeof content === 'string'
    ? content
    : pg.getchildvnodearr(ss, cfg, gr, nd).join('/n')
}

const getvnode = (ss, cfg, gr, nd, pg, subj) => {
  const { div } = cfg.h(pg, nd)

  return (
    div('.:ui', [
      // extra div reduces buggy inferno behaviour
      div('.:ui-content', {
        dangerouslySetInnerHTML: {
          __html: pg.getcontent(ss, cfg, gr, nd, pg, subj)
        }
      })
    ])
  )
}

export default Object.assign({}, gnpgui, {
  type: 'uistatic',
  getcontent,
  getvnode
})
