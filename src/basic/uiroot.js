// Filename: uiroot.js
// Timestamp: 2017.12.06-17:25:48 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import gnpgui from '../gnpgui.js'

const getvnode = (ss, cfg, gr, nd, pg) => {
  const { div } = cfg.h(pg, nd)

  return (
    div('.:ui', pg.getchildvnodearr(ss, cfg, gr, nd))
  )
}

export default Object.assign({}, gnpgui, {
  type: 'uiroot',
  getvnode
})
