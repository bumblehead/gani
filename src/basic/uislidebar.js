// Filename: uislidebar.js
// Timestamp: 2018.12.09-00:25:42 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import gnpgui from '../gnpgui.js'

import {
  gndom_evelemget,
  gndom_docactiveelem,
  gndom_nodeelem,
  gndom_nodeelemid,
  gndom_nodeelemclassupdate
} from '../gndom.js'

const slideresolution = 1000

const getinputelem = (cfg, nd) => (
  gndom_nodeelem(cfg, nd, 'input'))

const getloadelem = (cfg, nd) => (
  gndom_nodeelem(cfg, nd, 'load'))

const isslideactive = (cfg, nd, pg) => {
  const slideelem = pg.getinputelem(cfg, nd)
  const activeelem = gndom_docactiveelem(cfg)

  return Boolean(
    slideelem && activeelem
      && activeelem.isEqualNode(slideelem))
}

// returns element from sequential list correspondant to percentnum
//
// (['one', 'two', 'three', 'four'], 0)  => 'one'
// (['one', 'two', 'three', 'four'], .1)  => 'two'
// (['one', 'two', 'three', 'four'], .49)  => 'two'
// (['one', 'two', 'three', 'four'], .50)  => 'three'
// (['one', 'two', 'three', 'four'], .99)  => 'three'
// (['one', 'two', 'three', 'four'], 1)  => 'four'
const getpercentelem = (percentnum, statearr) => {
  const elemlist = statearr || []
  const len = elemlist.length
  const getper = (elemlist, percentnum) => (
    percentnum <= 1 / elemlist.length
      ? elemlist[0]
      : getper(elemlist.slice(1), percentnum))

  let elem = null
  if (percentnum === 0) {
    [elem] = elemlist
  } else if (percentnum === 1) {
    elem = elemlist[len - 1]
  } else if (len < 3) {
    elem = elemlist[Math.round(percentnum)]
  } else {
    elem = getper(elemlist.slice(1, -1), percentnum)
  }

  return elem
}

const setpercentstate = (ss, cfg, gr, nd, pg, subj, slideper) => (
  gndom_nodeelemclassupdate(
    cfg, nd, 'st-state', pg.getpercentelem(slideper, subj.states)))
//  elemst.up(
//    gndom_nodeelem(cfg, nd),
//    `state-${pg.getpercentelem(slideper, subj.states)}`))

const getslidepos = (cfg, nd, pg) => {
  const slideelem = nd.getinputelem(cfg, nd, pg)

  return slideelem && (slideelem.value / slideelem.max)
}

const getvnode = (ss, cfg, gr, nd, pg, subj) => {
  const { label, progress, input, span, div } = cfg.h(pg, nd)
  const {
    labelprimary,
    labelsecondary,
    percent_current,
    states
  } = subj
  const slidepos = (
    pg.isslideactive(cfg, nd, pg)
      ? pg.getslidepos(cfg, nd, pg) : percent_current)
  const state = pg.getpercentelem(slidepos, states)

  return (
    label(`.:ui.st-state-${state}#:key`, {
      for: gndom_nodeelemid(nd)
    }, [
      labelprimary &&
        span('.:ui-label-secondary', labelprimary),
      labelsecondary &&
        span('.:ui-label-tertiary', labelsecondary),
      div('.:ui-slide', [
        // acutal slide position
        progress('.:ui-slide-load#:key---load', {
          attributes: { tabindex: '0' },
          value: slidepos,
          min: 0,
          max: 1
        }),
        input('.:ui-slide-input#:key---input', {
          type: 'range',
          value: slidepos * pg.slideresolution,
          min: 0,
          max: pg.slideresolution
        })
      ])
    ])
  )
}

const oninput = (ss, cfg, gr, nd, pg, prev, subj, ev) => {
  const inputelem = gndom_evelemget(ev)
  const loadelem = pg.getloadelem(cfg, nd, pg)
  const slideper = inputelem.value / inputelem.max

  loadelem.value = slideper
  pg.setpercentstate(ss, cfg, gr, nd, pg, subj, slideper)
  pg.publish(ss, cfg, nd, {
    percent_current: slideper * subj.end
  })
}

const onsubj = (ss, cfg, gr, nd, pg, prev, subj) => {
  const { percent_current } = subj
  const loadelem = pg.getloadelem(cfg, nd, pg)
  const inputelem = pg.getinputelem(cfg, nd, pg)
  const slidepos = pg.isslideactive(cfg, nd, pg)
    ? pg.getslidepos(cfg, nd, pg) : percent_current

  loadelem.value = slidepos
  inputelem.value = slidepos * pg.slideresolution
  pg.setpercentstate(ss, cfg, gr, nd, pg, subj, slidepos)

  return true
}

export default Object.assign({}, gnpgui, {
  type: 'uislidebar',
  slideresolution,
  getinputelem,
  getloadelem,
  isslideactive,
  getpercentelem,
  setpercentstate,
  getslidepos,
  getvnode,
  oninput,
  onsubj
})
