// Filename: uihidden.js
// Timestamp: 2017.12.11-03:12:59 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import gnpgui from '../gnpgui.js'

const getvnode = (ss, cfg, gr, nd, pg, subj) => {
  const { input } = cfg.h(pg, nd)

  return (
    input('.:ui', {
      type: 'hidden',
      name: nd.get('name'),
      value: subj.value
    })
  )
}

export default Object.assign({}, gnpgui, {
  type: 'uihidden',
  getvnode
})
