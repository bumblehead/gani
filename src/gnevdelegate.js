// Filename: gani_evdelegate.js
// Timestamp: 2018.05.05-00:26:03 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import lockfnthrottling from 'lockfn/throttling'

import {
  gnsess_isrequrlpath
} from './gnsess.js'

import {
  gndom_evkeyget,
  gndom_evcreate,
  gndom_elemgetclickable
} from './gndom.js'

import {
  gnacc_subj
} from './gnacc.js'

import {
  gnnode_pgget
} from './gnnode.js'

import {
  gnpathdef_prefixstrip,
  gnpathdef_haspathstr
} from './gnpathdef.js'

const gnevdelegate_dispatchevnode = (ss, cfg, gr, ndkey, eventtype, ev) => {
  const nd = gr.get(ndkey)
  const ndpg = nd && gnnode_pgget(cfg, gr, nd)
  const pguiobjevfn = ndpg && ndpg[`on${eventtype}`]

  if (typeof pguiobjevfn === 'function') {
    ev = ev || gndom_evcreate(ndkey, eventtype)

    const prev = ndpg.prevget(cfg, nd)
    const subj = gnacc_subj.get(nd)

    pguiobjevfn(ss, cfg, gr, nd, ndpg, prev, subj, ev, ndkey)
  }
}

// collect common/cheap events, delegate them to related node / page object
const gnevdelegate_dispatchev = (ss, cfg, ev, custtype) => {
  const gr = cfg.graph
  const event = ev || window.event
  const key = gndom_evkeyget(cfg, event)
  const eventtype = custtype || event.type

  gnevdelegate_dispatchevnode(ss, cfg, gr, key, eventtype, ev)
}


const gnevdelegate_dispatchevgraph = (ss, cfg, gr, e, evtype) => (
  gr.map(nd => (
    gnevdelegate_dispatchevnode(ss, cfg, gr, nd.get('key'), evtype, e))))

const gnevdelegate_setishover = (ss, cfg, e, boolval) => {
  if (cfg.ishover !== boolval) {
    cfg.ishover = boolval
    return boolval
      ? gnevdelegate_dispatchevgraph(ss, cfg, cfg.graph, e, 'hoverstart')
      : gnevdelegate_dispatchevgraph(ss, cfg, cfg.graph, e, 'hoverend')
  }
}

// use listen to pass cfg...
const gnevdelegate_listen = (ss, cfg, rootelem) => {
  // simplify managemnt of 'hover' in tandem w/ touch device
  [['touchend', false],
    ['touchcancel', false],
    ['mouseout', false]
  ].forEach(([evname, iscapture]) => (
    rootelem.addEventListener(evname, e => (
      gnevdelegate_setishover(ss, cfg, e, false), iscapture))))

  ;[['touchstart', false],
    ['mouseover', false],
    ['mousemove', false]
  ].forEach(([evname, iscapture]) => (
    rootelem.addEventListener(evname, e => (
      gnevdelegate_setishover(ss, cfg, e, true), iscapture))))

  ;[['blur', true],
    ['focus', true],
    ['focusin', false],
    ['focusout', false],
    ['keyup', false],
    ['mouseup', false],
    ['touchend', false],
    ['keydown', false],
    ['mouseout', false],
    ['mousedown', false],
    ['touchstart', false],
    ['touchcancel', false],
    ['change', false],
    ['submit', false],
    ['input', false],
    ['vrdisplaypresentchange', false]
  ].forEach(([evname, iscapture]) => (
    rootelem.addEventListener(evname, ev => (
      gnevdelegate_dispatchev(ss, cfg, ev), iscapture))))
}

const gnevdelegate_dispatchresize = (ss, cfg) => (
  gnevdelegate_dispatchevgraph(ss, cfg, cfg.graph, {}, 'resize'))

const gnevdelegate_listenwindow = (ss, cfg, dom) => {
  const win = (dom && dom.window) || window
  const doc = win.document || document

  win.addEventListener('resize', lockfnthrottling({ ms: 500 }, e => (
    gnevdelegate_dispatchevgraph(ss, cfg, cfg.graph, e, 'resize'))))

  if (cfg.screenfull && cfg.screenfull.raw) {
    doc.addEventListener(cfg.screenfull.raw.fullscreenchange, e => (
      gnevdelegate_dispatchev(ss, cfg, e, 'fullscreenchange')))
  }
}

// note: pathname is empty string '' for links with no href
const gnevdelegate_linkdetail = (elem, cfg) => {
  const win = cfg.dom.window
  const winorigin = win.location.origin
  const winpathname = win.location.pathname
  const prefixdir = cfg.prefixdir
  const linkelem = elem || {}
  const pathname = linkelem.pathname
  const linkorigin = linkelem.origin
  const pathnamenoprefix = gnpathdef_prefixstrip(pathname, prefixdir)
  const linkhash = linkelem.hash
  const ishashpath = Boolean(linkhash)
  const istargetblank = linkelem.target === '_blank'
  const islocalorigin = linkorigin === '' || linkorigin === winorigin
  const islocalpath = islocalorigin &&
    (pathname === '' || pathname === winpathname)

  return {
    pathname,
    pathnamenoprefix,
    linkhash,
    ishashpath,
    istargetblank,
    islocalorigin,
    islocalpath,
    islocalhash: islocalorigin && ishashpath,
    isapppath: islocalorigin && typeof pathname !== 'undefined' && (
      pathname === '' ||
        gnpathdef_haspathstr(cfg.pathdef.pagearr, pathnamenoprefix))
  }
}

const gnevdelegate_listenclick = (ss, cfg, rootelem, renderfn) => [
  ['click', false]
].forEach(([evname]) => (
  rootelem.addEventListener(evname, ev => {
    const key = gndom_evkeyget(cfg, ev)
    const clickelem = gndom_elemgetclickable(ev && ev.target)
    const clickelemisA = (clickelem || {}).tagName === 'A'
    const linkdetail = gnevdelegate_linkdetail(clickelem, cfg)
    const nodedispatch = () => cfg.graph.has(key) &&
      gnevdelegate_dispatchev(ss, cfg, ev)

    if (linkdetail.istargetblank)
      return // do nothing, open in new window

    if (!clickelemisA) {
      return nodedispatch()
    }

    if (linkdetail.isapppath) {
      // skip these: /current/path#anchor
      if (linkdetail.islocalpath) {
        if (linkdetail.linkhash)
          window.location.hash = linkdetail.linkhash

        if (linkdetail.pathname === '')
          nodedispatch()

        return null
      }

      // TODO handle two types of dispatch
      //  1. things that should always dispatch, such as analytics
      //  2. things that should only dispatch, if tree is not reconstructed
      //
      if (linkdetail.pathname === ''
        || gnsess_isrequrlpath(ss, linkdetail.pathname)) {
        // unless local hash link: use default behaviour
        if (clickelemisA && !linkdetail.islocalhash)
          ev.preventDefault()

        nodedispatch()
      } else {
        // opportunity to tree or node dispatch
        renderfn(ev, linkdetail, nodedispatch)
      }
    }
  })
))

export {
  gnevdelegate_dispatchev,
  gnevdelegate_dispatchevnode,
  gnevdelegate_dispatchevgraph,
  gnevdelegate_setishover,
  gnevdelegate_listen,
  gnevdelegate_dispatchresize,
  gnevdelegate_listenwindow,
  gnevdelegate_linkdetail,
  gnevdelegate_listenclick
}
