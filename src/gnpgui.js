// Filename: gani_uidom.js
// Timestamp: 2018.05.05-00:28:36 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import gnpg from './gnpg.js'

import {
  sphnode_resetchilds,
  sphnode_assign
} from 'spectraph/src/sphnode.js'

import {
  gnerr_nodenotfound,
  gnerr_nodechildnotfoundindex,
  gnerr_nodechildnotfoundvnode,
  gnerr_pgreqdfnnotfoundvnode
} from './gnerr.js'

import {
  gnacc_subj
} from './gnacc.js'

/*
import {
  gnkey_docdecodekey,
  gnkey_docencodekey,
  gnkey_docencodekeyarr,
  gnkey_docdecodekeyarr
} from './gnkey.js'
*/

import {
  gnnode_pgget,
  gnnode_graphset,
  gnnode_pgvnodeget
} from './gnnode.js'

import {
  gnenumKEY as KEY,
  gnenumISDOM as ISDOM,
  gnenumNAMEKEY as NAMEKEY,
  gnenumDOMCHILDKEY as DOMCHILDKEY,
  gnenumDOMCHILDKEYARR as DOMCHILDKEYARR
} from './gnenum.js'

const o = Object.assign({}, gnpg)

o.type = 'gnpgui'

// override to mutate or add default properties to spec
o.initnode = nd => sphnode_assign(nd, {
  [ISDOM]: true,
  [DOMCHILDKEY]: {},
  [DOMCHILDKEYARR]: []
})

o.resetnode = nd => (
  sphnode_resetchilds(
    sphnode_assign(nd, {
      [DOMCHILDKEY]: {},
      [DOMCHILDKEYARR]: []
    })))

o.getsubj = (nd, def = {}) => (
  gnacc_subj.get(nd, def))

// TODO cosolidate, use always gnnode_pgvnodeget or getkeyvnode
o.getkeyvnode = (ss, cfg, gr, ndkey) => {
  const nd = gr.get(ndkey)
  const ndpg = nd && gnnode_pgget(cfg, gr, nd)
  if (!nd)
    throw gnerr_nodenotfound(gr, ndkey)

  if (typeof ndpg.getvnode !== 'function')
    throw gnerr_pgreqdfnnotfoundvnode(cfg, ndpg)

  return gnnode_pgvnodeget(ss, cfg, gr, nd, ndpg)
}

o.getchildvnode = (ss, cfg, gr, nd, ndname, required) => {
  const ndkey = nd.get(DOMCHILDKEY)[ndname]
  const vnode = ndkey && o.getkeyvnode(ss, cfg, gr, ndkey)

  if (!ndkey && required !== false)
    throw gnerr_nodechildnotfoundvnode(gr, nd, ndname)

  return vnode
}

o.getchildvnodeelem = (ss, cfg, gr, nd, i) => {
  const nodekey = nd.get(DOMCHILDKEYARR)[i]
  const vnode = nodekey && o.getkeyvnode(ss, cfg, gr, nodekey)

  if (!nodekey || !vnode)
    throw gnerr_nodechildnotfoundindex(gr, nd, i)

  return vnode
}

// get _optional_ vnode for the child node at 'nodename'
o.getoptchildvnode = (ss, cfg, gr, nd, nodename) => (
  o.getchildvnode(ss, cfg, gr, nd, nodename, false))

o.getchildvnodearr = (ss, cfg, gr, nd, fn) => (
  nd.get(DOMCHILDKEYARR).map(
    key => fn
      ? fn(o.getkeyvnode(ss, cfg, gr, key))
      : o.getkeyvnode(ss, cfg, gr, key)
  ))

o.getvnode = (ss, cfg, gr, nd, ndpg) => (
  o.getchildvnodearr(ss, cfg, gr, nd, ndpg))

o.setdomchildkey = (ss, cfg, gr, pnode, cnode) => {
  const childkey = cnode.get(KEY)

  if (cnode.has(NAMEKEY)) {
    pnode.get(DOMCHILDKEY)[cnode.get(NAMEKEY)] = childkey
  } else {
    pnode.get(DOMCHILDKEYARR).push(childkey)
  }

  return gnnode_graphset(gr, pnode)
}

o.onchildadd = (ss, cfg, gr, nd, cnode) => {
  if (cnode.get(ISDOM)) {
    let res = o.setdomchildkey(ss, cfg, gr, nd, cnode)
    gr = res[0]
    nd = res[1]
  }

  return [gr, nd, cnode]
}

o.onattach = (ss, cfg, gr, nd, pg) => (
  typeof pg.oncontentload === 'function'
    && pg.oncontentload(
      ss, cfg, gr, nd, pg, pg.prevget(cfg, nd), pg.getsubj(nd)))

export default o
