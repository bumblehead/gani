// Filename: gani_web.js
// Timestamp: 2018.12.09-00:12:32 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import domhis from 'domhis'
import fnguard from 'fnguard'

import {
  gninit_run0,
  gninit_run1
} from './gninit.js'

// import gncfg from './gncfg.js'
// import {
//   gncfg_init0_seedenv,
//   gncfg_init1_loadmanifest,
//   gncfg_init2_seedsess
// } from './gncfg.js'

import {
  gnfps_getfpsobj,
  gnfps_rafstart
} from './gnfps.js'

import {
  gnnode_metaget,
  gnnode_pgonsubj,
  gnnode_dfsfind,
  gnnode_dfshydrateall,
  gnnode_dfsattachchilds
} from './gnnode.js'

import {
  gnpatch_unitsfromkeyns,
  gnpatch_unitapplydeepall
} from './gnpatch.js'

import {
  gngraph_setnode,
  gngraph_renderpath,
  gngraph_onframekeyarr,
  gngraph_renderdiffpath,
  gngraph_attacheventsalld
} from './gngraph.js'

import {
  gnreport_getdelayunitarr
} from './gnreport.js'

import {
  gnpublish_vtree,
  gnpublish_domgraph,
  gnpublish_graphsubj,
  gnpublish_persistgraph
} from './gnpublish.js'

import {
  gnevdelegate_dispatchresize,
  gnevdelegate_listenwindow,
  gnevdelegate_listenclick,
  gnevdelegate_listen
} from './gnevdelegate.js'

import {
  gnerr
} from './gnerr.js'

import {
  gnlog_nodepopulated,
  gnlog_nodepatchempty
} from './gnlog.js'

import {
  gnacc_subj
} from './gnacc.js'

import {
  gnpathdef_prefixresolverootspec
} from './gnpathdef.js'

import {
  gnenumKEY as KEY,
  gnenumSUBJ as SUBJ
} from './gnenum.js'

const subjget = gnacc_subj.get

const gnweb_dispatchresize = gnevdelegate_dispatchresize

const gnweb_delaypopulated = (ss, cfg, gr, nddelay, rep, fn) => {
  fnguard.isobjlike(gr, nddelay).isobj(ss, cfg).isany(fn)

  gnnode_dfshydrateall(ss, cfg, gr, nddelay, rep, (e, gr, nddelay) => {
    if (e) return fn(e)

    const nodekey = nddelay.get(KEY)
    const valobj = gnacc_subj.get(nddelay)
    const keyns = [nodekey, SUBJ, valobj]
    gnpatch_unitsfromkeyns(ss, cfg, gr, keyns, (e, pxuarr) => {
      if (e) return fn(e)

      if (pxuarr.length === 0) {
        gnlog_nodepatchempty(cfg, gr, gr.get(nodekey), valobj)
      }

      gnpatch_unitapplydeepall(ss, cfg, gr, pxuarr, (e, gr, rep) => {
        if (e) return fn(e)

        fn(null, gr, rep)
      })
    })
  })
}

const gnweb_renderprogressive = (ss, cfg, gr, delaykeyarr, rep, fn) => {
  const delaykey = delaykeyarr[0]
  let nddelay = delaykey && gr.get(delaykey),
      res

  if (!delaykey) {
    return fn(null, gr, rep)
  }

  res = gngraph_setnode(gr, nddelay.set('isdelay', 0))
  gr = res[0]
  nddelay = res[1]

  gnweb_delaypopulated(ss, cfg, gr, nddelay, rep, (e, gr, rep) => {
    if (e) return console.error(e)

    gnpublish_domgraph(ss, cfg, gr, (e, gr) => {
      if (e) return fn(e)

      gnpublish_persistgraph(cfg, gr)
      cfg.graph = gr

      gnweb_renderprogressive(ss, cfg, gr, delaykeyarr.slice(1), rep, fn)
    })
  })
}

const gnweb_renderpatherr = (ss, cfg, path, e) => {
  const graph = cfg && cfg.graph
  const specerrfn = cfg && cfg.specerrfn
  const rnode = graph && graph.get('/')

  if (specerrfn && rnode) {
    cfg.specerrfn(ss, cfg, graph, rnode, e)
  }

  return gnerr(cfg, e)
}

const gnweb_updatepagemeta = (ss, cfg, subj, pnodearr) => (
  cfg.dom.window.document.title =
    cfg.pagemetaget(ss, cfg, subj, pnodearr).title)

const gnweb_win_render = (ss, cfg, gr, path, hash, fn) => {
  const renderid = Number(++cfg.lastrenderid)
  const iscontinuefn = () => cfg.lastrenderid === renderid

  fn = typeof fn === 'function' ? fn : () => {}
  ss.requrl = path

  gngraph_renderdiffpath(ss, cfg, gr, path, ss.objstack, (grpartial, fn) => (
    // why not 'reactively' reconstruct this portion of graph
    // by defining relationshiop on vnode?
    //
    // allows path permissions to be controlled in centralised location
    // makes the data value here available to rest of graph construction
    // and publishes any resultant side-effect patches to document
    // graph is incomplete, so 'false' prevents graph from being persisted
    // and erroneously reused by render calls from different stack
    gnpublish_graphsubj(ss, cfg, grpartial, [['/dataenv', SUBJ, {
      requrl: path
    }]], fn, false)
  ), iscontinuefn, (e, gr, pnodearr, objstack, rootnode, vtree, report) => {
    if (e) return gnweb_renderpatherr(ss, cfg, path, e)
    if (!iscontinuefn) return null

    ss.objstack = objstack

    domhis.pushState(path)

    gnnode_dfsfind(ss, cfg, gr, rootnode, (ss, cfg, gr, nd) => (
      gnnode_metaget(nd)
    ), (e, nd) => {
      if (e) return e

      gnweb_updatepagemeta(ss, cfg, nd ? subjget(nd) : {}, pnodearr)
    })

    gnlog_nodepopulated(cfg, rootnode.get(KEY), report)
    gnpublish_vtree(ss, cfg, gr, vtree, cfg.dom)

    if (hash && cfg.dom)
      cfg.dom.window.location.hash = hash

    gngraph_attacheventsalld(ss, cfg, gr)

    fn(null, gr)
  })
}

const gnwebprobelocaleenv = (sess, cfg) => {
  const manifestlocales = cfg.manifest.locales // comma-separated list
  const manifestlocaleslist = manifestlocales.split(',')
  if (manifestlocaleslist.length === 1) {
    return manifestlocaleslist[0]
  }

  throw new Error('tbd: needs to get locale from window.navigator')
}

const gnweb_init = async (globalcfg, envcfg, dom = {}, fn) => {
  const win = dom.window || window
  const doc = win.document
  const winlocation = win.location
  const cfgseed = await gninit_run0(globalcfg, {
    // attach dynamic-render behaviour to local links?
    islocallinkdynamic: false,
    probelocaleenv: gnwebprobelocaleenv,
    pagespecfromisopath: async (sess, cfg, isopath) => {
      // "$ROOTSPECS/spec/manifest.json" -> "/spec/manifest.json"
      const isopathenv = gnpathdef_prefixresolverootspec(isopath, cfg.prefixdir)
      if (cfg.speccache[isopathenv])
        return cfg.speccache[isopathenv]

      const res = await fetch(
        gnpathdef_prefixresolverootspec(isopath, cfg.prefixdir)
      ).then(async res => ({
        body: await res.json().catch(() => null),
        status: res.status
      }))

      if (res.body)
        cfg.speccache[isopathenv] = res.body

      return res.body
    },
    ...envcfg
  })
  let cfg = await gninit_run1(cfgseed, {
    requrl: winlocation.pathname,
    token: '1111' // could be a cookie or session value
  }, win)

  const ss = cfg.ss
  const path = cfg.issinglepage
    ? '/'
    : winlocation.pathname

  const res = await gngraph_renderpath(ss, cfg, path).catch(err => {
    gnweb_renderpatherr(ss, cfg, path, err)

    throw new Error(err)
  })

  const gr = res[0]
  const objstack = res[2]
  const rootnode = res[3]
  const vtree = res[4]
  const report = res[5]

  gnlog_nodepopulated(cfg, rootnode.get(KEY), report)

  win.gcfg = cfg
  win.gsess = ss

  ss.objstack = objstack

  gnpublish_vtree(ss, cfg, gr, vtree, dom)
  gngraph_attacheventsalld(ss, cfg, gr)

  // gnevdelegate_listenwindow(sess, cfg, win, dom)
  gnevdelegate_listenwindow(ss, cfg, dom)
  gnevdelegate_listen(ss, cfg, doc.body)

  gnevdelegate_listenclick(ss, cfg, doc.body, (ev, linkdetail, nodedisp) => {
    const path = linkdetail.pathname
    const hash = linkdetail.linkhash

    if (cfg.cfgenv.islocallinkdynamic) {
      ev.preventDefault()

      gnweb_win_render(ss, cfg, cfg.graph, path, hash)
    } else {
      nodedisp()
    }
  })

  cfg.fpsobj = gnfps_getfpsobj()
  cfg = gnfps_rafstart(cfg, cfg.fpsobj.mspf, dom, () => {
    gngraph_onframekeyarr(ss, cfg, cfg.graph, cfg.renderkeyarr)
  })

  if (cfg.cfgenv.islocallinkdynamic) {
    domhis.onpopstate((path, fn) => (
      gnweb_win_render(ss, cfg, cfg.graph, path), fn()))
  }

  gnweb_renderprogressive(
    ss, cfg, gr, gnreport_getdelayunitarr(report), report, (e, gr, rep) => {
      rep.replacearr.map(key => (
        gnnode_dfsattachchilds(ss, cfg, gr, gr.get(key))))

      rep.patchkeyarr.map(patchkey => (
        gnnode_pgonsubj(ss, cfg, gr, gr.get(patchkey))))

      typeof fn === 'function'
        && fn(null, 'rendered')
    })
}

export {
  gnweb_dispatchresize,
  gnweb_delaypopulated,
  gnweb_renderprogressive,
  gnweb_renderpatherr,
  gnweb_updatepagemeta,
  gnweb_win_render,
  gnweb_init
}
