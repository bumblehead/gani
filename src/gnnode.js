import fnguard from 'fnguard'

import {
  sphnode_getchildarr,
  sphnode_rmchildkey,
  sphnode_mvlistelem,
  sphnode_mvarrelem,
  sphnode_getfromjs,
  sphnode_issame,
  sphnode_getas
} from 'spectraph/src/sphnode.js'

import {
  sphfs_getchildkey,
  sphfs_getparentnode,
  sphfs_getrelnode,
  sphfs_getrelpath
} from 'spectraph/src/sphfs.js'

import {
  sphgraph_rmnode,
  sphgraph_setnodeedge,
  sphgraph_setnodechild,
  sphgraph_findkeychild,
  sphgraph_dokeychilds,
  sphgraph_setnode,
  sphgraph_getsiblingprev,
  sphgraph_getsiblingnext,
  sphgraph_getsiblingkeyprev,
  sphgraph_getsiblingkeynext
} from 'spectraph/src/sphgraph.js'

import {
  gnacc_nsmap,
  gnacc_fkey,
  gnacc_base,
  gnacc_init,
  gnacc_full,
  gnacc_subj,
  gnacc_shallowcp,
  gnacc_childsubj,
  gnacc_isvalidfkeyval,
  gnacc_baselist,
  gnacc_groupaccessor
} from './gnacc.js'

import {
  gnspec_childrefget,
  gnspec_nspropexpand,
  gnspec_traitresolve,
  gnspec_traitlistresolve,
  gnspec_childprefromspec,
  gnspec_traitisexternal,
  gnspec_childprefkeyfromdatakey
} from './gnspec.js'

import {
  gnreport_marktime,
  gnreport_markrmtime,
  gnreport_markdelaynode
} from './gnreport.js'

import {
  gnerr_nodechildnameinvalid,
  gnerr_nodespecnameinvalid,
  gnerr_nodensnotfound,
  gnerr_nodepgnotfound,
  gnerr_nodeonsubjmustboolean,
  // gnerr_invalidnodename,
  gnerr_nodedfskeynotfound,
  gnerr_nodespecchildinvalid,
  gnerrnode_childspecnotfound,
  gnerrnode_foreignnamespacenotfound,
  gnerrnode_pathrefnotfound,
  gnerrnode_edgenodenotfound,
  gnerrnode_pathnotfound
} from './gnerr.js'

import {
  gnenumNSPROP,
  gnenumSPECPROPNAMETYPE,
  gnenumNSISSUPPORTEDGERe,
  gnenumFKEYBASEELEM,
  gnenumDEF as DEF,
  gnenumKEY as KEY,
  gnenumSPEC as SPEC,
  gnenumNAMEKEY as NAMEKEY,
  gnenumSPECTYPES as SPECTYPES,
  gnenumCHILD as CHILD,
  gnenumCHILDARR as CHILDARR,
  gnenumDOMCHILDKEY as DOMCHILDKEY,
  gnenumDOMCHILDKEYARR as DOMCHILDKEYARR,
  gnenumISPERSISTEDTS as ISPERSISTEDTS,
  gnenumISDATABLEND,

  gnenumCLASSNAMEROOT,

  gnenumSUBJMETATITLE,
  gnenumSUBJMETADESCRIPTION,

  gnenum_isvalidnameval
} from './gnenum.js'

import {
  gnrun_callfn,
  gnrun_retopt,
  gnrun_getpass,
  gnrun_getopts,
  gnrun_getargs,
  gnrun_valfinish,
  gnrun_getfiltered
} from './gnrun.js'

const optfn = () => {}
const gnnode_create = sphnode_getfromjs
const gnnode_issame = sphnode_issame

const gnnode_datablendfullis = nd =>
  nd.get(gnenumISDATABLEND, false)

// if ns is 'full'
const gnnode_datablendfullisout = (nd, destproptype) => (
  gnnode_datablendfullis(nd) && destproptype === gnacc_full.ns)

// if ns is 'subj'
const gnnode_datablendfullisin = (nd, destproptype) => (
  gnnode_datablendfullis(nd) && destproptype === gnacc_subj.ns)

const gnnode_promisify = fn => (...args) => (
  new Promise((response, error) => fn(...args, (err, ...res) => (
    err ? error(err) : response(res)))))

const gnnode_pgget = (cfg, gr, nd) => {
  const ndpgname = nd.get('node')
  const ndpg = cfg.pgmap[ndpgname]

  if (!ndpg) {
    throw gnerr_nodepgnotfound(gr, nd)
  }

  return ndpg
}

const gnnode_metaget = nd => {
  const metatitle = gnacc_subj.getprop(nd, gnenumSUBJMETATITLE)
  const metadescription = gnacc_subj.getprop(nd, gnenumSUBJMETADESCRIPTION)

  return (metatitle || metadescription) && {
    metatitle,
    metadescription
  }
}

const gnnode_fromspecpginit = (cfg, gr, data) => {
  if (!gnenum_isvalidnameval(data.name))
    throw gnerr_nodespecnameinvalid(gr, null, data)

  const nd = sphnode_getas(data)
  const nd_post = gnnode_pgget(cfg, gr, nd).initnode(nd)

  return nd_post
}

const gnnode_pgreset = (cfg, gr, nd) => (
  gnnode_pgget(cfg, gr, nd).resetnode(nd))

const gnnode_parentget = sphfs_getparentnode

const gnnode_childget = (gr, nd, name) => {
  if (!gnenum_isvalidnameval(name)) {
    throw gnerr_nodechildnameinvalid(gr, nd, { name })
  }

  return gr.get(sphfs_getchildkey(nd, name))
}

// removes child and returns index position of child
//
// 'domchildkey' defined at runtime during parse
const gnnode_childkeyrm = (nd, ckey, childindex = null) => {
  if (nd.has(DOMCHILDKEY) &&
      nd.get(DOMCHILDKEY)[ckey])
    delete nd.get(DOMCHILDKEY)[ckey]

  if (!nd.has(DOMCHILDKEYARR))
    return [nd, null]

  nd = nd.set(DOMCHILDKEYARR, nd.get(DOMCHILDKEYARR)
    .filter((key, i) => (key !== ckey || (childindex = i, false))))

  return [nd, childindex]
}

const gnnode_childsetfromspec = (ss, cfg, gr, nd, ndpg, childspec) => {
  let cnode = gnnode_fromspecpginit(cfg, gr, childspec, nd)

  // copy fkey from parent to child
  if (gnacc_fkey.has(nd)) {
    cnode = gnacc_fkey.assign(cnode, gnacc_fkey.get(nd))
  }

  const res = sphgraph_setnodechild(
    gr, nd, cnode, cnode.get(NAMEKEY, null))
  gr = res[0]
  nd = res[1]
  cnode = res[2]

  // return [gr, nd, cnode]
  return ndpg.onchildadd(ss, cfg, gr, nd, cnode)
}

const gnnode_childgetall = nd => (
  sphnode_getchildarr(nd))

const gnnode_graphset = (gr, nd) => (
  sphgraph_setnode(gr, nd))

const gnnode_graphrm = (gr, nd) => (
  sphgraph_rmnode(gr, nd))

const gnnode_fromrelpath = (gr, nd, path) => (
  sphfs_getrelnode(gr, nd, path))

const gnnode_childgetfirst = (gr, nd) => (
  gr.get(gnnode_childgetall(nd).get(0)))

const gnnode_childkeymv = (nd, ckeyindex, cdomkeyindex) => (
  nd // eslint-disable-next-line max-len
    .set(DOMCHILDKEYARR, sphnode_mvarrelem(nd.get(DOMCHILDKEYARR), cdomkeyindex))
    .set(CHILDARR, sphnode_mvlistelem(gnnode_childgetall(nd), ckeyindex)))

// nsname is namespace from which edgemeta definition
// is constructed
//
// return [gr, nd, edge]
const gnnode_edgeset = (gr, nd, edgenode, nsname, edgemeta) => (
  // node === innode
  // edgenode == outnode
  sphgraph_setnodeedge(
    gr, nd, edgenode, edgemeta.prop,
    edgemeta[gnenumSPECPROPNAMETYPE],
    nsname, edgemeta))

const gnnode_fromspecfiltered = (ss, cfg, gr, nd, props, val, filters, fn) => {
  gnrun_getfiltered(ss, cfg, gr, nd, props, val, filters, (e, end, gr) => {
    fn(e, end, gr)
  })
}

const gnnode_stampmodified = (nd, date = Date.now()) => (
  nd.set(ISPERSISTEDTS, date))

const gnnode_stamppersisted = (nd, date = Date.now()) => (
  nd.set(ISPERSISTEDTS, date))

const gnnode_propset = (ss, cfg, gr, nd, nskey, prop, val) => {
  nd = prop
    ? gnacc_nsmap[nskey].setprop(nd, prop, val)
    : gnacc_nsmap[nskey].set(nd, gnacc_shallowcp(val))

  return gnnode_graphset(gr, nd)
}

const gnnode_pathrefresolve = (ss, cfg, gr, nd, pathrefnskey, pathrefprop) => {
  const dataacc = gnacc_nsmap[pathrefnskey]
  const pathrefval = dataacc && dataacc.getprop(nd, pathrefprop)

  if (!dataacc)
    throw gnerr_nodensnotfound(gr, nd, pathrefprop, pathrefnskey)
  if (!gnacc_isvalidfkeyval(pathrefval))
    throw gnerrnode_pathrefnotfound(gr, nd, pathrefprop, dataacc.get(nd))

  return pathrefval
}

const gnnode_pathresolve = (ss, cfg, gr, nd, opts) => {
  const pathopt = opts.path
  const pathlocal = pathopt && sphfs_getrelpath(nd, pathopt)
  const pathforeign = !pathlocal && opts.fnskey && opts.fnsprop
    && gnnode_pathrefresolve(ss, cfg, gr, nd, opts.fnskey, opts.fnsprop)
  const pathfull = pathlocal || pathforeign

  if (!gnacc_isvalidfkeyval(pathfull)) {
    throw pathlocal
      ? gnerrnode_pathnotfound(gr, nd, pathlocal)
      : gnerrnode_foreignnamespacenotfound(gr, nd, opts)
  }

  return pathfull
}

// `meta` MUST HAVE an explicitly defined path --not the case for 'spec'.
//
// spec may define abstract value such as 'userdata', unknown until runtime,
// but meta here must include the explicit path --the key to the userdata
//
// nsname and nsspec are the incoming node's nsname and spec defined to it
//
const gnnode_edgesetfromspec = (ss, cfg, gr, nd, nsname, nsspec) => {
  const path = gnnode_pathresolve(ss, cfg, gr, nd, nsspec)
  const meta = Object.assign({}, nsspec, { path })
  const mnode = gr.get(meta.path)

  if (!mnode && (DEF in nsspec === false))
    throw gnerrnode_edgenodenotfound(gr, nd, meta.path, nsspec)

  if (mnode) {
    const edgesetres = gnnode_edgeset(gr, mnode, nd, nsname, meta)

    gr = edgesetres[0]
    nd = edgesetres[2]
  }

  return [gr, nd]
}

// naming or these should change... {spec should be...}
// do not like the 'h' not greppable and wtf is it
// h is accumulator for all hydrated valus
// eslint-disable-next-line max-len
const gnnode_prophydratefromspec = (ss, cfg, gr, nd, nhy, nname, nspec, h, fn) => {
  return gnrun_retopt(ss, cfg, gr, nd, nhy, nspec, (e, val, gr) => {
    if (e) return fn(e)

    if (nspec[gnenumSPECPROPNAMETYPE] === gnenumNSPROP) {
      // 'mutates' props everywhere
      if (!nspec.isexpanded)
        Object.assign(nspec, gnspec_nspropexpand(nspec))

      // if no foreign ns, this is a spec type, such as 'literal',
      // which does not require an external reference
      if (nspec.fnsstr &&
        gnenumNSISSUPPORTEDGERe.test(nspec.nskey)
      ) {
        if (nspec.fnsstr) {
          try {
            const res = gnnode_edgesetfromspec(ss, cfg, gr, nd, nname, nspec)

            gr = res[0]
            nd = res[1]
          } catch (e) {
            return fn(e)
          }
        }
      }
    }

    // when spec.name is node namespace like 'full' or 'subj',
    // valfinish copies the value into ns as `{ [ns]: val }`
    //
    // if there is a subject value formed this way,
    // ```
    // requrl: d.typefn('getrequrl')
    // { 'gn:type': 'fn', fnname: 'getrequrl', name: 'requrl' }
    // ```
    //
    // function may return simply '/blog/' or similar and valfinish
    // reassigns value back into hydrtion space `h['requrl'] = val`
    //
    // the exception is when the namespace itself is defined,
    // ```
    // subj: d.typensprop('baselist')
    // { subj: {
    //   'gn:type': 'nsprop', prop: 'baselist', name: 'subj' }
    // }, ...node }
    // ```
    //
    // existance of `gnenumSPECPROPNAMETYPE` signals a spec obj
    const valfin = (
      nspec[gnenumSPECPROPNAMETYPE] === gnenumNSPROP
        && nspec.name === nname)
      ? gnrun_valfinish(cfg, h || {}, {}, gnacc_shallowcp(val))
      : gnrun_valfinish(cfg, h || {}, nspec, gnacc_shallowcp(val))

    fn(null, gr, nd, valfin)
  })
}

const gnnode_prophydratefromspecs = (ss, cfg, gr, nd, nsacc, nhyfn, fn) => {
  const nhy = nhyfn(nd)
  const nspec = nsacc.getspec(nd, [])
  const nname = nsacc.getns()
  const nspecarr = Array.isArray(nspec) ? nspec : [nspec]

  if (!nspecarr.length) {
    return fn(null, gr, nd, null)
  }

  (function next (x, nspecarr, gr, nd, props) {
    if (!x--) return fn(null, gr, nd, props)

    gnnode_prophydratefromspec(
      ss, cfg, gr, nd, nhy, nname, nspecarr[x], props, ((e, gr, nd, res) => {
        if (e) return fn(e)

        next(x, nspecarr, gr, nd, res)
      })
    )
  }(nspecarr.length, nspecarr, gr, nd, {}))
}

const gnnode_prophydratefromspecobj = (sess, cfg, gr, nd, nsacc, nhyfn, fn) => {
  const nspec = nsacc.getspec(nd)
  const nname = nsacc.getns()
  const nhy = nhyfn(nd)

  if (!nspec)
    return fn(null, gr, nd)

  gnnode_prophydratefromspec(sess, cfg, gr, nd, nhy, nname, nspec, {}, (
    (e, gr, nd, res) => {
      if (e) return fn(e)

      res = gnnode_graphset(gr, nsacc.set(nd, res))
      gr = res[0]
      nd = res[1]

      fn(null, gr, nd)
    }))
}

const gnnode_prophydratefromspecarr = (ss, cfg, gr, nd, acc, nhyfn, fn) => {
  gnnode_prophydratefromspecs(ss, cfg, gr, nd, acc, nhyfn, (e, gr, nd, res) => {
    if (e) return fn(e)

    if (res) {
      res = gnnode_graphset(gr, gnnode_stampmodified(acc.assign(nd, res)))
      gr = res[0]
      nd = res[1]
    }

    fn(null, gr, nd)
  })
}


const gnnode_prophydratefromspectree = (ss, cfg, gr, nd, nsacc, nhyfn, fn) => {
  const hydratefn = Array.isArray(nsacc.getspec(nd))
    ? gnnode_prophydratefromspecarr
    : gnnode_prophydratefromspecobj

  return hydratefn(ss, cfg, gr, nd, nsacc, nhyfn, fn)
}

const gnnode_prophydratefncreate = (nsacc, nhyfn) => (
  (ss, cfg, gr, nd, fn) => (
    gnnode_prophydratefromspectree(ss, cfg, gr, nd, nsacc, nhyfn, fn)))

const gnnode_prophydratebaselist = (
  gnnode_prophydratefncreate(gnacc_baselist, gnacc_groupaccessor([
    gnacc_fkey,
    gnacc_base,
    gnacc_init
  ])))

const gnnode_prophydratefull = (
  gnnode_prophydratefncreate(gnacc_full, gnacc_groupaccessor([
    gnacc_baselist,
    gnacc_init
  ])))

const gnnode_prophydratesubj = (
  gnnode_prophydratefncreate(gnacc_subj, gnacc_groupaccessor([
    gnacc_fkey,
    gnacc_init,
    gnacc_full,
    gnacc_base
  ])))

const gnnode_prophydratefkey = (
  gnnode_prophydratefncreate(gnacc_fkey, gnacc_groupaccessor([
    gnacc_fkey
  ])))

const gnnode_prophydratebase = (
  gnnode_prophydratefncreate(gnacc_base, gnacc_groupaccessor([
    gnacc_fkey,
    gnacc_init
  ])))

const gnnode_prophydrateinit = (
  gnnode_prophydratefncreate(gnacc_init, gnacc_groupaccessor([
    gnacc_fkey
  ])))

const gnnode_prophydrateonpost = (sess, cfg, gr, nd, fn) => {
  const ndpg = gnnode_pgget(cfg, gr, nd)

  ndpg.getbaseadapted(sess, cfg, gr, nd, ndpg, (e, gr, nd) => {
    if (e) return fn(e)

    gnnode_prophydratefull(sess, cfg, gr, nd, (e, gr, nd) => {
      if (e) return fn(e)

      gnnode_prophydratesubj(sess, cfg, gr, nd, (e, gr, nd) => {
        if (e) return fn(e)

        ndpg.hydratepartial(sess, cfg, gr, nd, ndpg, (e, gr, nd) => {
          if (e) return fn(e)

          const setres = gnnode_graphset(gr, nd)
          gr = setres[0]
          nd = setres[1]

          if (ndpg.onsubj)
            ndpg.prevget(cfg, nd)

          ndpg.onprerender(sess, cfg, gr, nd, fn)
        })
      })
    })
  })
}

const gnnode_prophydrateonpre = (ss, cfg, gr, nd, rep, fn) => {
  if (nd.get('isdelay')) {
    rep = gnreport_markdelaynode(rep, nd.get('isdelay'), nd.get(KEY))

    return fn(null, gr, nd, rep)
  }

  // See note above `gnenumFKEYBASEELEM` definition
  const fkeybaseelems = gnacc_fkey.hasprop(nd, gnenumFKEYBASEELEM)
    && gnacc_fkey.get(nd)

  gnnode_prophydratefkey(ss, cfg, gr, nd, (e, gr, nd) => {
    if (e) return fn(e)

    if (fkeybaseelems) {
      nd = gnacc_fkey.assign(nd, fkeybaseelems)
    }

    gnnode_prophydrateinit(ss, cfg, gr, nd, (e, gr, nd) => {
      if (e) return fn(e)

      gnnode_prophydratebase(ss, cfg, gr, nd, (e, gr, nd) => {
        if (e) return fn(e)

        gnnode_prophydratebaselist(ss, cfg, gr, nd, (e, gr, nd) => {
          if (e) return fn(e)

          fn(null, gr, nd, rep)
        })
      })
    })
  })
}

const gnnode_pgvnodeget = (ss, cfg, gr, nd, ndpg, subj, ndvn) => (
  ndpg = ndpg || gnnode_pgget(cfg, gr, nd),
  // note: 'subj' and 'prev' are synchronised when vnode is rerendered
  // ephemeral input values for things like text fields can remain
  // unchanged by vdom renders causing un-expected issues.
  //
  // for example, there a spec_fn "cleared" a text field by settings
  // its value to empty string "" and because prev held the initial
  // value of the as empty string "", when subj.value "" was published
  // to the component it was ignored because not different from prev
  // _each component include logic it needs to minimally 'diff' prev
  // to skip or 'onsubj' behaviours that are possibly expensive
  //
  // though different possible solutions, the goal at time of writing
  // is to avoid special-case logic at ephemeral-value components and
  // avoid tracking too many details.
  //
  // 'keyup' event used with text input at situation above triggered
  // rerender, which is probably unwise/expensive. this note to help
  // refresh about why this was done, for when further change is
  // needed later
  //
  // it might make sense for some components to 'synchronise' themselves
  // but leaving this as future problem to solve
  subj = ndpg.prevset(cfg, nd, gnacc_subj.get(nd, {})),
  ndvn = ndpg.getvnode(ss, cfg, gr, nd, ndpg, subj),
  subj[gnenumCLASSNAMEROOT]
    && (ndvn.className += subj[gnenumCLASSNAMEROOT].replace(/\./g, ' ')),
  ndvn
)


const gnnode_childspecatom = (ss, cfg, gr, nd, refspec, spectypes) => {
  const childspecatom = refspec.spec || refspec

  if (typeof childspecatom === 'string') {
    return (spectypes || nd.get(SPECTYPES))[childspecatom]
  }

  return childspecatom
}

// refspec,
// { gn:type: 'local-ref',
//   path: '../root-data-child/eng-US.json' },
// { spec: "user" }
// { spec: { name: "todo", node: "uitodo", child: [] } }
// { spec: "user",
//   specassign: {
//     onsubj: [{ 'gn:type': "fn", fnname: "cacheupdate", args: ["node"] }]
//   }
// }
const gnnode_childspecfromref = async (ss, cfg, gr, nd, refspec, spectypes) => {
  const childspecatom = gnnode_childspecatom(
    ss, cfg, gr, nd, refspec, spectypes)
  const childspecfull = gnspec_traitisexternal(childspecatom)
    ? await gnspec_traitresolve(ss, cfg, childspecatom)
    : childspecatom

  if (!childspecfull) {
    throw gnerrnode_childspecnotfound(gr, nd, refspec, spectypes)
  }

  return refspec.specassign
    ? Object.assign({}, childspecfull, refspec.specassign)
    : childspecfull
}

const gnnode_childspecfiltered = (ss, cfg, gr, nd, spec, fn) => {
  if (!spec.when)
    return fn(null, null, spec)

  const nsacc = gnacc_groupaccessor([
    gnacc_fkey,
    gnacc_init,
    gnacc_base
  ])(nd)

  gnrun_getpass(ss, cfg, gr, nd, nsacc, spec, async (e, errkey) => {
    if (e) return fn(e)

    errkey
      ? fn(null, errkey, null)
      : fn(null, null, await gnnode_childspecfromref(ss, cfg, gr, nd, spec))
  })
}

// returns first child spec that passes filter from parent
const gnnode_childspecwhenparentfirst = (ss, cfg, gr, ndp, fkey, specs, fn) => {
  (function next (x, specs, spec, props) {
    if (!x--) return fn(null, null)

    spec = specs[x]

    if (!spec.when) {
      if (spec.fkey) {
        spec = Object.assign({}, spec, {
          [gnacc_fkey.ns]: gnspec_childprefkeyfromdatakey(spec.fkey, fkey)
        })
      }

      return fn(null, spec)
    }

    props = props || Object.assign({
      init: gnacc_init.get(ndp, {}),
      ...(typeof fkey === 'string' && {
        fkey: Object.assign({}, gnacc_fkey.get(ndp, {}),
          gnspec_childprefkeyfromdatakey(spec.fkey, fkey))
      }),

      // why 'base' added here? base frequently used in 'when' comparisons.
      // exact needed properties unknown until when clause processes them
      base: typeof fkey === 'string'
        ? ({ subj: gnacc_subj.get(gr.get(fkey), {}) })
        : fkey
    })

    gnrun_getpass(ss, cfg, gr, ndp, props, spec, (
      (e, errmsg, ispass) => {
        if (e) return fn(e)

        ispass ? fn(null, spec) : next(x, specs, props)
      }))
  }(specs.length, specs))
}

const gnnode_childspecresolveeachfrombaselist = (
  ss, cfg, gr, nd, childspecs, onspecfn, fn
) => {
  const spectypes = nd.get(SPECTYPES)
  const dataarr = gnacc_baselist.get(nd, [])

  // this loop should only occur in development
  // resolve list of specs in advance, rather than for each child
  if (childspecs && gnspec_traitisexternal(childspecs[0])) {
    return gnspec_traitlistresolve(ss, cfg, childspecs, (e, specsresolved) => {
      gnnode_childspecresolveeachfrombaselist(
        ss, cfg, gr, nd, specsresolved, onspecfn, fn)
    })
  }

  ;(function next (dataarr, x, len, gr, nd, data) {
    if (x >= len) return fn(null, gr, nd)

    data = dataarr[x]

    // eslint-disable-next-line max-len
    gnnode_childspecwhenparentfirst(ss, cfg, gr, nd, data, childspecs, async (e, childspec) => {
      if (e) return fn(e)
      if (!childspec)
        return next(dataarr, ++x, len, gr, nd)

      const childspecatom = gnnode_childspecatom(
        ss, cfg, gr, nd, childspec, spectypes)
      const childspecfull = gnspec_traitisexternal(childspecatom)
        ? await gnspec_traitresolve(ss, cfg, childspecatom)
        : childspecatom

      childspec = gnspec_childprefromspec(
        ss, cfg, gr, nd, data, x, childspec, childspecfull)

      onspecfn(gr, nd, childspec, x, (e, gr, nd) => {
        if (e) return fn(e)

        next(dataarr, ++x, len, gr, nd)
      })
    })
  }(dataarr, 0, dataarr.length, gr, nd))
}

// AN OLD NOTE BELOW
// if addressing this kludge see also
// gnspec_childrefget, which similarly
// delays usage of spec property when namekey is defined
const gnnode_specgetequip = spec => {
  const specobj = spec[NAMEKEY] ? spec : spec[SPEC] || spec

  // copy 'specific' properties, which may be defined on 'spec', to a
  // ganeric resuable 'spec' definition a 'spec.spec'
  // ex, spec.name or spec.base
  // copy base, datakeyprop, name, etc...
  return (spec[SPEC] && spec.specassign)
    ? Object.assign({}, specobj, spec.specassign)
    : specobj
}

// spec may be a single item or a list
// processes each each item regardless of structure
//
// ex,
//
//   { when : { ... }
//     spec : { ... } }
//
//   { when : { ... }
//     spec : [{ ... }, { ... }] }
//
//   { ... }
//
//   [{ ... }, { ... }]
//
//
const gnnode_childspeceach = (ss, cfg, gr, nd, spec, nextfn, fn) => {
  const specobj = gnnode_specgetequip(spec)
  const specobjarr = Array.isArray(specobj) ? specobj : [specobj]

  ;(function next (x, len, specobjarr, gr, nd) {
    if (x >= len) return fn(null, gr, nd)

    nextfn(gr, nd, specobjarr[x], (e, gr, nd) => {
      if (e) return fn(e)

      next(++x, len, specobjarr, gr, nd)
    })
  }(0, specobjarr.length, specobjarr, gr, nd))
}

// mulitple addspecchild may blend childs to single parent node
// eslint-disable-next-line max-len
const gnnode_childspecresolve = (ss, cfg, gr, nd, spect, ndcnum, onspecfn, fn) => {
  if (!cfg.spec.isvalidspec(spect)) {
    return fn(gnerr_nodespecchildinvalid(gr, nd, spect))
  }

  gnnode_childspecfiltered(ss, cfg, gr, nd, spect, (e, errkey) => {
    if (e) return fn(e)

    if (errkey)
      return fn(null, gr, nd, ndcnum)

    gnnode_childspeceach(ss, cfg, gr, nd, spect, async (gr, nd, spec, fn) => {
      if (!cfg.spec.isvalidspec(spec))
        return fn(gnerr_nodespecchildinvalid(gr, nd, spec))

      spec = gnspec_traitisexternal(spec)
        ? await gnspec_traitresolve(ss, cfg, spec)
        : spec

      const childspec = gnspec_childrefget(spec, ++ndcnum)

      onspecfn(gr, nd, childspec, ndcnum, fn)
    }, (e, gr, nd) => {
      if (e) return fn(e)

      fn(null, gr, nd, ndcnum)
    })
  })
}

// is ndcarr a 'node' or a 'spec'?
const gnnode_childspecresolveeachfromtreelist = (
  ss, cfg, gr, nd, childspecs, onchildfn, fn
) => {
  (function next (childspecs, x, count, len, gr, nd) {
    if (x >= len) return fn(null, gr, nd)

    gnnode_childspecresolve(
      ss, cfg, gr, nd, childspecs[x], count, onchildfn, (
        (e, gr, nd, cnodecount) => {
          if (e) return fn(e)

          next(childspecs, ++x, cnodecount, len, gr, nd)
        })
    )
  }(childspecs, 0, 0, childspecs.length, gr, nd))
}

// recursive. deeply nested definitions for different types of collected data
// are flattened to one list. separate page objects can be used for separate
// lists
//
// if baselist, loop data|key list and map child spec to each
// else, loop child list as spec list

// conider the name...
// childspecseach OR
// specchildseach
const gnnode_childspecresolveeach = (ss, cfg, gr, nd, specs, nextfn, fn) => {
  const resolveeachfn = (
    gnacc_baselist.hasspec(nd)
      ? gnnode_childspecresolveeachfrombaselist
      : gnnode_childspecresolveeachfromtreelist)

  queueMicrotask(() => resolveeachfn(ss, cfg, gr, nd, specs, nextfn, fn))
}

// this function is to be called for each child hydrated
//
// creates child->parent edges and accumulates child-derived
// values persisted on parent, using accumulator functions
// described on the parent
//
// no "Great" way to do this. Setup is involved at each node and
// *all* childsubj patterns are looped and applied to the child.
//
// because this function must be called inside the child loop,
// post-child namespaces such as 'subj' and 'full' are unavailable
// from parent

const gnnode_prophydratefromchildsfncreate = (ndp, specarr) => {
  const nsacc = gnacc_childsubj
  const specnsname = nsacc.getns()
  const childnsmap = {}
  const childsubjspecarr = specarr || gnacc_childsubj.getspec(ndp, [])
  let res

  // call function one time with no child
  // to set initial value
  return (ss, cfg, gr, nd, ndc, ndcnum, fn) => {
    if (ndc) {
      res = gnnode_graphset(
        gr, gnacc_fkey.setprop(nd, CHILD, ndc.get(KEY)))
      gr = res[0]
      nd = res[1]
    }

    (async function next (x, specnsspecarr, gr, nd, res) {
      if (!x--) return fn(null, gr, nd, ndc)

      const nsspec = gnspec_nspropexpand(specnsspecarr[x])
      const props = ndc ? childnsmap[ndc.get(KEY)] || (
        childnsmap[ndc.get(KEY)] = {
          childsubj: gnacc_subj.get(ndc, {}) }) : {}
      const specaccum = nsspec.accum

      if (!ndc) {
        res = gnnode_graphset(
          gr, nsacc.setprop(nd, nsspec.name, specaccum.start))
        gr = res[0]
        nd = res[1]

        return queueMicrotask(() => (
          next(x, specnsspecarr, gr, nd, {})))
      }

      gnnode_prophydratefromspec(
        ss, cfg, gr, nd, props, specnsname, specnsspecarr[x], res,
        (e, gr, nd, res) => {
          if (e) return fn(e)

          gnrun_getopts(ss, cfg, gr, nd, props, specaccum, (e, opts, gr) => {
            if (e) return fn(e)

            const args = gnrun_getargs(ss, cfg, gr, nd, specaccum, props, opts)
            const prev = (
              ndcnum === 0
                ? gnacc_shallowcp(specaccum.start)
                : nsacc.getprop(nd, nsspec.name))

            // ( prev, next, index, ... )
            gnrun_callfn(ss, cfg, gr, nd, [
              prev, res[nsspec.name], ndcnum, ...args
            ], specaccum, (e, fin, gr) => {
              if (e) return fn(e)

              res = gnnode_graphset(
                gr, nsacc.setprop(nd, nsspec.name, fin))
              gr = res[0]
              nd = res[1]

              queueMicrotask(() => (
                next(x, specnsspecarr, gr, nd, {})))
            })
          })
        })
    }(childsubjspecarr.length, childsubjspecarr, gr, nd, {}))
  }
}

// TODO default vals should be pre-processed to JSON ahead of time,
// rather than being looped here
const gnnode_childsubjsetdefault = (nd, childsubjspecarr) => {
  if (childsubjspecarr && childsubjspecarr.length) {
    nd = gnacc_childsubj.set(nd, childsubjspecarr.reduce((acc, childsubj) => {
      if (childsubj.accum) {
        acc[childsubj.name] = childsubj.accum.start
      }

      return acc
    }, {}))
  }

  return nd
}

// tree traversal here is intentional. not as simple as basic dfs traversal...
//
// overall result is to hydrate all nodes w/ labels, fulldata/pooldata and
// child nodes to be used throughout this node's lifecycle
//
// just "downward" hydration is insufficient:
//   * prevents node from accessing child node data (most impactful for nodes
//     that manage lists of child nodes)
//
// just "upward" hydration insufficient:
//   * prevents node from generating dynamic node childs, child data is not
//     defined until too late to hydrate
//   * behaviour may be programmed into a node to rebuild itself using
//     traversal here and persisting the result back into graph
//
// this traversal enables dynamic lists... where the definition and total
// size of the node list may be unknown until processed at the parent.
// parent yields childs one at a time, allowing the parent to control the
// process through an async function redefinable by parent for special case.
// Each node is key'd, added to graph and then **traversed BEFORE next
// node is processed**. parent is configured to yield childs async so that
// any possible function is usable here,
//
//   1) DFS ENSURED, ensuring children are fully defined before parent,
//      enabling parent definition to read childs data and apply that
//      date to its own construction...
//
//   2) parent may yield childs in any way needed --from a list, a group of
//      lists, a single definition, a pattern etc. all defintions appear
//      'flatmapped' to parent (multiple lists appear joined as one)
//
//   3) parent has control to stop or continue or skip adding childs
//      ex, searchable and filterable list
//
// the callback and empty 'else' condition for forms that would obtain
// lists of data from an external node or service. assume this function
// can return an array of forms where caller is expected to recurse
// and return those here.
//
const gnnode_dfshydrateall = (ss, cfg, gr, nd, rep, fn, bgnts) => {
  fnguard.isobjlike(gr, nd).isobj(ss, cfg).isany(fn)

  bgnts = bgnts || Date.now()

  gnnode_prophydrateonpre(ss, cfg, gr, nd, rep, (e, gr, nd, rep) => {
    if (e) return fn(e)

    const childsubjspecarr = gnacc_childsubj.getspec(nd, [])

    nd = gnnode_childsubjsetdefault(nd, childsubjspecarr)

    const prophydratefromchilds = (
      gnacc_childsubj.hasspec(nd)
        ? gnnode_prophydratefromchildsfncreate(nd, childsubjspecarr)
        : (ss, cfg, gr, nd, ndc, ndcindex, fn) => fn(null, gr, nd, ndc))

    // memoised to reduce lookups further down sequence
    // where each child is mounted in parent-specific way using ndpg
    const ndpg = gnnode_pgget(cfg, gr, nd)

    gnnode_childspecresolveeach(ss, cfg, gr, nd, nd.get(CHILD, []), (
      (gr, nd, childspec, childnum, nextfn) => {
        const res = gnnode_childsetfromspec(
          ss, cfg, gr, nd, ndpg, childspec)
        const ndc = res[2]
        gr = res[0]
        nd = res[1]

        gnnode_dfshydrateall(ss, cfg, gr, ndc, rep, (e, gr, ndc) => {
          if (e) return fn(e)

          prophydratefromchilds(ss, cfg, gr, nd, ndc, childnum, (e, gr, nd) => {
            if (e) return fn(e)

            nextfn(e, gr, nd)
          })
        })
      }
    ), (e, gr, nd) => {
      if (e) return fn(e)

      gnnode_prophydrateonpost(ss, cfg, gr, nd, (e, gr, nd) => {
        if (e) return fn(e)

        rep = gnreport_marktime(rep, bgnts, nd.get(KEY))

        fn(null, gr, nd, rep)
      })
    })
  })
}

// loop key childs passing sess and cfg to each call
const gnnode_childkeysapplyall = (ss, cfg, gr, nd, dofn, fn = optfn) => {
  sphgraph_dokeychilds(gr, nd, (gr, ckey, i, fn) => (
    dofn(ss, cfg, gr, ckey, fn)
  ), fn)
}

// dofn: fn(null, graph, node)
// fn:  fn(err, graph, node)
const gnnode_childapplyall = (ss, cfg, gr, nd, dofn, fn = optfn) => (
  sphgraph_dokeychilds(gr, nd, (gr, ckey, i, fn) => (
    dofn(ss, cfg, gr, gr.get(ckey), i, fn)
  ), fn))

// returns child node for which 'dofn' returns 'true'
const gnnode_childfind = (ss, cfg, gr, nd, dofn, fn = optfn) => (
  sphgraph_findkeychild(gr, nd, (gr, ckey, i, fn) => (
    dofn(ss, cfg, gr, gr.get(ckey), i, fn)
  ), fn))

// ret, fn(err, graph, node)
//
// rebuild accumulator values...
const gnnode_prophydratefromchildsrefresh = (ss, cfg, gr, nd, fn) => {
  const pkey = nd.get(KEY)
  const ndp = gr.get(pkey)
  const childsubjspecarr = gnacc_childsubj.getspec(nd, [])
  const hydratefromchilds = (
    gnacc_childsubj.hasspec(nd)
      ? gnnode_prophydratefromchildsfncreate(nd, childsubjspecarr)
      : (ss, cfg, gr, nd, ndc, ndcindex, fn) => fn(null, gr, nd, ndc))

  // first call w/out childs to set initial 'start' values
  // which become the final values if no children exist
  hydratefromchilds(ss, cfg, gr, ndp, null, null, (e, gr, ndp) => {
    if (e) return fn(e)

    gnnode_childapplyall(ss, cfg, gr, ndp, (ss, cfg, gr, ndc, i, fn) => {
      hydratefromchilds(ss, cfg, gr, gr.get(pkey), ndc, i, (e, gr, nd) => {
        if (e) return fn(e)

        let res = gnnode_graphset(gr, nd)
        gr = res[0]
        nd = res[1]

        fn(null, gr, nd)
      })
    }, fn)
  })
}

const gnnode_dfsapply = (ss, cfg, gr, nd, applyfn, fn) => {
  const childarr = gnnode_childgetall(nd)

  ;(function dfs (gr, nd, childarr, len, fn, child) {
    if (!len--) return applyfn(gr, nd, fn)

    child = gr.get(childarr.get(len))

    if (!child)
      return fn(gnerr_nodedfskeynotfound(gr, nd, childarr.get(len)))

    gnnode_dfsapply(ss, cfg, gr, child, applyfn, (err, gr) => {
      if (err) return fn(err)

      dfs(gr, nd, childarr, len, fn)
    })
  }(gr, nd, childarr, childarr.count(), fn))
}

const gnnode_dfsfind = (ss, cfg, gr, nd, findfn, fn) => {
  const ndcarr = gnnode_childgetall(nd)

  if (findfn(ss, cfg, gr, nd)) {
    return fn(null, nd)
  }

  (function dfs (gr, nd, ndcarr, len, ndc) {
    if (!len--) return fn(null)

    ndc = gr.get(ndcarr.get(len))

    if (!ndc)
      return fn(gnerr_nodedfskeynotfound(gr, nd, ndcarr.get(len)))

    gnnode_dfsfind(ss, cfg, gr, ndc, findfn, (e, node) => {
      if (e) return fn(e)
      if (node) return fn(null, node)

      return dfs(gr, nd, ndcarr, len, fn)
    })
  }(gr, nd, ndcarr, ndcarr.count()))
}

const gnnode_dfsdetach = (ss, cfg, gr, nd, fn) => (
  gnnode_dfsapply(ss, cfg, gr, nd, (gr, nd, donefn) => {
    const res = gnnode_graphrm(gr, nd)
    gr = res[0]
    nd = res[1]

    donefn(null, gr, nd)
  }, fn))

// attaching events to specific node hierarchy
// specifically replace and attach this node to document
const gnnode_dfsattach = (ss, cfg, gr, nd, fn = optfn) => (
  gnnode_dfsapply(ss, cfg, gr, nd, (gr, nd, donefn) => {
    const pg = gnnode_pgget(cfg, gr, nd)

    pg.onattach(ss, cfg, gr, nd, pg)

    donefn(null, gr, nd)
  }, fn))

const gnnode_dfsdetachchilds = (ss, cfg, gr, nd, fn) => (
  gnnode_childapplyall(ss, cfg, gr, nd, (ss, cfg, gr, ndc, i, fn) => (
    gnnode_dfsdetach(ss, cfg, gr, ndc, fn)), fn))

const gnnode_dfsattachchilds = (ss, cfg, gr, nd, fn) => (
  gnnode_childapplyall(ss, cfg, gr, nd, (ss, cfg, gr, ndc, i, fn) => (
    gnnode_dfsattach(ss, cfg, gr, ndc, fn)), fn))

const gnnode_childrm = (ss, cfg, gr, nd, ckey, fn) => {
  const ndpg = gnnode_pgget(cfg, gr, nd)
  let res,
      childindex,
      domchildindex

  res = ndpg.onrmchild(ss, cfg, gr, nd, ckey)
  gr = res[0]
  nd = res[1]

  gnnode_dfsdetach(ss, cfg, gr, gr.get(ckey), (e, gr, ndc) => {
    if (e) return fn(e)

    res = sphnode_rmchildkey(nd, ckey)
    nd = res[0]
    childindex = res[1]

    res = gnnode_childkeyrm(nd, ckey)
    nd = res[0]
    domchildindex = res[1]

    res = gnnode_graphset(gr, nd)
    gr = res[0]
    nd = res[1]

    fn(null, gr, nd, ndc, childindex, domchildindex)
  })
}

// fn(err, graph, node)
const gnnode_childrmall = (sess, cfg, graph, node, fn) => {
  const nodekey = node.get(KEY)

  gnnode_childkeysapplyall(sess, cfg, graph, node, (
    (sess, cfg, graph, ckey, fn) => (
      gnnode_childrm(sess, cfg, graph, graph.get(nodekey), ckey, fn))
  ), fn)
}

const gnnode_pghydratepartial = (ss, cfg, gr, nd, fn, pg) => (
  pg = gnnode_pgget(cfg, gr, nd),
  pg.hydratepartial(ss, cfg, gr, nd, pg, fn))

const gnnode_dfsrefresh = (ss, cfg, gr, nd, rep, fn, bgnts) => {
  gnnode_dfsdetachchilds(ss, cfg, gr, nd, (e, gr, nd) => {
    if (e) return fn(e)

    rep = gnreport_markrmtime(rep, bgnts || Date.now(), nd.get(KEY))

    let res = gnnode_graphset(gr, gnnode_pgreset(cfg, gr, nd))
    gr = res[0]
    nd = res[1]

    gnnode_dfshydrateall(ss, cfg, gr, nd, rep, fn)
  })
}

// boolean indicates if call was accepted by the component
// if call was not accepted, and caller is patch function,
// caller may choose to apply update with rerender
const gnnode_pgonsubj = (ss, cfg, gr, nd) => {
  const pg = nd && gnnode_pgget(cfg, gr, nd)
  const pgonsubj = pg && pg.onsubj || false
  const pgonsubjres = pgonsubj && pgonsubj(
    ss, cfg, gr, nd, pg, pg.prevget(cfg, nd), gnacc_subj.get(nd, {}))

  if (typeof pgonsubjres !== 'boolean')
    throw gnerr_nodeonsubjmustboolean(gr, nd, pgonsubjres)

  return pgonsubjres
}

const gnnode_pgonframe = (ss, cfg, gr, nd) => {
  const pg = nd && gnnode_pgget(cfg, gr, nd)

  // all objects inherit 'onevframe' from pg,
  // regardless if inheritor defines onframe
  pg && pg.evonframe(ss, cfg, gr, nd, pg)
}

const gnnode_pgonframehas = (ss, cfg, gr, nd) => (
  Boolean(gnnode_pgget(cfg, gr, nd).onframe))

const gnnode_siblingprevget = sphgraph_getsiblingprev
const gnnode_siblingprevkeyget = sphgraph_getsiblingkeyprev
const gnnode_siblingnextget = sphgraph_getsiblingnext
const gnnode_siblingnextkeyget = sphgraph_getsiblingkeynext
const gnnode_siblingnextorprevkeyget = (ss, cfg, gr, nd) => (
  gnnode_siblingnextkeyget(gr, nd) ||
    gnnode_siblingprevkeyget(gr, nd))

export {
  gnnode_promisify,
  gnnode_fromspecpginit,
  gnnode_fromrelpath,
  gnnode_fromspecfiltered,

  gnnode_issame,
  gnnode_create,

  gnnode_datablendfullis,
  gnnode_datablendfullisout,
  gnnode_datablendfullisin,

  gnnode_pgget,
  gnnode_pgreset,
  gnnode_pgvnodeget,
  gnnode_pgonsubj,
  gnnode_pgonframe,
  gnnode_pgonframehas,
  gnnode_pghydratepartial,

  gnnode_metaget,
  gnnode_parentget,

  gnnode_childkeymv,
  gnnode_childkeyrm,
  gnnode_childkeysapplyall,
  gnnode_childgetfirst,
  gnnode_childgetall,
  gnnode_childget,
  gnnode_childsetfromspec,
  gnnode_childsubjsetdefault,
  gnnode_childapplyall,
  gnnode_childfind,
  gnnode_childrm,
  gnnode_childrmall,

  gnnode_childspeceach,
  gnnode_childspecfromref,
  gnnode_childspecfiltered,
  gnnode_childspecwhenparentfirst,
  gnnode_childspecresolve,
  gnnode_childspecresolveeach, // getnextchilds
  gnnode_childspecresolveeachfrombaselist, // mapdata
  gnnode_childspecresolveeachfromtreelist, // mapchild

  gnnode_siblingprevget,
  gnnode_siblingprevkeyget,
  gnnode_siblingnextget,
  gnnode_siblingnextkeyget,
  gnnode_siblingnextorprevkeyget,

  gnnode_edgeset,
  gnnode_edgesetfromspec,

  gnnode_graphset,
  gnnode_graphrm,

  gnnode_stampmodified,
  gnnode_stamppersisted,

  gnnode_pathrefresolve,
  gnnode_pathresolve,

  gnnode_propset,
  gnnode_prophydratefromspecarr,
  gnnode_prophydratefromspecobj,
  gnnode_prophydratefromspectree,
  gnnode_prophydratefromspec,
  gnnode_prophydratefromspecs,
  gnnode_prophydratefromchildsfncreate,
  gnnode_prophydratefromchildsrefresh,
  gnnode_prophydratefncreate,
  gnnode_prophydratebaselist,
  gnnode_prophydratefull,
  gnnode_prophydratesubj,
  gnnode_prophydratefkey,
  gnnode_prophydratebase,
  gnnode_prophydrateinit,

  gnnode_prophydrateonpost,
  gnnode_prophydrateonpre,

  gnnode_dfshydrateall,
  gnnode_dfsapply,
  gnnode_dfsfind,
  gnnode_dfsdetach,
  gnnode_dfsattach,
  gnnode_dfsdetachchilds,
  gnnode_dfsattachchilds,
  gnnode_dfsrefresh
}
