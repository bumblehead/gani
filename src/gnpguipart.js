// Filename: gnpguipart.js
// Timestamp: 2018.01.20-01:48:21 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>
//
// special properties used with partial operations
//
// `full` are elements from a list of data stored on the parent node.
// from each full are generated document-friendly presentation values,
// stored on the node as `fullhydrated`.
//
// Each `fullhydrated` value includes a *unique key* used represent the
// value and other metadata such as *labels* and *sorted position* are also
// generated.

import castas from 'castas'
import fnguard from 'fnguard'

import {
  sphgraph_setnode
} from 'spectraph/src/sphgraph.js'

import gnpgui from './gnpgui.js'

import {
  gnkey_docencodekeyarr
} from './gnkey.js'

import {
  gnerr_nodekeyinvalid,
  gnerr_nodepropselecttypeinvalid,
  gnerr_nodespecfullkeyinvalid,

  gnerr_nodensnotfound
} from './gnerr.js'

import {
  gnspec_nspropexpand,
  gnspec_keyisvalid
} from './gnspec.js'

import {
  gnacc_subj,
  gnacc_part,
  gnacc_full,
  gnacc_cache,
  gnacc_baselist
} from './gnacc.js'

import {
  gnenumSPECPROPNAMETYPE,
  gnenumSELECTTYPESINGLE,
  gnenumSELECTTYPEISSUPPORTED,

  gnenumKEY as KEY,
  gnenumNSPROP as NSPROP,
  gnenumDOMCHILDKEYARR as DOMCHILDKEYARR,
  gnenumISPATCHREHYDRATE as ISPATCHREHYDRATE
} from './gnenum.js'

import {
  gnrun_retobj,
  gnrun_retopt,
  gnrun_retDataWHERE
} from './gnrun.js'

const subjget = gnacc_subj.get
const subjgetprop = gnacc_subj.getprop

const FULLKEY = 'fullkey'
const SUBJKEY = 'subjkey'
const PARTKEY = 'partkey'
const RETURN = 'return'

const o = Object.assign({}, gnpgui)

o.defaultselecttype = gnenumSELECTTYPESINGLE

o.defaultreturnspec = gnspec_nspropexpand({
  name: RETURN,
  [gnenumSPECPROPNAMETYPE]: NSPROP,
  prop: 'part.value'
})

o.defaultsubjkeyspec = gnspec_nspropexpand({
  name: SUBJKEY,
  [gnenumSPECPROPNAMETYPE]: NSPROP,
  prop: 'subj.value',
  // NOTE: this automatically sets a default value of empty string ''
  // without this, runtime error throws when spec resolves `undefined`
  //
  // in practice, 'def' allows one to describe multi-part component such as
  // nav, dropdown or radio w/out specifying empty subj value `{ value: '' }`
  def: ''
})

o.defaultpartkeyspec = {
  name: PARTKEY,
  [gnenumSPECPROPNAMETYPE]: 'objprop',
  prop: 'partkey'
}

o.defaultpartspec = [{
  name: RETURN,
  [gnenumSPECPROPNAMETYPE]: 'this'
}, {
  name: FULLKEY,
  [gnenumSPECPROPNAMETYPE]: 'this'
}]

o.initnode = node => (
  node = node.set(ISPATCHREHYDRATE, (
    node.get(ISPATCHREHYDRATE, true))),
  gnpgui.initnode(node))

o.resetnode = nd => (
  nd = gnacc_part.rm(nd),
  nd = gnacc_full.rm(nd),
  nd = gnacc_full.rmspec(nd),
  gnpgui.resetnode(nd))

o.ispartkeyvalid = partkey => (
  partkey !== undefined)

o.isoptimised = node => (
  gnacc_subj.getprop(node, 'optimised', false))

//
// returns an object usable by uipartial-nodes
//
// keyn should be better documented it is a keyname for
// the data which _can_ be combined with full node 'key'
// to return a unique key for the given partial
//
// not all partials need that
//
o.getpart = (cfg, gr, nd, part, isactive) => {
  if (!gnspec_keyisvalid(part.fullkey)) {
    throw gnerr_nodekeyinvalid(gr, nd, part.fullkey)
  }

  return Object.assign({}, part, {
    partkey: part.fullkey,
    isactive: Boolean(isactive)
  })
}

o.getpartsimple = (cfg, gr, nd, val, isactive) => {
  if (!gnspec_keyisvalid(val)) {
    throw gnerr_nodekeyinvalid(gr, nd, val)
  }

  return {
    partkey: val,
    label: val,
    isactive: Boolean(isactive)
  }
}

// map each part to fn and, if isdockey, generate dockey
//
const mapparts = (nd, isdockey, fn) => {
  const ndkey = isdockey && nd.get(KEY)
  const parts = gnacc_part.get(nd, [])
  const mapfn = isdockey ? (part => fn(Object.assign({
    dockey: gnkey_docencodekeyarr([ndkey, part.partkey]),
    nodekey: ndkey
  }, part))) : fn

  return parts.map(mapfn)
}

o.seqnext = (bgn, end, cur) => cur >= end ? bgn : ++cur
o.seqprev = (bgn, end, cur) => cur <= bgn ? end : --cur

o.getnextelem = (elemlist, elem) => (
  elemlist[o.seqnext(0, elemlist.length - 1, elemlist.indexOf(elem))])

o.getprevelem = (elemlist, elem) => (
  elemlist[o.seqprev(0, elemlist.length - 1, elemlist.indexOf(elem))])

o.getfullobjsortfn = (opts, prop = opts.fullobjprop) => (
  (a, b) => (a[prop] > b[prop]) ? -1 : 1)

// `partial` would be a single option in an input featuring multiple options
// eslint-disable-next-line max-len
// such as a dropdown. 'each requires a partkey, label and return value directives.
// 'partial' is a complete and selectable data
o.getselecttype = (cfg, gr, nd) => {
  const selecttype = subjgetprop(nd, 'selecttype', o.defaultselecttype)

  if (!gnenumSELECTTYPEISSUPPORTED(selecttype))
    throw gnerr_nodepropselecttypeinvalid(gr, nd, selecttype)

  return selecttype
}

// if 'full' object is a page node, it has 'subj',
// else subj is foreign (not generated here)
//
// subj :
//   name      : "global"
//   svgsymbol : "./img/symbol/symbols.sprite.svg#ic_public_24px"
//
o.getfullsubj = full => {
  if (full.subj) {
    full.subj.key = full.key
  }

  return full.subj || full
}

// full, raw data such as original string in a list
// fullsubj, operating subj for this partial
//
// partkey sparated from key in-case data needs to be posted
// partkey should be usable as html input value for, for example,
// forms submitted w/out javascript
o.getpartial = (ss, cfg, gr, nd, full, subj, fn) => {
  const part = typeof full === 'object' ? full : { name: full }
  const partspec = gnacc_part.getspec(nd, [])
  const fullsubj = o.getfullsubj(part)
  const ns = Object.assign({ part }, fullsubj)

  // `multitple`, provide an array of subj,
  // `single`, provide a single subj
  if (o.getselecttype(cfg, gr, nd) === gnenumSELECTTYPESINGLE) {
    subj = [subj]
  }

  // conver part namespace using pattern to full 'part'
  gnrun_retobj(ss, cfg, gr, nd, ns, partspec, (e, part, gr) => {
    if (e) return fn(e)

    if (!o.ispartkeyvalid(part[FULLKEY])) {
      return fn(gnerr_nodespecfullkeyinvalid(
        gr, nd, fullsubj, partspec, part, FULLKEY))
    }

    // subj (array) are raw values from the subj, which may be objects
    // the query describes the value from the subj which should match
    // a value in keyarr
    gnrun_retDataWHERE(ss, cfg, gr, nd, subj, {}, {
      basekey: o.getsubjkeyspec(nd),
      keyarr: [part[FULLKEY]]
    }, (e, resultarr) => {
      if (e) return fn(e)

      fn(null, o.getpart(cfg, gr, nd, part, resultarr.length))
    })
  })
}

// quickly return active key val
// for simple data with mapping 1:1
o.getreturn = (sess, cfg, graph, node, full, fn) => {
  const part = o.getfullsubj(full)
  const returnspec = o.getreturnspec(node)

  return returnspec[gnenumSPECPROPNAMETYPE] === 'this'
    ? fn(null, part, graph)
    : gnrun_retopt(sess, cfg, graph, node, { part }, returnspec, fn)
}

//
// recursively resolve each the 'return' for each 'part'
//
o.getreturnarr = (sess, cfg, graph, node, partarr, fn, sarr = []) => {
  if (partarr.length < 1) {
    return fn(null, sarr, graph)
  }

  o.getreturn(sess, cfg, graph, node, partarr[0], (err, res, graph) => {
    if (err) return fn(err)

    sarr.push(res)

    o.getreturnarr(sess, cfg, graph, node, partarr.slice(1), fn, sarr)
  })
}

const getkeysaspartarr = (ss, cfg, gr, nd, keyarr, fn) => {
  const partarr = gnacc_part.get(nd)

  gnrun_retDataWHERE(ss, cfg, gr, nd, partarr, {}, {
    basekey: o.defaultpartkeyspec,
    keyarr
  }, fn)
}

// 'return' and 'subjkey' patterns are special and applied to full outside
// the 'part' context.
//
// to avoid constructing a 'part' namespace around all base values when
// searching for return, 'part' is instead removed from return pattern.
//
// this allows the host app to use familiar patterns with a 'part' namespace
//
// ex,
//
// { "name" : "return",
//   "gn:type" : "nsprop",
//   "prop" : "part.type" } // part.type becomes type
//
o.adaptreturnspec = retspec => {
  if (retspec.prop) {
    retspec = Object.assign({}, retspec, {
      prop: retspec.prop.replace(/part\./, '')
    })
  }

  return retspec
}

o.getpartkeyspec = node => (
  gnacc_cache.getprop(node, PARTKEY, o.defaultpartkeyspec))

o.getreturnspec = node => (
  gnacc_cache.getprop(node, RETURN, o.defaultreturnspec))

o.getsubjkeyspec = node => (
  gnacc_cache.getprop(node, SUBJKEY, o.defaultsubjkeyspec))

o.getadaptedreturnspec = node => (
  o.adaptreturnspec(o.getreturnspec(node)))

o.getkeysasfullarr = (sess, cfg, graph, node, keyarr, fn) => (
  gnrun_retDataWHERE(sess, cfg, graph, node, gnacc_full.get(node), {}, {
    basekey: o.getadaptedreturnspec(node),
    keyarr
  }, fn))

//
// return value is resolved from base w/ filters
//
const getpartkeysasretarr = (ss, cfg, gr, nd, keyarr, fn) => (
  getkeysaspartarr(ss, cfg, gr, nd, keyarr, (e, partarr, gr) => {
    if (e) return fn(e)

    partarr = partarr.map(part => part[PARTKEY])

    o.getkeysasfullarr(ss, cfg, gr, nd, partarr, (e, fullarr, gr) => {
      if (e) return fn(e)

      o.getreturnarr(ss, cfg, gr, nd, fullarr, fn)
    })
  })
)

// from active key array,
// return final values from corresponding full object(s)
const getpartkeysasdata = (ss, cfg, gr, nd, pg, keyarr, fn) => (
  pg.getpartkeysasretarr(ss, cfg, gr, nd, keyarr, (e, retarr, gr) => {
    if (e) return fn(e)

    if (pg.getselecttype(cfg, gr, nd) === gnenumSELECTTYPESINGLE) {
      retarr = retarr[0]
    }

    fn(null, gr, retarr)
  })
)

const getpartkeyasdata = (ss, cfg, gr, nd, pg, value, fn) => (
  pg.getpartkeysasdata(ss, cfg, gr, nd, pg, [value], fn))

// `full` are usually returned as-is when raw values are expected from
// the list. method is here to be overridden by page objects which reference
// full defined elsewhere (ex., full might be key to node)
o.getfullelemdata = (sess, cfg, graph, node, fullarr, baselist, x) => (
  fullarr[x])

// example full, (horizontal nav)
// {
//   full: [
//     { name: 'About', type: 'about' },
//     { name: 'News', type: 'news' },
//     { name: 'Gallery', type: 'gallery' },
//     { name: 'Media', type: 'media' },
//     { name: 'Links', type: 'links' }
//   ],
//   subj: null
// }
//
// can be expensive
o.getpartialfulldata = (sess, cfg, graph, node) => (
  gnacc_full.get(node, []))

const getpartialarr = (ss, cfg, gr, nd, pg, fn) => {
  fnguard.isobjlike(gr, nd).isobj(ss, cfg).isfn(fn)

  const baselistarr = gnacc_baselist.get(nd, [])
  const subj = subjget(nd, {})

  const sorting = subjgetprop(nd, 'sorting', 'none')
  const fullarr = sorting !== 'none'
    ? gnacc_full.get(nd, []).slice().sort(o.getfullobjsortfn(sorting))
    : gnacc_full.get(nd, [])

  // where optimised,
  // - do not construct input partials
  // - assume input uses same definition for key/value/subj.
  // - do not construct labels and associations for each input
  // - useful for things like month/day/year dropdown
  // eslint-disable-next-line max-len
  // - numbers converted to string -document form controls return string values not numbers
  if (o.isoptimised(nd)) {
    // eslint-disable-next-line max-len
    gnrun_retopt(ss, cfg, gr, nd, { subj }, o.getsubjkeyspec(nd), (err, value, gr) => {
      if (err) return fn(err)

      fn(null, fullarr.map(elem => (
        o.getpartsimple(cfg, gr, nd, elem, value === elem)
      )))
    })
  } else {
    (function next (x, len, gr, nd, partialarr = [], full) {
      if (x >= len) return fn(null, partialarr)

      full = pg.getfullelemdata(
        ss, cfg, gr, nd, fullarr, baselistarr, x, subj)

      o.getpartial(ss, cfg, gr, nd, full, { subj }, (err, partial) => {
        if (err) return fn(err)

        partialarr.push(partial)
        next(++x, len, gr, nd, partialarr)
      })
    }(0, fullarr.length, gr, nd))
  }
}

o.getkeynext = (sess, cfg, graph, node, value) => {
  const keyarr = gnacc_part.get(node, [])
    .map(partial => partial.key)
  const casttype = keyarr.length ? typeof keyarr[0] : 'string'

  if (casttype !== 'string' && castas[casttype]) {
    value = castas[casttype](value)
  }

  return o.getnextelem(keyarr, value)
}

// 'subjkey' and 'return' patterns are included inside the 'part' namespace,
// but aren't needed for constructing partials. Here they are removed from
// 'part' and set to 'subj' over default values...
//
// expand and separate both lists
o.getpartcached = node => {
  let partspec = gnacc_part.getspec(node, []).reduce((prev, spec) => {
    spec = gnspec_nspropexpand(spec)

    if (SUBJKEY === spec.name ||
        PARTKEY === spec.name ||
        RETURN === spec.name) {
      node = gnacc_cache.setprop(node, spec.name, spec)
    } else {
      prev.push(spec)
    }

    return prev
  }, [])

  return gnacc_part.setspec(node, partspec)
}

// optimised defines spec items as a user would,
// next some items are elided and moved to cache store
// to be used outside the default partials pipeline
const hydratepartial = (ss, cfg, gr, nd, pg, fn) => {
  fnguard.isobjlike(gr, nd).isobj(ss, cfg)
  let res

  if (gnacc_baselist.hasspec(nd)) {
    res = sphgraph_setnode(
      gr, gnacc_full.set(nd, nd.get(DOMCHILDKEYARR)))
    gr = res[0]
    nd = res[1]
  }

  if (o.isoptimised(nd)) {
    // values to separated next step below
    res = sphgraph_setnode(
      gr, gnacc_part.setspec(nd, o.defaultpartspec))
    gr = res[0]
    nd = res[1]
  }

  if (gnacc_part.hasspec(nd)) {
    if (!gnacc_part.get(nd)) {
      res = sphgraph_setnode(gr, o.getpartcached(nd))
      gr = res[0]
      nd = res[1]
    }
  } else if (!o.isoptimised(nd)) {
    gnerr_nodensnotfound(gr, nd, gnacc_part.getns())
  }

  getpartialarr(ss, cfg, gr, nd, pg, (e, partialarr) => {
    if (e) return fn(e)

    nd = gnacc_part.set(nd, partialarr)

    res = sphgraph_setnode(gr, nd)
    gr = res[0]
    nd = res[1]

    fn(null, gr, nd)
  })
}

export default Object.assign({}, gnpgui, o, {
  type: 'gnpguipart',
  hydratepartial,
  getkeysaspartarr,
  getpartkeysasretarr,
  getpartkeysasdata,
  getpartkeyasdata,
  mapparts
})
