// Filename: gani_patch.js
// Timestamp: 2018.12.09-00:17:31 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>
//
// let samplepatch = [[
//   ['srckey', 'srcprop', 'type', 'srcpropval'],
//   ['endkey', 'endprop', 'type', 'endpropval'],
// ],[
//   ['srckey', 'srcprop', 'type', 'srcpropval'],
//   ['endkey', 'endprop', 'type', 'endpropval'],
// ]];
//
// naive solution filters data for single node and publishes result to source
// node (if any) then publishes to children
//
// patch is required for the scenario where multiple sources are updated
// patch represents atomic update of multiple nodes
//
// BECAUSE each node stores dependents in a list, directed dependency chains are
// not followed on patch creation. see comment with `o.applypatchunit`
//

import {
  sphgraph_walkedges
} from 'spectraph/src/sphgraph.js'

import {
  gnacc_subj,
  gnacc_childsubj,
  gnacc_full,
  gnacc_nsmap
} from './gnacc.js'

import {
  gnreport_patchget,
  gnreport_addreplacekey,
  gnreport_addreplaceaccumkey,
  gnreport_iskeyreplaced,
  gnreport_iskeypatched,
  gnreport_isnotinitiator,
  gnreport_addpatchkeyout,
  gnreport_addpatch,
  gnreport_end
} from './gnreport.js'

import {
  gnnode_graphset,
  gnnode_fromspecfiltered,
  gnnode_stampmodified,
  gnnode_propset,
  gnnode_pathresolve,
  gnnode_prophydratefromchildsrefresh,
  gnnode_pghydratepartial,
  gnnode_dfsrefresh,
  gnnode_datablendfullisout,
  gnnode_datablendfullisin
} from './gnnode.js'

import {
  gnerr_nodecompositevalueinvalid,
  gnerr_nodensnotfoundedge
} from './gnerr.js'

import {
  gnrun_valfinish
} from './gnrun.js'

import {
  gnspec_nsspecreduce
} from './gnspec.js'

import {
  gnenumKEY as KEY,
  gnenumISDATA as ISDATA,
  gnenumNSPROP as NSPROP,

  gnenumISPATCHREHYDRATE as ISPATCHREHYDRATE,
  gnenumSUBJ as SUBJ,
  gnenumFKEY as FKEY,
  gnenumCHILDSUBJ as CHILDSUBJ,

  // edge token enums
  gnenumREFNAME as REFNAME,
  gnenumTYPE as TYPE,
  gnenumMETA as META,
  gnenumNS as NS
} from './gnenum.js'

const gnpatch_unitcreate = (
  srckey, srcprop, srcns, srcpropval,
  endkey, endprop, endns, endpropval
) => ([
  [srckey, srcprop, srcns, srcpropval, 'src'],
  [endkey, endprop, endns, endpropval, 'end']])

const gnpatch_edgemsgcreate = (
  edgeout, edgeoutns, edgeoutpropval
) => (
  [edgeout, edgeoutns, edgeoutpropval, 'msgedge'])

// const gnpatch_msgcreate = (
//   endkey, endns, props
// ) => (
//   [endkey, endns, props, 'msgkeyns'])

// returns a patch from a page `node` to its source `data` node
const gnpatch_unitupstreamcreate = (ss, cfg, gr, nd, srcns, subjspec, fn) => {
  const srckey = nd.get(KEY)
  const endkey = gnnode_pathresolve(ss, cfg, gr, nd, subjspec)
  const srcfilter = subjspec.filteroutarr
  const srcval = subjspec.patchvalue
  /*
  let endkey

  try {
    endkey = gnnode_pathresolve(ss, cfg, gr, nd, subjspec)
  } catch (e) {
    return fn(e)
  }
  */
  // just a placeholder param to adhere to implied interface
  srcns = subjspec.nskey

  gnnode_fromspecfiltered(
    ss, cfg, gr, nd, {}, srcval, srcfilter, (e, endval, gr) => {
      if (e) return fn(e)

      fn(null, gnpatch_unitcreate(
        srckey, subjspec.patchname, srcns, srcval,
        endkey, subjspec.nsprop, srcns, endval), gr)
    })
}

// patch properties within the same node
//
// patchspec, {
//   patchname: "value",
//   patchvalue: "jkl",
//   ...spec
// }
//
// ns should be stored on edge...
//
const gnpatch_unitlocalcreate = (ss, cfg, gr, nd, srcns, patchspec, fn) => {
  const endkey = nd.get(KEY)
  const srcpropval = patchspec.patchvalue
  const filters = patchspec.filterinarr
  const patchname = patchspec.patchname

  gnnode_fromspecfiltered(
    ss, cfg, gr, nd, {}, srcpropval, filters, (e, endpropval, gr) => {
      if (e) return fn(e)

      fn(null, gnpatch_unitcreate(
        endkey, patchname, srcns, srcpropval,
        endkey, patchname, SUBJ, endpropval), gr)
    })
}

// returns a patch from data `node` to another `data` node, using an edge
const gnpatch_unitedgecreate = (ss, cfg, gr, nd, srcns, edge, fn) => {
  const srckey = nd.get(KEY)
  const endkey = edge.get(KEY)
  const endns = edge.get(NS)
  const refname = edge.get(REFNAME)
  const subj = gnacc_subj.get(nd)
  const srcpropval = subj[refname]
  const meta = edge.get(META)
  const filters = (meta && meta.filterinarr || [])

  gnnode_fromspecfiltered(
    ss, cfg, gr, nd, {}, srcpropval, filters, (e, endpropval, gr) => {
      if (e) return fn(e)

      fn(null, gnpatch_unitcreate(
        srckey, refname, srcns, srcpropval,
        endkey, meta.name, endns, endpropval), gr)
    })
}

// for each spec... list steps here
const gnpatch_unitsfromnsprops = (ss, cfg, gr, nd, nskey, nsval, fn, local) => {
  const nsspecs = gnacc_nsmap[nskey].getspec(nd, [])

  gnspec_nsspecreduce(nsspecs, nsval, [], (acc, props, spec, name, nextfn) => {
    if (name in props === false)
      return nextfn(null, acc)

    // consider checking if the value has changed..
    // may require other changes where unchanged publish is expected
    //
    //   ex, (specname in next && next[specname] !== spec[specname])
    //
    const patchspec = Object.assign({}, spec, {
      patchname: name,
      patchvalue: props[name]
    })

    const unitcreate = (
      !local && patchspec[TYPE] === NSPROP
        ? gnpatch_unitupstreamcreate
        : gnpatch_unitlocalcreate)

    unitcreate(ss, cfg, gr, nd, nskey, patchspec, (e, pxu) => {
      if (e) return fn(e)

      if (pxu !== null)
        acc.push(pxu)

      nextfn(null, acc)
    })
  }, fn)
}

const gnpatch_unitsfromnspropslocal = (ss, cfg, gr, nd, nskey, nsval, fn) => (
  gnpatch_unitsfromnsprops(ss, cfg, gr, nd, nskey, nsval, fn, true))

// possbly, ns should be _not_ embedded inside keyns,
// to allow caller to pass only a set of properties while ns
// attached separately by wrapper or compostion fn
const gnpatch_unitsfromkeyns = (ss, cfg, gr, keyns, fn) => {
  const nd = gr.get(keyns[0])
  const patchfn = nd.get(ISDATA)
    ? gnpatch_unitsfromnspropslocal
    : gnpatch_unitsfromnsprops

  patchfn(ss, cfg, gr, nd, keyns[1], keyns[2], fn)
}

// possbly, ns should be _not_ embedded inside keyns,
// to allow caller to pass only a set of properties while ns
// attached separately by wrapper or compostion fn
const gnpatch_unitsfromkeynsarr = (ss, cfg, gr, keynsarr, fn, pxuarr = []) => {
  if (!keynsarr.length)
    return fn(null, pxuarr, gr)

  gnpatch_unitsfromkeyns(ss, cfg, gr, keynsarr[0], (e, pxu) => {
    if (e) return fn(e)

    pxuarr = pxuarr.concat(pxu)

    gnpatch_unitsfromkeynsarr(ss, cfg, gr, keynsarr.slice(1), fn, pxuarr)
  })
}

// walk edges of data-type node to discover/update dependent nodes
//   if dependent node is data-type node
//       generate patch and apply patch unit to affected data-type node
//
// why are patches BOTH applied AND generated here?
//
//  1. fresh values,
//
//     a patched node will have a completed 'final' filtered subj value.
//     a patched node's contribution to subsequent patches will be fresh.
//
//  2. less looping,
//
//     here we loop through the edges of a src data-type node and with each
//     edge have all needed src and end data for a new patch. this is
//     important because edge is loop-accessible only (as edges are stored in
//     lists) --by building and applying any subsequent patch *here*, we avoid
//     looping elsewhere to obtain the same data:
//
const gnpatch_unitapply = (ss, cfg, gr, pxu) => {
  const px = pxu[1]
  const key = px[0]
  const prop = px[1]
  const type = px[2]
  const val = px[3]

  return gnnode_propset(ss, cfg, gr, gr.get(key), type, prop, val)
}

const gnpatch_edgeapplyrefresh = (ss, cfg, gr, ndsrc, edgeout, record, fn) => {
  const edgekey = edgeout.get(KEY)
  const edgenode = gr.get(edgekey) // endnode

  if (gnreport_iskeyreplaced(record, edgekey)) {
    return fn(null, gr, edgenode, record)
  }

  record = gnreport_addreplacekey(record, edgekey)
  if (!edgenode.get(ISDATA))
    return gnnode_dfsrefresh(ss, cfg, gr, edgenode, record, fn)

  gnpatch_unitedgecreate(ss, cfg, gr, ndsrc, SUBJ, edgeout, (e, pxu, gr) => {
    if (e) return fn(e)

    // destination values at fkey
    // will never have out-going edges, otherwise applypatchunit called
    const patch = gnpatch_unitapply(ss, cfg, gr, pxu)
    const patchgr = patch[0]
    const patchnode = patch[1]

    gnnode_dfsrefresh(ss, cfg, patchgr, patchnode, record, fn)
  })
}

// publishes a composite node value 'out' to rest of graph
// sequence originates at composite-fragment node, where patch
// is first emitted and applied to this node's 'full' value
//
// 1. the 'out-going' full value (@named-property "value") is updated
//    to match 'in-going' full values.
//
// 2. the new full value is used to send patch out to rest of graph
//
// 3. finally, composite-fragment nodes are updated
//
// value of main composite node
//
// x => blend[ x, y ] => [ x, y ]
const gnpatch_compositejoin = (ss, cfg, gr, nd, precord, fn) => {
  const nodekey = nd.get(KEY)
  const value = gnacc_full.get(nd)
  const filters = gnacc_full.getspec(nd).filteroutarr

  gnnode_fromspecfiltered(
    ss, cfg, gr, nd, {}, value, filters, (e, value, gr) => {
      if (e) return fn(e)

      nd = gnacc_subj.assign(nd, value)
      nd = gnacc_subj.setprop(nd, 'value', value)

      let res = gnnode_graphset(gr, nd)
      gr = res[0]
      nd = res[1]

      precord = gnreport_addpatchkeyout(precord, nodekey)

      gnpatch_unitsfromnsprops(
        ss, cfg, gr, gr.get(nodekey), SUBJ, { value }, (e, patch) => {
          if (e) return fn(e)

          // eslint-disable-next-line no-use-before-define
          gnpatch_unitapplydeepall(ss, cfg, gr, patch, (e, gr) => {
            if (e) return fn(e)

            // eslint-disable-next-line no-use-before-define
            gnpatch_compositesplit(ss, cfg, gr, nd, precord, fn)
          }, precord)
        })
    })
}

const gnpatch_edgeapply = (ss, cfg, gr, nd, edgenote, fn) => {
  // 'edgenode' simply to function signature verbosity down
  const edgeout = edgenote[0]
  const nskey = edgenote[1] // why not use ns from edge?
  const val = edgenote[2]
  const edgeoutmeta = edgeout.get(META)
  const edgeoutkey = edgeout.get(KEY)
  const ndoutstart = gr.get(edgeoutkey)
  const accessor = gnacc_nsmap[nskey]
  const ndoutns = accessor.get(ndoutstart)
  const filters = edgeoutmeta.filterinarr
  const props = { [nskey]: ndoutns }

  if (!ndoutns) {
    return fn(gnerr_nodensnotfoundedge(gr, nd, nskey, edgeout.toJS()))
  }

  gnnode_fromspecfiltered(ss, cfg, gr, ndoutstart, props, val, filters, (
    (e, val, gr) => {
      if (e) return fn(e)

      let res, ndout = accessor.assign(
        ndoutstart, gnrun_valfinish(cfg, ndoutns, edgeoutmeta, val))

      res = gnnode_graphset(gr, gnnode_stampmodified(ndout))
      gr = res[0]
      ndout = res[1]

      fn(null, gr, ndout)
    })
  )
}

const gnpatch_compositeisvalid = value => (
  value !== undefined)

// node here is 'datanode'
//
// publishes data 'inward' or 'downard' into the fragmented
// nodes which hold the composited values
const gnpatch_compositesplit = (ss, cfg, gr, nd, rec, fn) => {
  const endkey = nd.get(KEY)
  const val = gnacc_subj.getprop(nd, 'value')
  const fullspec = gnacc_full.getspec(nd)
  const filters = fullspec && fullspec.filterinarr

  if (!gnpatch_compositeisvalid(val) || !fullspec) {
    return fn(gnerr_nodecompositevalueinvalid(gr, nd))
  }

  nd = gnacc_subj.assign(nd, val)

  gnnode_fromspecfiltered(ss, cfg, gr, nd, {}, val, filters, (e, val, gr) => {
    if (e) return fn(e)

    let res = gnnode_graphset(gr, nd)
    gr = res[0]
    nd = res[1]

    // reconcile in-coming subj data w/ full dataa
    // eg, break-apart and distribute composite 'value'
    // need to change order of these... walkedges should be return
    sphgraph_walkedges(gr, nd, null, (gr, nd, edge, fn) => {
      const edgekey = edge.get(KEY)
      if (edgekey === endkey)
        return fn(null, gr, nd)

      const index = edge.get(REFNAME)
      const edgenote = gnpatch_edgemsgcreate(edge, SUBJ, val[index])

      gnpatch_edgeapply(ss, cfg, gr, nd, edgenote, (e, gr, nd) => {
        if (e) return fn(e)

        rec = gnreport_addpatchkeyout(rec, edgekey)

        fn(null, gr, nd)
      })
    }, (e, gr) => fn(null, gr, nd, rec))
  })
}

//
// nothing sets value of text field
//
const gnpatch_unitapplydeep = (ss, cfg, gr, pxu, record, fn) => {
  const end = pxu[1]
  const endkey = end[0]
  const endprop = end[1]
  const endns = end[2]

  // if end node not found, for example, subj node is populated
  // from empty default data
  if (!endkey)
    return fn(null, gr, null, record)

  const unitapplyres = gnpatch_unitapply(ss, cfg, gr, pxu)
  const endnode = unitapplyres[1]
  gr = unitapplyres[0]

  record = gnreport_addpatch(record, pxu)

  // recently added. should possibly be removed.
  // adds data node to patch onsubj updates
  if (gnreport_isnotinitiator(record, endkey))
    record = gnreport_addpatchkeyout(record, endkey)

  if (gnnode_datablendfullisout(endnode, endns)) {
    return gnpatch_compositejoin(ss, cfg, gr, endnode, record, fn)
  }

  sphgraph_walkedges(gr, endnode, endprop, (gr, srcnode, edgeout, fn) => {
    const edgeouttype = edgeout.get(TYPE)
    const edgeoutmeta = edgeout.get(META, {})
    const edgeoutkey = edgeout.get(KEY)
    const nodeout = gr.get(edgeoutkey)

    if (edgeouttype === NSPROP && edgeoutmeta.onpatch === 'refresh') {
    // if (edgeouttype === NSPROP) {
      // node hierarchies relying on dynamic foreign key (fkey)
      // are fully refreshed when referenced key value changes
      return gnpatch_edgeapplyrefresh(
        ss, cfg, gr, srcnode, edgeout, record, fn)
    }

    if (!nodeout || (
      (gnnode_datablendfullisin(nodeout, endns) &&
       gnreport_iskeypatched(record, edgeoutkey)))) {

      // in some cases, edge walk may yield an edge which is removed
      // indirectly through removal of different edge
      //
      // composite node updated once only, else infinite loop
      return fn(null, gr)
    }

    if (edgeoutmeta.accum) {
      record = gnreport_addreplaceaccumkey(record, edgeoutkey)

      return fn(null, gr, endnode, record)
    }

    // eslint-disable-next-line no-use-before-define
    gnpatch_edgeapplydeep(ss, cfg, gr, srcnode, pxu, edgeout, (
      (e, gr, nd, precord) => {
        if (e) return fn(e)

        record = precord

        fn(null, gr, nd)
      }), record)
  }, (e, gr, nd) => {
    if (e) return fn(e)

    record = gnreport_end(record)

    fn(null, gr, nd, record)
  })
}

const gnpatch_edgeapplydeep = (ss, cfg, gr, nd, pxu, edgeout, fn, rec) => {
  const edgeoutkey = edgeout.get(KEY)
  const edgeoutns = edgeout.get(NS)
  const ndend = gr.get(edgeoutkey)
  const srcpx = pxu[0]
  const endpx = pxu[1]
  const srckey = srcpx[0]
  const endkey = endpx[0]
  const endpropval = endpx[3]
  const ispatchexternal = srckey !== endkey
  const edgenote = gnpatch_edgemsgcreate(
    edgeout, edgeoutns, endpropval)

  rec = rec || gnreport_patchget('testkey')

  gnpatch_edgeapply(ss, cfg, gr, ndend, edgenote, (e, gr, ndend) => {
    if (e) return fn(e)

    if (ispatchexternal && ndend.get(ISPATCHREHYDRATE)) {
      return gnnode_pghydratepartial(ss, cfg, gr, ndend, (e, gr, ndend) => {
        if (e) return fn(e)

        rec = gnreport_addpatchkeyout(rec, edgeoutkey)

        fn(null, gr, ndend, rec)
      })
    }

    if (ispatchexternal && ndend.get(ISDATA)) {
      if (edgeout.get(META).accum) {
        rec = gnreport_addreplaceaccumkey(rec, edgeoutkey)

        return fn(null, gr, ndend, rec)
      }

      gnpatch_unitedgecreate(ss, cfg, gr, nd, SUBJ, edgeout, (e, pxu, gr) => {
        if (e) return fn(e)

        gnpatch_unitapplydeep(ss, cfg, gr, pxu, rec, (e, gr, nd, prec) => {
          if (e) return fn(e)

          // if (gnpgdatablend.isnodefullblendin(node, endns)) {
          //   gnpatch_compositesplit(sess, cfg, graph, endnode, precord, fn)
          // } else {
          //   fn(null, graph, endnode, precord)
          // }

          fn(null, gr, ndend, prec)
        })
      })
    } else if (edgeout.get(NS) === FKEY) {
      gnpatch_edgeapplyrefresh(ss, cfg, gr, nd, edgeout, rec, fn)
    } else {
      if (gnreport_isnotinitiator(rec, edgeoutkey)) {
        rec = gnreport_addpatchkeyout(rec, edgeoutkey)
      }

      fn(null, gr, ndend, rec)
    }
  })
}

const gnpatch_unitapplydeepall = (ss, cfg, gr, pxuarr, fn, r) => {
  r = r || gnreport_patchget(pxuarr)

  if (pxuarr[0]) {
    return gnpatch_unitapplydeep(ss, cfg, gr, pxuarr[0], r, (e, gr, nd, r) => {
      if (e) return fn(e)

      gnpatch_unitapplydeepall(ss, cfg, gr, pxuarr.slice(1), fn, r)
    })
  }


  const replaceaccums = r.replaceaccumarr
  if (!replaceaccums.length)
    return fn(null, gr, r)

  const nd = gr.get(replaceaccums.pop())

  // return the node with refreshed childsubj namespace.
  gnnode_prophydratefromchildsrefresh(ss, cfg, gr, nd, (e, gr, nd) => {
    if (e) return fn(e)

    // build new patch for update node.
    // send patch back through pipeline.
    const childsubj = gnacc_childsubj.get(nd)

    gnpatch_unitsfromnspropslocal(ss, cfg, gr, nd, CHILDSUBJ, childsubj, (
      (e, pxu) => {
        if (e) return fn(e)

        gnpatch_unitapplydeepall(ss, cfg, gr, pxu, fn, r)
      }))
  })
}

export {
  gnpatch_unitcreate,
  gnpatch_unitupstreamcreate,
  gnpatch_unitlocalcreate,
  gnpatch_unitapply,
  gnpatch_unitapplydeep,
  gnpatch_unitapplydeepall,
  gnpatch_unitedgecreate,

  gnpatch_unitsfromnspropslocal,
  gnpatch_unitsfromnsprops,

  gnpatch_unitsfromkeynsarr,
  gnpatch_unitsfromkeyns,

  gnpatch_edgemsgcreate,

  gnpatch_edgeapplyrefresh,
  gnpatch_edgeapplydeep,
  gnpatch_edgeapply,

  gnpatch_compositejoin,
  gnpatch_compositeisvalid,
  gnpatch_compositesplit
}
