import test from 'node:test'
import assert from 'node:assert/strict'

import {
  gnsess_isrequrlpath
} from '../src/gnsess.js'

test('isrequrlpath should return true if requrlpath', () => {
  const sess = { requrl: '/home' }

  assert.ok(gnsess_isrequrlpath(sess, '/home#user5'))
  assert.ok(gnsess_isrequrlpath(sess, '/home?user=5'))
})
