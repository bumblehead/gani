// Filename: gani.spec.js
// Timestamp: 2017.12.11-03:19:15 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import util from 'node:util'
import test from 'node:test'
import assert from 'node:assert/strict'
import infernoserver from 'inferno-server'
import mockcfg from './helper/mockcfg.js'
import uilabel from '../src/basic/uilabel.js'

import {
  gngraph_create
} from '../src/gngraph.js'

import {
  gnenumSPECPROPNAMETYPE
} from '../src/gnenum.js'

import {
  gnnode_pgvnodeget
} from '../src/gnnode.js'

import {
  gnacc_subj
} from '../src/gnacc.js'

import {
  mock_graph_todo_json
} from './helper/mockspecs.js'

const esess = { token: 'token' }

test('gani_spec.retobj supports common namespace for args list', async () => {
  const cfg = mockcfg()
  const graph_todo = gngraph_create(mock_graph_todo_json)
  const datatodokey = '/todo/content/datatodo'
  const datatodonode = graph_todo.get(datatodokey)

  const res = await util.promisify(
    cfg.spec.retobj
  )(esess, cfg, graph_todo, datatodonode, {
    init: { key: 'keyval' }
  }, [{
    name: 'key',
    [gnenumSPECPROPNAMETYPE]: 'nsprop',
    prop: 'init.key'
  }, {
    name: 'key2',
    [gnenumSPECPROPNAMETYPE]: 'fn',
    fnname: 'returnargs',
    args: ['init.key']
  }])

  assert.strictEqual(res.key, 'keyval')
  assert.strictEqual(res.key2, 'keyval')
})

test('should support variable classNames, classNameNODE', async () => {
  const sess = { locale: 'eng-US' }
  const cfg = mockcfg()
  const graph_test = gngraph_create({
    '/test': {
      key: '/test',
      name: 'test',
      node: 'uilabel',
      subj: {
        label: 'label-test'
      },
      subjhydrated: {
        label: 'label-test'
      }
    },
    '/test-classNameROOT': {
      key: '/test-classNameROOT',
      name: 'test-classNameROOT',
      node: 'uilabel',
      subj: {
        label: 'label-test',
        classNameROOT: '.nameroot'
      },
      subjhydrated: {
        label: 'label-test',
        classNameROOT: '.nameroot'
      }
    },
    '/test-classNameNODE': {
      key: '/test-classNameNODE',
      name: 'test-classNameNODE',
      node: 'uilabel',
      subj: {
        label: 'label-test',
        classNameNODE: 'namenode'
      },
      subjhydrated: {
        label: 'label-test',
        classNameNODE: 'namenode'
      }
    },
    '/test-classNameROOTandNODE': {
      key: '/test-classNameROOTandNODE',
      name: 'test-classNameROOTandNODE',
      node: 'uilabel',
      subj: {
        label: 'label-test',
        classNameROOT: '.nameroot',
        classNameNODE: 'namenode'
      },
      subjhydrated: {
        label: 'label-test',
        classNameROOT: '.nameroot',
        classNameNODE: 'namenode'
      }
    }
  })

  const node_test = graph_test.get('/test')
  const node_testSubj = gnacc_subj.get(node_test)
  const vnode_test = gnnode_pgvnodeget(
    sess, cfg, graph_test, node_test, uilabel, node_testSubj)
  assert.strictEqual(infernoserver.renderToString(vnode_test), (
    '<div class="test uilabel">'
      + '<span class="test uilabel-label"'
      + ' id=":test">label-test</span></div>'
  ))

  const node_testNameNODE = graph_test.get('/test-classNameNODE')
  const node_testNameNODESubj = gnacc_subj.get(node_testNameNODE)
  const vnode_testNameNODE = gnnode_pgvnodeget(
    sess, cfg, graph_test, node_testNameNODE, uilabel, node_testNameNODESubj)
  assert.strictEqual(infernoserver.renderToString(vnode_testNameNODE), (
    '<div class="namenode uilabel">'
      + '<span class="namenode uilabel-label"'
      + ' id=":test-classNameNODE">label-test</span></div>'
  ))

  const node_testNameROOT = graph_test.get('/test-classNameROOT')
  const node_testNameROOTSubj = gnacc_subj.get(node_testNameROOT)
  const vnode_testNameROOT = gnnode_pgvnodeget(
    sess, cfg, graph_test, node_testNameROOT, uilabel, node_testNameROOTSubj)
  assert.strictEqual(infernoserver.renderToString(vnode_testNameROOT), (
    '<div class="test-classNameROOT uilabel nameroot">'
      + '<span class="test-classNameROOT uilabel-label"'
      + ' id=":test-classNameROOT">label-test</span></div>'
  ))

  const node_testNameROOTandNODE = graph_test.get('/test-classNameROOTandNODE')
  const node_testNameROOTandNODESubj = gnacc_subj.get(node_testNameROOTandNODE)
  const vnode_testNameROOTandNODE = gnnode_pgvnodeget(
    sess, cfg, graph_test,
    node_testNameROOTandNODE, uilabel, node_testNameROOTandNODESubj)
  assert.strictEqual(infernoserver.renderToString(vnode_testNameROOTandNODE), (
    '<div class="namenode uilabel nameroot">'
      + '<span class="namenode uilabel-label"'
      + ' id=":test-classNameROOTandNODE">label-test</span></div>'
  ))
})
