import util from 'node:util'
import test from 'node:test'
import assert from 'node:assert/strict'
import mockcfg from './helper/mockcfg.js'
import mocksess from './helper/mocksess.js'

import {
  gnenumISPATHNODE,
  gnenumSPECPROPNAMETYPE
} from '../src/gnenum.js'

import {
  gnnode_create
} from '../src/gnnode.js'

import {
  gngraph_create
} from '../src/gngraph.js'

import {
  gnspec_keyisvalid,
  gnspec_atomisvalid,
  gnspec_nsspecreduce,
  gnspec_nspropstrparse,
  gnspec_nspropexpand,
  gnspec_nspropexpandall,
  gnspec_childrefcreate,
  gnspec_childrefget,

  gnspec_childprenamefromnode,
  gnspec_childnamecreate,
  gnspec_childprefromspecobj,
  gnspec_childprefromspeckey,
  gnspec_childprefromspec,

  gnspec_datakeysetdeep,

  gnspec_pathseedcreate,
  gnspec_pathseedstackcreate,
  gnspec_pathseedanycreate
} from '../src/gnspec.js'

import {
  gnerr_nodespecchildinvalid,
  gnerr_nodespecobjinvalid
} from '../src/gnerr.js'

test('keyisvalid, should validate key', () => {
  assert.strictEqual(gnspec_keyisvalid(), false)
  assert.strictEqual(gnspec_keyisvalid('key'), true)
})

test('atomisvalid, should validate spec atom', () => {
  assert.strictEqual(gnspec_atomisvalid({ name: 'name' }), true)
  assert.strictEqual(gnspec_atomisvalid({ name: '' }), false)
  assert.strictEqual(gnspec_atomisvalid(null), false)
})

test('nsspecreduce, atom, dynamic', async () => {
  const accum = await util.promisify(gnspec_nsspecreduce)({
    name: 'activeuser',
    [gnenumSPECPROPNAMETYPE]: 'literal',
    value: '1'
  }, {
    activeuser: '1'
  }, [], (acc, subj, nsspec, specname, fn) => {
    acc.push([specname, subj, nsspec])

    fn(null, acc)
  })

  assert.deepStrictEqual(accum, [[
    'activeuser',
    { activeuser: '1' },
    {
      name: 'activeuser',
      [gnenumSPECPROPNAMETYPE]: 'literal',
      value: '1'
    }
  ]])
})

test('nsspecreduce, atom, static', async () => {
  const accum = await util.promisify(gnspec_nsspecreduce)({
    activeuser: '1',
    inactiveuser: '2'
  }, {
    activeuser: '1',
    inactiveuser: '2'
  }, [], (acc, subj, nsspec, specname, fn) => {
    acc.push([specname, subj, nsspec])

    fn(null, acc)
  })

  assert.deepStrictEqual(accum, [[
    'activeuser',
    {
      activeuser: '1',
      inactiveuser: '2'
    },
    {
      activeuser: '1',
      inactiveuser: '2'
    }
  ], [
    'inactiveuser',
    {
      activeuser: '1',
      inactiveuser: '2'
    },
    {
      activeuser: '1',
      inactiveuser: '2'
    }
  ]])
})

test('nsspecreduce, list, dynamic + static', async () => {
  const accum = await util.promisify(gnspec_nsspecreduce)([{
    name: 'activeuser',
    [gnenumSPECPROPNAMETYPE]: 'literal',
    value: '1'
  },{
    name: 'inactiveuser',
    [gnenumSPECPROPNAMETYPE]: 'literal',
    value: '2'
  }, {
    staticstr: 'static',
    staticbool: true
  }], {
    activeuser: '1',
    inactiveuser: '2',
    staticstr: 'static',
    staticbool: true
  }, [], (acc, subj, nsspec, specname, fn) => {
    acc.push([specname, subj, nsspec])

    fn(null, acc)
  })

  assert.deepStrictEqual(accum, [[
    'activeuser',
    {
      activeuser: '1',
      inactiveuser: '2',
      staticstr: 'static',
      staticbool: true
    },
    {
      name: 'activeuser',
      [gnenumSPECPROPNAMETYPE]: 'literal',
      value: '1'
    }
  ], [
    'inactiveuser',
    {
      activeuser: '1',
      inactiveuser: '2',
      staticstr: 'static',
      staticbool: true
    },
    {
      name: 'inactiveuser',
      [gnenumSPECPROPNAMETYPE]: 'literal',
      value: '2'
    }
  ], [
    'staticstr',
    {
      activeuser: '1',
      inactiveuser: '2',
      staticstr: 'static',
      staticbool: true
    },
    {
      staticstr: 'static',
      staticbool: true
    }
  ], [
    'staticbool',
    {
      activeuser: '1',
      inactiveuser: '2',
      staticstr: 'static',
      staticbool: true
    },
    {
      staticstr: 'static',
      staticbool: true
    }
  ]])
})

test('nspropstrparse, should return a parsed nsstr', async () => {
  assert.deepStrictEqual(
    gnspec_nspropstrparse('[/dataerrors].subj.childremoved'),
    {
      fullstr: '[/dataerrors].subj.childremoved',
      nsstr: 'subj.childremoved',
      nskey: 'subj',
      nsprop: 'childremoved',
      fnsstr: '/dataerrors',
      fnspath: '/dataerrors',
      fnskey: undefined,
      fnsprop: undefined,

      path: '/dataerrors',
      prop: 'childremoved'
    })

  assert.deepStrictEqual(
    gnspec_nspropstrparse('[fkey.shapedata].subj.geometry'),
    {
      fullstr: '[fkey.shapedata].subj.geometry',
      nsstr: 'subj.geometry',
      nskey: 'subj',
      nsprop: 'geometry',
      fnsstr: 'fkey.shapedata',
      fnspath: undefined,
      fnskey: 'fkey',
      fnsprop: 'shapedata',

      path: undefined,
      prop: 'geometry'
    })

  assert.deepStrictEqual(
    gnspec_nspropstrparse('pkg.start'),
    {
      fullstr: 'pkg.start',
      nsstr: 'pkg.start',
      nskey: 'pkg',
      nsprop: 'start',
      fnsstr: undefined,
      fnspath: undefined,
      fnskey: undefined,
      fnsprop: undefined,

      path: undefined,
      prop: 'pkg.start'
    })
})

test('nspropstrexpand, should return a parsed nsstr', async () => {
  assert.deepStrictEqual(
    gnspec_nspropexpand({ prop: '[/dataerrors].subj.childremoved' }),
    {
      isexpanded: true,
      fullstr: '[/dataerrors].subj.childremoved',
      nsstr: 'subj.childremoved',
      nskey: 'subj',
      nsprop: 'childremoved',
      fnsstr: '/dataerrors',
      fnspath: '/dataerrors',
      fnskey: undefined,
      fnsprop: undefined,

      path: '/dataerrors',
      prop: 'childremoved'
    })
})

test('nspropstrexpandall, should return list of parsed nsstr', async () => {
  assert.deepStrictEqual(
    gnspec_nspropexpandall([{ prop: '[/dataerrors].subj.childremoved' }]),
    [{
      isexpanded: true,
      fullstr: '[/dataerrors].subj.childremoved',
      nsstr: 'subj.childremoved',
      nskey: 'subj',
      nsprop: 'childremoved',
      fnsstr: '/dataerrors',
      fnspath: '/dataerrors',
      fnskey: undefined,
      fnsprop: undefined,

      path: '/dataerrors',
      prop: 'childremoved'
    }])
})

test('childrefcreate, should return a childref spec', async () => {
  assert.deepStrictEqual(
    gnspec_childrefcreate({
      type: 'nodespec',
      namekey: 'content',
      spec: {
        name: 'todo',
        node: 'uitodo',
        child: []
      }
    }),
    {
      name: 'todo',
      child: [],
      namekey: 'content',
      node: 'uitodo'
    }
  )
})

test('childrefget, should return a childref spec', async () => {
  assert.deepStrictEqual(
    gnspec_childrefget({
      type: 'nodespec',
      namekey: 'content',
      spec: {
        name: 'todo',
        node: 'uitodo',
        child: []
      }
    }),
    {
      name: 'todo',
      child: [],
      namekey: 'content',
      node: 'uitodo'
    }
  )

  assert.deepStrictEqual(
    gnspec_childrefget({
      type: 'nodespec',
      spec: {
        name: 'todo',
        node: 'uitodo',
        child: []
      }
    }),
    {
      name: 'todo',
      child: [],
      node: 'uitodo'
    }
  )
})

test('childdatanamefromnode, return data[datakeyprop], string', async () => {
  const sess = mocksess()
  const cfg = mockcfg()
  const graphempty = gngraph_create({})
  const nodebox = gnnode_create({
    key: '/box',
    node: 'gnpgui',
    name: 'box',
    domchildkeyarr: [],

    child: [{
      spec: 'user',
      specassign: {
        onsubj: [{
          [gnenumSPECPROPNAMETYPE]: 'fn',
          fnname: 'cacheupdate',
          args: ['node']
        }]
      }
    }],

    spectypes: {
      user: {
        node: 'gnpgdata',
        name: 'datauser',
        datakeyprop: 'id',
        subj: [{
          name: 'id',
          [gnenumSPECPROPNAMETYPE]: 'nsprop',
          prop: 'base.id',
          def: {
            [gnenumSPECPROPNAMETYPE]: 'fn',
            fnname: 'datenow'
          }
        },{
          name: 'name',
          [gnenumSPECPROPNAMETYPE]: 'nsprop',
          prop: 'base.name',
          def: ''
        },{
          name: 'iscompleted',
          [gnenumSPECPROPNAMETYPE]: 'nsprop',
          prop: 'base.iscompleted',
          def: false
        }]
      }
    },
    baselist: {
      [gnenumSPECPROPNAMETYPE]: 'childkeys',
      args: ['/datausers', 'subj.id', 1]
    },
    baselisthydrated: [
      { id: 200, name: 'jim' },
      { id: 201, name: 'jane' },
      { id: 202, name: 'richard' }
    ]
  })
  const graphstart = graphempty.set('/box', nodebox)
  const spec = nodebox.get('spectypes').user
  const fkey = 'what'
  const obj = nodebox.get('baselisthydrated')[2]
  const childdataname = gnspec_childprenamefromnode(
    sess, cfg, graphstart, nodebox, spec, fkey, obj, 2)

  assert.strictEqual('202', childdataname)
})

test('childdatanamefromnode, return data[datakeyprop], dynamic', async () => {
  const sess = mocksess()
  const cfg = mockcfg({
    specfn: {
      getminikey: ([key]) => `mini-${key}`
    }
  })
  const graphempty = gngraph_create({})
  const nodebox = gnnode_create({
    key: '/box',
    node: 'gnpgui',
    name: 'box',
    domchildkeyarr: [],

    child: [{
      spec: 'user',
      specassign: {
        onsubj: [{
          [gnenumSPECPROPNAMETYPE]: 'fn',
          fnname: 'cacheupdate',
          args: ['node']
        }]
      }
    }],

    spectypes: {
      user: {
        node: 'gnpgdata',
        name: 'datauser',
        datakeyprop: {
          fnname: 'getminikey',
          args: ['base.id']
        },

        subj: [{
          iam: 'mini'
        },{
          name: 'name',
          [gnenumSPECPROPNAMETYPE]: 'nsprop',
          prop: 'base.name'
        },{
          name: 'key',
          [gnenumSPECPROPNAMETYPE]: 'nsprop',
          prop: 'init.key'
        }]
      }
    },
    baselist: {
      [gnenumSPECPROPNAMETYPE]: 'childkeys',
      args: ['/datausers', 'subj.id', 1]
    },
    baselisthydrated: [
      { id: 200, name: 'jim' },
      { id: 201, name: 'jane' },
      { id: 202, name: 'richard' }
    ]
  })
  const graphstart = graphempty.set('/box', nodebox)
  const spec = nodebox.get('spectypes').user
  const fkey = 'what'
  const obj = nodebox.get('baselisthydrated')[2]
  const childdataname = gnspec_childprenamefromnode(
    sess, cfg, graphstart, nodebox, spec, fkey, obj, 2)

  assert.strictEqual('mini-202', childdataname)
})

test('childdatanamefromnode, should fallback to unsafe-index', async () => {
  const sess = mocksess()
  const cfg = mockcfg()
  const graphempty = gngraph_create({})
  const nodebox = gnnode_create({
    key: '/box',
    node: 'gnpgui',
    name: 'box',
    domchildkeyarr: [],

    child: [{
      spec: 'user',
      specassign: {
        onsubj: [{
          [gnenumSPECPROPNAMETYPE]: 'fn',
          fnname: 'cacheupdate',
          args: ['node']
        }]
      }
    }],

    spectypes: {
      user: {
        node: 'gnpgdata',
        name: 'datauser',
        datakeyprop: 'unfoundprop',
        subj: [{
          iam: 'mini'
        },{
          name: 'name',
          [gnenumSPECPROPNAMETYPE]: 'nsprop',
          prop: 'base.name'
        },{
          name: 'key',
          [gnenumSPECPROPNAMETYPE]: 'nsprop',
          prop: 'init.key'
        }]
      }
    },
    baselist: {
      [gnenumSPECPROPNAMETYPE]: 'childkeys',
      args: ['/datausers', 'subj.id', 1]
    },
    baselisthydrated: [
      { id: 200, name: 'jim' },
      { id: 201, name: 'jane' },
      { id: 202, name: 'richard' }
    ]
  })
  const graphstart = graphempty.set('/box', nodebox)
  const spec = nodebox.get('spectypes').user
  const fkey = 'what'
  const obj = nodebox.get('baselisthydrated')[2]
  const childdataname = gnspec_childprenamefromnode(
    sess, cfg, graphstart, nodebox, spec, fkey, obj, 2)

  assert.strictEqual('unsafe2-datauser', childdataname)
})

test('childnamecreate, should create child name from key', async () => {
  assert.strictEqual(
    gnspec_childnamecreate({ name: 'childuser' }, '/path/to/key', 2),
    'childuser_key')

  assert.strictEqual(
    gnspec_childnamecreate({ name: 'unsafechilduser' }, '/path/to/key', 2),
    'unsafe2_unsafechilduser_key')
})

test('childdatafromspecobj, return childdata from spec', async () => {
  const sess = mocksess()
  const cfg = mockcfg()
  const graphempty = gngraph_create({})
  const nodebox = gnnode_create({
    key: '/box',
    node: 'gnpgui',
    name: 'box',
    domchildkeyarr: [],

    child: [{
      spec: 'user',
      specassign: {
        onsubj: [{
          [gnenumSPECPROPNAMETYPE]: 'fn',
          fnname: 'cacheupdate',
          args: ['node']
        }]
      }
    }],

    spectypes: {
      user: {
        node: 'gnpgdata',
        name: 'datauser',
        datakeyprop: 'id',
        subj: [{
          name: 'id',
          [gnenumSPECPROPNAMETYPE]: 'nsprop',
          prop: 'base.id',
          def: {
            [gnenumSPECPROPNAMETYPE]: 'fn',
            fnname: 'datenow'
          }
        },{
          name: 'name',
          [gnenumSPECPROPNAMETYPE]: 'nsprop',
          prop: 'base.name',
          def: ''
        },{
          name: 'iscompleted',
          [gnenumSPECPROPNAMETYPE]: 'nsprop',
          prop: 'base.iscompleted',
          def: false
        }]
      }
    },
    baselist: {
      [gnenumSPECPROPNAMETYPE]: 'childkeys',
      args: ['/datausers', 'subj.id', 1]
    },
    baselisthydrated: [
      { id: 200, name: 'jim' },
      { id: 201, name: 'jane' },
      { id: 202, name: 'richard' }
    ]
  })
  const graphstart = graphempty.set('/box', nodebox)
  const spec = nodebox.get('spectypes').user
  const fkey = 'what'
  const obj = nodebox.get('baselisthydrated')[2]
  const childdataspec = gnspec_childprefromspecobj(
    sess, cfg, graphstart, nodebox, spec, fkey, obj, 2)

  assert.deepStrictEqual(childdataspec, {
    name: '202',
    node: 'gnpgdata',
    datakeyprop: 'id',
    basehydrated: {
      id: 202,
      name: 'richard'
    },
    init: [],
    subj: [{
      id: 202,
      name: 'richard',
      spread: true
    }, {
      def: { fnname: 'datenow', [gnenumSPECPROPNAMETYPE]: 'fn' },
      name: 'id',
      prop: 'base.id',
      [gnenumSPECPROPNAMETYPE]: 'nsprop'
    }, {
      def: '',
      name: 'name',
      prop: 'base.name',
      [gnenumSPECPROPNAMETYPE]: 'nsprop'
    }, {
      def: false,
      name: 'iscompleted',
      prop: 'base.iscompleted',
      [gnenumSPECPROPNAMETYPE]: 'nsprop'
    }]
  })
})

test('childdatafromspecobj, should error if spec invalid', async () => {
  const sess = mocksess()
  const cfg = mockcfg()
  const graphempty = gngraph_create({})
  const nodebox = gnnode_create({
    key: '/box',
    node: 'gnpgui',
    name: 'box',
    domchildkeyarr: [],

    child: [{
      spec: 'user',
      specassign: {
        onsubj: [{
          [gnenumSPECPROPNAMETYPE]: 'fn',
          fnname: 'cacheupdate',
          args: ['node']
        }]
      }
    }],

    spectypes: {
      user: {
        node: 'gnpgdata',
        name: 'datauser',
        datakeyprop: 'id',
        subj: [{
          name: 'id',
          [gnenumSPECPROPNAMETYPE]: 'nsprop',
          prop: 'base.id',
          def: {
            [gnenumSPECPROPNAMETYPE]: 'fn',
            fnname: 'datenow'
          }
        },{
          name: 'name',
          [gnenumSPECPROPNAMETYPE]: 'nsprop',
          prop: 'base.name',
          def: ''
        },{
          name: 'iscompleted',
          [gnenumSPECPROPNAMETYPE]: 'nsprop',
          prop: 'base.iscompleted',
          def: false
        }]
      }
    },
    baselist: {
      [gnenumSPECPROPNAMETYPE]: 'childkeys',
      args: ['/datausers', 'subj.id', 1]
    },
    baselisthydrated: [
      { id: 200, name: 'jim' },
      { id: 201, name: 'jane' },
      { id: 202, name: 'richard' }
    ]
  })
  const graphstart = graphempty.set('/box', nodebox)
  // const spec = nodebox.get('spectypes').user
  const spec = null
  const fkey = 'what'
  const obj = nodebox.get('baselisthydrated')[2]

  await assert.rejects(async () => (
    gnspec_childprefromspecobj(
      sess, cfg, graphstart, nodebox, spec, fkey, obj, 2)
  ), {
    message: gnerr_nodespecchildinvalid(
      graphstart, nodebox, spec).message
  })
})

test('childdatafromspecobj, should error if specobj invalid', async () => {
  const sess = mocksess()
  const cfg = mockcfg()
  const graphempty = gngraph_create({})
  const nodebox = gnnode_create({
    key: '/box',
    node: 'gnpgui',
    name: 'box',
    domchildkeyarr: [],

    child: [{
      spec: 'user',
      specassign: {
        onsubj: [{
          [gnenumSPECPROPNAMETYPE]: 'fn',
          fnname: 'cacheupdate',
          args: ['node']
        }]
      }
    }],

    spectypes: {
      user: {
        node: 'gnpgdata',
        name: 'datauser',
        datakeyprop: 'id',
        subj: [{
          name: 'id',
          [gnenumSPECPROPNAMETYPE]: 'nsprop',
          prop: 'base.id',
          def: {
            [gnenumSPECPROPNAMETYPE]: 'fn',
            fnname: 'datenow'
          }
        },{
          name: 'name',
          [gnenumSPECPROPNAMETYPE]: 'nsprop',
          prop: 'base.name',
          def: ''
        },{
          name: 'iscompleted',
          [gnenumSPECPROPNAMETYPE]: 'nsprop',
          prop: 'base.iscompleted',
          def: false
        }]
      }
    },
    baselist: {
      [gnenumSPECPROPNAMETYPE]: 'childkeys',
      args: ['/datausers', 'subj.id', 1]
    },
    baselisthydrated: [
      { id: 200, name: 'jim' },
      { id: 201, name: 'jane' },
      { id: 202, name: 'richard' }
    ]
  })
  const graphstart = graphempty.set('/box', nodebox)
  const spec = nodebox.get('spectypes').user
  const fkey = 'what'
  const obj = null // nodebox.get('baselisthydrated')[2]

  await assert.rejects(async () => (
    gnspec_childprefromspecobj(
      sess, cfg, graphstart, nodebox, spec, fkey, obj, 2)
  ), {
    message: gnerr_nodespecobjinvalid(
      graphstart, nodebox, spec, obj).message
  })
})

test('childdatafromspeckey, return childdata from spec', async () => {
  const sess = mocksess()
  const cfg = mockcfg()
  const graphempty = gngraph_create({})
  const nodebox = gnnode_create({
    key: '/box',
    node: 'gnpgui',
    name: 'box',
    domchildkeyarr: [],

    child: [{
      spec: 'user',
      specassign: {
        onsubj: [{
          [gnenumSPECPROPNAMETYPE]: 'fn',
          fnname: 'cacheupdate',
          args: ['node']
        }]
      }
    }],

    spectypes: {
      user: {
        node: 'gnpgdata',
        name: 'datauser',
        datakeyprop: 'id',
        subj: [{
          name: 'id',
          [gnenumSPECPROPNAMETYPE]: 'nsprop',
          prop: 'base.id',
          def: {
            [gnenumSPECPROPNAMETYPE]: 'fn',
            fnname: 'datenow'
          }
        },{
          name: 'name',
          [gnenumSPECPROPNAMETYPE]: 'nsprop',
          prop: 'base.name',
          def: ''
        },{
          name: 'iscompleted',
          [gnenumSPECPROPNAMETYPE]: 'nsprop',
          prop: 'base.iscompleted',
          def: false
        }]
      }
    },
    baselist: {
      [gnenumSPECPROPNAMETYPE]: 'childkeys',
      args: ['/datausers'],
      options: {
        sortBy: "subj.id",
        sortDirection: "desc"
      }
    },
    baselisthydrated: [
      { id: 200, name: 'jim' },
      { id: 201, name: 'jane' },
      { id: 202, name: 'richard' }
    ]
  })
  const graphstart = graphempty.set('/box', nodebox)
  const spec = nodebox.get('spectypes').user
  const key = '/data/key/jimbones1'
  const childdataspec = gnspec_childprefromspeckey(
    sess, cfg, graphstart, nodebox, spec, {
      '[./].baseelem': ['what'],
      what: '[./].baseelem'
    }, key, 2)

  assert.deepStrictEqual(childdataspec, {
    name: 'datauser_jimbones1',
    node: 'gnpgdata',
    datakeyprop: 'id',
    fkeyhydrated: {
      '[./].baseelem': null,
      what: '/data/key/jimbones1'
    },
    mapsrckey: '/data/key/jimbones1',
    subj: [{
      def: { fnname: 'datenow', [gnenumSPECPROPNAMETYPE]: 'fn' },
      name: 'id',
      prop: 'base.id',
      [gnenumSPECPROPNAMETYPE]: 'nsprop'
    }, {
      def: '',
      name: 'name',
      prop: 'base.name',
      [gnenumSPECPROPNAMETYPE]: 'nsprop'
    }, {
      def: false,
      name: 'iscompleted',
      prop: 'base.iscompleted',
      [gnenumSPECPROPNAMETYPE]: 'nsprop'
    }]
  })
})

test('childdatafromspec, return childdata from spec', async () => {
  const sess = mocksess()
  const cfg = mockcfg()
  const graphempty = gngraph_create({})
  const nodebox = gnnode_create({
    key: '/box',
    node: 'gnpgui',
    name: 'box',
    domchildkeyarr: [],

    child: [{
      spec: 'user',
      specassign: {
        onsubj: [{
          [gnenumSPECPROPNAMETYPE]: 'fn',
          fnname: 'cacheupdate',
          args: ['node']
        }]
      }
    }],

    spectypes: {
      user: {
        node: 'gnpgdata',
        name: 'datauser',
        datakeyprop: 'id',
        subj: [{
          name: 'id',
          [gnenumSPECPROPNAMETYPE]: 'nsprop',
          prop: 'base.id',
          def: {
            [gnenumSPECPROPNAMETYPE]: 'fn',
            fnname: 'datenow'
          }
        },{
          name: 'name',
          [gnenumSPECPROPNAMETYPE]: 'nsprop',
          prop: 'base.name',
          def: ''
        },{
          name: 'iscompleted',
          [gnenumSPECPROPNAMETYPE]: 'nsprop',
          prop: 'base.iscompleted',
          def: false
        }]
      }
    },
    baselist: {
      [gnenumSPECPROPNAMETYPE]: 'childkeys',
      args: ['/datausers'],
      options: {
        sortBy: "subj.id",
        sortDirection: "desc"
      }
    },
    baselisthydrated: [
      { id: 200, name: 'jim' },
      { id: 201, name: 'jane' },
      { id: 202, name: 'richard' }
    ]
  })
  const graphstart = graphempty.set('/box', nodebox)
  const spec = nodebox.get('spectypes').user
  const key = '/data/key/jimbones1'// nodebox.get('baselisthydrated')[2]
  const childdataspec = gnspec_childprefromspec(
    sess, cfg, graphstart, nodebox, key, 2, {
      fkey: {
        '[./].baseelem': ['what'],
        what: '[./].baseelem'
      }
    }, spec)

  assert.deepStrictEqual(childdataspec, {
    name: 'datauser_jimbones1',
    node: 'gnpgdata',
    datakeyprop: 'id',
    fkeyhydrated: {
      '[./].baseelem': null,
      what: '/data/key/jimbones1'
    },
    mapsrckey: '/data/key/jimbones1',
    subj: [{
      def: { fnname: 'datenow', [gnenumSPECPROPNAMETYPE]: 'fn' },
      name: 'id',
      prop: 'base.id',
      [gnenumSPECPROPNAMETYPE]: 'nsprop'
    }, {
      def: '',
      name: 'name',
      prop: 'base.name',
      [gnenumSPECPROPNAMETYPE]: 'nsprop'
    }, {
      def: false,
      name: 'iscompleted',
      prop: 'base.iscompleted',
      [gnenumSPECPROPNAMETYPE]: 'nsprop'
    }]
  })
})

test('datakeysetdeep should add key to spec', () => {
  const specprenode = {
    name: '/',
    node: 'uiroot',
    child: [],
    ispathnode: true
  }

  assert.deepStrictEqual(
    gnspec_datakeysetdeep(specprenode, 'fkey', 'user', 'fred56'), {
      name: '/',
      node: 'uiroot',
      child: [],
      ispathnode: true,
      fkey: { user: 'fred56' },
      fkeyhydrated: { user: 'fred56' }
    })

  const specprenodefkeyarr = {
    name: '/',
    node: 'uiroot',
    child: [],
    fkey: [],
    ispathnode: true
  }

  assert.deepStrictEqual(
    gnspec_datakeysetdeep(specprenodefkeyarr, 'fkey', 'user', 'fred56'), {
      name: '/',
      node: 'uiroot',
      child: [],
      ispathnode: true,
      fkey: [{ datakey: 'fred56' }],
      fkeyhydrated: { user: 'fred56' }
    })
})

test('gnspec_pathseedcreate, returns a name-corresponding spec', async () => {
  const sess = mocksess()
  const specpathmap = {
    '$ROOTSPECS/spec/root/eng-US.json': {
      name: '/',
      node: 'uiroot',
      child: []
    }
  }

  const cfg = mockcfg()

  cfg.pagespecfromisopath = (sess, cfg, isopath) => (
    specpathmap[isopath])

  const prenodespec = await gnspec_pathseedcreate(sess, cfg, {
    pathpattern: 'root'
  })

  assert.deepStrictEqual(prenodespec, {
    ...specpathmap['$ROOTSPECS/spec/root/eng-US.json'],
    [gnenumISPATHNODE]: true
  })
})

test('stackpathnodes, returns a name-corresponding specs', async () => {
  const sess = mocksess()
  const pathnodearr = [{
    path: '',
    pathvanilla: '',
    pathre: { keys: [] },
    pathpattern: 'root',
    pagearr: [{
      pathpattern: 'root-pg-home',
      path: '/',
      pathvanilla: '/',
      pathre: { keys: [] }
    }]
  }, {
    path: '/',
    pathvanilla: '/',
    pathre: { keys: [] },
    pathpattern: 'root-pg-home'
  }]
  const specpathmap = {
    '$ROOTSPECS/spec/root/eng-US.json': {
      name: '/',
      node: 'uiroot',
      child: [{
        ispathnode: true
      }]
    },
    '$ROOTSPECS/spec/root-pg-home/eng-US.json': {
      name: 'home',
      node: 'uispread',
      child: []
    }
  }

  const cfg = mockcfg()

  cfg.pagespecfromisopath = (sess, cfg, isopath) => (
    specpathmap[isopath])

  const speclist = (
    await gnspec_pathseedstackcreate(sess, cfg, pathnodearr))

  // the node at the front of the list is 'stacked'
  assert.deepStrictEqual(speclist, [{
    name: '/',
    node: 'uiroot',
    ispathnode: true,
    child: [{
      name: 'home',
      node: 'uispread',
      child: [],
      ispathnode: true
    }]
  },{
    name: 'home',
    node: 'uispread',
    child: [],
    ispathnode: true
  }])
})

test('frompathdefs, returns a name-corresponding specs', async () => {
  const sess = mocksess()
  const pathnodearr = [{
    path: '',
    pathvanilla: '',
    pathre: { keys: [] },
    pathpattern: 'root',
    pagearr: [{
      pathpattern: 'root-pg-home',
      path: '/',
      pathvanilla: '/',
      pathre: { keys: [] }
    }]
  }, {
    path: '/',
    pathvanilla: '/',
    pathre: { keys: [] },
    pathpattern: 'root-pg-home'
  }]

  const specpathmap = {
    '$ROOTSPECS/spec/root/eng-US.json': {
      name: '/',
      node: 'uiroot',
      child: [{
        ispathnode: true
      }]
    },
    '$ROOTSPECS/spec/root-pg-home/eng-US.json': {
      name: 'home',
      node: 'uispread',
      child: []
    }
  }

  const cfg = mockcfg()

  cfg.pagespecfromisopath = (sess, cfg, isopath) => (
    specpathmap[isopath])

  const speclist = (
    await gnspec_pathseedanycreate(sess, cfg, pathnodearr))

  // the node at the front of the list is 'stacked'
  assert.deepStrictEqual(speclist, [{
    name: '/',
    node: 'uiroot',
    ispathnode: true,
    child: [{
      name: 'home',
      node: 'uispread',
      child: [],
      ispathnode: true
    }]
  },{
    name: 'home',
    node: 'uispread',
    child: [],
    ispathnode: true
  }])
})

