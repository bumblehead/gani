import test from 'node:test'
import assert from 'node:assert/strict'
import jsdom from 'jsdom'
import mockcfg from './helper/mockcfg.js'
import mocksess from './helper/mocksess.js'

import {
  gngraph_create
} from '../src/gngraph.js'

import {
  gnevdelegate_setishover,
  gnevdelegate_dispatchev,
  gnevdelegate_linkdetail
} from '../src/gnevdelegate.js'

// conider renaming to evdisp
test('gnevdelegate_linkdetail should return link details', async () => {
  // check both /blog and /blog/?
  const dom = new jsdom.JSDOM(
    '<!DOCTYPE html><body><a href="/blog/"></a></body>', {
      url: 'https://0.0.0.0:4545/'
    })
  const cfg = mockcfg({ dom })
  const elemlink = dom.window.document.body.firstChild

  assert.deepStrictEqual(gnevdelegate_linkdetail(elemlink, cfg), {
    pathname: '/blog/',
    pathnamenoprefix: '/blog/',
    linkhash: '',
    ishashpath: false,
    istargetblank: false,
    islocalorigin: true,
    islocalpath: false,
    islocalhash: false,
    isapppath: true
  })
})

test('evdelegate_setishover should call onishover for node pg', async () => {
  const cfg = mockcfg()
  const sess = mocksess()
  const onev = []
  const graph = gngraph_create({
    '/box': {
      key: '/box',
      node: 'pgtest',
      name: 'box',
      subjhydrated: {
        details: true
      },
      childarr: [
        '/box/child'
      ]
    },
    '/box/child': {
      key: '/box/child',
      node: 'pgtest',
      name: 'child'
    }
  })

  cfg.pgmap.pgtest = {
    prevget: (cfg, nd) => (
      Object.assign({}, nd.get('subjhydrated'))),
    onhoverstart: (ss, cfg, gr, nd) => (
      onev.push('hoverstart:' + nd.get('key')))
  }

  cfg.graph = graph
  gnevdelegate_setishover(sess, cfg, {}, true)

  assert.deepStrictEqual(onev, [
    'hoverstart:/box',
    'hoverstart:/box/child'
  ])
})

test('evdelegate_dispatchev should dispatch event node pg', async () => {
  const cfg = mockcfg()
  const sess = mocksess()
  const onev = []
  const graph = gngraph_create({
    '/box': {
      key: '/box',
      node: 'pgtest',
      name: 'box',
      subjhydrated: {
        details: true
      },
      childarr: [
        '/box/child'
      ]
    },
    '/box/child': {
      key: '/box/child',
      node: 'pgtest',
      name: 'child'
    }
  })

  cfg.pgmap.pgtest = {
    prevget: (cfg, nd) => (
      Object.assign({}, nd.get('subjhydrated'))),
    // onhoverstart: (ss, cfg, gr, nd) => (
    onclick: (ss, cfg, gr, nd) => (
      onev.push('onclick:' + nd.get('key')))
  }

  cfg.graph = graph
  gnevdelegate_dispatchev(sess, cfg, {
    type: 'click',
    target: {
      tagName: 'a',
      id: ':box'
    }
  })

  assert.deepStrictEqual(onev, [
    'onclick:/box'
  ])
})
