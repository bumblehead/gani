// Filename: gani_token.spec.js
// Timestamp: 2017.12.11-03:19:20 (last modified)
// Author(s): bumblehead <chris@bumblehead.com>

import test from 'node:test'
import assert from 'node:assert/strict'

import {
  gnenum_isvalidnameval
} from '../src/gnenum.js'

test('isvalidnodename, should return true if name is string of length', () => {
  assert.ok(gnenum_isvalidnameval('name'))
})

test('isvalidnodename, should return false if name not string', () => {
  assert.ok(!gnenum_isvalidnameval(null))
})
