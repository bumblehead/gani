import test from 'node:test'
import assert from 'node:assert/strict'

import mockcfg from './helper/mockcfg.js'
import mocksess from './helper/mocksess.js'

import {
  gnenumSPECPROPNAMETYPE
} from '../src/gnenum.js'

import {
  gngraph_create,
  gngraph_renderpath,
  gngraph_getpopulated,
  gngraph_renderdiffpath
} from '../src/gngraph.js'

test('getpopulated should define and hydrate nodes', async () => {
  const sess = { locale: 'eng-US' }
  const cfg = mockcfg({
    specfn: { getversion: () => '1.0' }
  })

  const graphstart = gngraph_create({
    '/': {
      key: '/',
      name: '/',
      node: 'uiroot',
      ispathnode: true,
      isdom: true,
      domchildkey: {},
      domchildkeyarr: [],
      child: [{
        node: 'uispread',
        name: 'bottomcopy',
        child: [{
          node: 'uistatic',
          name: 'copy',
          subj: [{
            title: 'copy',
            timedate: 1480104360000,
            content: '<p>© <a href="mailto:mail@box.com">jim</a></p>\n'
          }]
        }, {
          node: 'uispread',
          name: 'links',
          child: [{
            node: 'uilabel',
            name: 'version',
            subj: [{
              [gnenumSPECPROPNAMETYPE]: 'fn',
              fnname: 'getversion',
              name: 'label'
            }]
          }, {
            node: 'uilink',
            name: 'sitemap',
            subj: [{
              labelprimary: 'site map',
              href: '/site-map/'
            }]
          }]
        }]
      }]
    }
  })

  const [graph, rootnode] = await new Promise(resolve => {
    gngraph_getpopulated(sess, cfg, graphstart, true, (
      (err, graph, rootnode, report) => {
        resolve([graph, rootnode, report])
      }))
  })

  assert.strictEqual(rootnode.get('key'), '/')
  assert.deepStrictEqual(
    graph.get('/bottomcopy/links/version').get('subjhydrated'),
    {
      label: '1.0'
    }
  )
})

test('renderpath applies path as hydrated nodes on graph', async () => {
  const cfg = mockcfg()
  const sess = mocksess()
  const rootspec = {
    name: '/',
    node: 'uiroot',
    child: [{
      node: 'uispread',
      name: 'top'
    },{
      ispathnode: true
    },{
      node: 'uispread',
      name: 'bottom'
    }]
  }

  const linkspec = {
    node: 'uilink',
    name: 'sitemap',
    subj: [{
      labelprimary: 'site map',
      href: '/site-map/'
    }]
  }

  const specpathmap = {
    '$ROOTSPECS/spec/root/eng-US.json': rootspec,
    '$ROOTSPECS/spec/root-links/eng-US.json': linkspec
  }

  cfg.pagespecfromisopath = (sess, cfg, isopath) => (
    specpathmap[isopath])

  const [
    graph, finpnodearr, stackarr, rootnode, vtree, report
  ] = await gngraph_renderpath(sess, cfg, '/links/')

  assert.ok(graph && finpnodearr && stackarr && rootnode && vtree && report)
})

// renderpath needs to be covered first
test('renderdiffpath applies new path as hydrated nodes on graph', async () => {
  const cfg = mockcfg()
  const sess = { locale: 'eng-US' }
  const iscontinuefn = () => true
  const onpartialfn = (graph, fn) => fn(null, graph)

  const rootspec = {
    name: '/',
    node: 'uiroot',
    child: [{
      node: 'uispread',
      name: 'top'
    },{
      ispathnode: true
    },{
      node: 'uispread',
      name: 'bottom'
    }]
  }

  const linkspec = {
    node: 'uilink',
    name: 'sitemap',
    subj: [{
      labelprimary: 'site map',
      href: '/site-map/'
    }]
  }

  const aboutspec = {
    node: 'uilink',
    name: 'about',
    subj: [{
      labelprimary: 'about'
    }]
  }

  const specpathmap = {
    '$ROOTSPECS/spec/root/eng-US.json': rootspec,
    '$ROOTSPECS/spec/root-links/eng-US.json': linkspec,
    '$ROOTSPECS/spec/root-about/eng-US.json': aboutspec
  }

  cfg.pagespecfromisopath = (sess, cfg, isopath) => (
    specpathmap[isopath])

  const [
    graphs, finpnodearrs, stackarrs, rootnodes, vtrees, reports
  ] = await gngraph_renderpath(sess, cfg, '/links/')

  const [
    graph, finpnodearr, objarr, rootnode, vtree, report
  ] = await new Promise(resolve => {
    gngraph_renderdiffpath(
      sess, cfg, graphs, '/about/', stackarrs, onpartialfn, iscontinuefn, (
        (err, graph, finpnodearr, objarr, rootnode, vtree, report) => {
          resolve([graph, finpnodearr, objarr, rootnode, vtree, report])
        }))
  })

  assert.ok(
    graphs && finpnodearrs && stackarrs && rootnodes && vtrees && reports &&
      graph && finpnodearr && objarr && rootnode && vtree && report)
})
