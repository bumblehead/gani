import test from 'node:test'
import assert from 'node:assert/strict'
import gnpguipart from '../src/gnpguipart.js'

import {
  gngraph_create
} from '../src/gngraph.js'

import {
  gnnode_create
} from '../src/gnnode.js'

import {
  // gnerr_nodespecfullkeyinvalid,
  gnerrnode_nspropresolvedundefined
} from '../src/gnerr.js'

import {
  gnenumSPECPROPNAMETYPE
} from '../src/gnenum.js'

import {
  mock_graph_todo_json,
  mock_node_uinav_optimised_json
} from './helper/mockspecs.js'

import promisify from './helper/promisify.js'
import mockcfg from './helper/mockcfg.js'
import mocksess from './helper/mocksess.js'

test('gethydratedpartial, returns node w/ hydrated part data', async () => {
  const sess = mocksess()
  const cfg = mockcfg()
  const graph_todo = gngraph_create(mock_graph_todo_json)
  const uinav = gnnode_create(mock_node_uinav_optimised_json)
  const [graph, node] = await promisify(gnpguipart.hydratepartial)(
    sess, cfg, graph_todo, uinav, gnpguipart)

  assert.ok(graph)
  assert.ok(typeof node === 'object')
})

test('getpartial should error if fullkey is invalid', async () => {
  const sess = mocksess()
  const cfg = mockcfg()
  const nodelist = gnnode_create({
    key: "/box",
    node: "uiobjls",
    name: "list",

    full: {
      note: [
        "this could be defined as type 'cb'",
        "using a callback to obtain list from service"
      ],
      [gnenumSPECPROPNAMETYPE]: "literal",
      value: [{
        name: "blog",
        type: "blog",
        href: "/blog/"
      },{
        name: "links",
        type: "links",
        href: "/links/"
      },{
        name: "about",
        type: "about",
        href: "/about/"
      }]
    },

    part: [{
      name: "labelprimary",
      [gnenumSPECPROPNAMETYPE]: "nsprop",
      prop: "part.name"
    },{
      name: "href",
      [gnenumSPECPROPNAMETYPE]: "nsprop",
      prop: "part.href"
    },{
      name: "fullkey",
      [gnenumSPECPROPNAMETYPE]: "nsprop",
      prop: "part.hrefs"
    }],

    subj: [{
      isstopev: false,
      selecttype: "single"
    }]
  })
  const graph = gngraph_create({}).set('/box', nodelist)

  await assert.rejects(async () => (
    promisify(gnpguipart.getpartial)(
      sess, cfg, graph, nodelist, nodelist.get('full').value[0], {
        // subj: {
        value: undefined,
        isstopev: false,
        selecttype: 'single'
        // }
      })
  ), {
    message: gnerrnode_nspropresolvedundefined(
      graph, nodelist,
      {
        part: {
          name: 'blog',
          type: 'blog',
          href: '/blog/'
        },
        name: 'blog',
        type: 'blog',
        href: '/blog/'
      },
      'fullkey',
      'part',
      'hrefs',
      'part.hrefs').message
  })
})

test('getpartial should allow unspecified subj.value', async () => {
  const sess = mocksess()
  const cfg = mockcfg()
  const nodelist = gnnode_create({
    key: "/nav",
    node: "uinav",
    name: "nav",

    full: {
      note: [
        "this could be defined as type 'cb'",
        "using a callback to obtain list from service"
      ],
      [gnenumSPECPROPNAMETYPE]: "literal",
      value: [{
        name: "blog",
        type: "blog",
        href: "/blog/"
      },{
        name: "links",
        type: "links",
        href: "/links/"
      },{
        name: "about",
        type: "about",
        href: "/about/"
      }]
    },

    part: [{
      name: "labelprimary",
      [gnenumSPECPROPNAMETYPE]: "nsprop",
      prop: "part.name"
    },{
      name: "href",
      [gnenumSPECPROPNAMETYPE]: "nsprop",
      prop: "part.href"
    },{
      name: "fullkey",
      [gnenumSPECPROPNAMETYPE]: "nsprop",
      prop: "part.href"
    }],

    subj: [{
      isstopev: false,
      selecttype: "single"
    }]
  })
  const graph = gngraph_create({}).set('/nav', nodelist)

  const res = await promisify(gnpguipart.getpartial)(
    sess, cfg, graph, nodelist, nodelist.get('full').value[0], {
      // value: undefined,
      isstopev: false,
      selecttype: 'single'
    })

  assert.deepStrictEqual(res[0], {
    labelprimary: 'blog',
    href: '/blog/',
    fullkey: '/blog/',
    partkey: '/blog/',
    isactive: false
  })
})
