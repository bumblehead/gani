import test from 'node:test'
import assert from 'node:assert/strict'

import { pathToRegexp } from 'path-to-regexp'

import mockcfg from './helper/mockcfg.js'

import {
  gnpathdef_haspathstr,
  gnpathdef_pathpgarrget,
  gnpathdef_pathstrtokens,
  gnpathdef_findmatchpatharr,
  gnpathdef_createfrommanifest
} from '../src/gnpathdef.js'

test('pathstrtokens returns path as tokens', () => {
  const tokens = gnpathdef_pathstrtokens('/me/too/three')

  assert.deepStrictEqual(tokens, ['me/', 'too/', 'three'])
})

test('findmatchpatharr returns matched paths', () => {
  const cfg = {}
  const patharr = ['links/']
  const routesobj = {
    path: '',
    pathvanilla: '',
    pathre: pathToRegexp('/').regexp,
    pathpattern: 'root',
    pagearr: [{
      path: '/',
      pathvanilla: '/',
      pathre: pathToRegexp('/').regexp,
      pathpattern: 'page-home'
    },{
      path: '/about/',
      pathvanilla: '/about',
      pathre: pathToRegexp('/about/').regexp,
      pathpattern: 'page-about'
    },{
      path: '/links/',
      pathvanilla: '/links',
      pathre: pathToRegexp('/links/').regexp,
      pathpattern: 'page-links'
    }]
  }

  const matchedpatharr = gnpathdef_findmatchpatharr(
    cfg, patharr, routesobj.pagearr, [routesobj])

  assert.deepStrictEqual(matchedpatharr, [
    routesobj,
    routesobj.pagearr[2]
  ])
})

test('build routes', () => {
  const pathdefsm = gnpathdef_createfrommanifest({
    deploytype: 'flat',
    routes: [
      '/',
      '/about/',
      '/links/',
      '/gallery/',
      '/site-map/',
      '/blog/', [[
        '/blog/',
        '/blog/all',
        '/blog/pg-:num',
        '/blog/:article'
      ]],
      '/media/', [[
        '/media/',
        '/media/all',
        '/media/pg-:num',
        '/media/:article'
      ]]
    ]
  })

  const pathReStringify = p => {
    if (p.pathre)
      p.pathre = String(p.pathre)
    if (p.pagearr)
      p.pagearr = p.pagearr.map(pathReStringify)

    return p
  }

  const pathdefsConverted = pathReStringify(pathdefsm)

  assert.deepStrictEqual(pathdefsConverted, {
    path: '',
    pathvanilla: '',
    pathre: pathdefsConverted.pathre,
    pathpattern: 'root',
    pagearr: [{
      path: '/',
      pathvanilla: '/',
      pathre: String(pathdefsConverted.pagearr[0].pathre),
      pathpattern: 'root-pg-index'
    }, {
      path: '/about/',
      pathvanilla: '/about',
      pathre: pathdefsConverted.pagearr[1].pathre,
      pathpattern: 'root-about'
    }, {
      path: '/links/',
      pathvanilla: '/links',
      pathre: pathdefsConverted.pagearr[2].pathre,
      pathpattern: 'root-links'
    }, {
      path: '/gallery/',
      pathvanilla: '/gallery',
      pathre: pathdefsConverted.pagearr[3].pathre,
      pathpattern: 'root-gallery'
    }, {
      path: '/site-map/',
      pathvanilla: '/site-map',
      pathre: pathdefsConverted.pagearr[4].pathre,
      pathpattern: 'root-site-map'
    }, {
      path: '/blog/{:item}',
      pathvanilla: '/blog',
      pathre: pathdefsConverted.pagearr[5].pathre,
      pathpattern: 'root-blog',
      pagearr: [{
        path: "/",
        pathvanilla: "/",
        pathpattern: "root-blog-pg-index",
        pathre: pathdefsConverted.pagearr[5].pagearr[0].pathre
      }, {
        path: "/all",
        pathvanilla: "/all",
        pathpattern: "root-blog-all",
        pathre: pathdefsConverted.pagearr[5].pagearr[1].pathre
      }, {
        path: "/pg-:num",
        pathvanilla: "/pg-:num",
        pathpattern: "root-blog-pg-num",
        pathre: pathdefsConverted.pagearr[5].pagearr[2].pathre
      }, {
        path: "/:article",
        pathvanilla: "/:article",
        pathpattern: "root-blog-article",
        pathre: pathdefsConverted.pagearr[5].pagearr[3].pathre
      }]
    }, {
      path: '/media/{:item}',
      pathvanilla: '/media',
      pathre: pathdefsConverted.pagearr[6].pathre,
      pathpattern: 'root-media',
      pagearr: [{
        path: '/',
        pathpattern: 'root-media-pg-index',
        pathre: pathdefsConverted.pagearr[6].pagearr[0].pathre,
        pathvanilla: '/'
      }, {
        path: '/all',
        pathpattern: 'root-media-all',
        pathre: pathdefsConverted.pagearr[6].pagearr[1].pathre,
        pathvanilla: '/all'
      }, {
        path: '/pg-:num',
        pathpattern: 'root-media-pg-num',
        pathre: pathdefsConverted.pagearr[6].pagearr[2].pathre,
        pathvanilla: '/pg-:num'
      }, {
        path: '/:article',
        pathpattern: 'root-media-article',
        pathre: pathdefsConverted.pagearr[6].pagearr[3].pathre,
        pathvanilla: '/:article'
      }]
    }]
  })
})

test('findmatchpatharr returns matched paths ...', () => {
  const cfg = {}
  const routesobj = gnpathdef_createfrommanifest({
    deploytype: 'flat',
    routes: [
      '/',
      '/about/',
      '/links/',
      '/gallery/',
      '/site-map/',
      '/blog/', [[
        '/blog/',
        '/blog/all',
        '/blog/pg-:num',
        '/blog/:article'
      ]],
      '/media/', [[
        '/media/',
        '/media/all',
        '/media/pg-:num',
        '/media/:article'
      ]]
    ]
  })

  const matchedpatharr = gnpathdef_findmatchpatharr(
    cfg, ['blog/', 'article-543'], routesobj.pagearr, [routesobj])

  assert.deepStrictEqual(matchedpatharr, [
    routesobj,
    routesobj.pagearr
      .find(ro => ro.path === '/blog/{:item}'),
    routesobj.pagearr
      .find(ro => ro.path === '/blog/{:item}')
      .pagearr.find(ro => ro.path === '/:article')
  ])
})

test('findmatchpatharr returns matched paths ...', () => {
  const cfg = mockcfg({
    manifest: {
      deploytype: 'flat',
      routes: [
        '/',
        '/about/',
        '/links/',
        '/gallery/',
        '/site-map/',
        '/blog/', [[
          '/blog/',
          '/blog/all',
          '/blog/pg-:num',
          '/blog/:article'
        ]],
        '/media/', [[
          '/media/',
          '/media/all',
          '/media/pg-:num',
          '/media/:article'
        ]]
      ]
    }
  })

  const pathpgarr = gnpathdef_pathpgarrget(cfg, '/blog/blog-all/')

  assert.ok(pathpgarr)
})

test('haspathstr should return true for matching pathdef', () => {
  const cfg = mockcfg({
    manifest: {
      deploytype: 'flat',
      routes: [
        '/',
        '/about/',
        '/links/',
        '/gallery/',
        '/site-map/',
        '/blog/', [[
          '/blog/',
          '/blog/all',
          '/blog/pg-:num',
          '/blog/:article'
        ]],
        '/media/', [[
          '/media/',
          '/media/all',
          '/media/pg-:num',
          '/media/:article'
        ]]
      ]
    }
  })

  assert.ok(gnpathdef_haspathstr(cfg.pathdef.pagearr, '/blog/'))
})

