import test from 'node:test'
import assert from 'node:assert/strict'
import uih1 from '../../src/basic/uih1.js'
import mockcfg from '../helper/mockcfg.js'
import mocksess from '../helper/mocksess.js'
import promisify from '../helper/promisify.js'

import {
  gnacc_subj
} from '../../src/gnacc.js'

import {
  gnnode_dfshydrateall
} from '../../src/gnnode.js'

import {
  gngraph_create
} from '../../src/gngraph.js'

import {
  gnreport_get
} from '../../src/gnreport.js'

test('uih1 should render subj.label', async () => {
  const sess = mocksess()
  const cfg = mockcfg({
    nodes: [
      uih1
    ]
  })
  const graphstart = gngraph_create({
    '/test': {
      key: '/test',
      node: 'uih1',
      name: 'text',
      subj: {
        label: 'test label'
      }
    }
  })

  const [graph] = await promisify(gnnode_dfshydrateall)(
    sess, cfg, graphstart, graphstart.get('/test'), gnreport_get('test'))
  const h1nodekey = '/test'
  const h1nodesubj = gnacc_subj.get(graph.get(h1nodekey))

  const h1elem = uih1.getvnode(
    sess, cfg, graph, graph.get(h1nodekey), uih1, h1nodesubj, {
      label: 'test label'
    })

  assert.strictEqual(h1elem.children, 'test label')
})
