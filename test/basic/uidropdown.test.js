import test from 'node:test'
import assert from 'node:assert/strict'
import { setTimeout } from 'node:timers/promises'
import mockcfg from '../helper/mockcfg.js'
import mocksess from '../helper/mocksess.js'
import promisify from '../helper/promisify.js'
import uitext from '../../src/basic/uitext.js'
import uilabel from '../../src/basic/uilabel.js'
import uispread from '../../src/basic/uispread.js'
import uifieldset from '../../src/basic/uifieldset.js'
import uidropdown from '../../src/basic/uidropdown.js'
import gnpgdata from '../../src/gnpgdata.js'

import {
  gnacc_subj
} from '../../src/gnacc.js'

import {
  gndom_nodeelem
} from '../../src/gndom.js'

import {
  gnpublish_vtree
} from '../../src/gnpublish.js'

import {
  gnnode_dfshydrateall
} from '../../src/gnnode.js'

import {
  gngraph_create
} from '../../src/gngraph.js'

import {
  gnenumSPECPROPNAMETYPE
} from '../../src/gnenum.js'

import {
  gnreport_get
} from '../../src/gnreport.js'

test('should render', async () => {
  const sess = mocksess()
  const cfg = mockcfg({
    nodes: [
      uilabel,
      uispread,
      uifieldset,
      uitext,
      uidropdown,
      gnpgdata
    ]
  })
  const graphstart = gngraph_create({
    '/test': {
      key: '/test',
      node: "uispread",
      name: "sample",
      domchildkeyarr: [],
      child: [{
        node: "gnpgdata",
        name: "data",
        subj: [{
          subjvalue: "1"
        }]
      }, {
        node: "uifieldset",
        name: "fieldset",
        subj: {
          labellegend: "uidropdown"
        },
        child: [{
          node: "uidropdown",
          name: "dropdownflat",
          className: "sample",
          subj: [{
            optimised: true,
            labelprimary: "labelprimary flat"
          },{
            name: "value",
            [gnenumSPECPROPNAMETYPE]: "nsprop",
            prop: "[../../data].subj.subjvalue"
          }],
          full: {
            [gnenumSPECPROPNAMETYPE]: "literal",
            value: ["0", "1", "2", "3"]
          }
        },{
          node: "uidropdown",
          name: "dropdown",
          className: "sample",
          full: {
            note: [
              "this could be defined as type 'cb'",
              "using a callback to obtain list from service"
            ],
            [gnenumSPECPROPNAMETYPE]: "literal",
            value: [
              { name: "option 0", type: "0" },
              { name: "option 1", type: "1" },
              { name: "option 2", type: "2" },
              { name: "option 3", type: "3" }]
          },
          part: [{
            name: "labelprimary",
            [gnenumSPECPROPNAMETYPE]: "nsprop",
            prop: "part.name"
          },{
            name: "return",
            [gnenumSPECPROPNAMETYPE]: "nsprop",
            prop: "part.type"
          },{
            name: "fullkey",
            [gnenumSPECPROPNAMETYPE]: "nsprop",
            prop: "part.type"
          }],
          subj: [{
            labelprimary: "labelprimary",
            sorting: {
              type: "none",
              fullobjprop: "type"
            }
          },{
            name: "value",
            [gnenumSPECPROPNAMETYPE]: "nsprop",
            prop: "[../../data].subj.subjvalue"
          }]
        }]
      },{
        node: "uilabel",
        name: "examplelabel",
        className: "sample",
        subj: [{
          labelprimary: "dropdown updates value"
        },{
          name: "label",
          [gnenumSPECPROPNAMETYPE]: "nsprop",
          prop: "[../data].subj.subjvalue"
        }]
      }]
    }
  })

  const [graph] = await promisify(gnnode_dfshydrateall)(
    sess, cfg, graphstart, graphstart.get('/test'), gnreport_get('test'))
  const dropdownnodekey = '/test/fieldset/dropdown'
  const vnode = uidropdown.getvnode(
    sess, cfg, graph, graph.get(dropdownnodekey), uidropdown,
    gnacc_subj.get(graph.get(dropdownnodekey)))

  global.window = cfg.dom.window
  global.document = cfg.dom.window.document
  gnpublish_vtree(sess, cfg, graph, vnode)

  const nodesubj = gnacc_subj.get(graph.get(dropdownnodekey))
  const elemdropdown = gndom_nodeelem(cfg, graph.get(dropdownnodekey))

  assert.deepStrictEqual(nodesubj, {
    value: '1',
    labelprimary: 'labelprimary',
    sorting: { type: 'none', fullobjprop: 'type' }
  })

  assert.strictEqual(elemdropdown.value, nodesubj.value)

  uidropdown.onsubj(
    sess, cfg, graph, graph.get(dropdownnodekey), uidropdown, nodesubj, {
      value: '2'
    })

  await setTimeout(500)
  assert.strictEqual(elemdropdown.value, '2')
  // assert.strictEqual(elemlabel.textContent, 'updated')
})
