import test from 'node:test'
import assert from 'node:assert/strict'
import mockcfg from '../helper/mockcfg.js'
import mocksess from '../helper/mocksess.js'
import promisify from '../helper/promisify.js'
import uiactivebox from '../../src/basic/uiactivebox.js'

import {
  gndom_nodeelem
} from '../../src/gndom.js'

import {
  gnpublish_vtree
} from '../../src/gnpublish.js'

import {
  gnnode_dfshydrateall
} from '../../src/gnnode.js'

import {
  gngraph_create
} from '../../src/gngraph.js'

import {
  gnreport_get
} from '../../src/gnreport.js'

test('should render', async () => {
  const sess = mocksess()
  const cfg = mockcfg({ nodes: [uiactivebox] })
  const graphstart = gngraph_create({
    '/test': {
      key: '/test',
      name: 'anode',
      node: 'uiactivebox',
      domchildkeyarr: [],
      subj: {
        labelprimary: "labelprimary",
        isactive: true
      }
    }
  })

  const [graph] = await promisify(gnnode_dfshydrateall)(
    sess, cfg, graphstart, graphstart.get('/test'), gnreport_get('test'))

  const vnode = uiactivebox.getvnode(
    sess, cfg, graph, graph.get('/test'), uiactivebox,
    graph.get('/test').get('subjhydrated'))

  global.window = cfg.dom.window
  global.document = cfg.dom.window.document
  gnpublish_vtree(sess, cfg, graph, vnode)

  const elem = gndom_nodeelem(cfg, graph.get('/test'))
  assert.ok(elem.className.includes('st-isactive-true'))
  assert.ok(!elem.className.includes('st-isactive-false'))
  uiactivebox.onsubj(
    sess, cfg, graph, graph.get('/test'), uiactivebox,
    {}, { isactive: false })
  assert.ok(!elem.className.includes('st-isactive-true'))
  assert.ok(elem.className.includes('st-isactive-false'))
})
