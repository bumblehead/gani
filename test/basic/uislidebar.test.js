import test from 'node:test'
import assert from 'node:assert/strict'
import mockcfg from '../helper/mockcfg.js'
import mocksess from '../helper/mocksess.js'
import promisify from '../helper/promisify.js'
import uislidebar from '../../src/basic/uislidebar.js'

import {
  gndom_nodeelem
} from '../../src/gndom.js'

import {
  gnpublish_vtree
} from '../../src/gnpublish.js'

import {
  gnnode_dfshydrateall
} from '../../src/gnnode.js'

import {
  gngraph_create
} from '../../src/gngraph.js'

import {
  gnenumSPECPROPNAMETYPE
} from '../../src/gnenum.js'

import {
  gnreport_get
} from '../../src/gnreport.js'

test('should render', async () => {
  const sess = mocksess()
  const cfg = mockcfg({ nodes: [uislidebar] })
  const graphstart = gngraph_create({
    '/test': {
      key: '/test',
      name: 'anode',
      node: 'uislidebar',
      full: {
        [gnenumSPECPROPNAMETYPE]: "literal",
        value: [
          { name: "option 0", type: "0" },
          { name: "option 1", type: "1" },
          { name: "option 2", type: "2" }]
      },
      part: [{
        name: "labelprimary",
        [gnenumSPECPROPNAMETYPE]: "nsprop",
        prop: "part.name"
      },{
        name: "return",
        [gnenumSPECPROPNAMETYPE]: "nsprop",
        prop: "part.type"
      },{
        name: "fullkey",
        [gnenumSPECPROPNAMETYPE]: "nsprop",
        prop: "part.type"
      }]
    }
  })

  const [graph] = await promisify(gnnode_dfshydrateall)(
    sess, cfg, graphstart, graphstart.get('/test'), gnreport_get('test'))

  const vnode = uislidebar.getvnode(
    sess, cfg, graph, graph.get('/test'), uislidebar, {
      labelprimary: 'labelprimary',
      labelsecondary: 'labelsecondary',
      percent_current: .30,
      states: ['one', 'two', 'three', 'four']
    })

  global.window = cfg.dom.window
  global.document = cfg.dom.window.document
  gnpublish_vtree(sess, cfg, graph, vnode)

  const elem = gndom_nodeelem(cfg, graph.get('/test'))
  const elemprogress = elem.querySelectorAll('progress')[0]
  const elemrange = elem.querySelectorAll('input[type=range]')[0]
  assert.ok(elem.className.includes('st-state-two'))
  assert.strictEqual(elemprogress.value, .30)
  assert.strictEqual(elemrange.value, '300')

  uislidebar.onsubj(
    sess, cfg, graph, graph.get('/test'), uislidebar,
    {}, {
      percent_current: .80,
      states: ['one', 'two', 'three', 'four']
    })
  assert.ok(elem.className.includes('st-state-three'))
  assert.strictEqual(elemprogress.value, .80)
  assert.strictEqual(elemrange.value, '800')
})
