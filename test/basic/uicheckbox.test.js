import test from 'node:test'
import assert from 'node:assert/strict'
import mockcfg from '../helper/mockcfg.js'
import mocksess from '../helper/mocksess.js'
import promisify from '../helper/promisify.js'
import uitext from '../../src/basic/uitext.js'
import uilabel from '../../src/basic/uilabel.js'
import uispread from '../../src/basic/uispread.js'
import uicheckbox from '../../src/basic/uicheckbox.js'
import uifieldset from '../../src/basic/uifieldset.js'

import gnpgdata from '../../src/gnpgdata.js'

import {
  gnacc_subj
} from '../../src/gnacc.js'

import {
  gndom_nodeelem
} from '../../src/gndom.js'

import {
  gnpublish_vtree
} from '../../src/gnpublish.js'

import {
  gnnode_dfshydrateall
} from '../../src/gnnode.js'

import {
  gngraph_create
} from '../../src/gngraph.js'

import {
  gnenumSPECPROPNAMETYPE
} from '../../src/gnenum.js'

import {
  gnreport_get
} from '../../src/gnreport.js'

test('should render', async () => {
  const sess = mocksess()
  const cfg = mockcfg({
    specfn: {
      getasstring: ([a]) => String(a)
    },
    nodes: [
      uilabel,
      uispread,
      uifieldset,
      uitext,
      uicheckbox,
      gnpgdata
    ]
  })
  const graphstart = gngraph_create({
    '/test': {
      key: '/test',
      node: "uispread",
      name: "sample",
      domchildkeyarr: [],
      child: [{
        node: "gnpgdata",
        name: "data",
        subj: [{
          subjvalue: true
        }]
      },{
        node: "uifieldset",
        name: "fieldset",
        subj: {
          labellegend: "uitext"
        },
        child: [{
          node: "uicheckbox",
          name: "checkbox",
          subj: [{
            labelprimary: "labelprimary"
          },{
            name: "value",
            [gnenumSPECPROPNAMETYPE]: "nsprop",
            prop: "[../../data].subj.subjvalue"
          }]
        }]
      },{
        node: "uilabel",
        name: "examplelabel",
        className: "sample",
        subj: [{
          labelprimary: "checkbox updates label"
        },{
          name: "label",
          [gnenumSPECPROPNAMETYPE]: "nsprop",
          prop: "[../data].subj.subjvalue",
          filterinarr: [{
            [gnenumSPECPROPNAMETYPE]: "fn",
            fnname: "getasstring",
            args: ["this"]
          }]
        }]
      }]
    }
  })

  const [graph] = await promisify(gnnode_dfshydrateall)(
    sess, cfg, graphstart, graphstart.get('/test'), gnreport_get('test'))
  const checkboxnodekey = '/test/fieldset/checkbox'
  const vnode = uicheckbox.getvnode(
    sess, cfg, graph, graph.get(checkboxnodekey), uicheckbox,
    gnacc_subj.get(graph.get(checkboxnodekey)))

  global.window = cfg.dom.window
  global.document = cfg.dom.window.document
  gnpublish_vtree(sess, cfg, graph, vnode)

  const nodesubj = gnacc_subj.get(graph.get(checkboxnodekey))
  const elemcheckbox = gndom_nodeelem(cfg, graph.get(checkboxnodekey))

  assert.deepStrictEqual(nodesubj, {
    labelprimary: 'labelprimary',
    value: true
  })

  assert.strictEqual(elemcheckbox.checked, nodesubj.value)

  uicheckbox.onsubj(
    sess, cfg, graph, graph.get(checkboxnodekey), uicheckbox, nodesubj, {
      value: false
    })

  assert.strictEqual(elemcheckbox.checked, false)
})
