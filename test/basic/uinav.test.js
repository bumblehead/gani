import test from 'node:test'
import assert from 'node:assert/strict'
import { setTimeout } from 'node:timers/promises'
import mockcfg from '../helper/mockcfg.js'
import mocksess from '../helper/mocksess.js'
import promisify from '../helper/promisify.js'
import cleanhtml from '../helper/cleanhtml.js'
import uitext from '../../src/basic/uitext.js'
import uilabel from '../../src/basic/uilabel.js'
import uispread from '../../src/basic/uispread.js'
import uifieldset from '../../src/basic/uifieldset.js'
import uinav from '../../src/basic/uinav.js'
import gnpgdata from '../../src/gnpgdata.js'

import {
  gnacc_subj
} from '../../src/gnacc.js'

import {
  gndom_nodeelem
} from '../../src/gndom.js'

import {
  gnpublish_vtree
} from '../../src/gnpublish.js'

import {
  gnnode_dfshydrateall
} from '../../src/gnnode.js'

import {
  gngraph_create
} from '../../src/gngraph.js'

import {
  gnenumSPECPROPNAMETYPE
} from '../../src/gnenum.js'

import {
  gnreport_get
} from '../../src/gnreport.js'

const htmluinav = `
<div>
  <div class="uinav uinav" id=":test:fieldset:uinav">
    <span class="uinav uinav-label-primary">label</span>
    <ul class="uinav uinav-list">
      <li class="uinav uinav-listitem">
        <a class="uinav uinav-listitem-target st-isactive-false"
            id=":test:fieldset:uinav---3">
          <span class="uinav uinav-listitem-target-label-primary">option
              3</span>
        </a>
      </li>
      <li class="uinav uinav-listitem">
        <a class="uinav uinav-listitem-target st-isactive-true"
            id=":test:fieldset:uinav---2">
          <span class="uinav uinav-listitem-target-label-primary">option
              2</span>
        </a>
      </li>
      <li class="uinav uinav-listitem">
        <a class="uinav uinav-listitem-target st-isactive-false"
            id=":test:fieldset:uinav---1">
          <span class="uinav uinav-listitem-target-label-primary">option
              1</span>
        </a>
      </li>
      <li class="uinav uinav-listitem">
        <a class="uinav uinav-listitem-target st-isactive-false"
            id=":test:fieldset:uinav---0">
          <span class="uinav uinav-listitem-target-label-primary">option
              0</span>
        </a>
      </li>
    </ul>
  </div>
</div>
`.slice(1, -1)

test('should render', async () => {
  const sess = mocksess()
  const cfg = mockcfg({
    nodes: [
      uilabel,
      uispread,
      uifieldset,
      uitext,
      uinav,
      gnpgdata
    ]
  })
  const graphstart = gngraph_create({
    '/test': {
      key: '/test',
      node: "uispread",
      name: "sample",
      domchildkeyarr: [],
      child: [{
        node: "gnpgdata",
        name: "data",
        subj: [{
          subjvalue: "1"
        }]
      }, {
        node: "uifieldset",
        name: "fieldset",
        subj: {
          labellegend: "uinav"
        },
        child: [{
          node: "uinav",
          name: "uinavflat",
          subj: [{
            optimised: true
          },{
            name: "value",
            [gnenumSPECPROPNAMETYPE]: "nsprop",
            prop: "[../../data].subj.subjvalue"
          }],
          full: {
            [gnenumSPECPROPNAMETYPE]: "literal",
            value: ["0", "1", "2", "3"]
          }
        },{
          node: "uinav",
          name: "uinav",
          full: {
            note: [
              "this could be defined as type 'cb'",
              "using a callback to obtain list from service"
            ],
            [gnenumSPECPROPNAMETYPE]: "literal",
            value: [
              { name: "option 0", type: "0" },
              { name: "option 1", type: "1" },
              { name: "option 2", type: "2" },
              { name: "option 3", type: "3" }]
          },
          part: [{
            name: "label",
            [gnenumSPECPROPNAMETYPE]: "nsprop",
            prop: "part.name"
          },{
            name: "return",
            [gnenumSPECPROPNAMETYPE]: "nsprop",
            prop: "part.type"
          },{
            name: "fullkey",
            [gnenumSPECPROPNAMETYPE]: "nsprop",
            prop: "part.type"
          }],
          subj: [{
            label: "label",
            sorting: {
              type: "none",
              fullobjprop: "type"
            }
          },{
            name: "value",
            [gnenumSPECPROPNAMETYPE]: "nsprop",
            prop: "[../../data].subj.subjvalue"
          }]
        }]
      },{
        node: "uilabel",
        name: "examplelabel",
        className: "sample",
        subj: [{
          labelprimary: "uinav updates value"
        },{
          name: "label",
          [gnenumSPECPROPNAMETYPE]: "nsprop",
          prop: "[../data].subj.subjvalue"
        }]
      }]
    }
  })

  const [graph] = await promisify(gnnode_dfshydrateall)(
    sess, cfg, graphstart, graphstart.get('/test'), gnreport_get('test'))
  const uinavnodekey = '/test/fieldset/uinav'
  const vnode = uinav.getvnode(
    sess, cfg, graph, graph.get(uinavnodekey), uinav,
    gnacc_subj.get(graph.get(uinavnodekey)))

  global.window = cfg.dom.window
  global.document = cfg.dom.window.document
  gnpublish_vtree(sess, cfg, graph, vnode)

  const nodesubj = gnacc_subj.get(graph.get(uinavnodekey))
  const elemuinav = gndom_nodeelem(cfg, graph.get(uinavnodekey))

  assert.deepStrictEqual(nodesubj, {
    value: '1',
    label: 'label',
    sorting: { type: 'none', fullobjprop: 'type' }
  })

  const elem = elemuinav.getElementsByClassName('st-isactive-true')
  assert.strictEqual(elem[0].text, `option 1`)
  assert.strictEqual(elem[0].text, `option ${nodesubj.value}`)

  uinav.onsubj(
    sess, cfg, graph, graph.get(uinavnodekey), uinav, nodesubj, {
      value: '2'
    })

  await setTimeout(500)
  assert.strictEqual(elem[0].text, `option 2`)
  assert.strictEqual(
    await cleanhtml(cfg.dom.window.document.body.innerHTML), htmluinav)
})
