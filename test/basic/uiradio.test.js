import test from 'node:test'
import assert from 'node:assert/strict'
import mockcfg from '../helper/mockcfg.js'
import mocksess from '../helper/mocksess.js'
import promisify from '../helper/promisify.js'
import uiradio from '../../src/basic/uiradio.js'

import {
  gndom_inputfromname
} from '../../src/gndom.js'

import {
  gnpublish_vtree
} from '../../src/gnpublish.js'

import {
  gnnode_dfshydrateall
} from '../../src/gnnode.js'

import {
  gngraph_create
} from '../../src/gngraph.js'

import {
  gnenumSPECPROPNAMETYPE
} from '../../src/gnenum.js'

import {
  gnreport_get
} from '../../src/gnreport.js'

test('should render', async () => {
  const sess = mocksess()
  const cfg = mockcfg({ nodes: [uiradio] })
  const graphstart = gngraph_create({
    '/test': {
      key: '/test',
      name: 'anode',
      node: 'uiradio',
      subj: {
        value: ''
      },
      full: {
        [gnenumSPECPROPNAMETYPE]: "literal",
        value: [
          { name: "option 0", type: "0" },
          { name: "option 1", type: "1" },
          { name: "option 2", type: "2" }]
      },
      part: [{
        name: "labelprimary",
        [gnenumSPECPROPNAMETYPE]: "nsprop",
        prop: "part.name"
      },{
        name: "return",
        [gnenumSPECPROPNAMETYPE]: "nsprop",
        prop: "part.type"
      },{
        name: "fullkey",
        [gnenumSPECPROPNAMETYPE]: "nsprop",
        prop: "part.type"
      }]
    }
  })

  const [graph] = await promisify(gnnode_dfshydrateall)(
    sess, cfg, graphstart, graphstart.get('/test'), gnreport_get('test'))

  const vnode = uiradio.getvnode(
    sess, cfg, graph, graph.get('/test'), uiradio, {
      labelprimary: 'labelprimary',
      labelsecondary: 'labelsecondary',
      labeltertiary: 'labeltertiary'
    })

  global.window = cfg.dom.window
  global.document = cfg.dom.window.document
  gnpublish_vtree(sess, cfg, graph, vnode)

  const inputradio = gndom_inputfromname(cfg, '/test')

  assert.ok(inputradio)
})
