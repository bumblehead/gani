import test from 'node:test'
import assert from 'node:assert/strict'
import mockcfg from '../helper/mockcfg.js'
import mocksess from '../helper/mocksess.js'
import promisify from '../helper/promisify.js'
import uitext from '../../src/basic/uitext.js'
import uilabel from '../../src/basic/uilabel.js'
import uispread from '../../src/basic/uispread.js'
import uifieldset from '../../src/basic/uifieldset.js'

import gnpgdata from '../../src/gnpgdata.js'

import {
  gnacc_subj
} from '../../src/gnacc.js'

import {
  gndom_nodeelem
} from '../../src/gndom.js'

import {
  gnpublish_vtree
} from '../../src/gnpublish.js'

import {
  gnnode_dfshydrateall
} from '../../src/gnnode.js'

import {
  gngraph_create
} from '../../src/gngraph.js'

import {
  gnenumSPECPROPNAMETYPE
} from '../../src/gnenum.js'

import {
  gnreport_get
} from '../../src/gnreport.js'

test('should render', async () => {
  const sess = mocksess()
  const cfg = mockcfg({
    nodes: [
      uilabel,
      uispread,
      uifieldset,
      uitext,
      gnpgdata
    ]
  })
  const graphstart = gngraph_create({
    '/test': {
      key: '/test',
      node: "uispread",
      name: "sample",
      domchildkeyarr: [],
      child: [{
        node: "gnpgdata",
        name: "data",
        subj: [{
          subjvalue: "edit me!"
        }]
      },{
        node: "uifieldset",
        name: "fieldset",
        subj: {
          labellegend: "uilabel"
        },
        child: [{
          node: "uilabel",
          name: "label",
          className: "sample",
          subj: [{
            name: "label",
            [gnenumSPECPROPNAMETYPE]: "nsprop",
            prop: "[../../data].subj.subjvalue"
          }]
        }]
      },{
        node: "uitext",
        name: "text",
        className: "sample",
        subj: [{
          labelprimary: "text field updates the label"
        },{
          name: "value",
          [gnenumSPECPROPNAMETYPE]: "nsprop",
          prop: "[../data].subj.subjvalue"
        }]
      }]
    }
  })

  const [graph] = await promisify(gnnode_dfshydrateall)(
    sess, cfg, graphstart, graphstart.get('/test'), gnreport_get('test'))
  const labelnodekey = '/test/fieldset/label'
  const vnode = uilabel.getvnode(
    sess, cfg, graph, graph.get(labelnodekey), uilabel,
    gnacc_subj.get(graph.get(labelnodekey)))

  global.window = cfg.dom.window
  global.document = cfg.dom.window.document
  gnpublish_vtree(sess, cfg, graph, vnode)

  const nodesubj = gnacc_subj.get(graph.get(labelnodekey))
  const elemlabel = gndom_nodeelem(cfg, graph.get(labelnodekey))

  assert.deepStrictEqual(nodesubj, { label: 'edit me!' })
  assert.strictEqual(elemlabel.textContent, nodesubj.label)

  uilabel.onsubj(
    sess, cfg, graph, graph.get(labelnodekey), uilabel, nodesubj, {
      label: 'updated'
    })

  assert.strictEqual(elemlabel.textContent, 'updated')
})
