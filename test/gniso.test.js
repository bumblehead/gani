import test from 'node:test'
import assert from 'node:assert/strict'

import mocksess from './helper/mocksess.js'

import {
  gniso_getpathiso,
  gniso_getlocalebest,
  gniso_patternfromfullpath,
  gniso_patternfrompath
} from '../src/gniso.js'

import {
  gnerr_isopatterninvalid,
  gnerr_isopathinvalid
} from '../src/gnerr.js'

import {
  gnenumSPECPROPNAMETYPE
} from '../src/gnenum.js'

test('getpathiso, returns an iso session path', () => {
  const sess = { locale: 'eng-US' }
  const pathisotpl = '/page/data/type-countries-arr/baseLangRegion.json'
  const pathisosess = gniso_getpathiso(sess, pathisotpl)

  assert.strictEqual(
    pathisosess, '/page/data/type-countries-arr/eng-US.json')
})

test('getpathiso, throws error if invalid path', async () => {
  const sess = { locale: 'eng-US' }
  const pathisotpl = '/page/data/type-countries-arr/invalidname.json'

  await assert.rejects(async () => gniso_getpathiso(sess, pathisotpl), {
    message: gnerr_isopathinvalid(pathisotpl).message
  })
})

test('getlocalebest returns best locale', async () => {
  assert.strictEqual(
    gniso_getlocalebest('any', 'eng-US'),
    'eng-US')

  assert.strictEqual(
    gniso_getlocalebest('eng', 'eng-US'),
    'eng-US')

  assert.strictEqual(
    gniso_getlocalebest('eng', 'eng-US,spa-ES'),
    'eng-US')

  assert.strictEqual(
    gniso_getlocalebest('eng', 'spa-ES,eng-US'),
    'eng-US')

  assert.strictEqual(
    gniso_getlocalebest('eng', 'eng-US,eng'),
    'eng')

  assert.strictEqual(
    gniso_getlocalebest('eng', 'eng-US,eng-GB'),
    'eng-US')
})

test('patternfromfullpath, returns configured iso pattern', async () => {
  const sess = { locale: 'eng-US' }
  const pathisotpl = '/page/data/type-countries-arr/baseLangRegion.json'
  const pathisosesspattern = ({
    node: 'gnpgdata',
    name: 'dataenv',
    subj: [{
      [gnenumSPECPROPNAMETYPE]: 'fn',
      fnname: 'getrequrl',
      name: 'requrl'
    }]
  })

  const pattern = await gniso_patternfromfullpath(sess, {
    pagespecfromisopath: () => pathisosesspattern
  }, pathisotpl)

  assert.deepStrictEqual(pattern, pathisosesspattern)
})

test('patternfromfullpath, throws error if no pattern', async () => {
  const sess = mocksess()
  const pathisotpl = '/page/data/type-countries-arr/baseLangRegion.json'
  const pathisosesspattern = null

  await assert.rejects(() => gniso_patternfromfullpath(sess, {
    pagespecfromisopath: () => pathisosesspattern
  }, pathisotpl), {
    message: gnerr_isopatterninvalid(
      gniso_getpathiso(sess, pathisotpl)).message
  })
})

test('patternfrompath, returns pattern from relpath', async () => {
  const sess = mocksess()
  const pathisotplrel = './data/type-countries-arr/'
  const pathisofull = './data/type-countries-arr/eng-US.json'
  const pathisosesspattern = ({
    node: 'gnpgdata',
    name: 'dataenv',
    subj: [{
      [gnenumSPECPROPNAMETYPE]: 'fn',
      fnname: 'getrequrl',
      name: 'requrl'
    }]
  })

  const pattern = await gniso_patternfrompath(sess, {
    specpath: '/page/',
    manifest: { locales: 'eng-US' },
    cachemapisourlspecs: {},
    pagespecfromisopath: (sess, cfg, path) => {
      return path === pathisofull
        && pathisosesspattern
    }
  }, pathisotplrel)

  assert.deepStrictEqual(pattern, pathisosesspattern)
})
