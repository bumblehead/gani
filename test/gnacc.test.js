import test from 'node:test'
import assert from 'node:assert/strict'

import {
  gnnode_create
} from '../src/gnnode.js'

import {
  gnacc_subj,
  gnacc_fkey
} from '../src/gnacc.js'

test('gnacc.hasspec, true if spec is defined else false', () => {
  const node = gnnode_create({
    name: 'test',
    page: 'test',
    subj: {
      hydrated: true
    }
  })

  assert.strictEqual(true, gnacc_subj.hasspec(node))
  assert.strictEqual(false, gnacc_fkey.hasspec(node))
})
