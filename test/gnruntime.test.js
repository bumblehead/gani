import util from 'node:util'
import test from 'node:test'
import assert from 'node:assert/strict'
import jsdom from 'jsdom'

import mockcfg from './helper/mockcfg.js'
import mocksess from './helper/mocksess.js'

import gnruntime from '../src/gnruntime.js'

import {
  gnerrnode_keynodenotfound,
  gnerrnode_parentkeynotfound,
  gnerrnode_nskeyrequired,
  gnerrnode_pathnotfound
} from '../src/gnerr.js'

import {
  gnnode_create
} from '../src/gnnode.js'

import {
  gngraph_create
} from '../src/gngraph.js'

import {
  gnenumSPECPROPNAMETYPE
} from '../src/gnenum.js'

import {
  gnacc_subj
} from '../src/gnacc.js'

test('retchildkeys, should error when parentkey invalid', async () => {
  const sess = mocksess()
  const cfg = mockcfg()
  const graph = gngraph_create({}).set('/sitemap', gnnode_create({
    node: 'uilink',
    name: 'sitemap',
    subj: [{
      labelprimary: 'site map',
      href: '/site-map/'
    }]
  }))
  const spec = { args: [] }
  const run = gnruntime({}, {}, {})
  await assert.rejects(async () => util.promisify(run.retchildkeys)(
    sess, cfg, graph, graph.get('/sitemap'), 'subj', spec
  ), {
    message: gnerrnode_parentkeynotfound(
      graph, graph.get('/sitemap'), spec).message
  })
})

test('retchildkeys, should error when parentkey node not found', async () => {
  const sess = mocksess()
  const cfg = mockcfg()

  const graph = gngraph_create({}).set('/sitemap', gnnode_create({
    node: 'uilink',
    name: 'sitemap',
    subj: [{
      labelprimary: 'site map',
      href: '/site-map/'
    }]
  }))
  const spec = { args: ['/parent-key-not-found'] }
  const run = gnruntime({}, {}, {})
  await assert.rejects(async () => util.promisify(run.retchildkeys)(
    sess, cfg, graph, graph.get('/sitemap'), 'subj', spec
  ), {
    message: gnerrnode_keynodenotfound(
      graph, graph.get('/sitemap'), spec.args[0]).message
  })
})

test('retfirstchildkey, should error when data node not found', async () => {
  const sess = mocksess()
  const cfg = mockcfg()

  const graph = gngraph_create({}).set('/sitemap', gnnode_create({
    node: 'uilink',
    name: 'sitemap',
    subj: [{
      labelprimary: 'site map',
      href: '/site-map/'
    }]
  }))
  const spec = { path: '/data-key-not-found' }
  const run = gnruntime({}, {}, {})
  await assert.rejects(async () => util.promisify(run.retdatafirstchildkey)(
    sess, cfg, graph, graph.get('/sitemap'), 'subj', spec
  ), {
    message: gnerrnode_pathnotfound(
      graph, graph.get('/sitemap'), spec.path).message
  })
})

test('retchildcount, should error when data node not found', async () => {
  const sess = mocksess()
  const cfg = mockcfg()

  const graph = gngraph_create({}).set('/sitemap', gnnode_create({
    node: 'uilink',
    name: 'sitemap',
    subj: [{
      labelprimary: 'site map',
      href: '/site-map/'
    }]
  }))
  const spec = { path: '/data-key-not-found' }
  const run = gnruntime({}, {}, {})
  await assert.rejects(async () => util.promisify(run.retchildcount)(
    sess, cfg, graph, graph.get('/sitemap'), 'subj', spec
  ), {
    message: gnerrnode_pathnotfound(
      graph, graph.get('/sitemap'), spec.path).message
  })
})

test('retnsprop, should error when data nskey not found', async () => {
  const sess = mocksess()
  const cfg = mockcfg()

  const graph = gngraph_create({})
    .set('/data/blog', gnnode_create({
      node: 'gnpgdata',
      name: 'blog',
      datakeyprop: 'id',
      subj: {
        title: 'my title'
      },
      subjhydrated: {
        title: 'my title'
      }
    }))
    .set('/sitemap', gnnode_create({
      node: 'uilink',
      name: 'sitemap',
      fkeyhydrated: {
        blogkey: '/data/blog'
      },
      subj: [{
        labelprimary: 'site map',
        href: '/site-map/'
      }]
    }))

  const spec = {
    // type: 'nsprop',
    isexpanded: true,
    [gnenumSPECPROPNAMETYPE]: 'nsprop',
    prop: 'title',
    name: 'title',
    fullstr: '[fkey.blogkey].subj.title',
    nsstr: 'subj.title',
    // nskey: 'subj',
    nskey: undefined,
    nsprop: 'title',
    fnsstr: 'fkey.blogkey',
    fnspath: undefined,
    fnskey: 'fkey',
    fnsprop: 'blogkey',
    path: undefined
    // name: 'title'
  }

  const run = gnruntime({}, {}, {})
  await assert.rejects(async () => util.promisify(run.retnsprop)(
    sess, cfg, graph, graph.get('/sitemap'), 'subj', spec
  ), {
    message: gnerrnode_nskeyrequired(
      graph, graph.get('/sitemap'), spec).message
  })
})

test('retpublishprop, should publish to graph', async () => {
  const sess = mocksess()
  const cfg = mockcfg()
  const graph = gngraph_create({
    '/sitemap': {
      key: '/sitemap',
      node: 'uilink',
      name: 'sitemap',
      subj: [{
        value: 'greppy'
      }],
      subjhydrated: {
        value: 'greppy'
      }
    }
  })

  const spec = {
    [gnenumSPECPROPNAMETYPE]: "publishprop",
    optarr: [{
      name: "value",
      [gnenumSPECPROPNAMETYPE]: "literal",
      value: "greppy-one-of-a-kind"
    }]
  }

  const run = gnruntime({}, {}, {})
  cfg.graph = graph
  const graphfinal = await util.promisify(run.retpublishprop)(
    sess, cfg, graph, graph.get('/sitemap'),
    graph.get('/sitemap').get('subjhydrated'), spec)

  assert.deepStrictEqual(
    graphfinal.get('/sitemap').get('subjhydrated'), {
      value: "greppy-one-of-a-kind"
    })
})

test('cb.applysubjtochilds, should publish to graph', async () => {
  const sess = mocksess()
  const cfg = mockcfg({
    dom: new jsdom.JSDOM(
      '<!DOCTYPE html><body><div></div></body>')
  })
  const graph = gngraph_create({
    '/datausers': {
      isdata: true,
      key: '/datausers',
      node: 'gnpgdata',
      subj: {
        childlength: 4,
        value: 'greppy'
      },
      subjhydrated: {
        childlength: 4,
        value: 'greppy'
      },
      childarr: ['/datausers/fred']
    },
    '/datausers/fred': {
      isdata: true,
      key: '/datausers/fred',
      node: 'gnpgdata',
      subj: {
        iscompleted: false,
        value: 'greppy'
      },
      subjhydrated: {
        iscompleted: false,
        value: 'greppy'
      },
      childarr: []
    },
    '/': {
      key: '/',
      node: 'uilink',
      name: 'sitemap',
      subj: [{
        value: 'greppy'
      }],
      subjhydrated: {
        value: 'greppy'
      }
    }
  })

  const spec = {
    [gnenumSPECPROPNAMETYPE]: "cb",
    cbname: "applysubjtochilds",
    argsdyn: [1],
    args: ["/datausers", {
      'gn:type': 'obj',
      optarr: [{
        [gnenumSPECPROPNAMETYPE]: "objprop",
        prop: "subj.value",
        name: "iscompleted",
        filterinarr: [{
          [gnenumSPECPROPNAMETYPE]: "fn",
          fnname: "isgreppy",
          args: ["this"]
        }]
      }]
    }]
  }
  const run = gnruntime({
    isgreppy: ([arg]) => {
      return arg === 'greppy'
    }
  }, {}, {})

  global.window = cfg.dom.window
  global.document = cfg.dom.window.document

  assert.deepStrictEqual(
    gnacc_subj.get(graph.get('/datausers/fred')), {
      iscompleted: false,
      value: 'greppy'
    })

  const graphfinal = await util.promisify(run.retcb)(
    sess, cfg, graph, graph.get('/'),
    {
      subj: graph.get('/').get('subjhydrated')
    }, spec)

  assert.deepStrictEqual(
    gnacc_subj.get(graphfinal.get('/datausers/fred')), {
      iscompleted: true,
      value: 'greppy'
    })

  assert.deepStrictEqual(
    gnacc_subj.get(graphfinal.get('/datausers')), {
      childlength: 1,
      value: 'greppy'
    })
})

test('childkeys, should return childkeys', async () => {
  const sess = mocksess()
  const cfg = mockcfg()
  const graph = gngraph_create({
    '/datausers': {
      isdata: true,
      key: '/datausers',
      node: 'gnpgdata',
      subj: {
        childlength: 4,
        value: 'greppy'
      },
      subjhydrated: {
        childlength: 4,
        value: 'greppy'
      },
      childarr: [
        '/datausers/fred',
        '/datausers/sally'
      ]
    },
    '/datausers/fred': {
      isdata: true,
      key: '/datausers/fred',
      node: 'gnpgdata',
      subj: {
        name: 'fred'
      },
      subjhydrated: {
        name: 'fred'
      },
      childarr: []
    },
    '/datausers/sally': {
      isdata: true,
      key: '/datausers/sally',
      node: 'gnpgdata',
      subj: {
        name: 'sally'
      },
      subjhydrated: {
        name: 'sally'
      },
      childarr: []
    }
  })

  const spec = {
    [gnenumSPECPROPNAMETYPE]: "childkeys",
    args: ["/datausers", "subj.name"]
  }

  const res = await util.promisify(cfg.spec.retchildkeys)(
    sess, cfg, graph, graph.get('/'),
    {
      ...graph.get('/datausers').get('subjhydrated')
    }, spec)

  assert.deepStrictEqual(res, [
    '/datausers/fred',
    '/datausers/sally'
  ])
})

test('datafirstchildkey, should return childkeys', async () => {
  const sess = mocksess()
  const cfg = mockcfg()
  const graph = gngraph_create({
    '/datausers': {
      isdata: true,
      key: '/datausers',
      node: 'gnpgdata',
      subj: {
        childlength: 4,
        value: 'greppy'
      },
      subjhydrated: {
        childlength: 4,
        value: 'greppy'
      },
      childarr: [
        '/datausers/fred',
        '/datausers/sally'
      ]
    },
    '/datausers/fred': {
      isdata: true,
      key: '/datausers/fred',
      node: 'gnpgdata',
      subj: {
        name: 'fred'
      },
      subjhydrated: {
        name: 'fred'
      },
      childarr: []
    },
    '/datausers/sally': {
      isdata: true,
      key: '/datausers/sally',
      node: 'gnpgdata',
      subj: {
        name: 'sally'
      },
      subjhydrated: {
        name: 'sally'
      },
      childarr: []
    }
  })

  const spec = {
    path: '/datausers'
  }

  const res = await util.promisify(cfg.spec.retdatafirstchildkey)(
    sess, cfg, graph, graph.get('/'),
    {
      ...graph.get('/datausers').get('subjhydrated')
    }, spec)

  assert.deepStrictEqual(res, '/datausers/fred')
})

test('fn.getchildkeys, should return childkeys', async () => {
  const sess = mocksess()
  const cfg = mockcfg()
  const graph = gngraph_create({
    '/datausers': {
      isdata: true,
      key: '/datausers',
      node: 'gnpgdata',
      subj: {
        childlength: 4,
        value: 'greppy'
      },
      subjhydrated: {
        childlength: 4,
        value: 'greppy'
      },
      childarr: [
        '/datausers/fred',
        '/datausers/sally'
      ]
    },
    '/datausers/fred': {
      isdata: true,
      key: '/datausers/fred',
      node: 'gnpgdata',
      subj: {
        name: 'fred'
      },
      subjhydrated: {
        name: 'fred'
      },
      childarr: []
    },
    '/datausers/sally': {
      isdata: true,
      key: '/datausers/sally',
      node: 'gnpgdata',
      subj: {
        name: 'sally'
      },
      subjhydrated: {
        name: 'sally'
      },
      childarr: []
    }
  })

  const spec = {
    [gnenumSPECPROPNAMETYPE]: "fn",
    fnname: "getchildkeys",
    args: ["/datausers"]
  }

  const res = await util.promisify(cfg.spec.retfn)(
    sess, cfg, graph, graph.get('/datausers'),
    {
      ...graph.get('/datausers').get('subjhydrated')
    }, spec)

  assert.deepStrictEqual(res, [
    '/datausers/fred',
    '/datausers/sally'
  ])
})

test('fn.getchildkeys, should error if parent node not found', async () => {
  const sess = mocksess()
  const cfg = mockcfg()
  const graph = gngraph_create({
    '/datausers': {
      isdata: true,
      key: '/datausers',
      node: 'gnpgdata',
      subj: {
        childlength: 4,
        value: 'greppy'
      },
      subjhydrated: {
        childlength: 4,
        value: 'greppy'
      },
      childarr: [
        '/datausers/fred',
        '/datausers/sally'
      ]
    },
    '/datausers/fred': {
      isdata: true,
      key: '/datausers/fred',
      node: 'gnpgdata',
      subj: {
        name: 'fred'
      },
      subjhydrated: {
        name: 'fred'
      },
      childarr: []
    },
    '/datausers/sally': {
      isdata: true,
      key: '/datausers/sally',
      node: 'gnpgdata',
      subj: {
        name: 'sally'
      },
      subjhydrated: {
        name: 'sally'
      },
      childarr: []
    }
  })

  const spec = {
    [gnenumSPECPROPNAMETYPE]: "fn",
    fnname: "getchildkeys",
    args: ["/datausersNOTFOUND"]
  }

  await assert.rejects(async () => util.promisify(cfg.spec.retfn)(
    sess, cfg, graph, graph.get('/datausers'),
    {
      ...graph.get('/datausers').get('subjhydrated')
    }, spec
  ), {
    message: gnerrnode_keynodenotfound(
      graph, graph.get('/datausers'), "/datausersNOTFOUND").message
  })
})
