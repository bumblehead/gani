import test from 'node:test'
import assert from 'node:assert/strict'
import mockcfg from './helper/mockcfg.js'
import mocksess from './helper/mocksess.js'

import {
  gnnode_create
} from '../src/gnnode.js'

import {
  gngraph_create
} from '../src/gngraph.js'

import {
  gnerr_nodenotfound,
  gnerr_nodechildnotfoundvnode
} from '../src/gnerr.js'

import gnpgui from '../src/gnpgui.js'

test('getkeyvnode should error if key node not found', async () => {
  const sess = mocksess()
  const cfg = mockcfg()
  const graphstart = gngraph_create({})

  await assert.rejects(async () => (
    gnpgui.getkeyvnode(sess, cfg, graphstart, '/missing-node')
  ), {
    message: gnerr_nodenotfound(
      graphstart, '/missing-node').message
  })
})

test('getchildvnode should error if child vnode not found', async () => {
  const sess = mocksess()
  const cfg = mockcfg()
  const graphstart = gngraph_create({})
  const nodebox = gnnode_create({
    key: "/box",
    node: "uiobj",
    name: "box",
    domchildkey: {
      user: undefined
    }
  })

  await assert.rejects(async () => (
    gnpgui.getchildvnode(sess, cfg, graphstart, nodebox, 'user')
  ), {
    message: gnerr_nodechildnotfoundvnode(
      graphstart, nodebox, 'user').message
  })
})
