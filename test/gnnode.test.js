import test from 'node:test'
import assert from 'node:assert/strict'
import nsstr from 'nsstr'
import mockcfg from './helper/mockcfg.js'
import mocksess from './helper/mocksess.js'
import promisify from './helper/promisify.js'

import {
  gnnode_pgget,
  gnnode_fromspecpginit,
  gnnode_create,
  gnnode_parentget,
  gnnode_childrmall,
  gnnode_childfind,
  gnnode_childget,
  gnnode_childgetfirst,
  gnnode_childspecwhenparentfirst,
  gnnode_siblingprevget,
  gnnode_prophydrateonpre,
  gnnode_prophydrateonpost,
  gnnode_prophydratefromspecs,
  gnnode_prophydratefromspectree,
  gnnode_prophydratefromspec,
  gnnode_childsetfromspec,
  gnnode_childspecfromref,
  gnnode_childspecfiltered,
  gnnode_childspeceach,
  gnnode_childspecresolve,
  gnnode_childspecresolveeach,
  gnnode_dfsfind,
  gnnode_dfshydrateall,
  gnnode_edgesetfromspec,
  gnnode_prophydratefromchildsrefresh,
  gnnode_prophydratefromchildsfncreate,
  gnnode_pathrefresolve,
  gnnode_pathresolve,
  gnnode_siblingnextorprevkeyget,
  gnnode_dfsdetachchilds,
  gnnode_dfsattachchilds
} from '../src/gnnode.js'

import {
  gngraph_create
} from '../src/gngraph.js'

import {
  gnacc_groupaccessor,
  gnacc_childsubj,
  gnacc_subj,
  gnacc_fkey,
  gnacc_init,
  gnacc_full,
  gnacc_base
} from '../src/gnacc.js'

import {
  gnerr_nodensnotfound,
  gnerr_nodespecnameinvalid,
  gnerr_nodepgnotfound,
  gnerr_nodedfskeynotfound,
  gnerr_nodespecchildinvalid,
  gnerr_nodechildnameinvalid,

  gnerrnode_childspecnotfound,
  gnerrnode_pathrefnotfound,
  gnerrnode_foreignnamespacenotfound,
  gnerrnode_edgenodenotfound
} from '../src/gnerr.js'

import {
  gnenumSPECPROPNAMETYPE
} from '../src/gnenum.js'

import {
  gnreport_get
} from '../src/gnreport.js'

import {
  mock_graph_todo_json
} from './helper/mockspecs.js'

test('fromspecpginit should return a node', () => {
  const cfg = mockcfg()
  const graphstart = gngraph_create({})
  const node = gnnode_fromspecpginit(cfg, graphstart, {
    name: 'anode',
    node: 'uilink'
  })

  assert.strictEqual(node.get('name'), 'anode')
})

test('pgget should return error if pg not found', async () => {
  const cfg = mockcfg()
  const graphstart = gngraph_create({})
  const node = gnnode_create({
    node: 'NOTFOUND',
    name: 'sitemap'
  })

  await assert.rejects(async () => (
    gnnode_pgget(cfg, graphstart, node)
  ), {
    message: gnerr_nodepgnotfound(graphstart, node).message
  })
})

test('fromspecpginit should error if node spec name invalid', async () => {
  const cfg = mockcfg()
  const graphstart = gngraph_create({})

  await assert.rejects(async () => (
    gnnode_fromspecpginit(cfg, graphstart, {
      node: 'uilink'
    })
  ), {
    message: gnerr_nodespecnameinvalid(graphstart, null, {
      node: 'uilink'
    }).message
  })
})

test('gnnode_parentget should return parent node', () => {
  const graph_todo = gngraph_create(mock_graph_todo_json)
  const node_listusers = graph_todo.get('/todo/content/box/listusers')
  const pnode = gnnode_parentget(graph_todo, node_listusers)

  assert.strictEqual(pnode.get('key'), '/todo/content/box')
})

test('gnnode_childgetfirst, should return first child', async () => {
  const graph = gngraph_create({
    '/box': {
      key: '/box',
      node: 'gnpgui',
      name: 'box',
      childarr: [
        '/box/child'
      ],
      domchildkeyarr: [
        '/box/child'
      ]
    },
    '/box/child': {
      key: '/box/child',
      node: 'gnpgui',
      name: 'child'
    }
  })

  const child = gnnode_childgetfirst(graph, graph.get('/box'))

  assert.strictEqual(child.get('key'), '/box/child')
})

test('gnnode_childget, should return child at name', async () => {
  const graph = gngraph_create({
    '/box': {
      key: '/box',
      node: 'gnpgui',
      name: 'box',
      domchildkeyarr: [
        '/box/child'
      ]
    },
    '/box/child': {
      key: '/box/child',
      node: 'gnpgui',
      name: 'child'
    }
  })

  const child = gnnode_childget(graph, graph.get('/box'), 'child')

  assert.strictEqual(child.get('key'), '/box/child')
})

test('gnnode_childget, should error if invalid child name', async () => {
  const graph = gngraph_create({
    '/box': {
      key: '/box',
      node: 'gnpgui',
      name: 'box',
      domchildkeyarr: []
    }
  })

  await assert.rejects(async () => (
    gnnode_childget(graph, graph.get('/box'), 123)
  ), {
    message: gnerr_nodechildnameinvalid(
      graph, graph.get('/box'), { name: 123 }).message
  })
})

test('gnnode_childrmall, should remove all childs', async () => {
  const cfg = mockcfg()
  const sess = { locale: 'eng-US' }
  const graph_todo = gngraph_create(mock_graph_todo_json)
  const node_listusers = graph_todo.get('/todo/content/box/listusers')

  const [graph, node] = await promisify(gnnode_childrmall)(
    sess, cfg, graph_todo, node_listusers)

  assert.ok(graph)
  assert.strictEqual(node.get('childarr').count(), 0)
})

test('gnnode_siblingprevget, should return prev sibling', () => {
  const graph_todo = gngraph_create(mock_graph_todo_json)
  const node_user2 = graph_todo.get('/todo/content/datausers/id2')
  const node_sibling = gnnode_siblingprevget(graph_todo, node_user2)

  assert.strictEqual(node_sibling.get('key'), '/todo/content/datausers/id1')
})

test('gnnode_getsiblingprev, should return null if no prev sibling', () => {
  const graph_todo = gngraph_create(mock_graph_todo_json)
  const node_user1 = graph_todo.get('/todo/content/datausers/id1')
  const node_sibling = gnnode_siblingprevget(graph_todo, node_user1)

  assert.strictEqual(node_sibling, null)
})

test('getposthydrated, should return posthydrated node', async () => {
  const cfg = mockcfg()
  const sess = { locale: 'eng-US' }
  const graphstart = gngraph_create({})
  const nodelink = gnnode_create({
    node: 'uilink',
    name: 'sitemap',
    subj: [{
      labelprimary: 'site map',
      href: '/site-map/'
    }],
    full: [{
      foo: 'bar'
    }]
  })

  const [graph, node] = await promisify(
    gnnode_prophydrateonpost)(sess, cfg, graphstart, nodelink)

  assert.deepStrictEqual(node.get('subjhydrated'), {
    labelprimary: 'site map',
    href: '/site-map/'
  })

  assert.deepStrictEqual(node.get('fullhydrated'), {
    foo: 'bar'
  })

  // graph used for fkey lookups, not used in this test
  assert.ok(graph)
})

test('getprehydrated, should return prehydrated node', async () => {
  const cfg = mockcfg()
  const sess = { locale: 'eng-US' }
  const graphstart = gngraph_create({})
  const nodelink = gnnode_create({
    node: 'uilink',
    name: 'sitemap',
    fkey: [{ rain: 'spain' }],
    init: [{ foo: 'bar' }],
    base: [{ jim: 'jane' }],
    baselist: [{ far: 'out' }]
  })

  const rep = gnreport_get('test')
  const [graph, node] = await promisify(
    gnnode_prophydrateonpre)(sess, cfg, graphstart, nodelink, rep)

  assert.deepStrictEqual(node.get('fkeyhydrated'), {
    rain: 'spain'
  })

  assert.deepStrictEqual(node.get('inithydrated'), {
    foo: 'bar'
  })

  assert.deepStrictEqual(node.get('basehydrated'), {
    jim: 'jane'
  })

  assert.deepStrictEqual(node.get('baselisthydrated'), {
    far: 'out'
  })

  // graph used for fkey lookups, not used in this test
  assert.ok(graph)
})

test('spechydrated, should return spec, hydrated', async () => {
  const cfg = mockcfg()
  const sess = { locale: 'eng-US' }
  const graphstart = gngraph_create({})
  const specnodelink = {
    node: 'uilink',
    name: 'sitemap',
    subj: [{
      scenetype: 'architectonic',
      projection: 'spherical',
      pos: [0, 0, 0]
    }]
  }
  const specnsname = 'subj'
  const nsstore = {
    fkey: { shapetype: 'architectonic' },
    init: undefined,
    full: undefined,
    base: undefined
  }
  const specnsspec = {
    scenetype: 'architectonic',
    projection: 'spherical',
    pos: [0, 0, 0]
  }
  const val = {}

  const [graph, specnode, finval] = (
    await promisify(gnnode_prophydratefromspec)(
      sess, cfg, graphstart, specnodelink,
      nsstore, specnsname, specnsspec, val))

  assert.ok(graph)
  assert.deepStrictEqual(specnode, specnodelink)
  assert.deepStrictEqual(finval, {
    scenetype: 'architectonic',
    projection: 'spherical',
    pos: [0, 0, 0]
  })
})

test('getspecarrhydrated, should return spec arr, hydrated', async () => {
  const cfg = mockcfg()
  const sess = mocksess()
  const graphstart = gngraph_create({})
  const specnodelink = gnnode_create({
    node: 'uilink',
    name: 'sitemap',
    subj: [{
      scenetype: 'architectonic',
      projection: 'spherical',
      pos: [0, 0, 0]
    }]
  })

  const nsacc = gnacc_subj
  const namespacedata = gnacc_groupaccessor([
    gnacc_fkey,
    gnacc_init,
    gnacc_full,
    gnacc_base
  ])

  const [graph, specnode, finval] = (
    await promisify(gnnode_prophydratefromspecs)(
      sess, cfg, graphstart, specnodelink, nsacc, namespacedata))

  assert.ok(graph)
  assert.deepStrictEqual(specnode, specnodelink)
  assert.deepStrictEqual(finval, {
    scenetype: 'architectonic',
    projection: 'spherical',
    pos: [0, 0, 0]
  })
})

test('getdatahydrated, should return node w/ hydrated ns', async () => {
  const cfg = mockcfg()
  const sess = { locale: 'eng-US' }
  const graphstart = gngraph_create({})
  const specnodelink = gnnode_create({
    node: 'uilink',
    name: 'sitemap',
    subj: [{
      scenetype: 'architectonic',
      projection: 'spherical',
      pos: [0, 0, 0]
    }]
  })

  const nsacc = gnacc_subj
  const namespacedata = gnacc_groupaccessor([
    gnacc_fkey,
    gnacc_init,
    gnacc_full,
    gnacc_base
  ])

  const [graph, specnode] = (
    await promisify(gnnode_prophydratefromspectree)(
      sess, cfg, graphstart, specnodelink, nsacc, namespacedata))

  assert.ok(graph)
  assert.deepStrictEqual(specnode.get('subjhydrated'), {
    scenetype: 'architectonic',
    projection: 'spherical',
    pos: [0, 0, 0]
  })
})

test('getdatahydrated, should return node w/ hydrated ns (full)', async () => {
  const cfg = mockcfg()
  const sess = { locale: 'eng-US' }
  const graphstart = gngraph_create({})
  const specnodelink = gnnode_create({
    node: 'uilink',
    name: 'sitemap',
    inithydrated: ['a', 'b', 'c'],
    subj: {
      [gnenumSPECPROPNAMETYPE]: 'nsprop',
      prop: 'init',
      name: 'subj'
    }
  })

  const nsacc = gnacc_subj
  const namespacedata = gnacc_groupaccessor([
    gnacc_fkey,
    gnacc_init,
    gnacc_full,
    gnacc_base
  ])

  const [graph, specnode] = (
    await promisify(gnnode_prophydratefromspectree)(
      sess, cfg, graphstart, specnodelink, nsacc, namespacedata))

  assert.ok(graph)
  assert.deepStrictEqual(specnode.get('subjhydrated'),
    ['a', 'b', 'c'])
})

test('setnodechild, should create and child cnode to node', async () => {
  const cfg = mockcfg()
  const sess = { locale: 'eng-US' }
  const graphempty = gngraph_create({})
  const nodebox = gnnode_create({
    fkeyhydrated: {
      user: '/data/user-1'
    },
    key: '/box',
    node: 'uiobj',
    name: 'box'
  })

  const specnodelink = {
    node: 'uilabel',
    name: 'label',
    subj: [{
      labelprimary: 'a label'
    }]
  }

  const nodeobj = {
    onchildadd: (sess, cfg, graph, node, cnode) => (
      [graph, node, cnode])
  }

  const graphstart = graphempty.set('/box', nodebox)

  const [graph, node, cnode] = gnnode_childsetfromspec(
    sess, cfg, graphstart, nodebox, nodeobj, specnodelink)

  assert.strictEqual(cnode.get('key'), '/box/label')
  assert.deepStrictEqual(cnode.get('fkeyhydrated'), {
    user: '/data/user-1'
  })
  assert.deepStrictEqual(cnode, graph.get('/box/label'))
  assert.deepStrictEqual(node.get('childarr').toJS(), ['/box/label'])
  assert.ok(graph && node && cnode)
})

test('getnodechildspec, should return a childspec', async () => {
  const cfg = mockcfg()
  const sess = { locale: 'eng-US' }
  const graphempty = gngraph_create({})
  const nodebox = gnnode_create({
    key: '/box',
    node: 'uiobj',
    name: 'box'
  })

  const specnodelink = {
    node: 'uilabel',
    name: 'label',
    subj: [{
      labelprimary: 'a label'
    }]
  }

  const graphstart = graphempty.set('/box', nodebox)

  const refspec = { spec: specnodelink }
  const spectypes = {}

  const childspec = await gnnode_childspecfromref(
    sess, cfg, graphstart, nodebox, refspec, spectypes)

  assert.deepStrictEqual(childspec, specnodelink)
})

test('getnodechildspec, should return a childspec, dynamic', async () => {
  const cfg = mockcfg()
  const sess = { locale: 'eng-US' }
  const graphempty = gngraph_create({})
  const specnodelink = {
    node: 'uilabel',
    name: 'label',
    subj: [{
      labelprimary: 'a label'
    }]
  }
  const nodebox = gnnode_create({
    key: '/box',
    node: 'uiobj',
    name: 'box',
    spectypes: {
      nodelink: specnodelink
    }
  })

  const graphstart = graphempty.set('/box', nodebox)
  const refspec = { spec: 'nodelink' }
  const spectypes = null
  const childspec = await gnnode_childspecfromref(
    sess, cfg, graphstart, nodebox, refspec, spectypes)

  assert.deepStrictEqual(childspec, specnodelink)
})

test('getnodechildspec, should error if no childspec', async () => {
  const cfg = mockcfg()
  const sess = { locale: 'eng-US' }
  const graphempty = gngraph_create({})
  const nodebox = gnnode_create({
    key: '/box',
    node: 'uiobj',
    name: 'box'
  })

  const graph = graphempty.set('/box', nodebox)
  const refspec = { spec: 'notfound' }
  const spectypes = {}

  await assert.rejects(async () => (
    gnnode_childspecfromref(
      sess, cfg, graph, nodebox, refspec, spectypes)
  ), {
    message: gnerrnode_childspecnotfound(
      graph, nodebox, refspec, spectypes).message
  })
})

test('specgetfiltered, should return a filtered spec', async () => {
  const cfg = mockcfg({
    specfn: {
      issame: ([a, b]) => a === b,
      istrue: ([a]) => Boolean(a)
    }
  })
  const sess = { locale: 'eng-US' }
  const graphempty = gngraph_create({})
  const nodebox = gnnode_create({
    key: '/box',
    node: 'uiobj',
    name: 'box',
    fkeyhydrated: {
      typeshow: 'All'
    }
  })

  const graph = graphempty.set('/box', nodebox)
  const speclabel = {
    node: 'uilabel',
    name: 'label'
  }
  const spec = {
    spec: speclabel,
    when: {
      [gnenumSPECPROPNAMETYPE]: 'OR',
      whenarr: [{
        fnname: 'issame',
        args: ['fkey.typeshow', 'All']
      },{
        [gnenumSPECPROPNAMETYPE]: 'AND',
        whenarr: [{
          fnname: 'issame',
          args: ['fkey.typeshow', 'Completed']
        },{
          fnname: 'istrue',
          args: ['base.subj.iscompleted']
        }]
      }]
    }
  }

  // returns 'errkey' if there is nonde
  const [, specfiltered] = (
    await promisify(gnnode_childspecfiltered)(
      sess, cfg, graph, nodebox, spec))

  assert.deepStrictEqual(specfiltered, speclabel)
})

test('getspecfiltered, should return errmsg if filter not pass', async () => {
  const cfg = mockcfg({
    specfn: {
      issame: ([a, b]) => a === b,
      istrue: ([a]) => Boolean(a)
    }
  })
  const sess = { locale: 'eng-US' }
  const graphempty = gngraph_create({})
  const nodebox = gnnode_create({
    key: '/box',
    node: 'uiobj',
    name: 'box',
    fkeyhydrated: {
      typeshow: 'All'
    }
  })

  const graph = graphempty.set('/box', nodebox)
  const speclabel = {
    node: 'uilabel',
    name: 'label'
  }
  const spec = {
    spec: speclabel,
    when: {
      [gnenumSPECPROPNAMETYPE]: 'OR',
      whenarr: [{
        fnname: 'issame',
        args: ['fkey.typeshow', 'NONE']
      },{
        [gnenumSPECPROPNAMETYPE]: 'AND',
        whenarr: [{
          errkey: 'display_is_hidden',
          fnname: 'issame',
          args: ['fkey.typeshow', 'NONE']
        },{
          fnname: 'istrue',
          args: ['base.subj.iscompleted']
        }]
      }]
    }
  }

  // returns 'errkey' if there is nondea
  const [errkey] = (
    await promisify(gnnode_childspecfiltered)(
      sess, cfg, graph, nodebox, spec))

  assert.deepStrictEqual(errkey, 'display_is_hidden')

  delete spec.when.whenarr[1].whenarr[0].errkey

  const [errkeyfallback] = (
    await promisify(gnnode_childspecfiltered)(
      sess, cfg, graph, nodebox, spec))

  assert.deepStrictEqual(errkeyfallback, 'errkey')
})

test('childspeceach, should apply function to each child spec', async () => {
  const cfg = mockcfg()
  const sess = { locale: 'eng-US' }
  const graphempty = gngraph_create({})
  const specnodelink = [{
    node: 'uilabel',
    name: 'label-start'
  }, {
    node: 'uilabel',
    name: 'label-end'
  }]
  const nodebox = gnnode_create({
    key: '/box',
    node: 'uiobj',
    name: 'box',
    spectypes: {
      nodelink: specnodelink
    }
  })
  const graphstart = graphempty.set('/box', nodebox)
  const child = specnodelink
  const nodeobj = {
    onchildadd: (sess, cfg, graph, node, cnode) => (
      [graph, node, cnode])
  }

  let childnum = 0

  const [graph, node] = (
    await promisify(gnnode_childspeceach)(
      sess, cfg, graphstart, nodebox, child, (graph, node, spec, fn) => {
        childnum++

        ;[graph, node] = gnnode_childsetfromspec(
          sess, cfg, graph, node, nodeobj, spec)

        fn(null, graph, node)
      }))

  assert.ok(graph && node)
  assert.ok(graph.has('/box/label-start'))
  assert.ok(graph.has('/box/label-end'))
  assert.strictEqual(childnum, 2)
})

test('childspecresolve, should throw if spec invalid', async () => {
  const cfg = mockcfg()
  const sess = { locale: 'eng-US' }
  const graph = gngraph_create({
    '/box': {
      key: '/box',
      node: 'gnpgui',
      name: 'box',
      domchildkeyarr: []
    }
  })

  await assert.rejects(async () => (
    promisify(gnnode_childspecresolve)(
      sess, cfg, graph, graph.get('/box'), undefined, 0, () => {})
  ), {
    message: gnerr_nodespecchildinvalid(
      graph, graph.get('/box'), undefined).message
  })
})

test('childspecresolveeach, should map-add childs', async () => {
  const cfg = mockcfg()
  const sess = { locale: 'eng-US' }
  const graphempty = gngraph_create({})
  const nodebox = gnnode_create({
    key: '/box',
    node: 'gnpgui',
    name: 'box',
    domchildkeyarr: []
  })
  const graphstart = graphempty.set('/box', nodebox)
  const childarr = [
    { name: 'jim', node: 'uilabel' },
    { name: 'jane', node: 'uilabel' },
    { name: 'richard', node: 'uilabel' }]

  const [graph, node] = await promisify(gnnode_childspecresolveeach)(
    sess, cfg, graphstart, nodebox, childarr,
    (graph, node, childspec, childnum, nextfn) => {
      const ndpg = gnnode_pgget(cfg, graph, node)
      const res = gnnode_childsetfromspec(
        sess, cfg, graph, node, ndpg, childspec)

      nextfn(null, res[0], res[1])
    })

  assert.ok(graph && node)
  assert.ok(graph.has('/box/jim'))
  assert.ok(graph.has('/box/jane'))
  assert.ok(graph.has('/box/richard'))
  assert.deepStrictEqual(node.get('domchildkeyarr'), [
    '/box/jim',
    '/box/jane',
    '/box/richard'
  ])

  assert.deepStrictEqual(node.get('childarr').toJS(), [
    '/box/jim',
    '/box/jane',
    '/box/richard'
  ])
})

test('childspecresolveeach, should resolve each, baselist', async () => {
  const cfg = mockcfg()
  const sess = { locale: 'eng-US' }
  const graphempty = gngraph_create({})
  const nodebox = gnnode_create({
    key: '/box',
    node: 'gnpgui',
    name: 'box',
    domchildkeyarr: [],

    child: [{
      spec: 'user',
      specassign: {
        onsubj: [{
          [gnenumSPECPROPNAMETYPE]: 'fn',
          fnname: 'cacheupdate',
          args: ['node']
        }]
      }
    }],

    spectypes: {
      user: {
        node: 'gnpgdata',
        name: 'datauser',
        datakeyprop: 'id',
        subj: [{
          name: 'id',
          [gnenumSPECPROPNAMETYPE]: 'nsprop',
          prop: 'base.id',
          def: {
            [gnenumSPECPROPNAMETYPE]: 'fn',
            fnname: 'datenow'
          }
        },{
          name: 'name',
          [gnenumSPECPROPNAMETYPE]: 'nsprop',
          prop: 'base.name',
          def: ''
        },{
          name: 'iscompleted',
          [gnenumSPECPROPNAMETYPE]: 'nsprop',
          prop: 'base.iscompleted',
          def: false
        }]
      }
    },
    baselist: {
      [gnenumSPECPROPNAMETYPE]: 'childkeys',
      args: ['/datausers', 'subj.id', 1]
    },
    baselisthydrated: [
      { id: 200, name: 'jim' },
      { id: 201, name: 'jane' },
      { id: 202, name: 'richard' }
    ]
  })
  const graphstart = graphempty.set('/box', nodebox)
  const childarr = nodebox.get('child')

  const [graph, node] = await promisify(gnnode_childspecresolveeach)(
    sess, cfg, graphstart, nodebox, childarr,
    (graph, node, childspec, childnum, nextfn) => {
      const ndpg = gnnode_pgget(cfg, graph, node)
      const res = gnnode_childsetfromspec(
        sess, cfg, graph, node, ndpg, childspec)

      nextfn(null, res[0], res[1])
    })

  assert.ok(graph && node)
  assert.ok(graph.has('/box/200'))
  assert.ok(graph.has('/box/201'))
  assert.ok(graph.has('/box/202'))
  assert.deepStrictEqual(node.get('childarr').toJS(), [
    '/box/200',
    '/box/201',
    '/box/202'
  ])
})

test('dfsfind, should throw error if child not found in graph', async () => {
  const cfg = mockcfg()
  const sess = { locale: 'eng-US' }
  const graphempty = gngraph_create({})
  const nodebox = gnnode_create({
    key: '/box',
    node: 'gnpgui',
    name: 'box',
    subjhydrated: {
      details: true
    },
    childarr: [
      '/box/child'
    ]
  })

  await assert.rejects(async () => (
    promisify(gnnode_dfsfind)(
      sess, cfg, graphempty, nodebox, () => false)
  ), {
    message: gnerr_nodedfskeynotfound(
      graphempty, nodebox, nodebox.get('childarr').get(0)
    ).message
  })
})

test('dfsfind, should find node', async () => {
  const cfg = mockcfg()
  const sess = { locale: 'eng-US' }
  const graphempty = gngraph_create({})
  const nodebox = gnnode_create({
    key: '/box',
    node: 'gnpgui',
    name: 'box',
    childarr: [
      '/box/child'
    ]
  })

  const [nd] = await promisify(gnnode_dfsfind)(
    sess, cfg, graphempty, nodebox, () => true)

  assert.strictEqual(nd.get('key'), '/box')
})

test('dfshydrateall, should fully populate a node-hierarchy', async () => {
  const cfg = mockcfg({
    specfn: {
      tplstr: () => 'tplstr'
    }
  })
  const sess = { locale: 'eng-US' }
  const graphempty = gngraph_create({})
  const rootnode = gnnode_create({
    key: '/',
    node: 'uiroot',
    name: '/',
    domchildkeyarr: [],
    child: [{
      node: 'gnpgdatals',
      name: 'datausers',
      baselist: {
        [gnenumSPECPROPNAMETYPE]: 'literal',
        value: [
          { id: 200, name: 'jim' },
          { id: 201, name: 'jane' },
          { id: 202, name: 'richard' }
        ]
      },
      child: [{
        spec: 'user',
        specassign: {
          onsubj: [{
            [gnenumSPECPROPNAMETYPE]: 'fn',
            fnname: 'cacheupdate',
            args: ['node']
          }]
        }
      }],
      childsubj: {
        incompletetotal: 1
      },
      childsubjhydrated: {
        incompletetotal: 1
      },
      spectypes: {
        user: {
          node: 'gnpgdata',
          name: 'datauser',
          datakeyprop: 'id',
          subj: [{
            name: 'id',
            [gnenumSPECPROPNAMETYPE]: 'nsprop',
            prop: 'base.id',
            def: {
              [gnenumSPECPROPNAMETYPE]: 'fn',
              fnname: 'datenow'
            }
          },{
            name: 'name',
            [gnenumSPECPROPNAMETYPE]: 'nsprop',
            prop: 'base.name',
            def: ''
          },{
            name: 'iscompleted',
            [gnenumSPECPROPNAMETYPE]: 'nsprop',
            prop: 'base.iscompleted',
            def: false
          }]
        }
      }
    },{
      node: 'uispread',
      name: 'root-content',
      child: [{
        name: 'foot',
        node: 'uispread',
        // node: 'uils',
        className: 'footrow',
        child: [{
          name: 'footlabel',
          node: 'uilabel',
          subj: [{
            [gnenumSPECPROPNAMETYPE]: 'nsprop',
            prop: '[/datausers].childsubj.incompletetotal',
            name: 'label',
            filterinarr: [{
              [gnenumSPECPROPNAMETYPE]: 'fn',
              fnname: 'tplstr',
              args: [':arg items left', 'this']
            }]
          }]
        }]
      }]
    }]
  })

  const graphstart = graphempty.set('/', rootnode)
  const rep = gnreport_get('build')
  const [graph, node] = await promisify(gnnode_dfshydrateall)(
    sess, cfg, graphstart, rootnode, rep)

  // not a great test, but any test is great start now
  assert.ok(graph && node)
  assert.ok(graph.has('/'))
  assert.ok(graph.has('/datausers'))
  assert.ok(graph.has('/datausers/200'))
  assert.ok(graph.has('/datausers/201'))
  assert.ok(graph.has('/datausers/202'))
  assert.ok(graph.has('/root-content'))
  assert.ok(graph.has('/root-content/foot'))
  assert.ok(graph.has('/root-content/foot/footlabel'))
})

test('prophydratefromchildsfncreate, should surface error', async () => {
  const cfg = mockcfg({
    specfn: {
      accumtrue: ([prev, next]) => (
        next ? prev + 1 : prev)
    }
  })
  const sess = { locale: 'eng-US' }
  const graphstart = gngraph_create({
    '/data-users': {
      key: '/data-users',
      node: 'gnpgdatals',
      name: 'data-users',
      childsubj: [{
        name: 'iscompletetotal',
        [gnenumSPECPROPNAMETYPE]: 'nsprop',
        prop: '[fkey.child].subj.iscompleted',
        accum: {
          start: 0,
          [gnenumSPECPROPNAMETYPE]: 'fn',
          fnname: 'accumtrueUNDEF'
        }
      }]
    },
    '/data-users/user-1': {
      key: '/data-users/user-1',
      node: 'gnpgdata',
      name: 'datauser',
      datakeyprop: 'id',
      subjhydrated: {
        iscompleted: true
      }
    },
    '/data-users/user-2': {
      key: '/data-users/user-2',
      node: 'gnpgdata',
      name: 'datauser',
      datakeyprop: 'id',
      subjhydrated: {
        iscompleted: true
      }
    }
  })

  const nodeparent = graphstart.get('/data-users')
  const nodeparetnchildsubjspecarr = gnacc_childsubj.getspec(nodeparent, [])
  const prophydratefromchilds = promisify(
    gnnode_prophydratefromchildsfncreate(
      nodeparent, nodeparetnchildsubjspecarr))

  const nodechild1 = graphstart.get('/data-users/user-1')

  await assert.rejects(async () => (
    prophydratefromchilds(
      sess, cfg, graphstart, nodeparent, nodechild1, 0)
  ), {
    message: 'invalid: fnname “accumtrueUNDEF”'
  })
})

test('prophydratefromchildsfncreate, should hydrate upward', async () => {
  const cfg = mockcfg({
    specfn: {
      accumtrue: ([prev, next]) => (
        next ? prev + 1 : prev)
    }
  })
  const sess = { locale: 'eng-US' }
  const graphstart = gngraph_create({
    '/data-users': {
      key: '/data-users',
      node: 'gnpgdatals',
      name: 'data-users',
      childsubj: [{
        name: 'iscompletetotal',
        [gnenumSPECPROPNAMETYPE]: 'nsprop',
        prop: '[fkey.child].subj.iscompleted',
        accum: {
          start: 0,
          [gnenumSPECPROPNAMETYPE]: 'fn',
          fnname: 'accumtrue'
        }
      }]
    },
    '/data-users/user-1': {
      key: '/data-users/user-1',
      node: 'gnpgdata',
      name: 'datauser',
      datakeyprop: 'id',
      subjhydrated: {
        iscompleted: true
      }
    },
    '/data-users/user-2': {
      key: '/data-users/user-2',
      node: 'gnpgdata',
      name: 'datauser',
      datakeyprop: 'id',
      subjhydrated: {
        iscompleted: true
      }
    }
  })

  const nodeparent = graphstart.get('/data-users')
  const prophydratefromchilds = promisify(
    gnnode_prophydratefromchildsfncreate(nodeparent))

  const nodechild1 = graphstart.get('/data-users/user-1')
  const nodechild2 = graphstart.get('/data-users/user-2')

  const [graph1, nodeparent1] = await prophydratefromchilds(
    sess, cfg, graphstart, nodeparent, nodechild1, 0)

  assert.deepStrictEqual(
    graph1.get(nodeparent.get('key')).get('childsubjhydrated'),
    { iscompletetotal: 1 })

  const [graph2] = await prophydratefromchilds(
    sess, cfg, graphstart, nodeparent1, nodechild2, 1)

  assert.deepStrictEqual(
    graph2.get(nodeparent.get('key')).get('childsubjhydrated'),
    { iscompletetotal: 2 })
})

test('prophydratefromchildsrefresh should refresh nodes affected', async () => {
  const cfg = mockcfg({
    specfn: {
      accumtrue: ([prev, next]) => (
        next ? prev + 1 : prev)
    }
  })
  const sess = { locale: 'eng-US' }
  const graphstart = gngraph_create({
    '/data-users': {// gnnode_create({
      key: '/data-users',
      node: 'gnpgdatals',
      name: 'data-users',
      childarr: [
        '/data-users/user-1',
        '/data-users/user-2'
      ],
      childsubj: [{
        name: 'iscompletetotal',
        [gnenumSPECPROPNAMETYPE]: 'nsprop',
        prop: '[fkey.child].subj.iscompleted',
        accum: {
          start: 0,
          [gnenumSPECPROPNAMETYPE]: 'fn',
          fnname: 'accumtrue'
        }
      }]
    },
    '/data-users/user-1': {
      key: '/data-users/user-1',
      node: 'gnpgdata',
      name: 'datauser',
      datakeyprop: 'id',
      subjhydrated: {
        iscompleted: true
      }
    },
    '/data-users/user-2': {
      key: '/data-users/user-2',
      node: 'gnpgdata',
      name: 'datauser',
      datakeyprop: 'id',
      subjhydrated: {
        iscompleted: true
      }
    }
  })

  const nodeparent = graphstart.get('/data-users')
  const prophydratefromchilds = promisify(
    gnnode_prophydratefromchildsfncreate(nodeparent))

  const nodechild1 = graphstart.get('/data-users/user-1')
  const nodechild2 = graphstart.get('/data-users/user-2')


  const [graph1, nodeparent1] = await prophydratefromchilds(
    sess, cfg, graphstart, nodeparent, nodechild1, 0)
  const [graph2] = await prophydratefromchilds(
    sess, cfg, graph1, nodeparent1, nodechild2, 1)

  assert.deepStrictEqual(
    gnacc_childsubj.get(graph2.get('/data-users')), {
      iscompletetotal: 2
    })

  const graph3 = graph2.set(
    '/data-users/user-1',
    nodechild1.set('subjhydrated', {
      iscompleted: false
    }))

  const [graph] = await promisify(
    gnnode_prophydratefromchildsrefresh)(
    sess, cfg, graph3, graph3.get('/data-users'))

  assert.deepStrictEqual(
    gnacc_childsubj.get(graph.get('/data-users')), {
      iscompletetotal: 1
    })
})

test('childfind should return truthy child', async () => {
  const cfg = mockcfg()
  const sess = { locale: 'eng-US' }
  const graphstart = gngraph_create({
    '/data-users': {// gnnode_create({
      key: '/data-users',
      node: 'gnpgdatals',
      name: 'data-users',
      childarr: [
        '/data-users/user-1',
        '/data-users/user-2'
      ],
      childsubj: [{
        name: 'iscompletetotal',
        [gnenumSPECPROPNAMETYPE]: 'nsprop',
        prop: '[fkey.child].subj.iscompleted',
        accum: {
          start: 0,
          [gnenumSPECPROPNAMETYPE]: 'fn',
          fnname: 'accumtrue'
        }
      }]
    },
    '/data-users/user-1': {
      key: '/data-users/user-1',
      node: 'gnpgdata',
      name: 'datauser',
      datakeyprop: 'id',
      subjhydrated: {
        iscompleted: true
      }
    },
    '/data-users/user-2': {
      key: '/data-users/user-2',
      node: 'gnpgdata',
      name: 'datauser',
      datakeyprop: 'id',
      subjhydrated: {
        iscompleted: true
      }
    }
  })

  const res = await promisify(gnnode_childfind)(
    sess, cfg, graphstart, graphstart.get('/data-users'), (
      (ss, cfg, gr, nd, i, fn) => {
        fn(null, gr, nd, i === 1)
      }))

  assert.strictEqual(res[1].get('key'), '/data-users/user-2')
})

test('siblingnextorprevkeyget should return next or prev key', async () => {
  const cfg = mockcfg()
  const sess = { locale: 'eng-US' }
  const graphstart = gngraph_create({
    '/data-users': {// gnnode_create({
      key: '/data-users',
      node: 'gnpgdatals',
      name: 'data-users',
      childarr: [
        '/data-users/user-1',
        '/data-users/user-2'
      ],
      childsubj: [{
        name: 'iscompletetotal',
        [gnenumSPECPROPNAMETYPE]: 'nsprop',
        prop: '[fkey.child].subj.iscompleted',
        accum: {
          start: 0,
          [gnenumSPECPROPNAMETYPE]: 'fn',
          fnname: 'accumtrue'
        }
      }]
    },
    '/data-users/user-1': {
      key: '/data-users/user-1',
      node: 'gnpgdata',
      name: 'datauser',
      datakeyprop: 'id',
      subjhydrated: {
        iscompleted: true
      }
    },
    '/data-users/user-2': {
      key: '/data-users/user-2',
      node: 'gnpgdata',
      name: 'datauser',
      datakeyprop: 'id',
      subjhydrated: {
        iscompleted: true
      }
    }
  })

  assert.strictEqual(
    gnnode_siblingnextorprevkeyget(
      sess, cfg, graphstart, graphstart.get('/data-users/user-1')),
    '/data-users/user-2')

  assert.strictEqual(
    gnnode_siblingnextorprevkeyget(
      sess, cfg, graphstart, graphstart.get('/data-users/user-2')),
    '/data-users/user-1')
})

test('should runtime-process fkeys that reference [./].baseelem', async () => {
  const cfg = mockcfg({
    specfn: {
      issame: ([a, b]) => a === b
    }
  })
  const sess = mocksess()
  const graphempty = gngraph_create({})

  const userspec = {
    node: 'gnpgdata',
    name: 'datauser',
    datakeyprop: 'id',
    subj: [{
      name: 'id',
      [gnenumSPECPROPNAMETYPE]: 'nsprop',
      prop: 'base.id',
      def: {
        [gnenumSPECPROPNAMETYPE]: 'fn',
        fnname: 'datenow'
      }
    },{
      name: 'name',
      [gnenumSPECPROPNAMETYPE]: 'nsprop',
      prop: 'base.name',
      def: ''
    },{
      name: 'iscompleted',
      [gnenumSPECPROPNAMETYPE]: 'nsprop',
      prop: 'base.iscompleted',
      def: false
    }]
  }

  const rootnode = gnnode_create({
    key: '/',
    node: 'uiroot',
    name: '/',
    domchildkeyarr: [],
    child: [{
      node: 'gnpgdatals',
      name: 'data-users',
      baselist: {
        [gnenumSPECPROPNAMETYPE]: 'literal',
        value: [
          { id: 200, name: 'jim' },
          { id: 201, name: 'jane' },
          { id: 202, name: 'richard' }
        ]
      },
      child: [userspec]
    }, {
      node: 'uispread',
      name: 'list-users',
      baselist: {
        [gnenumSPECPROPNAMETYPE]: 'literal',
        value: [
          '/data-users/200',
          '/data-users/201',
          '/data-users/202'
        ]
      },
      child: [{
        node: 'uilabel',
        name: 'label',
        fkey: {
          '[./].baseelem': ['datatodo'],
          datatodo: '[./].baseelem'
        },
        subj: [{
          'gn:type': 'nsprop',
          prop: '[fkey.datatodo].subj.name',
          name: 'value'
        }]
      }]
    }]
  })

  const graphstart = graphempty.set('/', rootnode)
  const rep = gnreport_get('build')
  const [graph] = await promisify(gnnode_dfshydrateall)(
    sess, cfg, graphstart, rootnode, rep)

  const datausers202 = graph.get('/data-users/202').toJS()
  assert.deepStrictEqual(datausers202, {
    key: '/data-users/202',
    node: 'gnpgdata',
    isdata: true,
    ispersistedts: datausers202.ispersistedts,
    datakeyprop: 'id',
    basehydrated: { id: 202, name: 'richard' },
    name: '202',
    init: [],
    subj: datausers202.subj,
    subjhydrated: {
      iscompleted: false,
      name: 'richard',
      id: 202,
      spread: true
    },
    dataoutarr: [{
      'gn:type': 'nsprop',
      id: '[/list-users/label_202].subj.value ⸺> [fkey.datatodo].subj.name',
      key: '/list-users/label_202',
      meta: datausers202.dataoutarr[0].meta,
      ns: 'subj',
      refname: 'name'
    }]
  })

  const listuserslabel202 = graph.get('/list-users/label_202').toJS()
  assert.deepStrictEqual(listuserslabel202, {
    key: '/list-users/label_202',
    node: 'uilabel',
    isdom: true,
    domchildkey: {},
    domchildkeyarr: [],
    ispersistedts: listuserslabel202.ispersistedts,
    name: 'label_202',
    datainarr: [{
      'gn:type': 'nsprop',
      id: '[/list-users/label_202].subj.value ⸺> [fkey.datatodo].subj.name',
      key: '/data-users/202',
      refname: 'name'
    }],
    mapsrckey: '/data-users/202',
    fkey: { '[./].baseelem': null, datatodo: '/data-users/202' },
    fkeyhydrated: { '[./].baseelem': null, datatodo: '/data-users/202' },
    subj: listuserslabel202.subj,
    subjhydrated: { value: 'richard' }
  })
})

test('should runtime-build when+spec+fkeys using [./].baseelem', async () => {
  const cfg = mockcfg({
    specfn: {
      issame: ([a, b]) => a === b
    }
  })
  const sess = { locale: 'eng-US' }
  const graphempty = gngraph_create({})

  const userspec = {
    node: 'gnpgdata',
    name: 'datauser',
    datakeyprop: 'id',
    subj: [{
      name: 'id',
      [gnenumSPECPROPNAMETYPE]: 'nsprop',
      prop: 'base.id',
      def: {
        [gnenumSPECPROPNAMETYPE]: 'fn',
        fnname: 'datenow'
      }
    },{
      name: 'name',
      [gnenumSPECPROPNAMETYPE]: 'nsprop',
      prop: 'base.name',
      def: ''
    },{
      name: 'iscompleted',
      [gnenumSPECPROPNAMETYPE]: 'nsprop',
      prop: 'base.iscompleted',
      def: false
    }]
  }

  const rootnode = gnnode_create({
    key: '/',
    node: 'uiroot',
    name: '/',
    domchildkeyarr: [],
    child: [{
      node: 'gnpgdatals',
      name: 'data-users',
      baselist: {
        [gnenumSPECPROPNAMETYPE]: 'literal',
        value: [
          { id: 200, name: 'jim' },
          { id: 201, name: 'jane' },
          { id: 202, name: 'richard' }
        ]
      },
      child: [userspec]
    }, {
      node: 'uispread',
      name: 'list-users',
      baselist: {
        [gnenumSPECPROPNAMETYPE]: 'literal',
        value: [
          '/data-users/200',
          '/data-users/201',
          '/data-users/202'
        ]
      },
      child: [{
        fkey: {
          '[./].baseelem': ['datatodo'],
          datatodo: '[./].baseelem'
        },
        when: {
          fnname: 'issame',
          args: ['true', 'true']
        },
        spec: {
          node: 'uilabel',
          name: 'label',
          subj: [{
            'gn:type': 'nsprop',
            prop: '[fkey.datatodo].subj.name',
            name: 'value'
          }]
        }
      }]
    }]
  })

  const graphstart = graphempty.set('/', rootnode)
  const rep = gnreport_get('build')
  const [graph] = await promisify(gnnode_dfshydrateall)(
    sess, cfg, graphstart, rootnode, rep)

  const datausers202 = graph.get('/data-users/202').toJS()
  assert.deepStrictEqual(datausers202, {
    key: '/data-users/202',
    node: 'gnpgdata',
    isdata: true,
    ispersistedts: datausers202.ispersistedts,
    datakeyprop: 'id',
    basehydrated: { id: 202, name: 'richard' },
    name: '202',
    init: [],
    subj: datausers202.subj,
    subjhydrated: {
      iscompleted: false,
      name: 'richard',
      id: 202,
      spread: true
    },
    dataoutarr: [{
      'gn:type': 'nsprop',
      id: '[/list-users/label_202].subj.value ⸺> [fkey.datatodo].subj.name',
      key: '/list-users/label_202',
      meta: datausers202.dataoutarr[0].meta,
      ns: 'subj',
      refname: 'name'
    }]
  })

  const listuserslabel202 = graph.get('/list-users/label_202').toJS()
  assert.deepStrictEqual(listuserslabel202, {
    key: '/list-users/label_202',
    node: 'uilabel',
    isdom: true,
    domchildkey: {},
    domchildkeyarr: [],
    ispersistedts: listuserslabel202.ispersistedts,
    name: 'label_202',
    datainarr: [{
      'gn:type': 'nsprop',
      id: '[/list-users/label_202].subj.value ⸺> [fkey.datatodo].subj.name',
      key: '/data-users/202',
      refname: 'name'
    }],
    mapsrckey: '/data-users/202',
    fkeyhydrated: {
      '[./].baseelem': null,
      datatodo: '/data-users/202'
    },
    subj: listuserslabel202.subj,
    subjhydrated: { value: 'richard' }
  })
})

test('join node dyn fkeys w/ inherited baselem, when+spec+fkeys', async () => {
  const cfg = mockcfg({
    specfn: {
      issame: ([a, b]) => a === b
    }
  })
  const sess = { locale: 'eng-US' }
  const graphempty = gngraph_create({})

  const userspec = {
    node: 'gnpgdata',
    name: 'datauser',
    datakeyprop: 'id',
    subj: [{
      name: 'id',
      [gnenumSPECPROPNAMETYPE]: 'nsprop',
      prop: 'base.id',
      def: {
        [gnenumSPECPROPNAMETYPE]: 'fn',
        fnname: 'datenow'
      }
    },{
      name: 'name',
      [gnenumSPECPROPNAMETYPE]: 'nsprop',
      prop: 'base.name',
      def: ''
    },{
      name: 'iscompleted',
      [gnenumSPECPROPNAMETYPE]: 'nsprop',
      prop: 'base.iscompleted',
      def: false
    }]
  }

  const rootnode = gnnode_create({
    key: '/',
    node: 'uiroot',
    name: '/',
    domchildkeyarr: [],
    child: [{
      node: 'gnpgdatals',
      name: 'data-users',
      baselist: {
        [gnenumSPECPROPNAMETYPE]: 'literal',
        value: [
          { id: 200, name: 'jim' },
          { id: 201, name: 'jane' },
          { id: 202, name: 'richard' }
        ]
      },
      // TODO fails when not a list
      subj: [{ usertype: 'employee' }],
      child: [userspec]
    }, {
      node: 'uispread',
      name: 'list-users',
      baselist: {
        [gnenumSPECPROPNAMETYPE]: 'literal',
        value: [
          '/data-users/200',
          '/data-users/201',
          '/data-users/202'
        ]
      },
      child: [{
        fkey: {
          '[./].baseelem': ['datatodo'],
          datatodo: '[./].baseelem'
        },
        when: {
          fnname: 'issame',
          args: ['true', 'true']
        },
        spec: {
          node: 'uilabel',
          name: 'label',
          fkey: [{
            'gn:type': 'nsprop',
            prop: '[/data-users].subj.usertype',
            name: 'usertype'
          }],
          subj: [{
            'gn:type': 'nsprop',
            prop: '[fkey.datatodo].subj.name',
            name: 'value'
          }, {
            'gn:type': 'nsprop',
            prop: 'fkey.usertype',
            name: 'label'
          }]
        }
      }]
    }]
  })

  const graphstart = graphempty.set('/', rootnode)
  const rep = gnreport_get('build')
  const [graph] = await promisify(gnnode_dfshydrateall)(
    sess, cfg, graphstart, rootnode, rep)

  const datausers202 = graph.get('/data-users/202').toJS()
  assert.deepStrictEqual(datausers202, {
    key: '/data-users/202',
    node: 'gnpgdata',
    isdata: true,
    ispersistedts: datausers202.ispersistedts,
    datakeyprop: 'id',
    basehydrated: { id: 202, name: 'richard' },
    name: '202',
    init: [],
    subj: datausers202.subj,
    subjhydrated: {
      iscompleted: false,
      name: 'richard',
      id: 202,
      spread: true
    },
    dataoutarr: [{
      'gn:type': 'nsprop',
      id: '[/list-users/label_202].subj.value ⸺> [fkey.datatodo].subj.name',
      key: '/list-users/label_202',
      meta: datausers202.dataoutarr[0].meta,
      ns: 'subj',
      refname: 'name'
    }]
  })

  const listuserslabel202 = graph.get('/list-users/label_202').toJS()
  assert.deepStrictEqual(listuserslabel202, {
    key: '/list-users/label_202',
    node: 'uilabel',
    isdom: true,
    domchildkey: {},
    domchildkeyarr: [],
    ispersistedts: listuserslabel202.ispersistedts,
    name: 'label_202',
    datainarr: [{
      id:
        '[/list-users/label_202].fkey.usertype ⸺> [/data-users].subj.usertype',
      'gn:type': 'nsprop',
      refname: 'usertype',
      key: '/data-users'
    }, {
      id: '[/list-users/label_202].subj.value ⸺> [fkey.datatodo].subj.name',
      'gn:type': 'nsprop',
      refname: 'name',
      key: '/data-users/202'
    }],
    mapsrckey: '/data-users/202',
    fkey: listuserslabel202.fkey,
    fkeyhydrated: {
      '[./].baseelem': null,
      datatodo: '/data-users/202',
      usertype: 'employee'
    },
    subj: listuserslabel202.subj,
    subjhydrated: { value: 'richard', label: 'employee' }
  })
})

test('join static fkeys w/ inherited baselem, when+spec+fkeys', async () => {
  const cfg = mockcfg({
    specfn: {
      issame: ([a, b]) => a === b
    }
  })
  const sess = mocksess()
  const graphempty = gngraph_create({})

  const userspec = {
    node: 'gnpgdata',
    name: 'datauser',
    datakeyprop: 'id',
    subj: [{
      name: 'id',
      [gnenumSPECPROPNAMETYPE]: 'nsprop',
      prop: 'base.id',
      def: {
        [gnenumSPECPROPNAMETYPE]: 'fn',
        fnname: 'datenow'
      }
    },{
      name: 'name',
      [gnenumSPECPROPNAMETYPE]: 'nsprop',
      prop: 'base.name',
      def: ''
    },{
      name: 'iscompleted',
      [gnenumSPECPROPNAMETYPE]: 'nsprop',
      prop: 'base.iscompleted',
      def: false
    }]
  }

  const rootnode = gnnode_create({
    key: '/',
    node: 'uiroot',
    name: '/',
    domchildkeyarr: [],
    child: [{
      node: 'gnpgdatals',
      name: 'data-users',
      baselist: {
        [gnenumSPECPROPNAMETYPE]: 'literal',
        value: [
          { id: 200, name: 'jim' },
          { id: 201, name: 'jane' },
          { id: 202, name: 'richard' }
        ]
      },
      // TODO fails when not a list
      subj: [{ usertype: 'employee' }],
      child: [userspec]
    }, {
      node: 'uispread',
      name: 'list-users',
      baselist: {
        [gnenumSPECPROPNAMETYPE]: 'literal',
        value: [
          '/data-users/200',
          '/data-users/201',
          '/data-users/202'
        ]
      },
      child: [{
        fkey: {
          '[./].baseelem': ['datatodo'],
          datatodo: '[./].baseelem'
        },
        when: {
          fnname: 'issame',
          args: ['true', 'true']
        },
        spec: {
          node: 'uilabel',
          name: 'label',
          // fkey: [{
          //   'gn:type': 'nsprop',
          //   prop: '[/data-users].subj.usertype',
          //   name: 'usertype'
          // }],

          fkey: {
            usertype: 'employee'
          },
          subj: [{
            'gn:type': 'nsprop',
            prop: '[fkey.datatodo].subj.name',
            name: 'value'
          }, {
            'gn:type': 'nsprop',
            prop: 'fkey.usertype',
            name: 'label'
          }]
        }
      }]
    }]
  })

  const graphstart = graphempty.set('/', rootnode)
  const rep = gnreport_get('build')
  const [graph] = await promisify(gnnode_dfshydrateall)(
    sess, cfg, graphstart, rootnode, rep)

  const datausers202 = graph.get('/data-users/202').toJS()
  assert.deepStrictEqual(datausers202, {
    key: '/data-users/202',
    node: 'gnpgdata',
    isdata: true,
    ispersistedts: datausers202.ispersistedts,
    datakeyprop: 'id',
    basehydrated: { id: 202, name: 'richard' },
    name: '202',
    init: [],
    subj: datausers202.subj,
    subjhydrated: {
      iscompleted: false,
      name: 'richard',
      id: 202,
      spread: true
    },
    dataoutarr: [{
      'gn:type': 'nsprop',
      id: '[/list-users/label_202].subj.value ⸺> [fkey.datatodo].subj.name',
      key: '/list-users/label_202',
      meta: datausers202.dataoutarr[0].meta,
      ns: 'subj',
      refname: 'name'
    }]
  })

  const listuserslabel202 = graph.get('/list-users/label_202').toJS()
  assert.deepStrictEqual(listuserslabel202, {
    key: '/list-users/label_202',
    node: 'uilabel',
    isdom: true,
    domchildkey: {},
    domchildkeyarr: [],
    ispersistedts: listuserslabel202.ispersistedts,
    name: 'label_202',
    datainarr: [{
      id: '[/list-users/label_202].subj.value ⸺> [fkey.datatodo].subj.name',
      'gn:type': 'nsprop',
      refname: 'name',
      key: '/data-users/202'
    }],
    mapsrckey: '/data-users/202',
    fkey: listuserslabel202.fkey,
    fkeyhydrated: {
      '[./].baseelem': null,
      datatodo: '/data-users/202',
      usertype: 'employee'
    },
    subj: listuserslabel202.subj,
    subjhydrated: { value: 'richard', label: 'employee' }
  })
})


test('inherited baselem deeply, spectypes+when+spec+fkeys', async () => {
  // a difficult-to-reproduce scenario
  const cfg = mockcfg({
    specfn: {
      issame: ([a, b]) => a === b,
      getseqactionkey: () => '/shapedata'
    }
  })
  const sess = mocksess()
  const graphempty = gngraph_create({})

  const rootnode = gnnode_create({
    key: '/',
    node: 'uiroot',
    name: '/',
    domchildkeyarr: [],
    child: [{
      node: 'gnpgdata',
      name: 'shapedata',
      subj: {
        color: 'red'
      }
    },{
      node: 'uispread',
      name: 'canvasportalbg',
      fkey: {
        shapetype: 'architectonic'
      },
      baselist: {
        [gnenumSPECPROPNAMETYPE]: 'literal',
        value: [
          '/shapes/key/1'
        ]
      },
      spectypes: {
        seq: {
          node: 'uispread',
          name: 'seq',
          fkey: [{
            fnname: "getseqactionkey",
            "gn:type": "fn",
            args: ["fkey.sequence"],
            name: "shapedata"
          }],
          child: [{
            name: 'seqactionshape',
            node: 'uilabel',
            subj: [{
              "gn:type": "nsprop",
              prop: "[fkey.shapedata].subj.color",
              name: "color"
            }]
          }]
        }
      },
      child: [{
        fkey: {
          '[./].baseelem': ['sequence'],
          sequence: '[./].baseelem'
        },
        spec: "seq"
      }]
    }]
  })

  const graphstart = graphempty.set('/', rootnode)
  const rep = gnreport_get('build')
  const [graph] = await promisify(gnnode_dfshydrateall)(
    sess, cfg, graphstart, rootnode, rep)

  const seqactionshapenode = graph
    .get('/canvasportalbg/seq_1/seqactionshape').toJS()

  assert.deepStrictEqual(seqactionshapenode, {
    node: 'uilabel',
    domchildkey: {},
    ispersistedts: seqactionshapenode.ispersistedts,
    fkeyhydrated: {
      '[./].baseelem': null,
      sequence: '/shapes/key/1',
      shapetype: 'architectonic',
      shapedata: '/shapedata'
    },
    subj: seqactionshapenode.subj,
    name: 'seqactionshape',
    datainarr: seqactionshapenode.datainarr,
    isdom: true,
    subjhydrated: { color: 'red' },
    domchildkeyarr: [],
    key: '/canvasportalbg/seq_1/seqactionshape'
  })
})

test('should runtime-build when+!spec+fkeys using [./].baseelem', async () => {
  const cfg = mockcfg({
    specfn: {
      issame: ([a, b]) => a === b
    }
  })
  const sess = { locale: 'eng-US' }
  const graphempty = gngraph_create({})

  const userspec = {
    node: 'gnpgdata',
    name: 'datauser',
    datakeyprop: 'id',
    subj: [{
      name: 'id',
      [gnenumSPECPROPNAMETYPE]: 'nsprop',
      prop: 'base.id',
      def: {
        [gnenumSPECPROPNAMETYPE]: 'fn',
        fnname: 'datenow'
      }
    },{
      name: 'name',
      [gnenumSPECPROPNAMETYPE]: 'nsprop',
      prop: 'base.name',
      def: ''
    },{
      name: 'iscompleted',
      [gnenumSPECPROPNAMETYPE]: 'nsprop',
      prop: 'base.iscompleted',
      def: false
    }]
  }

  const rootnode = gnnode_create({
    key: '/',
    node: 'uiroot',
    name: '/',
    domchildkeyarr: [],
    child: [{
      node: 'gnpgdatals',
      name: 'data-users',
      baselist: {
        [gnenumSPECPROPNAMETYPE]: 'literal',
        value: [
          { id: 200, name: 'jim' },
          { id: 201, name: 'jane' },
          { id: 202, name: 'richard' }
        ]
      },
      child: [userspec]
    }, {
      node: 'uispread',
      name: 'list-users',
      baselist: {
        [gnenumSPECPROPNAMETYPE]: 'literal',
        value: [
          '/data-users/200',
          '/data-users/201',
          '/data-users/202'
        ]
      },
      child: [{
        node: 'uispread',
        name: 'spread',
        fkey: {
          '[./].baseelem': ['datatodo'],
          datatodo: '[./].baseelem'
        },
        when: {
          fnname: 'issame',
          args: ['true', 'true']
        },
        child: [{
          node: 'uilabel',
          name: 'label',
          subj: [{
            'gn:type': 'nsprop',
            prop: '[fkey.datatodo].subj.name',
            name: 'value'
          }]
        }]
      }]
    }]
  })

  const graphstart = graphempty.set('/', rootnode)
  const rep = gnreport_get('build')
  const [graph] = await promisify(gnnode_dfshydrateall)(
    sess, cfg, graphstart, rootnode, rep)

  const nodeuser202labelkey = '/list-users/spread_202/label'
  const nodeuser202label = graph.get('/list-users/spread_202/label').toJS()
  assert.deepStrictEqual(nodeuser202label, {
    key: '/list-users/spread_202/label',
    node: 'uilabel',
    name: 'label',
    isdom: true,
    domchildkey: {},
    domchildkeyarr: [],
    ispersistedts: nodeuser202label.ispersistedts,
    fkeyhydrated: { '[./].baseelem': null, datatodo: '/data-users/202' },
    datainarr: [{
      'gn:type': 'nsprop',
      id: `[${nodeuser202labelkey}].subj.value ⸺> [fkey.datatodo].subj.name`,
      key: '/data-users/202',
      refname: 'name'
    }],
    subj: nodeuser202label.subj,
    subjhydrated: { value: 'richard' }
  })
})

test('childpecwhenparentfirst should return conditional child', async () => {
  const cfg = mockcfg({
    specfn: {
      issame: ([a, b]) => a === b
    }
  })
  const sess = { locale: 'eng-US' }
  const graphstart = gngraph_create({
    '/data-env': {
      key: '/data-env',
      node: 'gnpgdata',
      subjhydrated: {
        hello: 'world'
      }
    },
    '/data-users': {
      key: '/data-users',
      node: 'gnpgdatals',
      name: 'data-users',
      fkeyhydrated: {
        env: '/data-env'
      }
    }
  })

  const spec = {
    spec: {
      node: 'uilabel',
      name: 'label'
    },
    fkey: {
      env: './'
    },
    when: {
      [gnenumSPECPROPNAMETYPE]: 'OR',
      whenarr: [{
        fnname: 'issame',
        args: ['base.subj.hello', 'world']
      },{
        fnname: 'issame',
        args: ['base.subj.hello', 'dave']
      }]
    }
  }

  const [cspec] = await promisify(gnnode_childspecwhenparentfirst)(
    sess, cfg, graphstart, graphstart.get('/data-users'), '/data-env', [
      spec
    ])

  assert.deepStrictEqual(cspec, spec)
})

test('childpecwhenparentfirst should surface gnrun error', async () => {
  const cfg = mockcfg()
  const sess = { locale: 'eng-US' }
  const graphstart = gngraph_create({
    '/data-env': {
      key: '/data-env',
      node: 'gnpgdata',
      subjhydrated: {
        hello: 'world'
      }
    },
    '/data-users': {
      key: '/data-users',
      node: 'gnpgdatals',
      name: 'data-users',
      fkeyhydrated: {
        env: '/data-env'
      }
    }
  })

  const spec = {
    spec: {
      node: 'uilabel',
      name: 'label'
    },
    fkey: {
      env: './'
    },
    when: {
      [gnenumSPECPROPNAMETYPE]: 'OR',
      whenarr: [{
        fnname: 'issameUNDEF',
        args: ['base.subj.hello', 'world']
      },{
        fnname: 'issameUNDEF',
        args: ['base.subj.hello', 'dave']
      }]
    }
  }

  await assert.rejects(async () => (
    promisify(gnnode_childspecwhenparentfirst)(
      sess, cfg, graphstart, graphstart.get('/data-users'), '/data-env', [
        spec
      ])
  ), {
    message: 'invalid: fnname “issameUNDEF”'
  })
})

test('pathrefresolve, should error if ns not found', async () => {
  const cfg = mockcfg()
  const sess = { locale: 'eng-US' }
  const graph = gngraph_create()
  const nodebox = gnnode_create({
    key: '/box',
    node: 'gnpgui',
    name: 'box',
    domchildkeyarr: []
  })

  await assert.rejects(async () => (
    gnnode_pathrefresolve(
      sess, cfg, graph, nodebox, 'invalidns', 'details')
  ), {
    message: gnerr_nodensnotfound(
      graph, nodebox, 'details', 'invalidns').message
  })
})

test('pathrefresolve, should error if ns[prop] not found', async () => {
  const cfg = mockcfg()
  const sess = { locale: 'eng-US' }
  const graph = gngraph_create()
  const nodebox = gnnode_create({
    key: '/box',
    node: 'gnpgui',
    name: 'box',
    subjhydrated: {
      details: true
    },
    domchildkeyarr: []
  })

  await assert.rejects(async () => (
    gnnode_pathrefresolve(
      sess, cfg, graph, nodebox, 'subj', 'detailsAAA')
  ), {
    message: gnerrnode_pathrefnotfound(
      graph, nodebox, 'detailsAAA', { details: true }).message
  })
})

test('pathresolve, should error if resolves invalid', async () => {
  const cfg = mockcfg()
  const sess = { locale: 'eng-US' }
  const graph = gngraph_create()
  const nodebox = gnnode_create({
    key: '/box',
    node: 'gnpgui',
    name: 'box',
    subjhydrated: {
      details: true
    },
    domchildkeyarr: []
  })

  await assert.rejects(async () => gnnode_pathresolve(
    sess, cfg, graph, nodebox, { path: undefined }
  ), {
    message: gnerrnode_foreignnamespacenotfound(
      graph, nodebox, { path: undefined }).message
  })
})

test('edgesetfromspec, should error if edge node not found', async () => {
  const cfg = mockcfg()
  const sess = { locale: 'eng-US' }
  const graph = gngraph_create({})
  const nodebox = gnnode_create({
    key: '/box',
    node: 'gnpgui',
    name: 'box',
    subjhydrated: {
      details: true
    },
    domchildkeyarr: []
  })

  await assert.rejects(async () => gnnode_edgesetfromspec(
    sess, cfg, graph, nodebox, 'subj', { path: './data' }
  ), {
    message: gnerrnode_edgenodenotfound(
      graph, nodebox, '/box/data', { path: './data' }
    ).message
  })
})

test('edgesetfromspec should set an edge', () => {
  const cfg = mockcfg()
  const sess = { locale: 'eng-US' }
  const graphstart = gngraph_create({
    '/datausers': {
      key: "/datausers",
      name: "datausers",
      node: "gnpgdatals",
      isdata: true,
      baselist: {
        [gnenumSPECPROPNAMETYPE]: "literal",
        value: [
          { id: 200, name: "jim" },
          { id: 201, name: "jane" },
          { id: 202, name: "richard" }
        ]
      },
      baselisthydrated: [
        { id: 200, name: "jim" },
        { id: 201, name: "jane" },
        { id: 202, name: "richard" }
      ],
      childarr: [
        "/datausers/200",
        "/datausers/201",
        "/datausers/202"
      ],
      subj: [{
        activekey: "",
        childlength: "",
        childadded: "",
        childaddedfirst: "",
        childremoved: "",
        childremovedlast: ""
      }],
      subjhydrated: {
        childadded: "",
        childlength: 3,
        activekey: "",
        childaddedfirst: "",
        childremoved: "",
        childremovedlast: ""
      },
      childsubj: {
        incompletetotal: 1
      },
      childsubjhydrated: {
        incompletetotal: 1
      },
      child: [{
        spec: "user",
        specassign: {
          onsubj: [{
            [gnenumSPECPROPNAMETYPE]: "fn",
            fnname: "cacheupdate",
            args: ["node"]
          }]
        }
      }],
      spectypes: {
        user: {
          node: "gnpgdata",
          name: "datauser",
          datakeyprop: "id",
          subj: [{
            name: "id",
            [gnenumSPECPROPNAMETYPE]: "nsprop",
            prop: "base.id",
            def: {
              [gnenumSPECPROPNAMETYPE]: "fn",
              fnname: "datenow"
            },
            ...nsstr('base.id')
          }, {
            name: "name",
            [gnenumSPECPROPNAMETYPE]: "nsprop",
            prop: "base.name",
            def: "",
            ...nsstr('base.name')
          }, {
            name: "iscompleted",
            [gnenumSPECPROPNAMETYPE]: "nsprop",
            prop: "base.iscompleted",
            def: false,
            ...nsstr('base.iscompleted')
          }]
        }
      }
    },
    '/root-content/foot/footlabel': {
      key: "/root-content/foot/footlabel",
      name: "footlabel",
      node: "uilabel",
      isdom: true,
      domchildkey: {},
      domchildkeyarr: [],
      subj: [{
        [gnenumSPECPROPNAMETYPE]: "nsprop",
        prop: "incompletetotal",
        path: "/datausers",
        name: "label",
        ...nsstr('[/datausers].childsubj.incompletetotal'),
        filterinarr: [{
          [gnenumSPECPROPNAMETYPE]: "fn",
          fnname: "tplstr",
          args: [
            ":arg items left",
            "this"
          ]
        }]
      }]
    }
  })

  const nsspec = graphstart.get('/root-content/foot/footlabel').get('subj')[0]
  const [
    graph
  ] = gnnode_edgesetfromspec(
    sess, cfg, graphstart,
    graphstart.get('/root-content/foot/footlabel'), 'subj', nsspec)

  assert.deepStrictEqual(
    graph.get('/root-content/foot/footlabel').get('datainarr').toJS(),
    [{
      key: "/datausers",
      id: "[/root-content/foot/footlabel].subj.label" +
        " ⸺> [/datausers].childsubj.incompletetotal",
      [gnenumSPECPROPNAMETYPE]: "nsprop",
      refname: "incompletetotal"
    }])

  assert.deepStrictEqual(
    graph.get('/datausers').get('dataoutarr').toJS(),
    [{
      key: "/root-content/foot/footlabel",
      meta: graphstart
        .get('/root-content/foot/footlabel').get('subj')[0],
      ns: "subj",
      id: "[/root-content/foot/footlabel].subj.label" +
        " ⸺> [/datausers].childsubj.incompletetotal",
      [gnenumSPECPROPNAMETYPE]: "nsprop",
      refname: "incompletetotal"
    }])
})

test('dfsattachchilds should call attachchilds on childs only', async () => {
  const cfg = mockcfg()
  const sess = { locale: 'eng-US' }
  const attachkeys = []
  const graph = gngraph_create({
    '/box': {
      key: '/box',
      node: 'pgtest',
      name: 'box',
      subjhydrated: {
        details: true
      },
      childarr: [
        '/box/child'
      ]
    },
    '/box/child': {
      key: '/box/child',
      node: 'pgtest',
      name: 'child'
    }
  })

  cfg.pgmap.pgtest = {
    onattach: (ss, cfg, gr, nd) => attachkeys.push(nd.get('key'))
  }

  await promisify(gnnode_dfsattachchilds)(sess, cfg, graph, graph.get('/box'))


  assert.deepStrictEqual(attachkeys, [
    '/box/child'
  ])
})

test('dfsdetachchilds should call detachchilds childs only', async () => {
  const cfg = mockcfg()
  const sess = { locale: 'eng-US' }
  const graph = gngraph_create({
    '/box': {
      key: '/box',
      node: 'pgtest',
      name: 'box',
      subjhydrated: {
        details: true
      },
      childarr: [
        '/box/child'
      ]
    },
    '/box/child': {
      key: '/box/child',
      node: 'pgtest',
      name: 'child'
    }
  })

  const [graphfinal] = await promisify(
    gnnode_dfsdetachchilds)(sess, cfg, graph, graph.get('/box'))

  assert.ok(graphfinal.has('/box'))
  assert.ok(!graphfinal.has('/box/child'))
})
