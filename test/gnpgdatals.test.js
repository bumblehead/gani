import test from 'node:test'
import assert from 'node:assert/strict'
import mockcfg from './helper/mockcfg.js'
import mocksess from './helper/mocksess.js'
import promisify from './helper/promisify.js'

import {
  gnreport_get
} from '../src/gnreport.js'

import {
  gnacc_subj
} from '../src/gnacc.js'

import {
  gnenumSPECPROPNAMETYPE
} from '../src/gnenum.js'

import {
  // gnnode_create,
  gnnode_dfshydrateall
} from '../src/gnnode.js'

import {
  gngraph_create
} from '../src/gngraph.js'

import {
  gnerrnode_nspropresolvedundefined
} from '../src/gnerr.js'

import gnpgdatals from '../src/gnpgdatals.js'

test('should hydrate childsubj', async () => {
  const cfg = mockcfg({
    specfn: {
      tplstr: () => 'tplstr',
      accumtrue: ([accum, next]) => next ? accum + 1 : accum
    }
  })
  const sess = mocksess()
  const graphstart = gngraph_create({
    '/': {
      key: '/',
      node: 'uiroot',
      name: '/',
      domchildkeyarr: [],
      child: [{
        node: 'gnpgdatals',
        name: 'datausers',
        baselist: {
          [gnenumSPECPROPNAMETYPE]: 'literal',
          value: [
            { id: 200, name: 'jim', iscompleted: true },
            { id: 201, name: 'jane', iscompleted: false },
            { id: 202, name: 'richard', iscompleted: true }
          ]
        },
        childsubj: [{
          [gnenumSPECPROPNAMETYPE]: "nsprop",
          prop: "[fkey.child].subj.iscompleted",
          name: "iscompletetotal",
          accum: {
            [gnenumSPECPROPNAMETYPE]: "fn",
            fnname: "accumtrue",
            start: 0
          }
        }],
        child: [{
          spec: 'user',
          specassign: {
            onsubj: [{
              [gnenumSPECPROPNAMETYPE]: 'fn',
              fnname: 'cacheupdate',
              args: ['node']
            }]
          }
        }],
        spectypes: {
          user: {
            node: 'gnpgdata',
            name: 'datauser',
            datakeyprop: 'id',
            subj: [{
              name: 'id',
              [gnenumSPECPROPNAMETYPE]: 'nsprop',
              prop: 'base.id',
              def: {
                [gnenumSPECPROPNAMETYPE]: 'fn',
                fnname: 'datenow'
              }
            },{
              name: 'name',
              [gnenumSPECPROPNAMETYPE]: 'nsprop',
              prop: 'base.name',
              def: ''
            },{
              name: 'iscompleted',
              [gnenumSPECPROPNAMETYPE]: 'nsprop',
              prop: 'base.iscompleted',
              def: false
            }]
          }
        }
      }]
    }
  })

  const rep = gnreport_get('build')
  const [graph, node] = await promisify(gnnode_dfshydrateall)(
    sess, cfg, graphstart, graphstart.get('/'), rep)

  assert.ok(node)
  assert.deepStrictEqual(
    graph.get('/datausers').get('childsubjhydrated'),
    { iscompletetotal: 2 })
})

test('should rmchildarr', async () => {
  const cfg = mockcfg({
    specfn: {
      tplstr: () => 'tplstr',
      accumtrue: ([accum, next]) => next ? accum + 1 : accum
    }
  })
  const sess = { locale: 'eng-US' }
  const graphstart = gngraph_create({
    '/': {
      key: '/',
      node: 'uiroot',
      name: '/',
      domchildkeyarr: [],
      child: [{
        node: 'gnpgdatals',
        name: 'datausers',
        baselist: {
          [gnenumSPECPROPNAMETYPE]: 'literal',
          value: [
            { id: 200, name: 'jim', iscompleted: true },
            { id: 201, name: 'jane', iscompleted: false },
            { id: 202, name: 'richard', iscompleted: true }
          ]
        },
        childsubj: [{
          [gnenumSPECPROPNAMETYPE]: "nsprop",
          prop: "[fkey.child].subj.iscompleted",
          name: "iscompletetotal",
          accum: {
            [gnenumSPECPROPNAMETYPE]: "fn",
            fnname: "accumtrue",
            start: 0
          }
        }],
        child: [{
          spec: 'user',
          specassign: {
            onsubj: [{
              [gnenumSPECPROPNAMETYPE]: 'fn',
              fnname: 'cacheupdate',
              args: ['node']
            }]
          }
        }],
        spectypes: {
          user: {
            node: 'gnpgdata',
            name: 'datauser',
            datakeyprop: 'id',
            subj: [{
              name: 'id',
              [gnenumSPECPROPNAMETYPE]: 'nsprop',
              prop: 'base.id',
              def: {
                [gnenumSPECPROPNAMETYPE]: 'fn',
                fnname: 'datenow'
              }
            },{
              name: 'name',
              [gnenumSPECPROPNAMETYPE]: 'nsprop',
              prop: 'base.name',
              def: ''
            },{
              name: 'iscompleted',
              [gnenumSPECPROPNAMETYPE]: 'nsprop',
              prop: 'base.iscompleted',
              def: false
            }]
          }
        }
      }]
    }
  })

  const rep = gnreport_get('build')
  const [graph] = await promisify(gnnode_dfshydrateall)(
    sess, cfg, graphstart, graphstart.get('/'), rep)

  assert.deepStrictEqual(graph.get('/datausers').get('childarr').toJS(), [
    '/datausers/200',
    '/datausers/201',
    '/datausers/202'
  ])

  const [graph2, datausers] = await promisify(gnpgdatals.rmchildarr)(
    sess, cfg, graph, graph.get('/datausers'), gnpgdatals, [
      '/datausers/200',
      '/datausers/202'
    ])

  assert.deepStrictEqual(graph2.get('/datausers').get('childarr').toJS(), [
    '/datausers/201'
  ])

  assert.deepStrictEqual(datausers.get('childarr').toJS(), [
    '/datausers/201'
  ])
})

test('should hydrate childsubj, even when childs empty, defaults', async () => {
  const cfg = mockcfg({
    specfn: {
      tplstr: () => 'tplstr',
      accumtrue: ([accum, next]) => next ? accum + 1 : accum
    }
  })
  const sess = { locale: 'eng-US' }
  const graphstart = gngraph_create({
    '/': {
      key: '/',
      node: 'uiroot',
      name: '/',
      domchildkeyarr: [],
      child: [{
        node: 'gnpgdatals',
        name: 'datausers',
        baselist: {
          [gnenumSPECPROPNAMETYPE]: 'literal',
          value: [
          ]
        },
        childsubj: [{
          [gnenumSPECPROPNAMETYPE]: "nsprop",
          prop: "[fkey.child].subj.iscompleted",
          name: "iscompletetotal",
          accum: {
            [gnenumSPECPROPNAMETYPE]: "fn",
            fnname: "accumtrue",
            start: 0
          }
        }],
        child: [{
          spec: 'user',
          specassign: {
            onsubj: [{
              [gnenumSPECPROPNAMETYPE]: 'fn',
              fnname: 'cacheupdate',
              args: ['node']
            }]
          }
        }],
        spectypes: {
          user: {
            node: 'gnpgdata',
            name: 'datauser',
            datakeyprop: 'id',
            subj: [{
              name: 'id',
              [gnenumSPECPROPNAMETYPE]: 'nsprop',
              prop: 'base.id',
              def: {
                [gnenumSPECPROPNAMETYPE]: 'fn',
                fnname: 'datenow'
              }
            },{
              name: 'name',
              [gnenumSPECPROPNAMETYPE]: 'nsprop',
              prop: 'base.name',
              def: ''
            },{
              name: 'iscompleted',
              [gnenumSPECPROPNAMETYPE]: 'nsprop',
              prop: 'base.iscompleted',
              def: false
            }]
          }
        }
      }]
    }
  })

  const rep = gnreport_get('build')
  const [graph, node] = await promisify(gnnode_dfshydrateall)(
    sess, cfg, graphstart, graphstart.get('/'), rep)

  assert.ok(node)
  assert.deepStrictEqual(
    graph.get('/datausers').get('childsubjhydrated'),
    { iscompletetotal: 0 })
})

test('should throw useful error when lookup property is wrong', async () => {
  const cfg = mockcfg({
    specfn: {
      tplstr: () => 'tplstr',
      accumtrue: ([accum, next]) => next ? accum + 1 : accum
    }
  })
  const sess = { locale: 'eng-US' }

  const datalsusersspec = {
    node: 'gnpgdatals',
    name: 'datausers',
    baselist: {
      [gnenumSPECPROPNAMETYPE]: 'literal',
      value: [
        { id: 200, name: 'jim', iscompleted: true },
        { id: 201, name: 'jane', iscompleted: false },
        { id: 202, name: 'richard', iscompleted: true }
      ]
    },
    childsubj: [{
      [gnenumSPECPROPNAMETYPE]: "nsprop",
      prop: "[fkey.child].subj.iscompleted",
      name: "iscompletetotal",
      accum: {
        [gnenumSPECPROPNAMETYPE]: "fn",
        fnname: "accumtrue",
        start: 0
      }
    }],
    child: [{
      spec: 'user',
      specassign: {
        onsubj: [{
          [gnenumSPECPROPNAMETYPE]: 'fn',
          fnname: 'cacheupdate',
          args: ['node']
        }]
      }
    }],
    spectypes: {
      user: {
        node: 'gnpgdata',
        name: 'datauser',
        datakeyprop: 'id',
        subj: [{
          name: 'id',
          [gnenumSPECPROPNAMETYPE]: 'nsprop',
          prop: 'base.id',
          def: {
            [gnenumSPECPROPNAMETYPE]: 'fn',
            fnname: 'datenow'
          }
        },{
          name: 'name',
          [gnenumSPECPROPNAMETYPE]: 'nsprop',
          prop: 'base.name',
          def: ''
        },{
          name: 'iscompleted',
          [gnenumSPECPROPNAMETYPE]: 'nsprop',
          prop: 'base.iscompleted',
          def: false
        }]
      }
    }
  }

  const graphstart = gngraph_create({
    '/': {
      key: '/',
      node: 'uiroot',
      name: '/',
      domchildkeyarr: [],
      // expect datals to be *fully* hydrated inc childsubj *before*
      // sibling node is hydrated
      child: [datalsusersspec, {
        node: 'uispread',
        name: 'root-content',
        child: [{
          name: 'foot',
          node: 'uispread',
          child: [{
            name: 'footlabel',
            node: 'uilabel',
            subj: [{
              [gnenumSPECPROPNAMETYPE]: 'nsprop',
              prop: '[/datausers].childsubj.incompletetotal',
              name: 'label',
              filterinarr: [{
                [gnenumSPECPROPNAMETYPE]: 'fn',
                fnname: 'tplstr',
                argsdyn: [],
                args: [':arg items left', 'this']
              }]
            }]
          }]
        }]
      }]
    }
  })

  const rep = gnreport_get('build')

  // for now, run this test by itself to see the message clearly
  await assert.rejects(async () => (
    await promisify(gnnode_dfshydrateall)(
      sess, cfg, graphstart, graphstart.get('/'), rep)
  ), {
    message: gnerrnode_nspropresolvedundefined(
      graphstart,
      {
        get: () => '/root-content/foot/footlabel',
        toJS: () => ''
      },
      {iscompletetotal: 2},
      'label',
      'childsubj',
      'incompletetotal',
      '[/datausers].childsubj.incompletetotal'
    ).message
  })

  assert.ok(true)
})

test('add child data should add new child', async () => {
  const cfg = mockcfg({
    specfn: {
      tplstr: () => 'tplstr',
      accumtrue: ([accum, next]) => next ? accum + 1 : accum,
      accumnottrue: ([accum, next]) => next ? accum: accum + 1
    }
  })
  const sess = { locale: 'eng-US' }

  const datalsusersspec = {
    node: 'gnpgdatals',
    name: 'datausers',
    baselist: {
      [gnenumSPECPROPNAMETYPE]: 'literal',
      value: [
        { id: 200, name: 'jim', iscompleted: true },
        { id: 201, name: 'jane', iscompleted: false },
        { id: 202, name: 'richard', iscompleted: true }
      ]
    },
    childsubj: [{
      [gnenumSPECPROPNAMETYPE]: "nsprop",
      prop: "[fkey.child].subj.iscompleted",
      name: "iscompletetotal",
      accum: {
        [gnenumSPECPROPNAMETYPE]: "fn",
        fnname: "accumtrue",
        start: 0
      }
    }],
    child: [{
      spec: 'user',
      specassign: {
        onsubj: [{
          [gnenumSPECPROPNAMETYPE]: 'fn',
          fnname: 'cacheupdate',
          args: ['node']
        }]
      }
    }],
    spectypes: {
      user: {
        node: 'gnpgdata',
        name: 'datauser',
        datakeyprop: 'id',
        subj: [{
          name: 'id',
          [gnenumSPECPROPNAMETYPE]: 'nsprop',
          prop: 'base.id',
          def: {
            [gnenumSPECPROPNAMETYPE]: 'fn',
            fnname: 'datenow'
          }
        },{
          name: 'name',
          [gnenumSPECPROPNAMETYPE]: 'nsprop',
          prop: 'base.name',
          def: ''
        },{
          name: 'iscompleted',
          [gnenumSPECPROPNAMETYPE]: 'nsprop',
          prop: 'base.iscompleted',
          def: false
        }]
      }
    }
  }

  const graphstart = gngraph_create({
    '/': {
      key: '/',
      node: 'uiroot',
      name: '/',
      domchildkeyarr: [],
      // expect datals to be *fully* hydrated inc childsubj *before*
      // sibling node is hydrated
      child: [datalsusersspec, {
        node: 'uispread',
        name: 'root-content',
        child: [{
          name: 'foot',
          node: 'uispread',
          child: [{
            name: 'footlabel',
            node: 'uilabel',
            subj: [{
              [gnenumSPECPROPNAMETYPE]: 'nsprop',
              prop: '[/datausers].childsubj.iscompletetotal',
              name: 'label',
              filterinarr: [{
                [gnenumSPECPROPNAMETYPE]: 'fn',
                fnname: 'tplstr',
                args: [':arg items left', 'this']
              }]
            }]
          }]
        }]
      }]
    }
  })

  const rep = gnreport_get('build')
  const [graph, node] = await promisify(gnnode_dfshydrateall)(
    sess, cfg, graphstart, graphstart.get('/'), rep)

  assert.ok(graph.has('/datausers/200'))
  assert.ok(graph.has('/datausers/201'))
  assert.ok(graph.has('/datausers/202'))
  assert.ok(!graph.has('/datausers/203'))

  const [graphfinal] = await promisify(gnpgdatals.addchilddata)(
    sess, cfg, graph, graph.get('/datausers'), gnpgdatals, {
      id: 203,
      name: 'megumi'
    })

  assert.ok(graphfinal.has('/datausers/200'))
  assert.ok(graphfinal.has('/datausers/201'))
  assert.ok(graphfinal.has('/datausers/202'))
  assert.ok(graphfinal.has('/datausers/203'))
  assert.ok(graph && node)
})

test('rmchildarr should remove childs', async () => {
  const cfg = mockcfg({
    specfn: {
      tplstr: () => 'tplstr'
    }
  })
  const sess = { locale: 'eng-US' }
  const datalsusersspec = {
    node: 'gnpgdatals',
    name: 'datausers',
    baselist: {
      [gnenumSPECPROPNAMETYPE]: 'literal',
      value: [
        { id: 200, name: 'jim', iscompleted: true },
        { id: 201, name: 'jane', iscompleted: false },
        { id: 202, name: 'richard', iscompleted: true }
      ]
    },
    childsubj: [{
      [gnenumSPECPROPNAMETYPE]: "nsprop",
      prop: "[fkey.child].subj.iscompleted",
      name: "iscompletetotal",
      accum: {
        [gnenumSPECPROPNAMETYPE]: "fn",
        fnname: "accumtrue",
        start: 0
      }
    }],
    child: [{
      spec: 'user',
      specassign: {
        onsubj: [{
          [gnenumSPECPROPNAMETYPE]: 'fn',
          fnname: 'cacheupdate',
          args: ['node']
        }]
      }
    }],
    spectypes: {
      user: {
        node: 'gnpgdata',
        name: 'datauser',
        datakeyprop: 'id',
        subj: [{
          name: 'id',
          [gnenumSPECPROPNAMETYPE]: 'nsprop',
          prop: 'base.id',
          def: {
            [gnenumSPECPROPNAMETYPE]: 'fn',
            fnname: 'datenow'
          }
        },{
          name: 'name',
          [gnenumSPECPROPNAMETYPE]: 'nsprop',
          prop: 'base.name',
          def: ''
        },{
          name: 'iscompleted',
          [gnenumSPECPROPNAMETYPE]: 'nsprop',
          prop: 'base.iscompleted',
          def: false
        }]
      }
    }
  }
  const graphstart = gngraph_create({
    '/': {
      key: '/',
      node: 'uiroot',
      name: '/',
      domchildkeyarr: [],
      child: [datalsusersspec, {
        node: 'uispread',
        name: 'root-content',
        child: [{
          name: 'foot',
          node: 'uispread',
          child: [{
            name: 'footlabel',
            node: 'uilabel',
            subj: [{
              [gnenumSPECPROPNAMETYPE]: 'nsprop',
              prop: '[/datausers].childsubj.iscompletetotal',
              name: 'label',
              filterinarr: [{
                [gnenumSPECPROPNAMETYPE]: 'fn',
                fnname: 'tplstr',
                args: [':arg items left', 'this']
              }]
            }]
          }]
        }]
      }]
    }
  })

  const rep = gnreport_get('build')
  const [graph, node] = await promisify(gnnode_dfshydrateall)(
    sess, cfg, graphstart, graphstart.get('/'), rep)

  assert.ok(graph.has('/datausers/200'))
  assert.ok(graph.has('/datausers/201'))
  assert.ok(graph.has('/datausers/202'))

  // console.log('--<', gnpgdatals.rmchilddata)
  const [graphfinal] = await promisify(gnpgdatals.rmchildarr)(
    sess, cfg, graph, graph.get('/datausers'), gnpgdatals, ['/datausers/201'])

  assert.ok(graphfinal.has('/datausers/200'))
  assert.ok(!graphfinal.has('/datausers/201'))
  assert.ok(graphfinal.has('/datausers/202'))
  assert.ok(graph && node)
})

test('setnextactivekey should set activekey on node', async () => {
  const cfg = mockcfg({
    specfn: {
      tplstr: () => 'tplstr'
    }
  })
  const sess = { locale: 'eng-US' }
  const datalsusersspec = {
    node: 'gnpgdatals',
    name: 'datausers',
    baselist: {
      [gnenumSPECPROPNAMETYPE]: 'literal',
      value: [
        { id: 200, name: 'jim', iscompleted: true },
        { id: 201, name: 'jane', iscompleted: false },
        { id: 202, name: 'richard', iscompleted: true }
      ]
    },
    childsubj: [{
      [gnenumSPECPROPNAMETYPE]: "nsprop",
      prop: "[fkey.child].subj.iscompleted",
      name: "iscompletetotal",
      accum: {
        [gnenumSPECPROPNAMETYPE]: "fn",
        fnname: "accumtrue",
        start: 0
      }
    }],
    child: [{
      spec: 'user',
      specassign: {
        onsubj: [{
          [gnenumSPECPROPNAMETYPE]: 'fn',
          fnname: 'cacheupdate',
          args: ['node']
        }]
      }
    }],
    spectypes: {
      user: {
        node: 'gnpgdata',
        name: 'datauser',
        datakeyprop: 'id',
        subj: [{
          name: 'id',
          [gnenumSPECPROPNAMETYPE]: 'nsprop',
          prop: 'base.id',
          def: {
            [gnenumSPECPROPNAMETYPE]: 'fn',
            fnname: 'datenow'
          }
        },{
          name: 'name',
          [gnenumSPECPROPNAMETYPE]: 'nsprop',
          prop: 'base.name',
          def: ''
        },{
          name: 'iscompleted',
          [gnenumSPECPROPNAMETYPE]: 'nsprop',
          prop: 'base.iscompleted',
          def: false
        }]
      }
    }
  }

  const graphstart = gngraph_create({
    '/': {
      key: '/',
      node: 'uiroot',
      name: '/',
      domchildkeyarr: [],
      child: [datalsusersspec, {
        node: 'uispread',
        name: 'root-content',
        child: [{
          name: 'foot',
          node: 'uispread',
          child: [{
            name: 'footlabel',
            node: 'uilabel',
            subj: [{
              [gnenumSPECPROPNAMETYPE]: 'nsprop',
              prop: '[/datausers].childsubj.iscompletetotal',
              name: 'label',
              filterinarr: [{
                [gnenumSPECPROPNAMETYPE]: 'fn',
                fnname: 'tplstr',
                args: [':arg items left', 'this']
              }]
            }]
          }]
        }]
      }]
    }
  })

  const rep = gnreport_get('build')
  const [graph] = await promisify(gnnode_dfshydrateall)(
    sess, cfg, graphstart, graphstart.get('/'), rep)

  assert.ok(graph.has('/datausers/200'))
  assert.ok(graph.has('/datausers/201'))
  assert.ok(graph.has('/datausers/202'))

  // console.log('--<', gnpgdatals.rmchilddata)
  const nodefinal = gnpgdatals.setnextactivekey(
    sess, cfg, graph, graph.get('/datausers'), '/datausers/201')

  assert.strictEqual(
    gnacc_subj.getprop(nodefinal, 'activekey'), '/datausers/202')
})

