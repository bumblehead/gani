import test from 'node:test'
import assert from 'node:assert/strict'
import jsdom from 'jsdom'
import mockcfg from './helper/mockcfg.js'
import mocksess from './helper/mocksess.js'
import promisify from './helper/promisify.js'

import {
  gnenumSPECPROPNAMETYPE
} from '../src/gnenum.js'

import {
  gnnode_create
} from '../src/gnnode.js'

import {
  gngraph_create,
  gngraph_renderpath
} from '../src/gngraph.js'

import {
  gnreport_get
} from '../src/gnreport.js'

import {
  cfgenv,
  cfgglobal
} from './helper/mockcfg.js'

import {
  gnweb_init,
  gnweb_renderpatherr,
  gnweb_delaypopulated,
  gnweb_renderprogressive,
  gnweb_win_render
} from '../src/gnweb.js'

test('delaypopulated, should return a graph; w delay?', async () => {
  const cfg = mockcfg({
    specfn: {
      tplstr: () => 'tplstr'
    }
  })
  const sess = mocksess()
  const graphempty = gngraph_create({})
  const rootnode = gnnode_create({
    key: '/',
    node: 'uiroot',
    name: '/',
    domchildkeyarr: [],
    child: [{
      node: 'gnpgdatals',
      name: 'datausers',
      baselist: {
        [gnenumSPECPROPNAMETYPE]: 'literal',
        value: [
          { id: 200, name: 'jim' },
          { id: 201, name: 'jane' },
          { id: 202, name: 'richard' }
        ]
      },
      childsubj: {
        incompletetotal: 0
      },
      childsubjhydrated: {
        incompletetotal: 0
      },
      child: [{
        spec: 'user',
        specassign: {
          onsubj: [{
            [gnenumSPECPROPNAMETYPE]: 'fn',
            fnname: 'cacheupdate',
            args: ['node']
          }]
        }
      }],
      spectypes: {
        user: {
          node: 'gnpgdata',
          name: 'datauser',
          datakeyprop: 'id',
          subj: [{
            name: 'id',
            [gnenumSPECPROPNAMETYPE]: 'nsprop',
            prop: 'base.id',
            def: {
              [gnenumSPECPROPNAMETYPE]: 'fn',
              fnname: 'datenow'
            }
          },{
            name: 'name',
            [gnenumSPECPROPNAMETYPE]: 'nsprop',
            prop: 'base.name',
            def: ''
          },{
            name: 'iscompleted',
            [gnenumSPECPROPNAMETYPE]: 'nsprop',
            prop: 'base.iscompleted',
            def: false
          }]
        }
      }
    },{
      node: 'uispread',
      name: 'root-content',
      child: [{
        name: 'foot',
        node: 'uispread',
        // node: 'uils',
        className: 'footrow',
        child: [{
          name: 'footlabel',
          node: 'uilabel',
          subj: [{
            [gnenumSPECPROPNAMETYPE]: 'nsprop',
            prop: '[/datausers].childsubj.incompletetotal',
            name: 'label',
            filterinarr: [{
              [gnenumSPECPROPNAMETYPE]: 'fn',
              fnname: 'tplstr',
              args: [':arg items left', 'this']
            }]
          }]
        }]
      }]
    }]
  })

  const graphstart = graphempty.set('/', rootnode)
  const [graph, rep] = await promisify(gnweb_delaypopulated)(
    sess, cfg, graphstart, rootnode, gnreport_get('build'))

  assert.ok(graph && rep)
})

test('renderprogressive, should hydrate the graph', async () => {
  const dom = new jsdom.JSDOM(
    '<!DOCTYPE html><body><div id="first-element"></div></body>')

  dom.reconfigure({
    // url: 'https://0.0.0.0:4545/whatever/url/you/want'
    url: 'https://0.0.0.0:4545/'
  })

  global.window = dom.window
  global.document = dom.window.document

  const cfg = mockcfg({
    specfn: {
      tplstr: () => 'tplstr'
    }
  })
  const sess = mocksess()
  const graphempty = gngraph_create({})
  const rootnode = gnnode_create({
    key: '/',
    node: 'uiroot',
    name: '/',
    domchildkeyarr: [],
    child: [{
      node: 'gnpgdatals',
      name: 'datausers',
      baselist: {
        [gnenumSPECPROPNAMETYPE]: 'literal',
        value: [
          { id: 200, name: 'jim' },
          { id: 201, name: 'jane' },
          { id: 202, name: 'richard' }
        ]
      },
      childsubj: {
        incompletetotal: 0
      },
      childsubjhydrated: {
        incompletetotal: 0
      },
      child: [{
        spec: 'user',
        specassign: {
          onsubj: [{
            [gnenumSPECPROPNAMETYPE]: 'fn',
            fnname: 'cacheupdate',
            args: ['node']
          }]
        }
      }],
      spectypes: {
        user: {
          node: 'gnpgdata',
          name: 'datauser',
          datakeyprop: 'id',
          subj: [{
            name: 'id',
            [gnenumSPECPROPNAMETYPE]: 'nsprop',
            prop: 'base.id',
            def: {
              [gnenumSPECPROPNAMETYPE]: 'fn',
              fnname: 'datenow'
            }
          },{
            name: 'name',
            [gnenumSPECPROPNAMETYPE]: 'nsprop',
            prop: 'base.name',
            def: ''
          },{
            name: 'iscompleted',
            [gnenumSPECPROPNAMETYPE]: 'nsprop',
            prop: 'base.iscompleted',
            def: false
          }]
        }
      }
    },{
      node: 'uispread',
      name: 'root-content',
      child: [{
        name: 'foot',
        node: 'uispread',
        // node: 'uils',
        className: 'footrow',
        child: [{
          name: 'footlabel',
          node: 'uilabel',
          subj: [{
            [gnenumSPECPROPNAMETYPE]: 'nsprop',
            prop: '[/datausers].childsubj.incompletetotal',
            name: 'label',
            filterinarr: [{
              [gnenumSPECPROPNAMETYPE]: 'fn',
              fnname: 'tplstr',
              args: [':arg items left', 'this']
            }]
          }]
        }]
      }]
    }]
  })

  const graphstart = graphempty.set('/', rootnode)
  const [graph, rep] = await promisify(gnweb_renderprogressive)(
    sess, cfg, graphstart, ['/'], gnreport_get('test'))

  assert.ok(graph && rep)
  assert.deepStrictEqual(Object.keys(graph.toJS()), [
    '/',
    '/datausers',
    '/datausers/200',
    '/datausers/201',
    '/datausers/202',
    '/root-content',
    '/root-content/foot',
    '/root-content/foot/footlabel'
  ])
})

test('renderpatherr, throws error, even when cfg, gani undefined', async () => {
  const sess = mocksess()
  await assert.rejects(async () => {
    throw gnweb_renderpatherr(
      sess, {}, '/path/cause/error', new Error('an error'))
  }, {
    name: Error.name,
    message: 'an error'
  })
})

test('init, should complete happy path', async () => {
  const dom = new jsdom.JSDOM(
    '<!DOCTYPE html><body><div id="first-element"></div></body>')

  dom.reconfigure({
    // url: 'https://0.0.0.0:4545/whatever/url/you/want'
    url: 'https://0.0.0.0:4545/'
  })

  global.window = dom.window
  global.document = dom.window.document

  await promisify(gnweb_init)(cfgglobal(), {
    ...cfgenv(),
    // getpattern: async (sess, cfg, isopath) => {
    pagespecfromisopath: async () => {
      return ({
        deploytype: 'flat',
        locales: 'eng-US',
        routes: [],
        name: '/',
        node: 'uiroot',
        child: [{
          node: 'uispread',
          name: 'top'
          // },{
          //   ispathnode: true
          // },{
          //   node: 'uispread',
          //   name: 'bottom'
        }]
      })
    }
  }, dom)
})

test('win_render, should apply new path to existing graph', async () => {
  const sess = mocksess()
  const cfg = mockcfg({
    dom: new jsdom.JSDOM(
      '<!doctype html><body><div id="root"></div></body>', {
        url: 'https://0.0.0.0:4545/'
      }),
    specfn: {
      getrequrl: (args, ss) => ss.requrl
    }
  })

  const dataenvspec = {
    node: 'gnpgdata',
    name: 'dataenv',
    subj: [{
      [gnenumSPECPROPNAMETYPE]: 'fn',
      fnname: 'getrequrl',
      name: 'requrl'
    }]
  }

  const rootspec = {
    name: '/',
    node: 'uiroot',
    child: [(
      dataenvspec
    ), {
      node: 'uispread',
      name: 'top'
    },{
      ispathnode: true
    },{
      node: 'uispread',
      name: 'bottom'
    }]
  }

  const linkspec = {
    node: 'uilink',
    name: 'sitemap',
    subj: [{
      labelprimary: 'site map',
      href: '/site-map/'
    }]
  }

  const aboutspec = {
    node: 'uilink',
    name: 'about',
    subj: [{
      labelprimary: 'about',
      href: '/about/'
    }]
  }

  const specpathmap = {
    '$ROOTSPECS/spec/root/eng-US.json': rootspec,
    '$ROOTSPECS/spec/root-links/eng-US.json': linkspec,
    '$ROOTSPECS/spec/root-about/eng-US.json': aboutspec,
    '$ROOTSPECS/spec/root-data-env/eng-US.json': dataenvspec
  }

  global.window = cfg.dom.window
  global.document = cfg.dom.window.document

  cfg.pagespecfromisopath = (sess, cfg, isopath) => (
    specpathmap[isopath])

  const [graph1, , objstack] = (
    await gngraph_renderpath(sess, cfg, '/links/'))

  sess.objstack = objstack

  const [graph2] = await promisify(gnweb_win_render)(
    sess, cfg, graph1, '/about/', null)

  assert.ok(gnweb_win_render)
  assert.deepStrictEqual(Object.keys(graph2.toJS()), [
    '/', '/dataenv', '/top', '/bottom', '/about'
  ])

  assert.ok(true)
})

// TODO simplify number of lines needed to run test
test('should render a root w/no routes as example', async () => {
  const dataenvspec = {
    node: 'gnpgdata',
    name: 'dataenv',
    subj: [{
      [gnenumSPECPROPNAMETYPE]: 'fn',
      fnname: 'getrequrl',
      name: 'requrl'
    }]
  }

  const rootspec = {
    name: '/',
    node: 'uiroot',
    child: [dataenvspec, {
      name: "rmtodo",
      node: "uibutton",
      subj: {
        labelbutton: "✕"
      }
    }]
  }

  const specpathmap = {
    '$ROOTSPECS/spec/root/eng-US.json': rootspec,
    '$ROOTSPECS/spec/manifest.json': {
      deploytype: 'flat',
      locales: 'eng-US',
      routes: []
    }
  }

  const dom = new jsdom.JSDOM(
    '<!doctype html><body><div id="root"></div></body>', {
      url: 'https://0.0.0.0:4545/'
    })

  // needed for inferno :(
  global.window = dom.window
  global.document = dom.window.document

  await gnweb_init(cfgglobal({
    manifest: {
      deploytype: 'flat',
      locales: 'eng-US',
      routes: []
    }
  }), cfgenv({
    dom,
    pagespecfromisopath: (sess, cfg, isopath) => (
      specpathmap[isopath]),
    specfn: {
      getrequrl: (args, ss) => ss.requrl
    }
  }), {
    window: dom.window
  })

  const cfg = dom.window.gcfg
  assert.ok(cfg)

  const label = document.getElementsByTagName('label')[0]
  const button = document.getElementsByTagName('button')[0]
  assert.ok(label)
  assert.strictEqual(label.className, 'rmtodo uibutton')
  assert.strictEqual(label.getAttribute('for'), '/rmtodo')

  assert.strictEqual(button.className, 'rmtodo uibutton-button')
  assert.strictEqual(button.name, 'rmtodo')
  assert.strictEqual(button.id, ':rmtodo')
})

test('should manage events associated with node', async () => {
  const state = {}
  const dataenvspec = {
    node: 'gnpgdata',
    name: 'dataenv',
    subj: [{
      [gnenumSPECPROPNAMETYPE]: 'fn',
      fnname: 'getrequrl',
      name: 'requrl'
    }]
  }

  const rootspec = {
    name: '/',
    node: 'uiroot',
    child: [dataenvspec, {
      name: "rmtodo",
      node: "uibutton",
      subj: {
        labelbutton: "✕"
      },
      onclick: [{
        [gnenumSPECPROPNAMETYPE]: "fn",
        fnname: "wasclicked",
        argsdyn: [],
        args: ["fkey.datatodo"]
      }]
    }, {
      name: "buttonbadclick",
      node: "uibutton",
      subj: {
        labelbutton: "✕"
      },
      onclick: [{
        [gnenumSPECPROPNAMETYPE]: "fn",
        fnname: "wasclicked",
        argsdyn: [],
        args: ["fkey.datatodo"]
      }]
    }]
  }

  const specpathmap = {
    '$ROOTSPECS/spec/root/eng-US.json': rootspec,
    '$ROOTSPECS/spec/manifest.json': {
      deploytype: 'flat',
      locales: 'eng-US',
      routes: []
    }
  }

  const dom = new jsdom.JSDOM(
    '<!doctype html><body><div id="root"></div></body>', {
      url: 'https://0.0.0.0:4545/'
    })

  // needed for inferno :(
  global.window = dom.window
  global.document = dom.window.document

  await gnweb_init(cfgglobal({
    manifest: {
      deploytype: 'flat',
      locales: 'eng-US',
      routes: []
    }
  }), cfgenv({
    dom,
    pagespecfromisopath: (sess, cfg, isopath) => (
      specpathmap[isopath]),
    specfn: {
      getrequrl: (args, ss) => ss.requrl,
      wasclicked: (args, ss, cfg, gr) => {
        state.wasclicked = true

        return gr
      }
    }
  }), {
    window: dom.window
  })

  dom.window.document
    .getElementById(':rmtodo')
    .dispatchEvent(new dom.window.MouseEvent('click', { bubbles: true }))

  assert.strictEqual(state.wasclicked, true)
})
