import fs from 'node:fs'
import url from 'node:url'
import path from 'node:path'

const [
  mock_graph_todo_json,
  mock_node_uicheck_subjarr_json,
  mock_node_uicheck_subjobj_json,
  mock_node_uinav_optimised_json
] = [
  './graph_todo.json',
  './node_uicheck_subjarr.json',
  './node_uicheck_subjobj.json',
  './node_uinav_optimised.json'
].map(name => JSON.parse(
  fs.readFileSync(
    path.join(url.fileURLToPath(new URL('.', import.meta.url)), name))))

export {
  mock_graph_todo_json,
  mock_node_uicheck_subjarr_json,
  mock_node_uicheck_subjobj_json,
  mock_node_uinav_optimised_json
}
