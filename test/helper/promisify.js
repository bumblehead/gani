export default fn => (...args) => (
  new Promise((response, error) => fn(...args, (err, ...res) => (
    err ? error(err) : response(res)))))
