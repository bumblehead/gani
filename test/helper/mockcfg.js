import fs from 'node:fs/promises'
import jsdom from 'jsdom'

import gncfg from '../../src/gncfg.js'
import uih1 from '../../src/basic/uih1.js'
import uiobj from '../../src/basic/uiobj.js'
import uiobjls from '../../src/basic/uiobjls.js'
import uiroot from '../../src/basic/uiroot.js'
import uitext from '../../src/basic/uitext.js'
import uilink from '../../src/basic/uilink.js'
import uilabel from '../../src/basic/uilabel.js'
import uispread from '../../src/basic/uispread.js'
import uibutton from '../../src/basic/uibutton.js'
import uistatic from '../../src/basic/uistatic.js'
import uifieldset from '../../src/basic/uifieldset.js'
import datablend from '../../src/gnpgdatablend.js'

import {
  gnenumSUBJMETATITLE,
  gnenumSUBJMETADESCRIPTION
} from '../../src/gnenum.js'

import {
  gnpathdef_createfrommanifest
} from '../../src/gnpathdef.js'

const cfgmanifest = {
  deploytype: 'flat',
  locales: 'eng-US',
  routes: [
    '/',
    '/about/',
    '/links/',
    '/gallery/',
    '/site-map/',
    '/blog/', [[
      '/blog/',
      '/blog/all',
      '/blog/pg-:num',
      '/blog/:article'
    ]],
    '/media/', [[
      '/media/',
      '/media/all',
      '/media/pg-:num',
      '/media/:article'
    ]]
  ]
}

const cfgglobal = (opts = {}) => ({
  version: process.env.npm_package_version,

  // specpath: '/spec/',
  specpath: './spec/view/',

  manifest: opts.manifest || cfgmanifest,
  pathdef: gnpathdef_createfrommanifest(opts.manifest || cfgmanifest),

  nodes: opts.nodes || [
    uih1,
    uiobj,
    uiobjls,
    uiroot,
    uitext,
    uilink,
    uilabel,
    uispread,
    uibutton,
    uistatic,
    uifieldset,
    datablend
  ]
})

const cfgenv = (opts = {}) => ({
  locale: 'eng-US',
  origin: 'https://0.0.0.0', // required
  prefixdir: '', // required
  specpath: './spec/view/',
  iserrorenabled: false,
  islogenabled: false,
  dom: 'dom' in opts ? opts.dom : new jsdom.JSDOM(
    '<!DOCTYPE html><body><div></div></body>', {
      url: 'https://0.0.0.0:4545/'
    }),
  specfn: {
    returnargs: args => {
      return String(args)
    },
    ...opts.specfn
  },
  speccb: {
    returnargs: (args, fn) =>
      fn(null, String(args)),
    ...opts.speccb
  },

  pagemetaget: cfgglobal.pagemetaget || (
    (sess, cfg, subj, pnodearr) => ({
      description: subj[gnenumSUBJMETADESCRIPTION],
      title: subj[gnenumSUBJMETATITLE] || (
        pnodearr
          .map(pnode => pnode.pathvanilla.replace(/\//i, ''))
          .filter(e => e)
      )
    })),

  // legacy
  // pagetitleget: (ss, cfg, subj, pagedefarr) => [
  //   subj.gnmetatitle,
  //   pagedefarr && (pagedefarr.length > 1
  //    || !subj.gnmetatitle) && pagedefarr[0],
  //   'gani'
  // ].filter(e => e).join(' - '),
  pagespecfromisopath: async (sess, cfg, isopath) => {
    isopath = new URL('../../public/' + isopath, import.meta.url)
    isopath = await fs.readFile(isopath, { encoding: 'utf8' })

    return JSON.parse(isopath)
  },

  ...(opts.pagespecfromisopath && {
    pagespecfromisopath: opts.pagespecfromisopath
  })
})

const mockcfg = (opts = {}) => (
  Object.assign(gncfg(cfgglobal(opts), cfgenv(opts)), {
    manifest: opts.manifest || cfgmanifest
  }))

export {
  mockcfg as default,
  cfgenv,
  cfgglobal
}
