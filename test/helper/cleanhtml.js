import cleaner from 'clean-html'

export default async str => new Promise(resolve => (
  cleaner.clean(str, {
    'break-around-comments': false,
    'break-around-tags': [
      'body',
      'blockquote',
      'br',
      'span', 'ul', 'li',
      'div',
      'h1', 'h2', 'h3', 'h4', 'h5', 'h6',
      'head', 'hr', 'link', 'meta', 'p', 'table',
      'title', 'td', 'tr'
    ],
    'decode-entities': true,
    'remove-tags': ['b', 'i', 'center', 'font'],
    'wrap': 80
  }, output => resolve(output))))
