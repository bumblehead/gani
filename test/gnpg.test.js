import test from 'node:test'
import assert from 'node:assert/strict'
import mockcfg from './helper/mockcfg.js'
import mocksess from './helper/mocksess.js'

import gnpg from '../src/gnpg.js'

import {
  gnnode_create
} from '../src/gnnode.js'

import {
  gngraph_create
} from '../src/gngraph.js'

import {
  gnerr_nodeevhandlermustbelist
} from '../src/gnerr.js'

test('evPublishPOST should expect array of actions', async () => {
  const cfg = mockcfg()
  const sess = mocksess()
  const graphstart = gngraph_create({})
  const node = gnnode_create({
    key: '/nodename',
    node: 'NOTFOUND',
    name: 'sitemap',
    onclick: { // should be list
      "gn:type": "fn",
      fnname: "wasclicked",
      args: ["fkey.datatodo"]
    }
  })

  const graph = graphstart.set('/nodename', node)
  await assert.rejects(async () => (
    gnpg.evPublishPOST(sess, cfg, graph, node, gnpg, 'onclick')
  ), {
    message: gnerr_nodeevhandlermustbelist(
      graphstart, node, 'onclick', node.get('onclick')).message
  })
})
