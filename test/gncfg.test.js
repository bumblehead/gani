import test from 'node:test'
import assert from 'node:assert/strict'
import path from 'node:path'

import gnpgui from '../src/gnpgui.js'
import gnpgdata from '../src/gnpgdata.js'

import {
  gnenumSPECPROPNAMETYPE
} from '../src/gnenum.js'

import {
  gnerr_cfgprefixdirinvalid,
  gnerr_cfgenvinvalidorigin,
  gnerr_cfgreqdfnnotfoundpagespecfromisopath,
  gnerr_invalidpg
} from '../src/gnerr.js'

import gncfg  from '../src/gncfg.js'

test('gngfc should return a basic config', () => {
  const cfgglobal = {
    version: process.env.npm_package_version,
    baseurl: path.join(process.cwd(), '/src/'),
    dirs_path: path.join(process.cwd(), '/src/'),
    indexname: './index.tpl.html',
    specpath: './spec/view/',
    speccb: {},
    specfn: {},
    servicearr: [],

    routes: [
      'root', {
        pagearr: [
          ['/', 'page-home'],
          ['/about/', 'page-about'],
          ['/links/', 'page-links'],
          ['/gallery/', 'page-gallery'],
          ['/site-map/', 'page-sitemap'],
          ['/blog/:item?', 'page-blog', {
            pagearr: [
              ['/', 'page-blog-pgs'],
              ['/all', 'page-blog-all'],
              ['/pg-:num', 'page-blog-pgs'],
              ['/:article', 'page-blog-article']
            ]
          }],
          ['/media/:item?', 'page-media', {
            pagearr: [
              ['/', 'page-media-pgs'],
              ['/all', 'page-media-all'],
              ['/pg-:num', 'page-media-pgs'],
              ['/:article', 'page-media-article']
            ]
          }]
        ]
      }
    ]
  }

  const cfgenv = {
    locale: 'eng-US',
    origin: 'https://0.0.0.0',

    // clarify where these properties belong
    specpath: './spec/view/',

    // getpattern: async (sess, cfg, isopathww) => {
    getpattern: async () => {
      return JSON.parse('pattern')
    },

    srv_getfn: (method, path, data, token, fn) => {
      fn(null, 'service fn')
    },

    pagetitlecompose: (title, pagedirarr) => [
      title,
      pagedirarr && (pagedirarr.length > 1 || !title) && pagedirarr[0],
      'gani'
    ].filter(e => e).join(' - ')
  }

  const cfg = gncfg(cfgglobal, cfgenv)

  assert.strictEqual(cfg.origin, cfgenv.origin)
})

test('gncfg should throw error if origin undefined', async () => {
  const cfgglobal = {
    version: process.env.npm_package_version
  }
  const cfgenv = {}

  await assert.rejects(async () => (
    gncfg(cfgglobal, cfgenv)
  ), {
    message: gnerr_cfgenvinvalidorigin().message
  })
})

test('gncfg should throw error if prefixdir does not endslash', async () => {
  const cfgenv = {
    locale: 'eng-US',
    origin: 'https://0.0.0.0'
  }

  await assert.rejects(async () => (
    gncfg({
      prefixdir: "prefixdir/"
    }, cfgenv)
  ), {
    message: gnerr_cfgprefixdirinvalid("prefixdir/").message
  })

  await assert.rejects(async () => (
    gncfg({
      prefixdir: "/prefixdir"
    }, cfgenv)
  ), {
    message: gnerr_cfgprefixdirinvalid("/prefixdir").message
  })
})

test('gncfg should error for invalid pg', async () => {
  const pginvalid = {
    [gnenumSPECPROPNAMETYPE]: undefined,
    name: 'pginvalid'
  }

  const cfgglobal = {
    version: process.env.npm_package_version,
    baseurl: path.join(process.cwd(), '/src/'),
    dirs_path: path.join(process.cwd(), '/src/'),
    indexname: './index.tpl.html',
    specpath: './spec/view/',
    routes: [
      'root', {
        pagearr: [
          ['/', 'page-home']
        ]
      }
    ],
    nodes: [
      pginvalid,
      gnpgui,
      gnpgdata
    ]
  }

  const cfgenv = {
    locale: 'eng-US',
    origin: 'https://0.0.0.0',

    // clarify where these properties belong
    specpath: './spec/view/',

    // getpattern: async (sess, cfg, isopathww) => {
    getpattern: async () => {
      return JSON.parse('pattern')
    }
  }

  await assert.rejects(async () => (
    gncfg(cfgglobal, cfgenv)
  ), {
    message: gnerr_invalidpg(pginvalid).message
  })
})

test('gncfg should throw if pagespecfromisopath called not found', async () => {
  const sess = { locale: 'eng-US' }
  const cfgglobal = {
    version: process.env.npm_package_version
  }

  const cfgenv = {
    locale: 'eng-US',
    origin: 'https://0.0.0.0',
    islogenabled: false,
    iserrorenabled: false
  }

  const cfg = gncfg(cfgglobal, cfgenv)

  await assert.rejects(async () => (
    cfg.pagespecfromisopath(sess, cfg)
  ), {
    message: gnerr_cfgreqdfnnotfoundpagespecfromisopath(cfg).message
  })
})
