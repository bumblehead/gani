import { promisify } from 'node:util'
import test from 'node:test'
import assert from 'node:assert/strict'

import nsstr from 'nsstr'

import {
  gnpatch_unitcreate,
  gnpatch_unitlocalcreate,
  gnpatch_unitapplydeepall,
  gnpatch_unitapplydeep,
  gnpatch_unitapply,
  gnpatch_unitupstreamcreate,
  gnpatch_unitsfromnsprops,
  gnpatch_unitsfromkeyns,
  gnpatch_unitsfromkeynsarr,
  gnpatch_edgeapplyrefresh,
  gnpatch_edgeapplydeep,
  gnpatch_edgeapply,
  gnpatch_compositesplit,
  gnpatch_compositejoin
} from '../src/gnpatch.js'

import {
  gnnode_create,
  gnnode_edgeset,
  gnnode_dfshydrateall
} from '../src/gnnode.js'

import {
  gnacc_subj,
  gnacc_full
} from '../src/gnacc.js'

import {
  gngraph_create,
  gngraph_renderpath
} from '../src/gngraph.js'

import {
  gnreport_get,
  gnreport_patchget,
  gnreport_addreplacekey
} from '../src/gnreport.js'

import {
  gnerr_nodensnotfoundedge,
  gnerr_nodecompositevalueinvalid
} from '../src/gnerr.js'

import {
  gnenumSPECPROPNAMETYPE
} from '../src/gnenum.js'

import mockcfg from './helper/mockcfg.js'

test('localget, returns a local node patch', async () => {
  const sess = { locale: 'eng-US' }
  const cfg = mockcfg({
    specfn: {
      tonumber: numstr => {
        return +numstr
      }
    }
  })
  const graphempty = gngraph_create({})
  const nodebox = gnnode_create({
    key: '/box',
    node: 'uiobj',
    name: 'box',
    subjhydrated: {
      activeuser: '1'
    }
  })

  const graph = graphempty.set('/box', nodebox)

  // patch, un-filtered
  const patchunit = await promisify(gnpatch_unitlocalcreate)(
    sess, cfg, graph, nodebox, 'subj', {
      patchname: 'activeuser',
      patchvalue: '2'
    })

  assert.deepStrictEqual(patchunit, [
    ['/box', 'activeuser', 'subj', '2', 'src'],
    ['/box', 'activeuser', 'subj', '2', 'end']
  ])

  // patch, filtered
  const patchunitfiltered = await promisify(gnpatch_unitlocalcreate)(
    sess, cfg, graph, nodebox, 'subj', {
      patchname: 'activeuser',
      patchvalue: '2',
      filterinarr: [{
        [gnenumSPECPROPNAMETYPE]: 'fn',
        fnname: 'tonumber',
        args: ['this']
      }]
    })

  assert.deepStrictEqual(patchunitfiltered, [
    ['/box', 'activeuser', 'subj', '2', 'src'],
    ['/box', 'activeuser', 'subj', 2, 'end']
  ])
})

test('unitupstreamcreate, should create upstream patch', async () => {
  const sess = { locale: 'eng-US' }
  const cfg = mockcfg()
  const graphempty = gngraph_create({
    '/datausers': {
      key: '/datausers',
      node: 'gnpgdata',
      name: 'datausers',
      subj: {
        userspastmonth: '2'
      },
      subjhydrated: {
        userspastmonth: '2'
      }
    }
  })
  const nodebox = gnnode_create({
    key: '/box',
    node: 'uiobj',
    name: 'box',
    subj: {
      name: 'activeusers',
      [gnenumSPECPROPNAMETYPE]: 'literal',
      value: '1'
    },
    subjhydrated: {
      activeusers: '1'
    }
  })

  const graph = graphempty.set('/box', nodebox)
  const patchunit = await promisify(gnpatch_unitupstreamcreate)(
    sess, cfg, graph, nodebox, null, {
      ...nsstr('[/datausers].subj.userspastmonth'),
      path: '/datausers',
      patchname: 'activeusers',
      filteroutarr: null,
      patchvalue: '2'
    })

  assert.strictEqual(1, 1)
  assert.deepStrictEqual(patchunit, [
    ['/box', 'activeusers', 'subj', '2', 'src'],
    ['/datausers', 'userspastmonth', 'subj', '2', 'end']
  ])
})


test('patchunitapplynode should pass happy', () => {
  const cfg = mockcfg()
  const sess = { locale: 'eng-US' }
  const graphempty = gngraph_create({})
  const nodetarget = gnnode_create({
    key: '/target',
    node: 'uilabel',
    name: 'label',
    subjhydrated: {
      labelprimary: 'label primary',
      labelsecondary: 'label secondary'
    }
  })

  const graphstart = graphempty.set('/target', nodetarget)
  const patchunit = gnpatch_unitcreate(
    '/data', 'label', 'subj', 'new-value',
    '/target', 'labelprimary', 'subj', 'new-value')

  const [graph, node] = gnpatch_unitapply(
    sess, cfg, graphstart, patchunit)

  assert.deepStrictEqual(graph.get('/target').get('subjhydrated'), {
    labelprimary: 'new-value',
    labelsecondary: 'label secondary'
  })

  assert.deepStrictEqual(node.get('subjhydrated'), {
    labelprimary: 'new-value',
    labelsecondary: 'label secondary'
  })
})

test('unitapplydeep should handle composite node', async () => {
  const cfg = mockcfg()
  const sess = { locale: 'eng-US' }
  const graphstart = gngraph_create({
    '/data': {
      "isdata": true,
      "key": "/data",
      "node": "gnpgdata",
      "name": "data",
      "subj": [{
        "subjvalue": [10,-15]
      }],
      "subjhydrated": {
        "subjvalue": [10,-15]
      },
      "dataoutarr": [{
        "ns": "subj",
        "id": "[/label/sample].subj.rotationxy ⸺> [../../data].subj.subjvalue",
        [gnenumSPECPROPNAMETYPE]: "nsprop",
        "refname": "subjvalue",
        "key": "/label/sample",
        "meta": {
          "name": "rotationxy",
          [gnenumSPECPROPNAMETYPE]: "nsprop",
          "prop": "subjvalue",
          "path": "/data",
          ...nsstr('[../../data].subj.subjvalue')
        }
      }, {
        "ns": "full",
        "id": "[/datarotation].full ⸺> [../data].subj.subjvalue",
        [gnenumSPECPROPNAMETYPE]: "nsprop",
        "refname": "subjvalue",
        "key": "/datarotation",
        "meta": {
          [gnenumSPECPROPNAMETYPE]: "nsprop",
          "prop": "subjvalue",
          "path": "/data",
          ...nsstr('[../data].subj.subjvalue')
        }
      }, {
        "ns": "subj",
        "id": "[/datarotation].subj.value ⸺> [../data].subj.subjvalue",
        [gnenumSPECPROPNAMETYPE]: "nsprop",
        "refname": "subjvalue",
        "key": "/datarotation",
        "meta": {
          "name": "value",
          [gnenumSPECPROPNAMETYPE]: "nsprop",
          "prop": "subjvalue",
          "path": "/data",
          ...nsstr('[../data].subj.subjvalue')
        }
      }]
    },
    '/datarotation': {
      "key": "/datarotation",
      "name": "datarotation",
      "node": "gnpgdatablend",
      isdata: true,
      isdatablend: true,
      "full": {
        [gnenumSPECPROPNAMETYPE]: "nsprop",
        "prop": "subjvalue",
        "path": "../data",
        ...nsstr('[../data].subj.subjvalue')
      },
      fullhydrated: [10, -15],
      "subj": [{
        "name": "value",
        [gnenumSPECPROPNAMETYPE]: "nsprop",
        "prop": "subjvalue",
        "path": "../data",
        ...nsstr('[../data].subj.subjvalue')
      }, {
        [gnenumSPECPROPNAMETYPE]: "fullelem",
        "path": "./",
        "index": 0,
        "name": 0
      }, {
        [gnenumSPECPROPNAMETYPE]: "fullelem",
        "path": "./",
        "index": 1,
        "name": 1
      }],
      "subjhydrated": {
        "0": 99,
        "1": -99,
        "value": [99, -99]
      },

      "datainarr": [{
        "id": "[/datarotation].full ⸺> [../data].subj.subjvalue",
        [gnenumSPECPROPNAMETYPE]: "nsprop",
        "refname": "subjvalue",
        "key": "/data"
      }, {
        "id": "[/datarotation].subj.value ⸺> [../data].subj.subjvalue",
        [gnenumSPECPROPNAMETYPE]: "nsprop",
        "refname": "subjvalue",
        "key": "/data"
      }]
    }
  })

  const patchunit = gnpatch_unitcreate(
    '/datarotation', 'subjvalue', 'subj', [33, 31],
    '/datarotation', null, 'full', [33, 31])
  const recpatch = gnreport_patchget('testkey')
  const graph = await promisify(gnpatch_unitapplydeep)(
    sess, cfg, graphstart, patchunit, recpatch)

  assert.deepStrictEqual(
    graph.get('/datarotation').get('fullhydrated'), [33, 31])

  assert.deepStrictEqual(
    graph.get('/datarotation').get('subjhydrated'), {
      '0': 33,
      '1': 31,
      value: [33, 31]
    })
})

test('unitapplydeepall should pass happy', async () => {
  const cfg = mockcfg()
  const sess = { locale: 'eng-US' }
  const graphstart = gngraph_create({
    '/target': {
      key: '/target',
      node: 'uilabel',
      name: 'label',
      subjhydrated: {
        labelprimary: 'label primary',
        labelsecondary: 'label secondary'
      }
    }
  })

  const patchunit = gnpatch_unitcreate(
    '/data', 'label', 'subj', 'new-value',
    '/target', 'labelprimary', 'subj', 'new-value')
  const patchunits = [patchunit]

  const graph = await promisify(gnpatch_unitapplydeepall)(
    sess, cfg, graphstart, patchunits)

  assert.deepStrictEqual(graph.get('/target').get('subjhydrated'), {
    labelprimary: 'new-value',
    labelsecondary: 'label secondary'
  })
})

test('unitsfromnsprops, accum 1 patch; 1 node, 1 subj, dynamic', async () => {
  const sess = { locale: 'eng-US' }
  const cfg = mockcfg()
  const graphempty = gngraph_create({})
  const nodebox = gnnode_create({
    key: '/box',
    node: 'uiobj',
    name: 'box',
    subj: {
      name: 'activeuser',
      [gnenumSPECPROPNAMETYPE]: 'literal',
      value: '1'
    },
    subjhydrated: {
      activeuser: '1'
    }
  })

  const graph = graphempty.set('/box', nodebox)
  const patch = await promisify(gnpatch_unitsfromnsprops)(
    sess, cfg, graph, nodebox, 'subj', {
      activeuser: '2'
    })

  assert.strictEqual(patch.length, 1)
  assert.deepStrictEqual(patch, [[
    ['/box', 'activeuser', 'subj', '2', 'src'],
    ['/box', 'activeuser', 'subj', '2', 'end']
  ]])
})

test('unitsfromnodeprops, accum 1 patch; 1 node, 1 subj, static', async () => {
  const sess = { locale: 'eng-US' }
  const cfg = mockcfg({
    specfn: {
      tonumber: numstr => {
        return +numstr
      }
    }
  })
  const graphempty = gngraph_create({})
  const nodebox = gnnode_create({
    key: '/box',
    node: 'uiobj',
    name: 'box',
    subj: {
      activeuser: '1'
    },
    subjhydrated: {
      activeuser: '1'
    }
  })

  const graph = graphempty.set('/box', nodebox)

  // patch, un-filtered
  const patchunits = await promisify(gnpatch_unitsfromnsprops)(
    sess, cfg, graph, nodebox, 'subj', {
      activeuser: '2'
    })

  assert.deepStrictEqual(patchunits, [[
    ['/box', 'activeuser', 'subj', '2', 'src'],
    ['/box', 'activeuser', 'subj', '2', 'end']
  ]])
})

test('unitsfromkeyns, returns units', async () => {
  const sess = { locale: 'eng-US' }
  const cfg = mockcfg()
  const graphempty = gngraph_create({})
  const nodebox = gnnode_create({
    key: '/box',
    node: 'uiobj',
    name: 'box',
    subj: {
      name: 'activeuser',
      [gnenumSPECPROPNAMETYPE]: 'literal',
      value: '1'
    },
    subjhydrated: {
      activeuser: '1'
    }
  })

  const graph = graphempty.set('/box', nodebox)
  const patch = await promisify(gnpatch_unitsfromkeyns)(
    sess, cfg, graph, ['/box', 'subj', { activeuser: '2' }])

  assert.strictEqual(patch.length, 1)
  assert.deepStrictEqual(patch, [[
    ['/box', 'activeuser', 'subj', '2', 'src'],
    ['/box', 'activeuser', 'subj', '2', 'end']
  ]])

  const patch2 = await promisify(gnpatch_unitsfromkeynsarr)(
    sess, cfg, graph, [['/box', 'subj', { activeuser: '2' }]])

  assert.strictEqual(patch2.length, 1)
  assert.deepStrictEqual(patch2, [[
    ['/box', 'activeuser', 'subj', '2', 'src'],
    ['/box', 'activeuser', 'subj', '2', 'end']
  ]])
})

test('edgeapplyrefresh, should apply patch from edge, !DATA', async () => {
  const sess = { locale: 'eng-US' }
  const cfg = mockcfg()
  const graphempty = gngraph_create({
    '/datausers': {
      isdata: true,
      key: '/datausers',
      node: 'gnpgdata',
      name: 'datausers',
      meta: {},
      subj: {
        userspastmonth: '2'
      },
      subjhydrated: {
        userspastmonth: '2'
      }
    }
  })
  const nodebox = gnnode_create({
    key: '/box',
    node: 'uiobjcontent',
    name: 'box',
    fkey: {
      datausers: '/datausers'
    },
    fkeyhydrated: {
      datausers: '/datausers'
    },
    subj: {
      [gnenumSPECPROPNAMETYPE]: 'nsprop',
      prop: '[/datausers].subj.userspastmonth',
      name: 'activeusers'
    },
    subjhydrated: {
      activeusers: '1'
    }
  })

  const [graphstart, ndin, ndout] = gnnode_edgeset(
    graphempty.set('/box', nodebox),
    nodebox,
    graphempty.get('/datausers'),
    'subj',
    {
      ...nsstr('[/datausers].subj.userspastmonth'),
      // 'edgeout:disable'
      name: 'userspastmonth',
      fullstr: 'subj.activeusers',
      [gnenumSPECPROPNAMETYPE]: 'edgetype',
      prop: 'activeusers'
    }
  )

  const edge = ndout.get('datainarr').get(0)

  assert.ok(graphstart && ndin)

  const r = gnreport_patchget('testkey')
  const graph = await promisify(gnpatch_edgeapplyrefresh)(
    sess, cfg, graphstart, graphstart.get('/datausers'), edge, r)

  assert.deepStrictEqual(graph.get('/box').get('subjhydrated'), {
    activeusers: '2'
  })
})

test('edgeapplyrefresh, should apply patch from edge, DATA', async () => {
  const sess = { locale: 'eng-US' }
  const cfg = mockcfg()
  const graphstart = gngraph_create({
    '/datausers': {
      key: '/datausers',
      node: 'gnpgdata',
      name: 'datausers',
      isdata: true,
      meta: {},
      subj: {
        userspastmonth: '2'
      },
      subjhydrated: {
        userspastmonth: '2'
      }
    },
    '/datauserspermitted': {
      key: '/datauserspermitted',
      node: 'gnpgdata',
      name: 'datauserspermitted',
      isdata: true,
      fkey: {
        datausers: '/datausers'
      },
      fkeyhydrated: {
        datausers: '/datausers'
      },
      subj: {
        [gnenumSPECPROPNAMETYPE]: 'nsprop',
        prop: '[/datausers].subj.userspastmonth',
        name: 'activeusers',
        ...nsstr('[/datausers].subj.userspastmonth')
      },
      subjhydrated: {
        activeusers: '1'
      }
    }
  })

  const [graph1, ndin] = gnnode_edgeset(
    graphstart,
    graphstart.get('/datausers'),
    graphstart.get('/datauserspermitted'),
    'subj',
    {
      ...graphstart.get('/datauserspermitted').subj,
      [gnenumSPECPROPNAMETYPE]: 'edgetype',
      prop: 'userspastmonth',
      fullstr: '[/datausers].subj.userspastmonth',
      ...nsstr('[/datausers].subj.userspastmonth')
    }
  )

  const edgein = ndin.get('dataoutarr').get(0)

  assert.deepStrictEqual(edgein.toJS(), {
    id: '[/datauserspermitted].subj ⸺> [/datausers].subj.userspastmonth',
    [gnenumSPECPROPNAMETYPE]: 'edgetype',
    refname: 'userspastmonth',
    key: '/datauserspermitted',
    ns: 'subj',
    meta: {
      ...nsstr('[/datausers].subj.userspastmonth'),
      prop: 'userspastmonth',
      [gnenumSPECPROPNAMETYPE]: 'edgetype'
    }
  })

  const r = gnreport_patchget('testkey')
  const graph = await promisify(gnpatch_edgeapplyrefresh)(
    sess, cfg, graph1, graph1.get('/datausers'), edgein, r)

  assert.deepStrictEqual(graph.get('/datauserspermitted').get('subjhydrated'), {
    activeusers: '2'
  })
})

test('edgeapplyrefresh, should re-apply same patch', async () => {
  const sess = { locale: 'eng-US' }
  const cfg = mockcfg()
  const graphempty = gngraph_create({
    '/datausers': {
      isdata: true,
      key: '/datausers',
      node: 'gnpgdata',
      name: 'datausers',
      meta: {},
      subj: {
        userspastmonth: '2'
      },
      subjhydrated: {
        userspastmonth: '2'
      }
    }
  })
  const nodebox = gnnode_create({
    key: '/box',
    node: 'uiobjcontent',
    name: 'box',
    fkey: {
      datausers: '/datausers'
    },
    fkeyhydrated: {
      datausers: '/datausers'
    },
    subj: {
      [gnenumSPECPROPNAMETYPE]: 'nsprop',
      prop: '[/datausers].subj.userspastmonth',
      name: 'activeusers'
    },
    subjhydrated: {
      activeusers: '1'
    }
  })

  const [graphstart, , ndout] = gnnode_edgeset(
    graphempty.set('/box', nodebox),
    nodebox,
    graphempty.get('/datausers'),
    'subj',
    {
      ...nsstr('[/datausers].subj.userspastmonth'),
      // 'edgeout:disable'
      name: 'userspastmonth',
      fullstr: 'subj.activeusers',
      [gnenumSPECPROPNAMETYPE]: 'edgetype',
      prop: 'activeusers'
    }
  )

  const edge = ndout.get('datainarr').get(0)

  const r = gnreport_addreplacekey(
    gnreport_patchget('testkey'), '/box')
  const graph = await promisify(gnpatch_edgeapplyrefresh)(
    sess, cfg, graphstart, graphstart.get('/datausers'), edge, r)

  assert.deepStrictEqual(graphstart.toJS(), graph.toJS())
  assert.deepStrictEqual(graph.get('/box').get('subjhydrated'), {
    activeusers: '1'
  })
})

test('edgeapply, should apply patch from edge', async () => {
  const sess = { locale: 'eng-US' }
  const cfg = mockcfg()
  const graphempty = gngraph_create({
    '/datausers': {
      isdata: true,
      key: '/datausers',
      node: 'gnpgdata',
      name: 'datausers',
      meta: {},
      subj: {
        userspastmonth: '2'
      },
      subjhydrated: {
        userspastmonth: '2'
      }
    }
  })
  const nodebox = gnnode_create({
    key: '/box',
    node: 'uiobjcontent',
    name: 'box',
    fkey: {
      datausers: '/datausers'
    },
    fkeyhydrated: {
      datausers: '/datausers'
    },
    subj: {
      [gnenumSPECPROPNAMETYPE]: 'nsprop',
      prop: '[/datausers].subj.userspastmonth',
      name: 'activeusers'
    },
    subjhydrated: {
      activeusers: '1'
    }
  })

  const [graphstart, ndin] = gnnode_edgeset(
    graphempty.set('/box', nodebox),
    nodebox,
    graphempty.get('/datausers'),
    'subj',
    {
      ...nsstr('[/datausers].subj.userspastmonth'),
      // 'edgeout:disable'
      name: 'userspastmonth',
      fullstr: 'subj.activeusers',
      [gnenumSPECPROPNAMETYPE]: 'edgetype',
      prop: 'activeusers'
    }
  )

  const edge = ndin.get('dataoutarr').get(0)

  const edgenote = [edge, 'subj', '5']
  assert.ok(graphstart && ndin)

  const graph = await promisify(gnpatch_edgeapply)(
    sess, cfg, graphstart, graphstart.get('/datausers'), edgenote)

  assert.deepStrictEqual(graph.get('/datausers').get('subjhydrated'), {
    userspastmonth: '5'
  })
})

test('edgeapplydeep, should apply patch from edge', async () => {
  const sess = { locale: 'eng-US' }
  const cfg = mockcfg({
    specfn: {
      tplstr: ([tpl, num]) => (
        tpl.replace(/:arg/, num)),
      accumnottrue: ([prev, next]) => (
        next ? prev : prev + 1),
      accumtrue: ([prev, next]) => (
        next ? prev + 1 : prev)
    }
  })
  const graphstart = gngraph_create({
    '/': {
      key: '/',
      node: 'uiroot',
      name: '/',
      domchildkeyarr: [],
      child: [{
        node: 'gnpgdatals',
        name: 'datausers',
        baselist: {
          [gnenumSPECPROPNAMETYPE]: 'literal',
          value: [
            { id: 200, name: 'jim' },
            { id: 201, name: 'jane' },
            { id: 202, name: 'richard' }
          ]
        },
        childsubj: [{
          name: "incompletetotal",
          [gnenumSPECPROPNAMETYPE]: "nsprop",
          prop: "[fkey.child].subj.iscompleted",
          accum: {
            start: 0,
            [gnenumSPECPROPNAMETYPE]: "fn",
            fnname: "accumnottrue"
          }
        }],
        child: [{
          spec: 'user',
          specassign: {
            onsubj: [{
              [gnenumSPECPROPNAMETYPE]: 'fn',
              fnname: 'cacheupdate',
              args: ['node']
            }]
          }
        }],
        spectypes: {
          user: {
            node: 'gnpgdata',
            name: 'datauser',
            datakeyprop: 'id',
            subj: [{
              name: 'id',
              [gnenumSPECPROPNAMETYPE]: 'nsprop',
              prop: 'base.id',
              def: {
                [gnenumSPECPROPNAMETYPE]: 'fn',
                fnname: 'datenow'
              }
            },{
              name: 'name',
              [gnenumSPECPROPNAMETYPE]: 'nsprop',
              prop: 'base.name',
              def: ''
            },{
              name: 'iscompleted',
              [gnenumSPECPROPNAMETYPE]: 'nsprop',
              prop: 'base.iscompleted',
              def: false
            }]
          }
        }
      },{
        node: 'uispread',
        name: 'root-content',
        child: [{
          name: 'foot',
          node: 'uispread',
          className: 'footrow',
          child: [{
            name: 'footlabel',
            node: 'uilabel',
            subj: [{
              [gnenumSPECPROPNAMETYPE]: 'nsprop',
              prop: '[/datausers].childsubj.incompletetotal',
              name: 'label',
              filterinarr: [{
                [gnenumSPECPROPNAMETYPE]: 'fn',
                fnname: 'tplstr',
                args: [':arg items left', 'this']
              }]
            }]
          }]
        }]
      }]
    }
  })

  const rep = gnreport_get('build')
  const graph1 = await promisify(gnnode_dfshydrateall)(
    sess, cfg, graphstart, graphstart.get('/'), rep)

  const pxu = [
    ['/datausers', 'incompletetotal', 'childsubj', 5, 'src'],
    ['/root-content/foot/footlabel', 'label', 'subj', 5, 'end']]

  const graph2 = graph1.set(
    '/root-content/foot/footlabel',
    graph1.get('/root-content/foot/footlabel')
      .set('ispatchrehydrate', true))

  const graph = await promisify(gnpatch_edgeapplydeep)(
    sess, cfg, graph2, graph2.get('/datausers'), pxu,
    graph1.get('/datausers').get('dataoutarr').get(0))

  assert.deepStrictEqual(
    gnacc_subj.get(graph.get("/root-content/foot/footlabel")), {
      label: "5 items left"
    })
})

test('edgeapplydeep, should apply patch from edge, DATA', async () => {
  const sess = { locale: 'eng-US' }
  const cfg = mockcfg()
  const graphstart = gngraph_create({
    '/datausers': {
      key: '/datausers',
      node: 'gnpgdata',
      name: 'datausers',
      isdata: true,
      meta: {},
      subj: {
        userspastmonth: '2'
      },
      subjhydrated: {
        userspastmonth: '2'
      }
    },
    '/datauserspermitted': {
      key: '/datauserspermitted',
      node: 'gnpgdata',
      name: 'datauserspermitted',
      isdata: true,
      fkey: {
        datausers: '/datausers'
      },
      fkeyhydrated: {
        datausers: '/datausers'
      },
      subj: {
        [gnenumSPECPROPNAMETYPE]: 'nsprop',
        prop: '[/datausers].subj.userspastmonth',
        name: 'activeusers',
        ...nsstr('[/datausers].subj.userspastmonth')
      },
      subjhydrated: {
        activeusers: '1'
      }
    }
  })

  const [graph1, ndin] = gnnode_edgeset(
    graphstart,
    graphstart.get('/datausers'),
    graphstart.get('/datauserspermitted'),
    'subj',
    {
      ...graphstart.get('/datauserspermitted').subj,
      [gnenumSPECPROPNAMETYPE]: 'edgetype',
      prop: 'userspastmonth',
      name: 'activeusers',
      fullstr: '[/datausers].subj.userspastmonth',
      ...nsstr('[/datausers].subj.userspastmonth')
    }
  )

  const edgein = ndin.get('dataoutarr').get(0)
  assert.deepStrictEqual(edgein.toJS(), {
    // eslint-disable-next-line max-len
    id: '[/datauserspermitted].subj.activeusers ⸺> [/datausers].subj.userspastmonth',
    [gnenumSPECPROPNAMETYPE]: 'edgetype',
    refname: 'userspastmonth',
    key: '/datauserspermitted',
    ns: 'subj',
    meta: {
      ...nsstr('[/datausers].subj.userspastmonth'),
      prop: 'userspastmonth',
      [gnenumSPECPROPNAMETYPE]: 'edgetype',
      name: 'activeusers'
    }
  })

  const pxu = [
    ['/datausers', 'userspasthmonth', 'subj', '37', 'src'],
    ['/datauserspermitted', 'activeusers', 'subj', '37', 'end']]

  const ns = graph1.get('/datausers')
    .set('subjhydrated', { userspastmonth: '37' })

  const graph = await promisify(gnpatch_edgeapplydeep)(
    sess, cfg, graph1, ns, pxu, edgein)

  assert.deepStrictEqual(
    gnacc_subj.get(graph.get('/datauserspermitted')), {
      activeusers: '37'
    })
})

test('edgeapply, should error if prop not found at edge node', async () => {
  const sess = { locale: 'eng-US' }
  const cfg = mockcfg()
  const graphempty = gngraph_create({
    '/datausers': {
      isdata: true,
      key: '/datausers',
      node: 'gnpgdata',
      name: 'datausers',
      meta: {},
      subj: {
        userspastmonth: '2'
      },
      subjhydrated: {
        userspastmonth: '2'
      }
    }
  })
  const nodebox = gnnode_create({
    key: '/box',
    node: 'uiobjcontent',
    name: 'box',
    fkey: {
      datausers: '/datausers'
    },
    fkeyhydrated: {
      datausers: '/datausers'
    },
    subj: {
      [gnenumSPECPROPNAMETYPE]: 'nsprop',
      prop: '[/datausers].subj.userspastmonth',
      name: 'activeusers'
    },
    subjhydrated: {
      activeusers: '1'
    }
  })

  const [graphstart, ndin] = gnnode_edgeset(
    graphempty.set('/box', nodebox),
    nodebox,
    graphempty.get('/datausers'),
    'subj',
    {
      ...nsstr('[/datausers].subj.userspastmonth'),
      // 'edgeout:disable'
      name: 'userspastmonth',
      fullstr: 'subj.activeusers',
      [gnenumSPECPROPNAMETYPE]: 'edgetype',
      // prop: 'activeusers'
      prop: 'activeusers'
    }
  )

  const edge = ndin.get('dataoutarr').get(0)

  const edgenote = [edge, 'full', '5']

  await assert.rejects(async () => (
    promisify(gnpatch_edgeapply)(
      sess, cfg, graphstart, graphstart.get('/datausers'), edgenote)
  ), {
    message: gnerr_nodensnotfoundedge(
      graphstart,
      graphstart.get('/datausers'),
      'full',
      edge.toJS()
    ).message
  })
})

test('WAS compositesplit, PENDING MOVE', async () => {
  const sess = { locale: 'eng-US' }
  const cfg = mockcfg({
    pagespecfromisopath: async () => {
      return ({
        name: '/',
        node: 'uiroot',
        child: [{
          node: 'gnpgdatals',
          name: 'datablogs',
          // base: {
          //   // [gnenumSPECPROPNAMETYPE]: 'cb',
          //   // cbname: 'getdatablogpgsmeta'
          //   [gnenumSPECPROPNAMETYPE]: 'literal',
          //   value: [
          //     { title: 'title1', content: 'content1' },
          //     { title: 'title2', content: 'content2' },
          //     { title: 'title3', content: 'content3' },
          //     { title: 'title4', content: 'content4' },
          //     { title: 'title5', content: 'content5' },
          //     { title: 'title6', content: 'content6' }
          //   ]
          // },
          baselist: {
            // [gnenumSPECPROPNAMETYPE]: 'cb',
            // cbname: 'getdatablogpgs',
            // args: ['ns', 'fkey.pgnum'],
            // def: []
            [gnenumSPECPROPNAMETYPE]: 'literal',
            value: [
              { title: 'title1', content: 'content1' },
              { title: 'title2', content: 'content2' },
              { title: 'title3', content: 'content3' },
              { title: 'title4', content: 'content4' },
              { title: 'title5', content: 'content5' },
              { title: 'title6', content: 'content6' }
            ]
          },

          spectypes: {
            blog: {
              node: 'gnpgdata',
              name: 'datablog',
              datakeyprop: 'title',
              subj: [{
                spread: true,
                type: 'blog',
                activeuid: ''
              },{
                name: 'title',
                [gnenumSPECPROPNAMETYPE]: 'nsprop',
                prop: 'base.title'
              },{
                name: 'content',
                [gnenumSPECPROPNAMETYPE]: 'nsprop',
                prop: 'base.content'
              }]
            }
          },

          child: [{
            spec: 'blog'
          }]
        }]
      })
    }
  })

  const graphstart = await gngraph_renderpath(sess, cfg, '/')

  assert.ok(graphstart[0].toJS())
})

test('compositesplit, should error if node subj.value undefined', async () => {
  const sess = { locale: 'eng-US' }
  const cfg = mockcfg()
  const graph = gngraph_create({}).set(
    '/datausers', gnnode_create({
      key: '/datausers',
      node: 'gnpgdata',
      name: 'datausers',
      subj: {
        userspastmonth: '2'
      },
      subjhydrated: {
        userspastmonth: '2'
      }
    })
  )

  await assert.rejects(async () => promisify(gnpatch_compositesplit)(
    sess, cfg, graph, graph.get('/datausers'), {}
  ), {
    message: gnerr_nodecompositevalueinvalid(
      graph, graph.get('/datausers')).message
  })
})

test('compositesplit, should distribute data node subj.value', async () => {
  const sess = { locale: 'eng-US' }
  const cfg = mockcfg({
    specfn: {
      getrounded: numstr => {
        return Math.round(+numstr)
      },
      parsefloat: numstr => {
        return Number.parseFloat(numstr, 10)
        // return +numstr
      }
    },
    pagespecfromisopath: async () => {
      return ({
        name: '/',
        node: 'uiroot',
        child: [{
          node: 'gnpgdata',
          name: 'data',
          subj: [{
            subjvalue: [10, -15]
          }]
        },{
          node: 'uifieldset',
          name: 'label',
          subj: {
            labellegend: 'uitext'
          },
          child: [{
            // node: 'canvtrackball',
            node: 'uilabel',
            name: 'sample',
            subj: [{
              xcolor: 'rgb(255, 0, 158)',
              ycolor: 'rgb(78, 248, 78)',
              zcolor: 'rgb(0, 188, 249)',
              fgcolor: 'rgb(232, 248, 248)',
              bgcolor: 'rgb(43, 51, 63)'
            },{
              name: 'rotationxy',
              [gnenumSPECPROPNAMETYPE]: 'nsprop',
              prop: '[../../data].subj.subjvalue'
            }]
          }]
        },{
          name: 'datarotation',
          node: 'gnpgdatablend',
          full: {
            [gnenumSPECPROPNAMETYPE]: 'nsprop',
            prop: '[../data].subj.subjvalue'
          },

          subj: [{
            name: 'value',
            [gnenumSPECPROPNAMETYPE]: 'nsprop',
            prop: '[../data].subj.subjvalue'
          },{
            [gnenumSPECPROPNAMETYPE]: 'fullelem',
            path: './',
            index: 0,
            name: 0
          },{
            [gnenumSPECPROPNAMETYPE]: 'fullelem',
            path: './',
            index: 1,
            name: 1
          }]
        },{
          node: 'uitext',
          name: 'examplelabel',
          className: 'sample',
          subj: [{
            inputtype: 'number'
          },{
            name: 'value',
            [gnenumSPECPROPNAMETYPE]: 'nsprop',
            prop: '[../datarotation].full.0',
            filterinarr: [{
              [gnenumSPECPROPNAMETYPE]: 'fn',
              fnname: 'getrounded',
              args: ['this'],
              opts: {
                round: 2
              }
            }],

            filteroutarr: [{
              [gnenumSPECPROPNAMETYPE]: 'fn',
              fnname: 'parsefloat',
              args: ['this']
            }]
          }]
        },{
          node: 'uitext',
          name: 'examplelabel2',
          className: 'sample',
          subj: [{
            inputtype: 'number'
          },{
            name: 'value',
            [gnenumSPECPROPNAMETYPE]: 'nsprop',
            prop: '[../datarotation].full.1',
            filterinarr: [{
              [gnenumSPECPROPNAMETYPE]: 'fn',
              fnname: 'getrounded',
              args: ['this'],
              opts: {
                round: 2
              }
            }],
            filteroutarr: [{
              [gnenumSPECPROPNAMETYPE]: 'fn',
              fnname: 'parsefloat',
              args: ['this']
            }]
          }]
        }]
      })
    }
  })

  const renderres = await gngraph_renderpath(sess, cfg, '/')
  const graphstart = renderres[0]

  // this node has subj.value
  const nodecomposite = graphstart.get('/datarotation')
  const nodecompositeupdated = gnacc_subj
    .setprop(nodecomposite, 'value', [99, -99])

  const recmaybe = gnreport_patchget('testkey')
  const res = await promisify(gnpatch_compositesplit)(
    sess, cfg, graphstart, nodecompositeupdated, recmaybe)

  assert.deepStrictEqual(
    res.get('/datarotation').get('subjhydrated'), {
      '0': 99,
      '1': -99,
      value: [99, -99]
    })

  assert.deepStrictEqual(
    res.get('/examplelabel').get('subjhydrated'), {
      value: 99,
      inputtype: 'number'
    })

  assert.deepStrictEqual(
    res.get('/examplelabel2').get('subjhydrated'), {
      value: -99,
      inputtype: 'number'
    })

  assert.deepStrictEqual( // seems to not update this value
    res.get('/label/sample').get('subjhydrated'), {
      rotationxy: [10, -15],
      xcolor: 'rgb(255, 0, 158)',
      ycolor: 'rgb(78, 248, 78)',
      zcolor: 'rgb(0, 188, 249)',
      fgcolor: 'rgb(232, 248, 248)',
      bgcolor: 'rgb(43, 51, 63)'
    })
})

test('compositejoin, should join data node subj.value', async () => {
  const sess = { locale: 'eng-US' }
  const cfg = mockcfg({
    specfn: {
      getrounded: numstr => {
        return Math.round(+numstr)
      },
      parsefloat: numstr => {
        return Number.parseFloat(numstr, 10)
        // return +numstr
      }
    },
    pagespecfromisopath: async () => ({
      name: '/',
      node: 'uiroot',
      child: [{
        node: 'gnpgdata',
        name: 'data',
        subj: [{
          subjvalue: [10, -15]
        }]
      },{
        node: 'uifieldset',
        name: 'label',
        subj: {
          labellegend: 'uitext'
        },
        child: [{
          // node: 'canvtrackball',
          node: 'uilabel',
          name: 'sample',
          subj: [{
            xcolor: 'rgb(255, 0, 158)',
            ycolor: 'rgb(78, 248, 78)',
            zcolor: 'rgb(0, 188, 249)',
            fgcolor: 'rgb(232, 248, 248)',
            bgcolor: 'rgb(43, 51, 63)'
          },{
            name: 'rotationxy',
            [gnenumSPECPROPNAMETYPE]: 'nsprop',
            prop: '[../../data].subj.subjvalue'
          }]
        }]
      },{
        name: 'datarotation',
        node: 'gnpgdatablend',
        full: {
          [gnenumSPECPROPNAMETYPE]: 'nsprop',
          prop: '[../data].subj.subjvalue'
        },

        subj: [{
          name: 'value',
          [gnenumSPECPROPNAMETYPE]: 'nsprop',
          prop: '[../data].subj.subjvalue'
        },{
          [gnenumSPECPROPNAMETYPE]: 'fullelem',
          path: './',
          index: 0,
          name: 0
        },{
          [gnenumSPECPROPNAMETYPE]: 'fullelem',
          path: './',
          index: 1,
          name: 1
        }]
      },{
        node: 'uitext',
        name: 'examplelabel',
        className: 'sample',
        subj: [{
          inputtype: 'number'
        },{
          name: 'value',
          [gnenumSPECPROPNAMETYPE]: 'nsprop',
          prop: '[../datarotation].full.0',
          filterinarr: [{
            [gnenumSPECPROPNAMETYPE]: 'fn',
            fnname: 'getrounded',
            args: ['this'],
            opts: {
              round: 2
            }
          }],
          filteroutarr: [{
            [gnenumSPECPROPNAMETYPE]: 'fn',
            fnname: 'parsefloat',
            args: ['this']
          }]
        }]
      },{
        node: 'uitext',
        name: 'examplelabel2',
        className: 'sample',
        subj: [{
          inputtype: 'number'
        },{
          name: 'value',
          [gnenumSPECPROPNAMETYPE]: 'nsprop',
          prop: '[../datarotation].full.1',
          filterinarr: [{
            [gnenumSPECPROPNAMETYPE]: 'fn',
            fnname: 'getrounded',
            args: ['this'],
            opts: {
              round: 2
            }
          }],
          filteroutarr: [{
            [gnenumSPECPROPNAMETYPE]: 'fn',
            fnname: 'parsefloat',
            args: ['this']
          }]
        }]
      }]
    })
  })

  const renderres = await gngraph_renderpath(sess, cfg, '/')
  const graphstart = renderres[0]
  const nodecomposite = gnacc_full.set(
    graphstart.get('/datarotation'), [99, -99])

  assert.deepStrictEqual(
    graphstart
      .get('/data').get('dataoutarr').toJS().map(out => out.id).sort(),
    [
      '[/datarotation].full ⸺> [../data].subj.subjvalue',
      '[/datarotation].subj.value ⸺> [../data].subj.subjvalue',
      '[/label/sample].subj.rotationxy ⸺> [../../data].subj.subjvalue'
    ])

  const recmaybe = gnreport_patchget('testkey')
  const res = await promisify(gnpatch_compositejoin)(
    sess, cfg, graphstart, nodecomposite, recmaybe)

  assert.deepStrictEqual(
    res.get('/datarotation').get('subjhydrated'), {
      '0': 99,
      '1': -99,
      value: [99, -99]
    })

  assert.deepStrictEqual(
    res.get('/examplelabel').get('subjhydrated'), {
      value: 99,
      inputtype: 'number'
    })

  assert.deepStrictEqual(
    res.get('/examplelabel2').get('subjhydrated'), {
      value: -99,
      inputtype: 'number'
    })

  assert.deepStrictEqual( // seems to not update this value
    res.get('/label/sample').get('subjhydrated'), {
      rotationxy: [99, -99],
      xcolor: 'rgb(255, 0, 158)',
      ycolor: 'rgb(78, 248, 78)',
      zcolor: 'rgb(0, 188, 249)',
      fgcolor: 'rgb(232, 248, 248)',
      bgcolor: 'rgb(43, 51, 63)'
    })
})

test('unitapplydeepall should update accum-referencing nodes', async () => {
  const cfg = mockcfg({
    specfn: {
      tplstr: ([tpl, num]) => (
        tpl.replace(/:arg/, num)),
      accumnottrue: ([prev, next]) => (
        next ? prev : prev + 1),
      accumtrue: ([prev, next]) => (
        next ? prev + 1 : prev)
    }
  })
  const sess = { locale: 'eng-US' }
  const graphstart = gngraph_create({
    '/': {
      key: '/',
      node: 'uiroot',
      name: '/',
      domchildkeyarr: [],
      child: [{
        node: 'gnpgdatals',
        name: 'datausers',
        baselist: {
          [gnenumSPECPROPNAMETYPE]: 'literal',
          value: [
            { id: 200, name: 'jim' },
            { id: 201, name: 'jane' },
            { id: 202, name: 'richard' }
          ]
        },
        childsubj: [{
          name: "incompletetotal",
          [gnenumSPECPROPNAMETYPE]: "nsprop",
          prop: "[fkey.child].subj.iscompleted",
          accum: {
            start: 0,
            [gnenumSPECPROPNAMETYPE]: "fn",
            fnname: "accumnottrue"
          }
        }],
        child: [{
          spec: 'user',
          specassign: {
            onsubj: [{
              [gnenumSPECPROPNAMETYPE]: 'fn',
              fnname: 'cacheupdate',
              args: ['node']
            }]
          }
        }],
        spectypes: {
          user: {
            node: 'gnpgdata',
            name: 'datauser',
            datakeyprop: 'id',
            subj: [{
              name: 'id',
              [gnenumSPECPROPNAMETYPE]: 'nsprop',
              prop: 'base.id',
              def: {
                [gnenumSPECPROPNAMETYPE]: 'fn',
                fnname: 'datenow'
              }
            },{
              name: 'name',
              [gnenumSPECPROPNAMETYPE]: 'nsprop',
              prop: 'base.name',
              def: ''
            },{
              name: 'iscompleted',
              [gnenumSPECPROPNAMETYPE]: 'nsprop',
              prop: 'base.iscompleted',
              def: false
            }]
          }
        }
      },{
        node: 'uispread',
        name: 'root-content',
        child: [{
          name: 'foot',
          node: 'uispread',
          // node: 'uils',
          className: 'footrow',
          child: [{
            name: 'footlabel',
            node: 'uilabel',
            subj: [{
              [gnenumSPECPROPNAMETYPE]: 'nsprop',
              prop: '[/datausers].childsubj.incompletetotal',
              name: 'label',
              filterinarr: [{
                [gnenumSPECPROPNAMETYPE]: 'fn',
                fnname: 'tplstr',
                args: [':arg items left', 'this']
              }]
            }]
          }]
        }]
      }]
    }
  })

  const rep = gnreport_get('build')
  const graph1 = await promisify(gnnode_dfshydrateall)(
    sess, cfg, graphstart, graphstart.get('/'), rep)

  assert.deepStrictEqual(
    gnacc_subj.get(graph1.get('/root-content/foot/footlabel')), {
      label: '3 items left'
    })

  const patcharr = await promisify(gnpatch_unitsfromkeynsarr)(
    sess, cfg, graph1, [['/datausers/200', 'subj', { iscompleted: true }]])
  const graphfinal = await promisify(gnpatch_unitapplydeepall)(
    sess, cfg, graph1, patcharr)

  // SHOULD NOT BE MUTATED, BUT IS CURRENT MUTATED :S
  // assert.deepStrictEqual(
  //   gnacc_subj.get(graph1.get('/root-content/foot/footlabel')), {
  //     label: '2 items left'
  //  })

  assert.deepStrictEqual(
    gnacc_subj.get(graphfinal.get('/root-content/foot/footlabel')), {
      label: '2 items left'
    })
})

