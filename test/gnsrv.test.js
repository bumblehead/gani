import test from 'node:test'
import assert from 'node:assert/strict'

import mockcfg, {
  cfgenv,
  cfgglobal
} from './helper/mockcfg.js'

import {
  gnenumSPECPROPNAMETYPE
} from '../src/gnenum.js'

import {
  gnsrv_addViewRoutingKoa,
  gnsrv_start
} from '../src/gnsrv.js'

const indexfile = `
<!doctype html>
<head>
  <title>title</title>
  <meta name="name" content="content">
</head>
<body>
  <div id="root"></div>
</body>
</html>
`.slice(1, -1)

test('start should generate cfg', async () => {
  await gnsrv_start(cfgglobal(), cfgenv({
    pagespecfromisopath: () => {
      return ({
        deploytype: 'flat',
        routes: [
          '/',
          '/about/',
          '/links/',
          '/gallery/',
          '/site-map/',
          '/blog/', [[
            '/blog/',
            '/blog/all',
            '/blog/pg-:num',
            '/blog/:article'
          ]],
          '/media/', [[
            '/media/',
            '/media/all',
            '/media/pg-:num',
            '/media/:article'
          ]]
        ]
      })
    }
  }))

  assert.ok(true)
})

test('addViewRoutingKoa should define lazy route handlers', async () => {
  const cfg = mockcfg({
    specfn: {
      getrequrl: (args, ss) => {
        return ss.requrl
      }
    }
  })

  const dataenvspec = {
    node: 'gnpgdata',
    name: 'dataenv',
    subj: [{
      [gnenumSPECPROPNAMETYPE]: 'fn',
      fnname: 'getrequrl',
      name: 'requrl'
    }]
  }

  const rootspec = {
    name: '/',
    node: 'uiroot',
    child: [(
      dataenvspec
    ), {
      node: 'uispread',
      name: 'top'
    },{
      ispathnode: true
    },{
      node: 'uispread',
      name: 'bottom'
    }]
  }

  const aboutspec = {
    node: 'uilink',
    name: 'about',
    subj: [{
      label: 'about',
      href: '/about/'
    }]
  }

  const specpathmap = {
    '$ROOTSPECS/spec/root/eng-US.json': rootspec,
    '$ROOTSPECS/spec/root-about/eng-US.json': aboutspec,
    '$ROOTSPECS/spec/data-env/eng-US.json': dataenvspec
  }

  cfg.pagespecfromisopath = (sess, cfg, isopath) => (
    specpathmap[isopath])

  const unauthenticatedRouter = {
    route: {},
    get: (uri, fn) => {
      unauthenticatedRouter.route[uri] = fn
    }
  }
  const ISSERVER_RENDER = true

  cfg.cfgenv.isRoutesAttach = ISSERVER_RENDER
  cfg.cfgenv.attachRoute = (method, cfgseed, uri, pgspec, run1ctxfn, resfn) => {
    unauthenticatedRouter.get(uri, async ctx => {
      // const [, sess] = await gncfg.init(cfg, {
      //   requrl: ctx.originalUrl,
      //   token: ctx.cookies && ctx.cookies.sesstoken
      // })

      // necessary function pachinko!! why not call this inside gani??
      // allows application-layer to use completed cfg and sess
      // for handling error responses
      const cfg = await run1ctxfn(cfgseed, ctx)

      try {
        const res = await resfn(cfg, ctx)
        const rescontent = res[2]
        const resmeta = res[3]
        const body = (
          indexfile
            .replace(
              '<div id="root"></div>',
              `<div id="root">${rescontent}</div>`)
            .replace(/<title>([^<]*)<\/title>/g, (match, p1) => (
              match.replace(p1, resmeta.title)))
            .replace(/<meta name="description" content="([^"])">/g, (m, p1) => (
              m.replace(p1, resmeta.description || resmeta.title))))

        ctx.status = 200
        ctx.body = body
      } catch (err) {
        if (err.code === 1) {
          ctx.redirect = cfg.getUserDefaultPath(cfg.sess.user)
        } else {
          ctx.body = err
        }
      }

      return ctx
    })
  }

  gnsrv_addViewRoutingKoa(cfg, '/about/', {}, () => ({
    ...cfg,
    ss: {
      locale: 'eng-US',
      requrl: '/about/'
    }
  }))

  const ctx = {
    // originalUrl: '0.0.0.0:8001/about/index.html'
    // originalUrl: 'http://0.0.0.0:8001/about/index.html'
    originalUrl: 'http://0.0.0.0:8001/about/'
  }

  const finalctx = await unauthenticatedRouter.route['/about/'](ctx)

  assert.strictEqual(finalctx.originalUrl, 'http://0.0.0.0:8001/about/')
  assert.strictEqual(finalctx.status, 200)
  assert.ok(/<span[^>]*>about<\/span>/g.test(finalctx.body))
})
