import test from 'node:test'
import assert from 'node:assert/strict'
import jsdom from 'jsdom'

import promisify from './helper/promisify.js'
import mockcfg from './helper/mockcfg.js'
import mocksess from './helper/mocksess.js'

import {
  gngraph_create
} from '../src/gngraph.js'

import {
  gnpublish_getrootvtree,
  gnpublish_graphrmchilds,
  gnpublish_rmchilds
} from '../src/gnpublish.js'

test('getrootvtree should return body.firstElement or body', () => {
  const dom = new jsdom.JSDOM(
    '<!DOCTYPE html><body><div id="first-element"></div></body>')
  const cfg = { dom }

  assert.strictEqual(
    gnpublish_getrootvtree(cfg).outerHTML,
    '<div id="first-element"></div>')
})

test('graphrmchilds should remove childs', async () => {
  const sess = mocksess()
  const cfg = mockcfg()
  const nodekeybox = '/todo/content/box'
  const nodekeyboxlabel = '/todo/content/box/todolabel'
  const graphstart = gngraph_create({
    '/todo': {
      key: '/todo',
      name: '/todo',
      node: 'uiroot',
      childarr: ['/todo/content'],
      datainarr: [],
      dataoutarr: [],
      domchildkeyarr: [],
      domchildkey: {
        content: '/todo/content'
      }
    },
    '/todo/content': {
      key: '/todo/content',
      name: 'todo',
      node: 'uitodo',
      childarr: [
        '/todo/content/box'
      ],
      datainarr: [],
      dataoutarr: [],
      domchildkeyarr: [
        '/todo/content/box'
      ],
      domchildkey: {},
      namekey: 'content'
    },
    '/todo/content/box': {
      key: '/todo/content/box',
      name: 'box',
      node: 'gnpgui',
      childarr: [
        '/todo/content/box/todolabel'
      ],
      domchildkeyarr: [
        '/todo/content/box/todolabel'
      ],
      datainarr: [],
      dataoutarr: [],
      ismodifiedts: 1487399044865,
      domchildkey: {}
    },
    '/todo/content/box/todolabel': {
      key: '/todo/content/box/todolabel',
      name: 'todolabel',
      node: 'uilabel',
      childarr: [],
      subj: [{
        label: 'rick jones',
        labelprimary: 'todo: '
      }],
      domchildkeyarr: [],
      ismodifiedts: 1487399044882,
      domchildkey: {},
      dataoutarr: [],
      subjhydrated: {
        label: 'rick jones',
        labelprimary: 'todo: '
      }
    }
  })

  const [graph, node] = (
    await promisify(gnpublish_graphrmchilds)(
      sess, cfg, graphstart, nodekeybox))

  assert.strictEqual(graphstart.has(nodekeyboxlabel), true)
  assert.strictEqual(graph.has(nodekeyboxlabel), false)
  assert.strictEqual(node.get('key'), nodekeybox)
})

test('rmchilds should remove childs from cfg.graph', async () => {
  const sess = mocksess()
  const cfg = mockcfg()
  const nodekeybox = '/todo/content/box'
  const nodekeyboxlabel = '/todo/content/box/todolabel'
  const graphstart = gngraph_create({
    '/todo': {
      key: '/todo',
      name: '/todo',
      node: 'uiroot',
      childarr: ['/todo/content'],
      datainarr: [],
      dataoutarr: [],
      domchildkeyarr: [],
      domchildkey: {
        content: '/todo/content'
      }
    },
    '/todo/content': {
      key: '/todo/content',
      name: 'todo',
      node: 'uitodo',
      childarr: [
        '/todo/content/box'
      ],
      datainarr: [],
      dataoutarr: [],
      domchildkeyarr: [
        '/todo/content/box'
      ],
      domchildkey: {},
      namekey: 'content'
    },
    '/todo/content/box': {
      key: '/todo/content/box',
      name: 'box',
      node: 'gnpgui',
      childarr: [
        '/todo/content/box/todolabel'
      ],
      domchildkeyarr: [
        '/todo/content/box/todolabel'
      ],
      datainarr: [],
      dataoutarr: [],
      ismodifiedts: 1487399044865,
      domchildkey: {}
    },
    '/todo/content/box/todolabel': {
      key: '/todo/content/box/todolabel',
      name: 'todolabel',
      node: 'uilabel',
      childarr: [],
      subj: [{
        label: 'rick jones',
        labelprimary: 'todo: '
      }],
      domchildkeyarr: [],
      ismodifiedts: 1487399044882,
      domchildkey: {},
      dataoutarr: [],
      subjhydrated: {
        label: 'rick jones',
        labelprimary: 'todo: '
      }
    }
  })

  cfg.graph = graphstart
  const [graph, node] = (
    await promisify(gnpublish_rmchilds)(sess, cfg, nodekeybox))

  assert.strictEqual(graphstart.has(nodekeyboxlabel), true)
  assert.strictEqual(graph.has(nodekeyboxlabel), false)
  assert.strictEqual(node.get('key'), nodekeybox)
})
