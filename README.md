gani
====

<img src="https://bumblehead.gitlab.io/doc.gani/img/logo/gani.svg" width="200px" title="gani" />

[![License: GPL v3][2]][1]
[![coverage report][4]][3]


[0]: https://bumblehead.com                            "bumblehead"
[1]: https://www.gnu.org/licenses/agpl.txt
[2]: https://img.shields.io/badge/License-AGPLv3-blue.svg
[3]: https://gitlab.com/bumblehead/gani/commits/main
[4]: https://gitlab.com/bumblehead/gani/badges/main/coverage.svg


gani renders applications using a JSON-formatted [design language][1] to define layout, appearance and behaviour. It passes JSON "patterns" through an [interpreter][2] and uses the result to complete a [graph][3] (graph is made with [facebook's immutable.js][4]). Each node in the graph corresponds to a page object in the application, such as a text field a navigation list or an animated canvas. Updates to the application are applied quickly (real-time animation quick) using edges that connect data among nodes.

_this application isn't ready. documentation is being developed._

If the AGPLv3 does not work for you, you can get a commercial license free by contacting the author.

<!-- If the AGPLv3 does not work for you, you can get a commercial license on a sliding scale. If you have more than 1 server doing image processing your savings should cover the cost. -->


-----------------------------
<!-- 
[This paper][5] is interesting, a theoretical description of a system similar to gani. Besides a web application, the system is capable of generating Web Application Summary graphs and Entity Relationship diagrams. It utilises the "DFYW", Database Field Yielding Widget, as a dao container for automatically relating ui data with persistance layers.

The authors claim to have made a working implementation.
-->
<!--
There definitely is a market for a product like this. I think the best thing would be a native app with deep git integration. Engineers can see the entire project and edit templates while end users get a GUI to drag and drop those templates to make an article. The output from the user is encoded to either json or protobufs, and then passed through a function that outputs html which is then uploaded to s3. Everything would be under version control so you could just operate as if it were a custom designed webpage. You could fund development by having a store that takes a cut of templates that other people create.

I would work on this if I wasn't already running another startup.
-->




[0]: http://www.bumblehead.com "bumblehead"
[1]: http://www.jeffreynichols.com/papers/a17-nichols.pdf "nichols, uidl"
[2]: https://github.com/iambumblehead/specmob "specmob, interpreter"
[3]: https://github.com/iambumblehead/spectraph "spectraph, graph"
[4]: https://facebook.github.io/immutable-js/ "immutable-js"
[5]: http://researchbank.acu.edu.au/cgi/viewcontent.cgi?article=1357&context=flb_pub "visually modelling data intensive web applications"
