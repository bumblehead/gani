# changelog

 * **0.95.70** _2024.11.TBD_
   * use stricter fkey validation
   * allow unspecified subj.value for multipart ui nodes
   * decouple datablend behaviour from datablend node
   * remove array destructurings (slow)
   * remove import of publish from nodes, allow caller-defined publish
 * **0.95.69** _2024.10.30_
   * parse locale from manifest with session
   * fix papercuts
 * **0.95.67** _2024.10.26_
   * use standard fkey shape
   * resolve fkey issues
 * **0.95.66** _2024.10.23_
   * big rewrite, emerge from chaotic development cycle
   * partially resume CHANGELOG updates
   * add support for special fkey property "baseelem"
   * use smaller specfn and speccb, `(args, opts, ...rest)` to `(args, ...rest)`
 * **0.0.77** _2017.03.28_
   * **remove "type" : "nodespec"** where namekey is defined
 * **0.0.76** _2017.03.15_
   * **added pkg namespace**
   * **use explicit namespacing** to resolve "when" and "argprops" values
   * **make mulitiple hydrated** namespaces usable to define new namespace
 * **0.0.75** _2017.02.18_
   * **added more tests**
   * **huge speed improvments**
     * replacing `setTimeout` w/ `setImmediate`
     * replacing accumasync w/ for-loop callback idiom
 * **0.0.74** _2017.02.13_
   * **added support** for composite nodes
   * **move hyperscript functions** to page node
 * **0.0.73** _2017.02.11_
   * **added timerecord** file for recording render times
   * **removed destructured callback assignments**
   * **updated patch process** to loop patched nodes only
   * **updated templating** to always use locale page name and node className
   * **shortened className** boilerplate '.:className.:page' becomes '.:class.:pg'
   * **added example** composite node, 'pglisttext'
 * **0.0.72** _2017.02.10_
   * **added page object iframe**
   * **merged list processing pipelines** for nodes (datanode and pagenode)
   * **removed complex list processing** from datanode
 * **0.0.71** _2017.02.05_
   * **added page objects img and svg**
 * **0.0.70** _2017.02.03_
   * **added todo list example**
   * **added basic** directory with scripts for common page objects
 * **0.0.69** _2017.02.02_
   * **added namespacing for all data hydration**
   * **added files** for specifically adapting keys and patterns
   * **removed file** for "opt" style page nodes
   * **updated** hydration for some data to use same args in function call
 * **0.0.67** _2017.01.27_
   * **updated accessor** with 'rm' functions
   * **updated node errorlog** to persist graph and node at error
   * **added namespacing** to define namespace used to collect property values
   * **added rehydration** to patch process, completely refresh partial nodes
   * **added rmchild and addchild** methods for list data nodes
   * **removed definition of clutter** properties on data partial spec
   * **added patch record to "patch" function return values**
 * **0.0.66** _2017.01.22_
   * **added accessor** functions for accessing fixed number of data types found on nodes
 * **0.0.65** _2017.01.19_
   * **improved patching** behaviour, foreign key changes => rebuild dependents
 * **0.0.62** _2017.01.13_
   * **simplified publishing interface** with fewer functions, meaningful names
   * **added changelog** content to README
   * **removed subject filtering** now handled at specmob layer, all spec values
   * **moved refkey hydration** to occur before child nodes are defined
   * **added convenience method** getkeynext to pgpartial
   * **removed cast option** from pgpartial behaviour, determines type dynamically
   * **added handling of boolean type values** on partial data
