First of all, thank you for contributing. It’s appreciated.

# To submit a pull request

 1. Open a GitHub issue before doing significant amount of work.
 2. Clone the repo. If it was already cloned, then git pull to get the latest from master.
 3. Write code.
 4. Make a pull request.


# Why use callbacks?

Benchmarks show callbacks are faster than import-await. Normally this is not consequential, but for gani the accumulated results are consequential.

# Variables and Names

Variables names are abbreviated to improve readability of deeply nested callbacks. The following namings are used,

 * `ss` (sess) a mutable session object,
 * `cfg` (config) a mutable configuration object,
 * `gr` (graph) an immutable graph object,
 * `nd` (node) an immutable node object,
 * `ndc`  (node child) an immutable node object,
 * `ndp`  (node parent) an immutable node object,
 * `ndpg` (node page) a "page object" associated with a node, with functions to return markup and data-processing behaviour needed by the node
 * `props` (namespace map), some areas still use prop or namespace
 * `spec` (pattern) a spec pattern that may resolve to any value
 * `pxu` (patch unit)

 * `exu` (edge note unit), [edge, nskey, val]
 init,
 fkey,
 base { subj }
 
